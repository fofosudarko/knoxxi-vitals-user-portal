import * as yup from 'yup';

const USERNAME_INPUT_VALIDATION = {
  regex: {
    value: /^[_\w+][_\w\d]+$/,
    errorMessage:
      'Invalid username. Must be alphanumeric characters and underscore.',
  },
  length: { max: 50, min: 5 },
  required: {
    errorMessage: 'Username required',
  },
};

const PASSWORD_INPUT_VALIDATION = {
  regex: {
    value: /^[\W\w\d\D]+$/,
    errorMessage:
      'Invalid password. Must be alphanumeric characters and any special character.',
  },
  length: { max: 20, min: 8 },
  required: {
    errorMessage: 'Password required',
  },
};

const EMAIL_ADDRESS_INPUT_VALIDATION = {
  regex: {
    value: undefined,
    errorMessage: 'Invalid email address',
  },
  required: {
    errorMessage: 'Email address required',
  },
};

const MEDIA_INPUT_VALIDATION = {
  required: {
    errorMessage: 'Media required',
  },
};

const GENERAL_NAME_INPUT_VALIDATION = {
  regex: {
    value: /^[_\w][\w\d\s-'"]+$/,
    errorMessage:
      'Invalid name. Must be alphanumeric characters with either spaces or special characters.',
  },
  length: { max: 100, min: 2 },
  required: {
    errorMessage: 'Name required',
  },
};

const PHONE_NUMBER_INPUT_VALIDATION = {
  regex: {
    value: /^[+]?\d{12,15}$/,
    errorMessage: 'Invalid phone number. Must be at least 12 digits long.',
  },
  length: { max: 15, min: 0 },
  required: {
    errorMessage: 'Mobile number required',
  },
};

const GENERAL_PHONE_NUMBER_INPUT_VALIDATION = {
  regex: {
    value: /^[+]?\d{12,15}$/,
    errorMessage: 'Invalid phone number. Must be at least 12 digits long.',
  },
  length: { max: 15, min: 0 },
  required: {
    errorMessage: 'Mobile number required',
  },
};

// value: /^\d+\s+[\w\d\s]+(,\s+[\w\s]+)+$/,

const CONTENT_NAME_INPUT_VALIDATION = {
  string: {
    errorMessage: 'Content name must be a string',
  },
  required: {
    errorMessage: 'Content name required',
  },
};

const BP_DATA_SYSTOLIC_INPUT_VALIDATION = {
  number: {
    errorMessage: 'BP data systolic must be a number',
  },
  required: {
    errorMessage: 'BP data systolic required',
  },
};

const BP_DATA_DIASTOLIC_INPUT_VALIDATION = {
  number: {
    errorMessage: 'BP data diastolic must be a number',
  },

  required: {
    errorMessage: 'BP data diastolic required',
  },
};

const BP_DATA_PULSE_INPUT_VALIDATION = {
  number: {
    errorMessage: 'BP data pulse must be a number',
  },
  required: {
    errorMessage: 'BP data pulse required',
  },
};

const GLUCOSE_DATA_FASTING_READING_INPUT_VALIDATION = {
  number: {
    errorMessage: 'Glucose data fasting reading must be a number',
  },
};

const GLUCOSE_DATA_POSTPRANDIAL_READING_INPUT_VALIDATION = {
  number: {
    errorMessage: 'Glucose data postprandial reading must be a number',
  },
};

const CHOLESTEROL_DATA_TOTAL_CHOLESTEROL_READING_INPUT_VALIDATION = {
  number: {
    errorMessage: 'Cholesterol data total cholesterol reading must be a number',
  },
  required: {
    errorMessage: 'Cholesterol data total cholesterol reading required',
  },
};

const CHOLESTEROL_DATA_HDL_CHOLESTEROL_READING_INPUT_VALIDATION = {
  number: {
    errorMessage: 'Cholesterol data HDL cholesterol reading must be a number',
  },

  required: {
    errorMessage: 'Cholesterol data HDL cholesterol reading required',
  },
};

const CHOLESTEROL_DATA_LDL_CHOLESTEROL_READING_INPUT_VALIDATION = {
  number: {
    errorMessage: 'Cholesterol data LDL cholesterol reading must be a number',
  },
  required: {
    errorMessage: 'Cholesterol data LDL cholesterol reading required',
  },
};

const CHOLESTEROL_DATA_TRIGLYCERIDES_CHOLESTEROL_READING_INPUT_VALIDATION = {
  number: {
    errorMessage:
      'Cholesterol data triglycerides cholesterol reading must be a number',
  },
};

const TEMPERATURE_DATA_READING_INPUT_VALIDATION = {
  number: {
    errorMessage: 'Temperature data reading must be a number',
  },
  required: {
    errorMessage: 'Temperature data reading required',
  },
};

const WEIGHT_DATA_USER_WEIGHT_INPUT_VALIDATION = {
  number: {
    errorMessage: 'Weight data user weight must be a number',
  },
  required: {
    errorMessage: 'Weight data user weight required',
  },
};

const WATER_DATA_READING_INPUT_VALIDATION = {
  number: {
    errorMessage: 'Water data reading must be a number',
  },
  required: {
    errorMessage: 'Water data reading required',
  },
};

const OXYGEN_DATA_BLOOD_OXYGEN_INPUT_VALIDATION = {
  number: {
    errorMessage: 'Oxygen data blood oxygen must be a number',
  },
  required: {
    errorMessage: 'Oxygen data blood oxygen required',
  },
};

const MEAL_INPUT_VALIDATION = {
  string: {
    errorMessage: 'Meal must be a string',
  },
  required: {
    errorMessage: 'Meal required',
  },
};

const REMARKS_INPUT_VALIDATION = {
  string: {
    errorMessage: 'Remarks must be a string',
  },
};

const ADDITIONAL_LIFE_RELATIONSHIP_INPUT_VALIDATION = {
  string: {
    errorMessage: 'Additional life relationship must be a string',
  },
  required: {
    errorMessage: 'Additional life relationship required',
  },
};

const HEALTH_DATA_SHARE_PUBLIC_SECURITY_CODE_INPUT_VALIDATION = {
  regex: {
    value: /^[\w\d]+$/,
    errorMessage: 'Invalid security code. Must be alphanumeric characters.',
  },
  length: { max: 9, min: 3 },
  required: {
    errorMessage: 'Security code required',
  },
};

export const YUP_USERNAME_VALIDATOR = yup
  .string()
  .max(USERNAME_INPUT_VALIDATION.length.max)
  .min(USERNAME_INPUT_VALIDATION.length.min)
  .matches(
    USERNAME_INPUT_VALIDATION.regex.value,
    USERNAME_INPUT_VALIDATION.regex.errorMessage
  )
  .required(USERNAME_INPUT_VALIDATION.required.errorMessage);

export const YUP_PASSWORD_VALIDATOR = yup
  .string()
  .max(PASSWORD_INPUT_VALIDATION.length.max)
  .min(PASSWORD_INPUT_VALIDATION.length.min)
  .matches(
    PASSWORD_INPUT_VALIDATION.regex.value,
    PASSWORD_INPUT_VALIDATION.regex.errorMessage
  )
  .required(PASSWORD_INPUT_VALIDATION.required.errorMessage);

export const YUP_EMAIL_ADDRESS_VALIDATOR = yup
  .string()
  .nullable()
  .email(EMAIL_ADDRESS_INPUT_VALIDATION.regex.errorMessage);

export const YUP_MEDIA_VALIDATOR = yup
  .string()
  .nullable()
  .required(MEDIA_INPUT_VALIDATION.required.errorMessage);

export const YUP_GENERAL_NAME_VALIDATOR = yup
  .string()
  .max(GENERAL_NAME_INPUT_VALIDATION.length.max)
  .min(GENERAL_NAME_INPUT_VALIDATION.length.min)
  .matches(
    GENERAL_NAME_INPUT_VALIDATION.regex.value,
    GENERAL_NAME_INPUT_VALIDATION.regex.errorMessage
  )
  .required(GENERAL_NAME_INPUT_VALIDATION.required.errorMessage);

export const YUP_PHONE_NUMBER_VALIDATOR = yup
  .string()
  .nullable()
  .required(PHONE_NUMBER_INPUT_VALIDATION.required.errorMessage)
  .max(PHONE_NUMBER_INPUT_VALIDATION.length.max)
  .min(PHONE_NUMBER_INPUT_VALIDATION.length.min)
  .matches(
    PHONE_NUMBER_INPUT_VALIDATION.regex.value,
    PHONE_NUMBER_INPUT_VALIDATION.regex.errorMessage
  );

export const YUP_GENERAL_PHONE_NUMBER_VALIDATOR = yup
  .string()
  .nullable()
  .max(GENERAL_PHONE_NUMBER_INPUT_VALIDATION.length.max)
  .min(GENERAL_PHONE_NUMBER_INPUT_VALIDATION.length.min)
  .matches(
    GENERAL_PHONE_NUMBER_INPUT_VALIDATION.regex.value,
    GENERAL_PHONE_NUMBER_INPUT_VALIDATION.regex.errorMessage
  );

export const YUP_GENERAL_ADDRESS_VALIDATOR = yup.string().nullable();

export const YUP_DATE_OF_BIRTH_VALIDATOR = yup.string();

export const YUP_GENDER_VALIDATOR = yup.string();

export const YUP_POSTAL_ADDRESS_VALIDATOR = yup.string().nullable();

export const YUP_CONTENT_NAME_VALIDATOR = yup
  .string(CONTENT_NAME_INPUT_VALIDATION.string.errorMessage)
  .required(CONTENT_NAME_INPUT_VALIDATION.required.errorMessage);

export const YUP_BP_DATA_SYSTOLIC_VALIDATOR = yup
  .number(BP_DATA_SYSTOLIC_INPUT_VALIDATION.number.errorMessage)
  .required(BP_DATA_SYSTOLIC_INPUT_VALIDATION.required.errorMessage);

export const YUP_BP_DATA_DIASTOLIC_VALIDATOR = yup
  .number(BP_DATA_DIASTOLIC_INPUT_VALIDATION.number.errorMessage)
  .required(BP_DATA_DIASTOLIC_INPUT_VALIDATION.required.errorMessage);

export const YUP_BP_DATA_PULSE_VALIDATOR = yup
  .number(BP_DATA_PULSE_INPUT_VALIDATION.number.errorMessage)
  .required(BP_DATA_PULSE_INPUT_VALIDATION.required.errorMessage);

export const YUP_GLUCOSE_DATA_FASTING_READING_VALIDATOR = yup.number(
  GLUCOSE_DATA_FASTING_READING_INPUT_VALIDATION.number.errorMessage
);

export const YUP_GLUCOSE_DATA_POSTPRANDIAL_READING_VALIDATOR = yup.number(
  GLUCOSE_DATA_POSTPRANDIAL_READING_INPUT_VALIDATION.number.errorMessage
);

export const YUP_CHOLESTEROL_DATA_TOTAL_CHOLESTEROL_READING_VALIDATOR = yup
  .number(
    CHOLESTEROL_DATA_TOTAL_CHOLESTEROL_READING_INPUT_VALIDATION.number
      .errorMessage
  )
  .required(
    CHOLESTEROL_DATA_TOTAL_CHOLESTEROL_READING_INPUT_VALIDATION.required
      .errorMessage
  );

export const YUP_CHOLESTEROL_DATA_HDL_CHOLESTEROL_READING_VALIDATOR = yup
  .number(
    CHOLESTEROL_DATA_HDL_CHOLESTEROL_READING_INPUT_VALIDATION.number
      .errorMessage
  )
  .required(
    CHOLESTEROL_DATA_HDL_CHOLESTEROL_READING_INPUT_VALIDATION.required
      .errorMessage
  );

export const YUP_CHOLESTEROL_DATA_LDL_CHOLESTEROL_READING_VALIDATOR = yup
  .number(
    CHOLESTEROL_DATA_LDL_CHOLESTEROL_READING_INPUT_VALIDATION.number
      .errorMessage
  )
  .required(
    CHOLESTEROL_DATA_LDL_CHOLESTEROL_READING_INPUT_VALIDATION.required
      .errorMessage
  );

export const YUP_CHOLESTEROL_DATA_TRIGLYCERIDES_CHOLESTEROL_READING_VALIDATOR =
  yup.number(
    CHOLESTEROL_DATA_TRIGLYCERIDES_CHOLESTEROL_READING_INPUT_VALIDATION.number
      .errorMessage
  );

export const YUP_TEMPERATURE_DATA_READING_VALIDATOR = yup
  .number(TEMPERATURE_DATA_READING_INPUT_VALIDATION.number.errorMessage)
  .required(TEMPERATURE_DATA_READING_INPUT_VALIDATION.required.errorMessage);

export const YUP_WEIGHT_DATA_USER_WEIGHT_VALIDATOR = yup
  .number(WEIGHT_DATA_USER_WEIGHT_INPUT_VALIDATION.number.errorMessage)
  .required(WEIGHT_DATA_USER_WEIGHT_INPUT_VALIDATION.required.errorMessage);

export const YUP_WATER_DATA_READING_VALIDATOR = yup
  .number(WATER_DATA_READING_INPUT_VALIDATION.number.errorMessage)
  .required(WATER_DATA_READING_INPUT_VALIDATION.required.errorMessage);

export const YUP_OXYGEN_DATA_BLOOD_OXYGEN_VALIDATOR = yup
  .number(OXYGEN_DATA_BLOOD_OXYGEN_INPUT_VALIDATION.number.errorMessage)
  .required(OXYGEN_DATA_BLOOD_OXYGEN_INPUT_VALIDATION.required.errorMessage);

export const YUP_MEAL_VALIDATOR = yup
  .string(MEAL_INPUT_VALIDATION.string.errorMessage)
  .required(MEAL_INPUT_VALIDATION.required.errorMessage);

export const YUP_REMARKS_VALIDATOR = yup.string(
  REMARKS_INPUT_VALIDATION.string.errorMessage
);

export const YUP_ADDITIONAL_LIFE_RELATIONSHIP_VALIDATOR = yup
  .string(ADDITIONAL_LIFE_RELATIONSHIP_INPUT_VALIDATION.string.errorMessage)
  .required(
    ADDITIONAL_LIFE_RELATIONSHIP_INPUT_VALIDATION.required.errorMessage
  );

export const YUP_HEALTH_DATA_SHARE_PUBLIC_SECURITY_CODE_VALIDATOR = yup
  .string()
  .max(HEALTH_DATA_SHARE_PUBLIC_SECURITY_CODE_INPUT_VALIDATION.length.max)
  .min(HEALTH_DATA_SHARE_PUBLIC_SECURITY_CODE_INPUT_VALIDATION.length.min)
  .matches(
    HEALTH_DATA_SHARE_PUBLIC_SECURITY_CODE_INPUT_VALIDATION.regex.value,
    HEALTH_DATA_SHARE_PUBLIC_SECURITY_CODE_INPUT_VALIDATION.regex.errorMessage
  )
  .required(
    HEALTH_DATA_SHARE_PUBLIC_SECURITY_CODE_INPUT_VALIDATION.required
      .errorMessage
  );
