import { getListOfDates } from 'src/utils';

export const FILE_EXTENSION_REGEX = /\.[a-zA-Z0-9]+$/;
export const APP_NAME = 'Knoxxi Vitals User Portal';
export const DEFAULT_PAGE = 1;
export const DEFAULT_PAGE_SIZE = 30;
export const VALID_PHONE_NUMBER_REGEX = /^[+]?\d{12,15}$/;
export const ZEROES_WITHIN_PHONE_NUMBER_REGEX = /^(0{0,2}[1-9]{1,3})(0+)(.+)$/;
export const DEFAULT_API_TIMEOUT = 60000;
export const DEFAULT_SORT_DIRECTION = 'DESC';
export const SEARCH_MININUM_LENGTH = 2;
export const SEARCH_QUERY_DEBOUNCE_PERIOD = 0;
export const CKYC_TENANT = 'DFS';
export const APP_LOGO =
  process.env.NEXT_PUBLIC_BASE_PATH || '' + '/img/logo-knoxxi@2x.png';
export const APP_DASHBOARD_LOGO =
  process.env.NEXT_PUBLIC_BASE_PATH || '' + '/img/logo-knoxxi@2x.png';
export const APP_DASHBOARD_LOGO2 =
  process.env.NEXT_PUBLIC_BASE_PATH || '' + '/img/logo-knoxxi-2.png';
export const SPLASH_IMAGE =
  process.env.NEXT_PUBLIC_BASE_PATH ||
  '' + '/img/replugg-electronics-splash-image-2.jpg';
export const IMAGE_PLACEHOLDER =
  process.env.NEXT_PUBLIC_BASE_PATH || '' + '/img/knoxxi-logo-2.png';
export const DEFAULT_CARRIER_CAMPAIGNS_NUMBER = 10;
export const DEFAULT_ADVERT_DELIVERIES_NUMBER = 10;
export const DEFAULT_CONTENTS_NUMBER = 15;
export const PAGER = {
  PAGE: 'PAGE',
  END_PAGING: 'END_PAGING',
  RESET_PAGE: 'RESET_PAGE',
};
export const KNOXXI_CKYC_SERVICE_API_URL =
  process.env.NEXT_PUBLIC_KNOXXI_CKYC_SERVICE_API_URL || '';
export const KNOXXI_HEALTH_SERVICE_API_URL =
  process.env.NEXT_PUBLIC_KNOXXI_HEALTH_SERVICE_API_URL || '';
export const DEFAULT_SELECT_SIZE = 20;
export const DEFAULT_COMPANIES_SELECT_SIZE = 100;
export const CKYC_SERVICE_RESPONSE_CODE = {
  C004: 'C004',
  C006: 'C006',
};
export const STATIC_GRAPH_DATE_VALUES = getListOfDates(
  new Date(),
  30,
  1,
  'descending'
); /* [
  '2023-08-01',
  '2023-08-03',
  '2023-08-05',
  '2023-08-07',
  '2023-08-09',
  '2023-08-10',
  '2023-08-11',
  '2023-08-16',
  '2023-08-17',
]*/
export const SELECT_DATE_COUNT_OPTIONS = [1, 7, 15, 30, 60, 90, 365];
export const SELECT_DATE_INTERVAL_OPTIONS = [1, 2, 3, 5, 10, 20];
export const SELECT_DATE_ORDER_OPTIONS = ['descending', 'ascending'];
export const SELECT_DATE_PERIOD_OPTIONS = [
  'Daily',
  'Weekly',
  'Half-monthly',
  'Monthly',
  'Bi-monthly',
  'Tri-monthly',
  'Yearly',
];
export const SELECT_DATE_COUNT_PERIOD_MAP = {
  Daily: 1,
  Weekly: 7,
  'Half-monthly': 15,
  Monthly: 30,
  'Bi-monthly': 60,
  'Tri-monthly': 90,
  Yearly: 365,
};
export const DEFAULT_PARTNERED_BY_ALIAS = 'Knoxxi';
export const DEFAULT_DATA_CHANNEL = 'WEB_APP';
export const CORS_VALUE = process.env.NEXT_PUBLIC_CORS_VALUE || '';
export const REFERRER_VALUE = process.env.NEXT_PUBLIC_REFERRER_VALUE || '';
export const REFERRER_POLICY_VALUE =
  process.env.NEXT_PUBLIC_REFERRER_POLICY_VALUE || '';
