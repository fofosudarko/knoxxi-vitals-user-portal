import { useCallback } from 'react';
import { useRouter } from 'next/router';
import { useAuthStore } from 'src/stores/auth';

const routes = {
  INDEX_ROUTE: '/',
  SIGNIN_ROUTE: '/auth/signin',
  SIGNOUT_ROUTE: '/auth/signout',
  HOME_ROUTE: '/home',
  BP_DATA_LIST_ROUTE: '/bp-data',
  BP_DATA_NEW_ROUTE: '/bp-data/new',
  GLUCOSE_DATA_LIST_ROUTE: '/glucose-data',
  GLUCOSE_DATA_NEW_ROUTE: '/glucose-data/new',
  GLUCOSE_DATA_EDIT_ROUTE: '/glucose-data/{{GLUCOSE_DATA_ID}}/edit',
  TEMPERATURE_DATA_LIST_ROUTE: '/temperature-data',
  TEMPERATURE_DATA_NEW_ROUTE: '/temperature-data/new',
  WEIGHT_DATA_LIST_ROUTE: '/weight-data',
  WEIGHT_DATA_NEW_ROUTE: '/weight-data/new',
  WATER_DATA_LIST_ROUTE: '/water-data',
  WATER_DATA_NEW_ROUTE: '/water-data/new',
  OXYGEN_DATA_LIST_ROUTE: '/oxygen-data',
  OXYGEN_DATA_NEW_ROUTE: '/oxygen-data/new',
  CHOLESTEROL_DATA_LIST_ROUTE: '/cholesterol-data',
  CHOLESTEROL_DATA_NEW_ROUTE: '/cholesterol-data/new',
  INSIGHTS_ROUTE: '/insights',
  INSIGHTS_BP_DATA_ROUTE: '/insights/bp-data',
  INSIGHTS_GLUCOSE_DATA_ROUTE: '/insights/glucose-data',
  INSIGHTS_TEMPERATURE_DATA_ROUTE: '/insights/temperature-data',
  INSIGHTS_OXYGEN_DATA_ROUTE: '/insights/oxygen-data',
  INSIGHTS_CHOLESTEROL_DATA_ROUTE: '/insights/cholesterol-data',
  HEALTH_DATA_SHARES_ROUTE: '/health-data-shares',
  ADDITIONAL_LIVES_ROUTE: '/additional-lives',
  ADDITIONAL_LIFE_NEW_ROUTE: '/additional-lives/new',
  ADDITIONAL_LIFE_EDIT_ROUTE: '/additional-lives/{{ADDITIONAL_LIFE_ID}}/edit',
  SHARED_ROUTE: '/shared',
  SHARED_MENU_ROUTE: '/shared/menu',
  SHARED_BP_DATA_LIST_ROUTE: '/shared/bp-data',
  SHARED_TEMPERATURE_DATA_LIST_ROUTE: '/shared/temperature-data',
  SHARED_OXYGEN_DATA_LIST_ROUTE: '/shared/oxygen-data',
  SHARED_GLUCOSE_DATA_LIST_ROUTE: '/shared/glucose-data',
  SHARED_WEIGHT_DATA_LIST_ROUTE: '/shared/weight-data',
  SHARED_WATER_DATA_LIST_ROUTE: '/shared/water-data',
};

export default function useRoutes() {
  const router = useRouter();
  const appUser = useAuthStore((state) => state.appUser);
  const { account = null } = appUser ?? {};
  const username = account?.username;

  // handle back
  const handleBack = useCallback(() => {
    router.back();
  }, [router]);
  // handle reload
  const handleReload = useCallback(() => {
    router.reload();
  }, [router]);
  // use index route
  const useIndexRoute = useCallback(() => {
    const indexRoute = routes.INDEX_ROUTE;

    const handleIndexRoute = (options = {}) => {
      const { query = null } = options;
      router.push({ pathname: indexRoute, query });
    };

    return { indexRoute, handleIndexRoute };
  }, [router]);
  // use sign in route
  const useSignInRoute = useCallback(() => {
    const signInRoute = routes.SIGNIN_ROUTE;

    const handleSignInRoute = (options = {}) => {
      const { query = null } = options;
      router.push({ pathname: signInRoute, query });
    };

    return { signInRoute, handleSignInRoute };
  }, [router]);
  // use sign out route
  const useSignOutRoute = useCallback(() => {
    const signOutRoute = routes.SIGNOUT_ROUTE;

    const handleSignOutRoute = (options = {}) => {
      const { query = null } = options;
      router.replace({ pathname: signOutRoute, query });
    };

    return { signOutRoute, handleSignOutRoute };
  }, [router]);
  // use home route
  const useHomeRoute = useCallback(() => {
    const homeRoute = handleNamedRoute({
      route: routes.HOME_ROUTE,
      username,
    });

    const handleHomeRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: handleNamedRoute({
          route: homeRoute,
        }),
        query,
      });
    };

    return { homeRoute, handleHomeRoute };
  }, [router, username]);
  // use bp data list route
  const useBPDataListRoute = useCallback(() => {
    const bpDataListRoute = handleNamedRoute({
      route: routes.BP_DATA_LIST_ROUTE,
    });

    const handleBPDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: bpDataListRoute,
        query,
      });
    };

    return { bpDataListRoute, handleBPDataListRoute };
  }, [router]);
  // use bp data new route
  const useBPDataNewRoute = useCallback(() => {
    const bpDataNewRoute = handleNamedRoute({
      route: routes.BP_DATA_NEW_ROUTE,
    });

    const handleBPDataNewRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: bpDataNewRoute,
        query,
      });
    };

    return { bpDataNewRoute, handleBPDataNewRoute };
  }, [router]);
  // use glucose data list route
  const useGlucoseDataListRoute = useCallback(() => {
    const glucoseDataListRoute = handleNamedRoute({
      route: routes.GLUCOSE_DATA_LIST_ROUTE,
    });

    const handleGlucoseDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: glucoseDataListRoute,
        query,
      });
    };

    return { glucoseDataListRoute, handleGlucoseDataListRoute };
  }, [router]);
  // use glucose data new route
  const useGlucoseDataNewRoute = useCallback(() => {
    const glucoseDataNewRoute = handleNamedRoute({
      route: routes.GLUCOSE_DATA_NEW_ROUTE,
    });

    const handleGlucoseDataNewRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: glucoseDataNewRoute,
        query,
      });
    };

    return { glucoseDataNewRoute, handleGlucoseDataNewRoute };
  }, [router]);
  // use glucose data edit route
  const useGlucoseDataEditRoute = useCallback(
    (glucoseData = null) => {
      const glucoseDataEditRoute = handleNamedRoute({
        route: routes.GLUCOSE_DATA_EDIT_ROUTE,
        glucoseData,
      });

      const handleGlucoseDataEditRoute = (options = {}) => {
        const { query = null } = options;
        router.push({
          pathname: glucoseDataEditRoute,
          query,
        });
      };

      return { glucoseDataEditRoute, handleGlucoseDataEditRoute };
    },
    [router]
  );
  // use temperature data list route
  const useTemperatureDataListRoute = useCallback(() => {
    const temperatureDataListRoute = handleNamedRoute({
      route: routes.TEMPERATURE_DATA_LIST_ROUTE,
    });

    const handleTemperatureDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: temperatureDataListRoute,
        query,
      });
    };

    return { temperatureDataListRoute, handleTemperatureDataListRoute };
  }, [router]);
  // use temperature data new route
  const useTemperatureDataNewRoute = useCallback(() => {
    const temperatureDataNewRoute = handleNamedRoute({
      route: routes.TEMPERATURE_DATA_NEW_ROUTE,
    });

    const handleTemperatureDataNewRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: temperatureDataNewRoute,
        query,
      });
    };

    return { temperatureDataNewRoute, handleTemperatureDataNewRoute };
  }, [router]);
  // use weight data list route
  const useWeightDataListRoute = useCallback(() => {
    const weightDataListRoute = handleNamedRoute({
      route: routes.WEIGHT_DATA_LIST_ROUTE,
    });

    const handleWeightDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: weightDataListRoute,
        query,
      });
    };

    return { weightDataListRoute, handleWeightDataListRoute };
  }, [router]);
  // use weight data new route
  const useWeightDataNewRoute = useCallback(() => {
    const weightDataNewRoute = handleNamedRoute({
      route: routes.WEIGHT_DATA_NEW_ROUTE,
    });

    const handleWeightDataNewRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: weightDataNewRoute,
        query,
      });
    };

    return { weightDataNewRoute, handleWeightDataNewRoute };
  }, [router]);
  // use water data list route
  const useWaterDataListRoute = useCallback(() => {
    const waterDataListRoute = handleNamedRoute({
      route: routes.WATER_DATA_LIST_ROUTE,
    });

    const handleWaterDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: waterDataListRoute,
        query,
      });
    };

    return { waterDataListRoute, handleWaterDataListRoute };
  }, [router]);
  // use water data new route
  const useWaterDataNewRoute = useCallback(() => {
    const waterDataNewRoute = handleNamedRoute({
      route: routes.WATER_DATA_NEW_ROUTE,
    });

    const handleWaterDataNewRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: waterDataNewRoute,
        query,
      });
    };

    return { waterDataNewRoute, handleWaterDataNewRoute };
  }, [router]);
  // use oxygen data list route
  const useOxygenDataListRoute = useCallback(() => {
    const oxygenDataListRoute = handleNamedRoute({
      route: routes.OXYGEN_DATA_LIST_ROUTE,
    });

    const handleOxygenDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: oxygenDataListRoute,
        query,
      });
    };

    return { oxygenDataListRoute, handleOxygenDataListRoute };
  }, [router]);
  // use oxygen data new route
  const useOxygenDataNewRoute = useCallback(() => {
    const oxygenDataNewRoute = handleNamedRoute({
      route: routes.OXYGEN_DATA_NEW_ROUTE,
    });

    const handleOxygenDataNewRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: oxygenDataNewRoute,
        query,
      });
    };

    return { oxygenDataNewRoute, handleOxygenDataNewRoute };
  }, [router]);
  // use cholesterol data list route
  const useCholesterolDataListRoute = useCallback(() => {
    const cholesterolDataListRoute = handleNamedRoute({
      route: routes.CHOLESTEROL_DATA_LIST_ROUTE,
    });

    const handleCholesterolDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: cholesterolDataListRoute,
        query,
      });
    };

    return { cholesterolDataListRoute, handleCholesterolDataListRoute };
  }, [router]);
  // use cholesterol data new route
  const useCholesterolDataNewRoute = useCallback(() => {
    const cholesterolDataNewRoute = handleNamedRoute({
      route: routes.CHOLESTEROL_DATA_NEW_ROUTE,
    });

    const handleCholesterolDataNewRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: cholesterolDataNewRoute,
        query,
      });
    };

    return { cholesterolDataNewRoute, handleCholesterolDataNewRoute };
  }, [router]);
  // use insights route
  const useInsightsRoute = useCallback(() => {
    const insightsRoute = handleNamedRoute({
      route: routes.INSIGHTS_ROUTE,
    });

    const handleInsightsRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: insightsRoute,
        query,
      });
    };

    return { insightsRoute, handleInsightsRoute };
  }, [router]);
  // use insights bp data route
  const useInsightsBPDataRoute = useCallback(() => {
    const insightsBPDataRoute = handleNamedRoute({
      route: routes.INSIGHTS_BP_DATA_ROUTE,
    });

    const handleInsightsBPDataRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: insightsBPDataRoute,
        query,
      });
    };

    return { insightsBPDataRoute, handleInsightsBPDataRoute };
  }, [router]);
  // use insights glucose data route
  const useInsightsGlucoseDataRoute = useCallback(() => {
    const insightsGlucoseDataRoute = handleNamedRoute({
      route: routes.INSIGHTS_GLUCOSE_DATA_ROUTE,
    });

    const handleInsightsGlucoseDataRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: insightsGlucoseDataRoute,
        query,
      });
    };
    return { insightsGlucoseDataRoute, handleInsightsGlucoseDataRoute };
  }, [router]);
  // use insights temperature data route
  const useInsightsTemperatureDataRoute = useCallback(() => {
    const insightsTemperatureDataRoute = handleNamedRoute({
      route: routes.INSIGHTS_TEMPERATURE_DATA_ROUTE,
    });

    const handleInsightsTemperatureDataRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: insightsTemperatureDataRoute,
        query,
      });
    };

    return { insightsTemperatureDataRoute, handleInsightsTemperatureDataRoute };
  }, [router]);
  // use insights oxygen data route
  const useInsightsOxygenDataRoute = useCallback(() => {
    const insightsOxygenDataRoute = handleNamedRoute({
      route: routes.INSIGHTS_OXYGEN_DATA_ROUTE,
    });

    const handleInsightsOxygenDataRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: insightsOxygenDataRoute,
        query,
      });
    };

    return { insightsOxygenDataRoute, handleInsightsOxygenDataRoute };
  }, [router]);
  // use insights cholesterol data route
  const useInsightsCholesterolDataRoute = useCallback(() => {
    const insightsCholesterolDataRoute = handleNamedRoute({
      route: routes.INSIGHTS_CHOLESTEROL_DATA_ROUTE,
    });

    const handleInsightsCholesterolDataRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: insightsCholesterolDataRoute,
        query,
      });
    };

    return { insightsCholesterolDataRoute, handleInsightsCholesterolDataRoute };
  }, [router]);
  // use health data shares route
  const useHealthDataSharesRoute = useCallback(() => {
    const healthDataSharesRoute = handleNamedRoute({
      route: routes.HEALTH_DATA_SHARES_ROUTE,
    });

    const handleHealthDataSharesRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: healthDataSharesRoute,
        query,
      });
    };

    return { healthDataSharesRoute, handleHealthDataSharesRoute };
  }, [router]);
  // use additional lives route
  const useAdditionalLivesRoute = useCallback(() => {
    const additionalLivesRoute = handleNamedRoute({
      route: routes.ADDITIONAL_LIVES_ROUTE,
    });

    const handleAdditionalLivesRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: additionalLivesRoute,
        query,
      });
    };

    return { additionalLivesRoute, handleAdditionalLivesRoute };
  }, [router]);
  // use additional life new route
  const useAdditionalLifeNewRoute = useCallback(() => {
    const additionalLifeNewRoute = handleNamedRoute({
      route: routes.ADDITIONAL_LIFE_NEW_ROUTE,
    });

    const handleAdditionalLifeNewRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: additionalLifeNewRoute,
        query,
      });
    };

    return { additionalLifeNewRoute, handleAdditionalLifeNewRoute };
  }, [router]);
  // use additional life edit route
  const useAdditionalLifeEditRoute = useCallback(
    (additionalLife = null) => {
      const additionalLifeEditRoute = handleNamedRoute({
        route: routes.ADDITIONAL_LIFE_EDIT_ROUTE,
        additionalLife,
      });

      const handleAdditionalLifeEditRoute = (options = {}) => {
        const { query = null } = options;
        router.push({
          pathname: additionalLifeEditRoute,
          query,
        });
      };

      return { additionalLifeEditRoute, handleAdditionalLifeEditRoute };
    },
    [router]
  );
  // use shared route
  const useSharedRoute = useCallback(() => {
    const sharedRoute = handleNamedRoute({
      route: routes.SHARED_ROUTE,
    });

    const handleSharedRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: sharedRoute,
        query,
      });
    };

    return { sharedRoute, handleSharedRoute };
  }, [router]);
  // use shared menu route
  const useSharedMenuRoute = useCallback(() => {
    const sharedMenuRoute = handleNamedRoute({
      route: routes.SHARED_MENU_ROUTE,
    });

    const handleSharedMenuRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: sharedMenuRoute,
        query,
      });
    };

    return { sharedMenuRoute, handleSharedMenuRoute };
  }, [router]);
  // use shared bp data list route
  const useSharedBPDataListRoute = useCallback(() => {
    const sharedBPDataListRoute = handleNamedRoute({
      route: routes.SHARED_BP_DATA_LIST_ROUTE,
    });

    const handleSharedBPDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: sharedBPDataListRoute,
        query,
      });
    };

    return { sharedBPDataListRoute, handleSharedBPDataListRoute };
  }, [router]);
  // use shared temperature data list route
  const useSharedTemperatureDataListRoute = useCallback(() => {
    const sharedTemperatureDataListRoute = handleNamedRoute({
      route: routes.SHARED_TEMPERATURE_DATA_LIST_ROUTE,
    });

    const handleSharedTemperatureDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: sharedTemperatureDataListRoute,
        query,
      });
    };

    return {
      sharedTemperatureDataListRoute,
      handleSharedTemperatureDataListRoute,
    };
  }, [router]);
  // use shared oxygen data list route
  const useSharedOxygenDataListRoute = useCallback(() => {
    const sharedOxygenDataListRoute = handleNamedRoute({
      route: routes.SHARED_OXYGEN_DATA_LIST_ROUTE,
    });

    const handleSharedOxygenDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: sharedOxygenDataListRoute,
        query,
      });
    };

    return {
      sharedOxygenDataListRoute,
      handleSharedOxygenDataListRoute,
    };
  }, [router]);
  // use shared glucose data list route
  const useSharedGlucoseDataListRoute = useCallback(() => {
    const sharedGlucoseDataListRoute = handleNamedRoute({
      route: routes.SHARED_GLUCOSE_DATA_LIST_ROUTE,
    });

    const handleSharedGlucoseDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: sharedGlucoseDataListRoute,
        query,
      });
    };

    return {
      sharedGlucoseDataListRoute,
      handleSharedGlucoseDataListRoute,
    };
  }, [router]);
  // use shared weight data list route
  const useSharedWeightDataListRoute = useCallback(() => {
    const sharedWeightDataListRoute = handleNamedRoute({
      route: routes.SHARED_WEIGHT_DATA_LIST_ROUTE,
    });

    const handleSharedWeightDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: sharedWeightDataListRoute,
        query,
      });
    };

    return {
      sharedWeightDataListRoute,
      handleSharedWeightDataListRoute,
    };
  }, [router]);
  // use shared water data list route
  const useSharedWaterDataListRoute = useCallback(() => {
    const sharedWaterDataListRoute = handleNamedRoute({
      route: routes.SHARED_WATER_DATA_LIST_ROUTE,
    });

    const handleSharedWaterDataListRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: sharedWaterDataListRoute,
        query,
      });
    };

    return {
      sharedWaterDataListRoute,
      handleSharedWaterDataListRoute,
    };
  }, [router]);

  return {
    appUser,
    router,
    handleBack,
    handleReload,
    useSignInRoute,
    useSignOutRoute,
    useIndexRoute,
    useHomeRoute,
    useBPDataListRoute,
    useBPDataNewRoute,
    useGlucoseDataListRoute,
    useGlucoseDataNewRoute,
    useGlucoseDataEditRoute,
    useTemperatureDataListRoute,
    useTemperatureDataNewRoute,
    useWeightDataListRoute,
    useWeightDataNewRoute,
    useWaterDataListRoute,
    useWaterDataNewRoute,
    useOxygenDataListRoute,
    useOxygenDataNewRoute,
    useCholesterolDataListRoute,
    useCholesterolDataNewRoute,
    useInsightsRoute,
    useInsightsBPDataRoute,
    useInsightsGlucoseDataRoute,
    useInsightsTemperatureDataRoute,
    useInsightsOxygenDataRoute,
    useInsightsCholesterolDataRoute,
    useHealthDataSharesRoute,
    useAdditionalLivesRoute,
    useAdditionalLifeNewRoute,
    useAdditionalLifeEditRoute,
    useSharedRoute,
    useSharedMenuRoute,
    useSharedBPDataListRoute,
    useSharedTemperatureDataListRoute,
    useSharedOxygenDataListRoute,
    useSharedGlucoseDataListRoute,
    useSharedWeightDataListRoute,
    useSharedWaterDataListRoute,
    routes,
  };
}

function handleNamedRoute({
  route,
  username,
  glucoseData = null,
  additionalLife = null,
}) {
  route = route || '';

  const { id: glucoseDataId } = glucoseData ?? {};
  const { id: additionalLifeId } = additionalLife ?? {};

  if (username) {
    route = route.replace('{{APP_USERNAME}}', username);
  }
  if (glucoseDataId) {
    route = route.replace('{{GLUCOSE_DATA_ID}}', glucoseDataId);
  }
  if (additionalLifeId) {
    route = route.replace('{{ADDITIONAL_LIFE_ID}}', additionalLifeId);
  }

  return route;
}
