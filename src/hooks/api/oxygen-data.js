import { useCallback, useEffect } from 'react';

import { OxygenDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useOxygenDataStore } from 'src/stores/oxygen-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListOxygenDataList(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    oxygenDataList: _oxygenDataList,
    oxygenDataPage: __page,
    setOxygenDataPage: _setPage,
    oxygenDataEndPaging: __endPaging,
    setOxygenDataEndPaging: _setEndPaging,
    resetOxygenDataPage: resetPage,
    listOxygenDataList,
  } = useOxygenDataStore((state) => state);
  const { error, setError } = useItem();
  const oxygenDataList = _oxygenDataList
    ? _oxygenDataList[listKey]
    : _oxygenDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!oxygenDataList?.length) {
      (async () => {
        await handleListOxygenDataList({
          page,
          userDataId,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [oxygenDataList?.length]);

  useEffect(() => {
    if (oxygenDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListOxygenDataList({
          page,
          userDataId,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListOxygenDataList = useCallback(
    async ({ page, userDataId }) => {
      try {
        const response = await OxygenDataService.listOxygenDataList({
          page,
          userDataId,
        });
        const data = await response.json();
        if (response.ok) {
          const oxygenDataList = data.data;
          if (oxygenDataList?.length || page === 1) {
            listOxygenDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listOxygenDataList,
      setError,
    ]
  );

  return {
    handleListOxygenDataList,
    oxygenDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useListSharedOxygenDataList(options = null) {
  const { healthDataSharePublic } = options ?? {};
  const listKey = generateListKey();
  const {
    sharedOxygenDataList: _sharedOxygenDataList,
    sharedOxygenDataPage: __page,
    setSharedOxygenDataPage: _setPage,
    sharedOxygenDataEndPaging: __endPaging,
    setSharedOxygenDataEndPaging: _setEndPaging,
    resetSharedOxygenDataPage: resetPage,
    listSharedOxygenDataList,
  } = useOxygenDataStore((state) => state);
  const { error, setError } = useItem();
  const sharedOxygenDataList = _sharedOxygenDataList
    ? _sharedOxygenDataList[listKey]
    : _sharedOxygenDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!sharedOxygenDataList?.length) {
      (async () => {
        await handleListSharedOxygenDataList({
          page,
          healthDataSharePublic,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sharedOxygenDataList?.length]);

  useEffect(() => {
    if (sharedOxygenDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListSharedOxygenDataList({
          page,
          healthDataSharePublic,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListSharedOxygenDataList = useCallback(
    async ({ page, healthDataSharePublic }) => {
      try {
        const response = await OxygenDataService.listSharedOxygenDataList({
          page,
          healthDataSharePublic,
        });
        const data = await response.json();
        if (response.ok) {
          const sharedOxygenDataList = data.data;
          if (sharedOxygenDataList?.length || page === 1) {
            listSharedOxygenDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listSharedOxygenDataList,
      setError,
    ]
  );

  return {
    handleListSharedOxygenDataList,
    sharedOxygenDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetOxygenData({
  oxygenData: _oxygenData = null,
  ignoreLoadOnMount = false,
}) {
  const { oxygenData, setOxygenData } = useOxygenDataStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetOxygenData(_oxygenData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetOxygenData = useCallback(
    async (oxygenData = null) => {
      try {
        const response = await OxygenDataService.getOxygenData(oxygenData);
        const data = await response.json();
        if (response.ok) {
          const oxygenData = data.data;
          setOxygenData(oxygenData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setOxygenData, setError]
  );

  return {
    handleGetOxygenData,
    error,
    setError,
    processing,
    setProcessing,
    oxygenData,
  };
}

export function useFindOxygenData({
  oxygenData: _oxygenData = null,
  ignoreLoadOnMount = false,
  userDataId,
}) {
  const listKey = generateListKey({ userDataId });
  const {
    oxygenDataList: _oxygenDataList,
    setOxygenData,
    oxygenData,
  } = useOxygenDataStore((state) => state);
  const oxygenDataList = _oxygenDataList
    ? _oxygenDataList[listKey]
    : _oxygenDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetOxygenData } = useGetOxygenData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindOxygenData(_oxygenData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindOxygenData = useCallback(
    async (oxygenData = null) => {
      try {
        if (oxygenDataList?.length) {
          setOxygenData(
            oxygenDataList.find((item) => item.id === oxygenData?.id)
          );
        } else {
          await handleGetOxygenData(oxygenData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [oxygenDataList, setOxygenData, handleGetOxygenData, setError]
  );

  return {
    handleFindOxygenData,
    error,
    setError,
    processing,
    setProcessing,
    oxygenData,
  };
}

export function useCreateOxygenData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: oxygenData,
    setItem: setOxygenData,
    itemCreated: oxygenDataCreated,
    setItemCreated: setOxygenDataCreated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { addOxygenData } = useOxygenDataStore((state) => state);

  const handleCreateOxygenData = useCallback(
    async (body = null) => {
      try {
        setProcessing(true);
        const response = await OxygenDataService.createOxygenData(body);
        const data = await response.json();
        if (response.ok) {
          const newOxygenData = data.data;
          setOxygenData(newOxygenData);
          setOxygenDataCreated(true);
          addOxygenData({ key: listKey, item: newOxygenData });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setOxygenData,
      setOxygenDataCreated,
      addOxygenData,
      listKey,
      setError,
    ]
  );

  return {
    handleCreateOxygenData,
    error,
    setError,
    processing,
    setProcessing,
    oxygenData,
    setOxygenData,
    oxygenDataCreated,
    setOxygenDataCreated,
  };
}

export function useRemoveOxygenData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: oxygenData,
    setItem: setOxygenData,
    itemRemoved: oxygenDataRemoved,
    setItemRemoved: setOxygenDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeOxygenData } = useOxygenDataStore((state) => state);

  const handleRemoveOxygenData = useCallback(
    async (oxygenData = null) => {
      try {
        setProcessing(true);
        const response = await OxygenDataService.removeOxygenData(oxygenData);
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setOxygenData(oxygenData);
            setOxygenDataRemoved(true);
            removeOxygenData({
              key: listKey,
              item: oxygenData,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setOxygenData,
      setOxygenDataRemoved,
      removeOxygenData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveOxygenData,
    error,
    setError,
    processing,
    setProcessing,
    oxygenData,
    setOxygenData,
    oxygenDataRemoved,
    setOxygenDataRemoved,
  };
}

export function useResetOxygenDataList(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const { resetOxygenDataPage: resetPage, clearOxygenDataList } =
    useOxygenDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetOxygenDataList = useCallback(() => {
    handleResetPage();
    clearOxygenDataList({ key: listKey });
  }, [handleResetPage, clearOxygenDataList, listKey]);

  return handleResetOxygenDataList;
}

export function useResetSharedOxygenDataList() {
  //const { userDataId } = options ?? {};
  const listKey = generateListKey();
  const { resetSharedOxygenDataPage: resetPage, clearSharedOxygenDataList } =
    useOxygenDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetSharedOxygenDataList = useCallback(() => {
    handleResetPage();
    clearSharedOxygenDataList({ key: listKey });
  }, [handleResetPage, clearSharedOxygenDataList, listKey]);

  return handleResetSharedOxygenDataList;
}
