import { useCallback, useEffect } from 'react';

import { GlucoseDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useGlucoseDataStore } from 'src/stores/glucose-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListGlucoseDataList(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    glucoseDataList: _glucoseDataList,
    glucoseDataPage: __page,
    setGlucoseDataPage: _setPage,
    glucoseDataEndPaging: __endPaging,
    setGlucoseDataEndPaging: _setEndPaging,
    resetGlucoseDataPage: resetPage,
    listGlucoseDataList,
  } = useGlucoseDataStore((state) => state);
  const { error, setError } = useItem();
  const glucoseDataList = _glucoseDataList
    ? _glucoseDataList[listKey]
    : _glucoseDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!glucoseDataList?.length) {
      (async () => {
        await handleListGlucoseDataList({
          page,
          userDataId,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [glucoseDataList?.length]);

  useEffect(() => {
    if (glucoseDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListGlucoseDataList({
          page,
          userDataId,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListGlucoseDataList = useCallback(
    async ({ page, userDataId }) => {
      try {
        const response = await GlucoseDataService.listGlucoseDataList({
          page,
          userDataId,
        });
        const data = await response.json();
        if (response.ok) {
          const glucoseDataList = data.data;
          if (glucoseDataList?.length || page === 1) {
            listGlucoseDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listGlucoseDataList,
      setError,
    ]
  );

  return {
    handleListGlucoseDataList,
    glucoseDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useListSharedGlucoseDataList(options = null) {
  const { healthDataSharePublic } = options ?? {};
  const listKey = generateListKey();
  const {
    sharedGlucoseDataList: _sharedGlucoseDataList,
    sharedGlucoseDataPage: __page,
    setSharedGlucoseDataPage: _setPage,
    sharedGlucoseDataEndPaging: __endPaging,
    setSharedGlucoseDataEndPaging: _setEndPaging,
    resetSharedGlucoseDataPage: resetPage,
    listSharedGlucoseDataList,
  } = useGlucoseDataStore((state) => state);
  const { error, setError } = useItem();
  const sharedGlucoseDataList = _sharedGlucoseDataList
    ? _sharedGlucoseDataList[listKey]
    : _sharedGlucoseDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!sharedGlucoseDataList?.length) {
      (async () => {
        await handleListSharedGlucoseDataList({
          page,
          healthDataSharePublic,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sharedGlucoseDataList?.length]);

  useEffect(() => {
    if (sharedGlucoseDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListSharedGlucoseDataList({
          page,
          healthDataSharePublic,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListSharedGlucoseDataList = useCallback(
    async ({ page, healthDataSharePublic }) => {
      try {
        const response = await GlucoseDataService.listSharedGlucoseDataList({
          page,
          healthDataSharePublic,
        });
        const data = await response.json();
        if (response.ok) {
          const sharedGlucoseDataList = data.data;
          if (sharedGlucoseDataList?.length || page === 1) {
            listSharedGlucoseDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listSharedGlucoseDataList,
      setError,
    ]
  );

  return {
    handleListSharedGlucoseDataList,
    sharedGlucoseDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetGlucoseData({
  glucoseData: _glucoseData = null,
  ignoreLoadOnMount = false,
}) {
  const { glucoseData, setGlucoseData } = useGlucoseDataStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetGlucoseData(_glucoseData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetGlucoseData = useCallback(
    async (glucoseData = null) => {
      try {
        const response = await GlucoseDataService.getGlucoseData(glucoseData);
        const data = await response.json();
        if (response.ok) {
          const glucoseData = data.data;
          setGlucoseData(glucoseData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setGlucoseData, setError]
  );

  return {
    handleGetGlucoseData,
    error,
    setError,
    processing,
    setProcessing,
    glucoseData,
  };
}

export function useFindGlucoseData({
  glucoseData: _glucoseData = null,
  ignoreLoadOnMount = false,
  userDataId,
}) {
  const listKey = generateListKey({ userDataId });
  const {
    glucoseDataList: _glucoseDataList,
    setGlucoseData,
    glucoseData,
  } = useGlucoseDataStore((state) => state);
  const glucoseDataList = _glucoseDataList
    ? _glucoseDataList[listKey]
    : _glucoseDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetGlucoseData } = useGetGlucoseData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindGlucoseData(_glucoseData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindGlucoseData = useCallback(
    async (glucoseData = null) => {
      try {
        if (glucoseDataList?.length) {
          setGlucoseData(
            glucoseDataList.find((item) => item.id === glucoseData?.id)
          );
        } else {
          await handleGetGlucoseData(glucoseData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [glucoseDataList, setGlucoseData, handleGetGlucoseData, setError]
  );

  return {
    handleFindGlucoseData,
    error,
    setError,
    processing,
    setProcessing,
    glucoseData,
  };
}

export function useCreateGlucoseData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: glucoseData,
    setItem: setGlucoseData,
    itemCreated: glucoseDataCreated,
    setItemCreated: setGlucoseDataCreated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { addGlucoseData } = useGlucoseDataStore((state) => state);

  const handleCreateGlucoseData = useCallback(
    async (body = null) => {
      try {
        setProcessing(true);
        const response = await GlucoseDataService.createGlucoseData(body);
        const data = await response.json();
        if (response.ok) {
          const newGlucoseData = data.data;
          setGlucoseData(newGlucoseData);
          setGlucoseDataCreated(true);
          addGlucoseData({ key: listKey, item: newGlucoseData });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setGlucoseData,
      setGlucoseDataCreated,
      addGlucoseData,
      listKey,
      setError,
    ]
  );

  return {
    handleCreateGlucoseData,
    error,
    setError,
    processing,
    setProcessing,
    glucoseData,
    setGlucoseData,
    glucoseDataCreated,
    setGlucoseDataCreated,
  };
}

export function useUpdateGlucoseData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: glucoseData,
    setItem: setGlucoseData,
    itemUpdated: glucoseDataUpdated,
    setItemUpdated: setGlucoseDataUpdated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { updateGlucoseData } = useGlucoseDataStore((state) => state);

  const handleUpdateGlucoseData = useCallback(
    async (glucoseData = null, body = null) => {
      try {
        setProcessing(true);
        const response = await GlucoseDataService.updateGlucoseData(
          glucoseData,
          body
        );
        const data = await response.json();
        if (response.ok) {
          const glucoseData = data.data;
          setGlucoseData(glucoseData);
          setGlucoseDataUpdated(true);
          updateGlucoseData({ key: listKey, item: glucoseData });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setGlucoseData,
      setGlucoseDataUpdated,
      updateGlucoseData,
      listKey,
      setError,
    ]
  );

  return {
    handleUpdateGlucoseData,
    error,
    setError,
    processing,
    setProcessing,
    glucoseData,
    setGlucoseData,
    glucoseDataUpdated,
    setGlucoseDataUpdated,
  };
}

export function useRemoveGlucoseData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: glucoseData,
    setItem: setGlucoseData,
    itemRemoved: glucoseDataRemoved,
    setItemRemoved: setGlucoseDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeGlucoseData } = useGlucoseDataStore((state) => state);

  const handleRemoveGlucoseData = useCallback(
    async (glucoseData = null) => {
      try {
        setProcessing(true);
        const response = await GlucoseDataService.removeGlucoseData(
          glucoseData
        );
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setGlucoseData(glucoseData);
            setGlucoseDataRemoved(true);
            removeGlucoseData({
              key: listKey,
              item: glucoseData,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setGlucoseData,
      setGlucoseDataRemoved,
      removeGlucoseData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveGlucoseData,
    error,
    setError,
    processing,
    setProcessing,
    glucoseData,
    setGlucoseData,
    glucoseDataRemoved,
    setGlucoseDataRemoved,
  };
}

export function useResetGlucoseDataList(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const { resetGlucoseDataPage: resetPage, clearGlucoseDataList } =
    useGlucoseDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetGlucoseDataList = useCallback(() => {
    handleResetPage();
    clearGlucoseDataList({ key: listKey });
  }, [handleResetPage, clearGlucoseDataList, listKey]);

  return handleResetGlucoseDataList;
}

export function useResetSharedGlucoseDataList() {
  //const { userDataId } = options ?? {};
  const listKey = generateListKey();
  const { resetSharedGlucoseDataPage: resetPage, clearSharedGlucoseDataList } =
    useGlucoseDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetSharedGlucoseDataList = useCallback(() => {
    handleResetPage();
    clearSharedGlucoseDataList({ key: listKey });
  }, [handleResetPage, clearSharedGlucoseDataList, listKey]);

  return handleResetSharedGlucoseDataList;
}
