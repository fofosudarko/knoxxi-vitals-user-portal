import { useCallback, useReducer, useState } from 'react';
import { produce } from 'immer';

import {
  BPDataService,
  GlucoseDataService,
  TemperatureDataService,
  WeightDataService,
  WaterDataService,
  OxygenDataService,
  HealthDataShareService,
} from 'src/api/app/health';
import { DEFAULT_SELECT_SIZE } from 'src/config';

const initialState = {
  bpDataList: null,
  glucoseDataList: null,
  temperatureDataList: null,
  weightDataList: null,
  waterDataList: null,
  oxygenDataList: null,
  healthDataShares: null,
  error: null,
  processing: false,
};

const types = {
  SET_BP_DATA_LIST: 'SET_BP_DATA_LIST',
  SET_GLUCOSE_DATA_LIST: 'SET_GLUCOSE_DATA_LIST',
  SET_TEMPERATURE_DATA_LIST: 'SET_TEMPERATURE_DATA_LIST',
  SET_WEIGHT_DATA_LIST: 'SET_WEIGHT_DATA_LIST',
  SET_WATER_DATA_LIST: 'SET_WATER_DATA_LIST',
  SET_OXYGEN_DATA_LIST: 'SET_OXYGEN_DATA_LIST',
  SET_HEALTH_DATA_SHARES: 'SET_HEALTH_DATA_SHARES',
  CLEAR_SELECT_STATE: 'CLEAR_SELECT_STATE',
  SET_ERROR: 'SET_ERROR',
  SET_PROCESSING: 'SET_PROCESSING',
};

const reducer = produce((draft, action) => {
  switch (action.type) {
    case types.SET_BP_DATA_LIST:
      draft.bpDataList = action.payload;
      return;
    case types.SET_GLUCOSE_DATA_LIST:
      draft.glucoseDataList = action.payload;
      return;
    case types.SET_TEMPERATURE_DATA_LIST:
      draft.temperatureDataList = action.payload;
      return;
    case types.SET_WEIGHT_DATA_LIST:
      draft.weightDataList = action.payload;
      return;
    case types.SET_WATER_DATA_LIST:
      draft.waterDataList = action.payload;
      return;
    case types.SET_OXYGEN_DATA_LIST:
      draft.oxygenDataList = action.payload;
      return;
    case types.SET_HEALTH_DATA_SHARES:
      draft.healthDataShares = action.payload;
      return;
    case types.SET_ERROR:
      draft.error = action.payload;
      return;
    case types.SET_PROCESSING:
      draft.processing = action.payload;
      return;
    case types.CLEAR_SELECT_STATE:
      return initialState;
    default:
      return draft;
  }
});

export function useSelectItems() {
  const [
    {
      error,
      processing,
      bpDataList,
      glucoseDataList,
      temperatureDataList,
      weightDataList,
      waterDataList,
      oxygenDataList,
      healthDataShares,
    },
    dispatch,
  ] = useReducer(reducer, initialState);

  const setError = useCallback((error) => {
    dispatch({ type: types.SET_ERROR, payload: error });
  }, []);
  const setProcessing = useCallback((processing) => {
    dispatch({ type: types.SET_PROCESSING, payload: processing });
  }, []);
  const setBPDataList = useCallback((bpDataList) => {
    dispatch({ type: types.SET_BP_DATA_LIST, payload: bpDataList });
  }, []);
  const setGlucoseDataList = useCallback((glucoseDataList) => {
    dispatch({ type: types.SET_GLUCOSE_DATA_LIST, payload: glucoseDataList });
  }, []);
  const setTemperatureDataList = useCallback((temperatureDataList) => {
    dispatch({
      type: types.SET_TEMPERATURE_DATA_LIST,
      payload: temperatureDataList,
    });
  }, []);
  const setWeightDataList = useCallback((weightDataList) => {
    dispatch({
      type: types.SET_WEIGHT_DATA_LIST,
      payload: weightDataList,
    });
  }, []);
  const setWaterDataList = useCallback((waterDataList) => {
    dispatch({
      type: types.SET_WATER_DATA_LIST,
      payload: waterDataList,
    });
  }, []);
  const setOxygenDataList = useCallback((oxygenDataList) => {
    dispatch({
      type: types.SET_OXYGEN_DATA_LIST,
      payload: oxygenDataList,
    });
  }, []);
  const setHealthDataShares = useCallback((healthDataShares) => {
    dispatch({
      type: types.SET_HEALTH_DATA_SHARES,
      payload: healthDataShares,
    });
  }, []);

  const handleListBPDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await BPDataService.listBPDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const bpDataList = data.data;
          setBPDataList(bpDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setBPDataList, setError]
  );
  const handleListGlucoseDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await GlucoseDataService.listGlucoseDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const glucoseDataList = data.data;
          setGlucoseDataList(glucoseDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setGlucoseDataList, setError]
  );
  const handleListTemperatureDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await TemperatureDataService.listTemperatureDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const temperatureDataList = data.data;
          setTemperatureDataList(temperatureDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setTemperatureDataList, setError]
  );
  const handleListWeightDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await WeightDataService.listWeightDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const weightDataList = data.data;
          setWeightDataList(weightDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setWeightDataList, setError]
  );
  const handleListWaterDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await WaterDataService.listWaterDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const waterDataList = data.data;
          setWaterDataList(waterDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setWaterDataList, setError]
  );
  const handleListOxygenDataList = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const { userDataFullname, size = DEFAULT_SELECT_SIZE } = options;
        const response = await OxygenDataService.listOxygenDataList({
          userDataFullname,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const oxygenDataList = data.data;
          setOxygenDataList(oxygenDataList);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setOxygenDataList, setError]
  );
  const handleListHealthDataShares = useCallback(
    async (options = {}) => {
      try {
        setProcessing(true);
        const {
          category,
          scope,
          recipientType,
          sharedById,
          size = DEFAULT_SELECT_SIZE,
        } = options;
        const response = await HealthDataShareService.listHealthDataShares({
          category,
          scope,
          recipientType,
          sharedById,
          size,
        });
        const data = await response.json();
        if (response.ok) {
          const healthDataShares = data.data;
          setHealthDataShares(healthDataShares);
        } else {
          setError(data.error);
        }
        setProcessing(false);
      } catch (error) {
        setError(error);
      }
    },
    [setProcessing, setHealthDataShares, setError]
  );

  return {
    handleListBPDataList,
    bpDataList,
    setBPDataList,
    handleListGlucoseDataList,
    glucoseDataList,
    setGlucoseDataList,
    handleListTemperatureDataList,
    temperatureDataList,
    setTemperatureDataList,
    handleListWeightDataList,
    weightDataList,
    setWeightDataList,
    handleListWaterDataList,
    waterDataList,
    setWaterDataList,
    handleListOxygenDataList,
    oxygenDataList,
    setOxygenDataList,
    handleListHealthDataShares,
    healthDataShares,
    setHealthDataShares,
    error,
    setError,
    processing,
    setProcessing,
  };
}

export function useSearchItems() {
  const [isSearch, setIsSearch] = useState(false);
  const handleSearch = useCallback((isSearch) => {
    setIsSearch(isSearch);
  }, []);
  return { isSearch, setIsSearch, handleSearch };
}
