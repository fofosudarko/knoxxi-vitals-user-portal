import { useCallback, useEffect } from 'react';

import { AdditionalLifeService } from 'src/api/app/health';

import { usePager } from '../page';
import { useAdditionalLifeStore } from 'src/stores/additional-life';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListAdditionalLives(options = null) {
  const { enteredById } = options ?? {};
  const listKey = generateListKey({ enteredById });
  const {
    additionalLives: _additionalLives,
    additionalLifePage: __page,
    setAdditionalLifePage: _setPage,
    additionalLifeEndPaging: __endPaging,
    setAdditionalLifeEndPaging: _setEndPaging,
    resetAdditionalLifePage: resetPage,
    listAdditionalLives,
  } = useAdditionalLifeStore((state) => state);
  const { error, setError } = useItem();
  const additionalLives = _additionalLives
    ? _additionalLives[listKey]
    : _additionalLives;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!additionalLives?.length) {
      (async () => {
        await handleListAdditionalLives({
          page,
          enteredById,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [additionalLives?.length]);

  useEffect(() => {
    if (additionalLives?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListAdditionalLives({
          page,
          enteredById,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListAdditionalLives = useCallback(
    async ({ page, enteredById }) => {
      try {
        const response = await AdditionalLifeService.listAdditionalLives({
          page,
          enteredById,
        });
        const data = await response.json();
        if (response.ok) {
          const additionalLives = data.data;
          if (additionalLives?.length || page === 1) {
            listAdditionalLives({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listAdditionalLives,
      setError,
    ]
  );

  return {
    handleListAdditionalLives,
    additionalLives,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetAdditionalLife({
  additionalLife: _additionalLife = null,
  ignoreLoadOnMount = false,
}) {
  const { additionalLife, setAdditionalLife } = useAdditionalLifeStore(
    (state) => state
  );
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetAdditionalLife(_additionalLife);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetAdditionalLife = useCallback(
    async (additionalLife = null) => {
      try {
        const response = await AdditionalLifeService.getAdditionalLife(
          additionalLife
        );
        const data = await response.json();
        if (response.ok) {
          const additionalLife = data.data;
          setAdditionalLife(additionalLife);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setAdditionalLife, setError]
  );

  return {
    handleGetAdditionalLife,
    error,
    setError,
    processing,
    setProcessing,
    additionalLife,
  };
}

export function useFindAdditionalLife({
  additionalLife: _additionalLife = null,
  ignoreLoadOnMount = false,
  enteredById,
}) {
  const listKey = generateListKey({ enteredById });
  const {
    additionalLives: _additionalLives,
    setAdditionalLife,
    additionalLife,
  } = useAdditionalLifeStore((state) => state);
  const additionalLives = _additionalLives
    ? _additionalLives[listKey]
    : _additionalLives;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetAdditionalLife } = useGetAdditionalLife({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindAdditionalLife(_additionalLife);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindAdditionalLife = useCallback(
    async (additionalLife = null) => {
      try {
        if (additionalLives?.length) {
          setAdditionalLife(
            additionalLives.find((item) => item.id === additionalLife?.id)
          );
        } else {
          await handleGetAdditionalLife(additionalLife);
        }
      } catch (error) {
        setError(error);
      }
    },
    [additionalLives, setAdditionalLife, handleGetAdditionalLife, setError]
  );

  return {
    handleFindAdditionalLife,
    error,
    setError,
    processing,
    setProcessing,
    additionalLife,
  };
}

export function useCreateAdditionalLife(options = null) {
  const { enteredById } = options ?? {};
  const listKey = generateListKey({ enteredById });
  const {
    item: additionalLife,
    setItem: setAdditionalLife,
    itemCreated: additionalLifeCreated,
    setItemCreated: setAdditionalLifeCreated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { addAdditionalLife } = useAdditionalLifeStore((state) => state);

  const handleCreateAdditionalLife = useCallback(
    async (body = null) => {
      try {
        setProcessing(true);
        const response = await AdditionalLifeService.createAdditionalLife(body);
        const data = await response.json();
        if (response.ok) {
          const newAdditionalLife = data.data;
          setAdditionalLife(newAdditionalLife);
          setAdditionalLifeCreated(true);
          addAdditionalLife({ key: listKey, item: newAdditionalLife });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setAdditionalLife,
      setAdditionalLifeCreated,
      addAdditionalLife,
      listKey,
      setError,
    ]
  );

  return {
    handleCreateAdditionalLife,
    error,
    setError,
    processing,
    setProcessing,
    additionalLife,
    setAdditionalLife,
    additionalLifeCreated,
    setAdditionalLifeCreated,
  };
}

export function useUpdateAdditionalLife(options = null) {
  const { enteredById } = options ?? {};
  const listKey = generateListKey({ enteredById });
  const {
    item: additionalLife,
    setItem: setAdditionalLife,
    itemUpdated: additionalLifeUpdated,
    setItemUpdated: setAdditionalLifeUpdated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { updateAdditionalLife } = useAdditionalLifeStore((state) => state);

  const handleUpdateAdditionalLife = useCallback(
    async (additionalLife = null, body = null) => {
      try {
        setProcessing(true);
        const response = await AdditionalLifeService.updateAdditionalLife(
          additionalLife,
          body
        );
        const data = await response.json();
        if (response.ok) {
          const additionalLife = data.data;
          setAdditionalLife(additionalLife);
          setAdditionalLifeUpdated(true);
          updateAdditionalLife({ key: listKey, item: additionalLife });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setAdditionalLife,
      setAdditionalLifeUpdated,
      updateAdditionalLife,
      listKey,
      setError,
    ]
  );

  return {
    handleUpdateAdditionalLife,
    error,
    setError,
    processing,
    setProcessing,
    additionalLife,
    setAdditionalLife,
    additionalLifeUpdated,
    setAdditionalLifeUpdated,
  };
}

export function useRemoveAdditionalLife(options = null) {
  const { enteredById } = options ?? {};
  const listKey = generateListKey({ enteredById });
  const {
    item: additionalLife,
    setItem: setAdditionalLife,
    itemRemoved: additionalLifeRemoved,
    setItemRemoved: setAdditionalLifeRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeAdditionalLife } = useAdditionalLifeStore((state) => state);

  const handleRemoveAdditionalLife = useCallback(
    async (additionalLife = null) => {
      try {
        setProcessing(true);
        const response = await AdditionalLifeService.removeAdditionalLife(
          additionalLife
        );
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setAdditionalLife(additionalLife);
            setAdditionalLifeRemoved(true);
            removeAdditionalLife({
              key: listKey,
              item: additionalLife,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setAdditionalLife,
      setAdditionalLifeRemoved,
      removeAdditionalLife,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveAdditionalLife,
    error,
    setError,
    processing,
    setProcessing,
    additionalLife,
    setAdditionalLife,
    additionalLifeRemoved,
    setAdditionalLifeRemoved,
  };
}

export function useResetAdditionalLives(options = null) {
  const { enteredById } = options ?? {};
  const listKey = generateListKey({ enteredById });
  const { resetAdditionalLifePage: resetPage, clearAdditionalLives } =
    useAdditionalLifeStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetAdditionalLives = useCallback(() => {
    handleResetPage();
    clearAdditionalLives({ key: listKey });
  }, [handleResetPage, clearAdditionalLives, listKey]);

  return handleResetAdditionalLives;
}
