import { useCallback, useEffect } from 'react';

import { WeightDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useWeightDataStore } from 'src/stores/weight-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListWeightDataList(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    weightDataList: _weightDataList,
    weightDataPage: __page,
    setWeightDataPage: _setPage,
    weightDataEndPaging: __endPaging,
    setWeightDataEndPaging: _setEndPaging,
    resetWeightDataPage: resetPage,
    listWeightDataList,
  } = useWeightDataStore((state) => state);
  const { error, setError } = useItem();
  const weightDataList = _weightDataList
    ? _weightDataList[listKey]
    : _weightDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!weightDataList?.length) {
      (async () => {
        await handleListWeightDataList({
          page,
          userDataId,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [weightDataList?.length]);

  useEffect(() => {
    if (weightDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListWeightDataList({
          page,
          userDataId,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListWeightDataList = useCallback(
    async ({ page, userDataId }) => {
      try {
        const response = await WeightDataService.listWeightDataList({
          page,
          userDataId,
        });
        const data = await response.json();
        if (response.ok) {
          const weightDataList = data.data;
          if (weightDataList?.length || page === 1) {
            listWeightDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listWeightDataList,
      setError,
    ]
  );

  return {
    handleListWeightDataList,
    weightDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useListSharedWeightDataList(options = null) {
  const { healthDataSharePublic } = options ?? {};
  const listKey = generateListKey();
  const {
    sharedWeightDataList: _sharedWeightDataList,
    sharedWeightDataPage: __page,
    setSharedWeightDataPage: _setPage,
    sharedWeightDataEndPaging: __endPaging,
    setSharedWeightDataEndPaging: _setEndPaging,
    resetSharedWeightDataPage: resetPage,
    listSharedWeightDataList,
  } = useWeightDataStore((state) => state);
  const { error, setError } = useItem();
  const sharedWeightDataList = _sharedWeightDataList
    ? _sharedWeightDataList[listKey]
    : _sharedWeightDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!sharedWeightDataList?.length) {
      (async () => {
        await handleListSharedWeightDataList({
          page,
          healthDataSharePublic,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sharedWeightDataList?.length]);

  useEffect(() => {
    if (sharedWeightDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListSharedWeightDataList({
          page,
          healthDataSharePublic,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListSharedWeightDataList = useCallback(
    async ({ page, healthDataSharePublic }) => {
      try {
        const response = await WeightDataService.listSharedWeightDataList({
          page,
          healthDataSharePublic,
        });
        const data = await response.json();
        if (response.ok) {
          const sharedWeightDataList = data.data;
          if (sharedWeightDataList?.length || page === 1) {
            listSharedWeightDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listSharedWeightDataList,
      setError,
    ]
  );

  return {
    handleListSharedWeightDataList,
    sharedWeightDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetWeightData({
  weightData: _weightData = null,
  ignoreLoadOnMount = false,
}) {
  const { weightData, setWeightData } = useWeightDataStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetWeightData(_weightData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetWeightData = useCallback(
    async (weightData = null) => {
      try {
        const response = await WeightDataService.getWeightData(weightData);
        const data = await response.json();
        if (response.ok) {
          const weightData = data.data;
          setWeightData(weightData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setWeightData, setError]
  );

  return {
    handleGetWeightData,
    error,
    setError,
    processing,
    setProcessing,
    weightData,
  };
}

export function useFindWeightData({
  weightData: _weightData = null,
  ignoreLoadOnMount = false,
  userDataId,
}) {
  const listKey = generateListKey({ userDataId });
  const {
    weightDataList: _weightDataList,
    setWeightData,
    weightData,
  } = useWeightDataStore((state) => state);
  const weightDataList = _weightDataList
    ? _weightDataList[listKey]
    : _weightDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetWeightData } = useGetWeightData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindWeightData(_weightData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindWeightData = useCallback(
    async (weightData = null) => {
      try {
        if (weightDataList?.length) {
          setWeightData(
            weightDataList.find((item) => item.id === weightData?.id)
          );
        } else {
          await handleGetWeightData(weightData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [weightDataList, setWeightData, handleGetWeightData, setError]
  );

  return {
    handleFindWeightData,
    error,
    setError,
    processing,
    setProcessing,
    weightData,
  };
}

export function useCreateWeightData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: weightData,
    setItem: setWeightData,
    itemCreated: weightDataCreated,
    setItemCreated: setWeightDataCreated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { addWeightData } = useWeightDataStore((state) => state);

  const handleCreateWeightData = useCallback(
    async (body = null) => {
      try {
        setProcessing(true);
        const response = await WeightDataService.createWeightData(body);
        const data = await response.json();
        if (response.ok) {
          const newWeightData = data.data;
          setWeightData(newWeightData);
          setWeightDataCreated(true);
          addWeightData({ key: listKey, item: newWeightData });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setWeightData,
      setWeightDataCreated,
      addWeightData,
      listKey,
      setError,
    ]
  );

  return {
    handleCreateWeightData,
    error,
    setError,
    processing,
    setProcessing,
    weightData,
    setWeightData,
    weightDataCreated,
    setWeightDataCreated,
  };
}

export function useRemoveWeightData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: weightData,
    setItem: setWeightData,
    itemRemoved: weightDataRemoved,
    setItemRemoved: setWeightDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeWeightData } = useWeightDataStore((state) => state);

  const handleRemoveWeightData = useCallback(
    async (weightData = null) => {
      try {
        setProcessing(true);
        const response = await WeightDataService.removeWeightData(weightData);
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setWeightData(weightData);
            setWeightDataRemoved(true);
            removeWeightData({
              key: listKey,
              item: weightData,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setWeightData,
      setWeightDataRemoved,
      removeWeightData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveWeightData,
    error,
    setError,
    processing,
    setProcessing,
    weightData,
    setWeightData,
    weightDataRemoved,
    setWeightDataRemoved,
  };
}

export function useResetWeightDataList(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const { resetWeightDataPage: resetPage, clearWeightDataList } =
    useWeightDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetWeightDataList = useCallback(() => {
    handleResetPage();
    clearWeightDataList({ key: listKey });
  }, [handleResetPage, clearWeightDataList, listKey]);

  return handleResetWeightDataList;
}

export function useResetSharedWeightDataList() {
  //const { userDataId } = options ?? {};
  const listKey = generateListKey();
  const { resetSharedWeightDataPage: resetPage, clearSharedWeightDataList } =
    useWeightDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetSharedWeightDataList = useCallback(() => {
    handleResetPage();
    clearSharedWeightDataList({ key: listKey });
  }, [handleResetPage, clearSharedWeightDataList, listKey]);

  return handleResetSharedWeightDataList;
}
