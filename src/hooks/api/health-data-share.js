import { useCallback, useEffect } from 'react';

import { HealthDataShareService } from 'src/api/app/health';

import { usePager } from '../page';
import { useHealthDataShareStore } from 'src/stores/health-data-share';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListHealthDataShares(options = null) {
  const { sharedById } = options ?? {};
  const listKey = generateListKey({ sharedById });
  const {
    healthDataShares: _healthDataShares,
    healthDataSharePage: __page,
    setHealthDataSharePage: _setPage,
    healthDataShareEndPaging: __endPaging,
    setHealthDataShareEndPaging: _setEndPaging,
    resetHealthDataSharePage: resetPage,
    listHealthDataShares,
  } = useHealthDataShareStore((state) => state);
  const { error, setError } = useItem();
  const healthDataShares = _healthDataShares
    ? _healthDataShares[listKey]
    : _healthDataShares;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!healthDataShares?.length) {
      (async () => {
        await handleListHealthDataShares({
          page,
          sharedById,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [healthDataShares?.length]);

  useEffect(() => {
    if (healthDataShares?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListHealthDataShares({
          page,
          sharedById,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListHealthDataShares = useCallback(
    async ({ page, sharedById }) => {
      try {
        const response = await HealthDataShareService.listHealthDataShares({
          page,
          sharedById,
        });
        const data = await response.json();
        if (response.ok) {
          const healthDataShares = data.data;
          if (healthDataShares?.length || page === 1) {
            listHealthDataShares({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listHealthDataShares,
      setError,
    ]
  );

  return {
    handleListHealthDataShares,
    healthDataShares,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetHealthDataShare({
  healthDataShare: _healthDataShare = null,
  ignoreLoadOnMount = false,
}) {
  const { healthDataShare, setHealthDataShare } = useHealthDataShareStore(
    (state) => state
  );
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetHealthDataShare(_healthDataShare);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetHealthDataShare = useCallback(
    async (healthDataShare = null) => {
      try {
        const response = await HealthDataShareService.getHealthDataShare(
          healthDataShare
        );
        const data = await response.json();
        if (response.ok) {
          const healthDataShare = data.data;
          setHealthDataShare(healthDataShare);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setHealthDataShare, setError]
  );

  return {
    handleGetHealthDataShare,
    error,
    setError,
    processing,
    setProcessing,
    healthDataShare,
  };
}

export function useGetHealthDataSharePublic({
  healthDataShare: _healthDataShare = null,
  ignoreLoadOnMount = false,
}) {
  const {
    healthDataSharePublic,
    setHealthDataSharePublic,
    healthDataSharePublicRetrieved,
    setHealthDataSharePublicRetrieved,
  } = useHealthDataShareStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetHealthDataSharePublic(_healthDataShare);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetHealthDataSharePublic = useCallback(
    async (healthDataShare = null) => {
      try {
        const response = await HealthDataShareService.getHealthDataSharePublic(
          healthDataShare
        );
        const data = await response.json();
        if (response.ok) {
          const healthDataSharePublic = data.data;
          setHealthDataSharePublic(healthDataSharePublic);
          setHealthDataSharePublicRetrieved(true);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setHealthDataSharePublic, setHealthDataSharePublicRetrieved, setError]
  );

  return {
    handleGetHealthDataSharePublic,
    error,
    setError,
    processing,
    setProcessing,
    healthDataSharePublic,
    healthDataSharePublicRetrieved,
    setHealthDataSharePublicRetrieved,
  };
}

export function useFindHealthDataShare({
  healthDataShare: _healthDataShare = null,
  ignoreLoadOnMount = false,
  sharedById,
}) {
  const listKey = generateListKey({ sharedById });
  const {
    healthDataShares: _healthDataShares,
    setHealthDataShare,
    healthDataShare,
  } = useHealthDataShareStore((state) => state);
  const healthDataShares = _healthDataShares
    ? _healthDataShares[listKey]
    : _healthDataShares;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetHealthDataShare } = useGetHealthDataShare({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindHealthDataShare(_healthDataShare);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindHealthDataShare = useCallback(
    async (healthDataShare = null) => {
      try {
        if (healthDataShares?.length) {
          setHealthDataShare(
            healthDataShares.find((item) => item.id === healthDataShare?.id)
          );
        } else {
          await handleGetHealthDataShare(healthDataShare);
        }
      } catch (error) {
        setError(error);
      }
    },
    [healthDataShares, setHealthDataShare, handleGetHealthDataShare, setError]
  );

  return {
    handleFindHealthDataShare,
    error,
    setError,
    processing,
    setProcessing,
    healthDataShare,
  };
}

export function useCancelHealthDataShare(options = null) {
  const { sharedById } = options ?? {};
  const listKey = generateListKey({ sharedById });
  const {
    item: healthDataShare,
    setItem: setHealthDataShare,
    itemUpdated: healthDataShareUpdated,
    setItemUpdated: setHealthDataShareUpdated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { updateHealthDataShare } = useHealthDataShareStore((state) => state);

  const handleCancelHealthDataShare = useCallback(
    async (healthDataShare = null) => {
      try {
        setProcessing(true);
        const response = await HealthDataShareService.cancelHealthDataShare(
          healthDataShare
        );
        const data = await response.json();
        if (response.ok) {
          const healthDataShare = data.data;
          setHealthDataShare(healthDataShare);
          setHealthDataShareUpdated(true);
          updateHealthDataShare({ key: listKey, item: healthDataShare });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setHealthDataShare,
      setHealthDataShareUpdated,
      updateHealthDataShare,
      listKey,
      setError,
    ]
  );

  return {
    handleCancelHealthDataShare,
    error,
    setError,
    processing,
    setProcessing,
    healthDataShare,
    setHealthDataShare,
    healthDataShareUpdated,
    setHealthDataShareUpdated,
  };
}

export function useAllowHealthDataShare(options = null) {
  const { sharedById } = options ?? {};
  const listKey = generateListKey({ sharedById });
  const {
    item: healthDataShare,
    setItem: setHealthDataShare,
    itemUpdated: healthDataShareUpdated,
    setItemUpdated: setHealthDataShareUpdated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { updateHealthDataShare } = useHealthDataShareStore((state) => state);

  const handleAllowHealthDataShare = useCallback(
    async (healthDataShare = null) => {
      try {
        setProcessing(true);
        const response = await HealthDataShareService.allowHealthDataShare(
          healthDataShare
        );
        const data = await response.json();
        if (response.ok) {
          const healthDataShare = data.data;
          setHealthDataShare(healthDataShare);
          setHealthDataShareUpdated(true);
          updateHealthDataShare({ key: listKey, item: healthDataShare });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setHealthDataShare,
      setHealthDataShareUpdated,
      updateHealthDataShare,
      listKey,
      setError,
    ]
  );

  return {
    handleAllowHealthDataShare,
    error,
    setError,
    processing,
    setProcessing,
    healthDataShare,
    setHealthDataShare,
    healthDataShareUpdated,
    setHealthDataShareUpdated,
  };
}

export function useRemoveHealthDataShare(options = null) {
  const { sharedById } = options ?? {};
  const listKey = generateListKey({ sharedById });
  const {
    item: healthDataShare,
    setItem: setHealthDataShare,
    itemRemoved: healthDataShareRemoved,
    setItemRemoved: setHealthDataShareRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeHealthDataShare } = useHealthDataShareStore((state) => state);

  const handleRemoveHealthDataShare = useCallback(
    async (healthDataShare = null) => {
      try {
        setProcessing(true);
        const response = await HealthDataShareService.removeHealthDataShare(
          healthDataShare
        );
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setHealthDataShare(healthDataShare);
            setHealthDataShareRemoved(true);
            removeHealthDataShare({
              key: listKey,
              item: healthDataShare,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setHealthDataShare,
      setHealthDataShareRemoved,
      removeHealthDataShare,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveHealthDataShare,
    error,
    setError,
    processing,
    setProcessing,
    healthDataShare,
    setHealthDataShare,
    healthDataShareRemoved,
    setHealthDataShareRemoved,
  };
}

export function useResetHealthDataShares(options = null) {
  const { sharedById } = options ?? {};
  const listKey = generateListKey({ sharedById });
  const { resetHealthDataSharePage: resetPage, clearHealthDataShares } =
    useHealthDataShareStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetHealthDataShares = useCallback(() => {
    handleResetPage();
    clearHealthDataShares({ key: listKey });
  }, [handleResetPage, clearHealthDataShares, listKey]);

  return handleResetHealthDataShares;
}
