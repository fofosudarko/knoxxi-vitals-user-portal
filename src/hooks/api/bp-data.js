import { useCallback, useEffect } from 'react';

import { BPDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useBPDataStore } from 'src/stores/bp-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListBPDataList(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    bpDataList: _bpDataList,
    bpDataPage: __page,
    setBPDataPage: _setPage,
    bpDataEndPaging: __endPaging,
    setBPDataEndPaging: _setEndPaging,
    resetBPDataPage: resetPage,
    listBPDataList,
  } = useBPDataStore((state) => state);
  const { error, setError } = useItem();
  const bpDataList = _bpDataList ? _bpDataList[listKey] : _bpDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!bpDataList?.length) {
      (async () => {
        await handleListBPDataList({
          userDataId,
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [bpDataList?.length]);

  useEffect(() => {
    if (bpDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListBPDataList({
          userDataId,
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListBPDataList = useCallback(
    async ({ page, userDataId }) => {
      try {
        const response = await BPDataService.listBPDataList({
          page,
          userDataId,
        });
        const data = await response.json();
        if (response.ok) {
          const bpDataList = data.data;
          if (bpDataList?.length || page === 1) {
            listBPDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listBPDataList,
      setError,
    ]
  );

  return {
    handleListBPDataList,
    bpDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useListSharedBPDataList(options = null) {
  const { healthDataSharePublic } = options ?? {};
  const listKey = generateListKey();
  const {
    sharedBpDataList: _sharedBpDataList,
    sharedBpDataPage: __page,
    setSharedBPDataPage: _setPage,
    sharedBpDataEndPaging: __endPaging,
    setSharedBPDataEndPaging: _setEndPaging,
    resetSharedBPDataPage: resetPage,
    listSharedBPDataList,
  } = useBPDataStore((state) => state);
  const { error, setError } = useItem();
  const sharedBpDataList = _sharedBpDataList
    ? _sharedBpDataList[listKey]
    : _sharedBpDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!sharedBpDataList?.length) {
      (async () => {
        await handleListSharedBPDataList({
          healthDataSharePublic,
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sharedBpDataList?.length]);

  useEffect(() => {
    if (sharedBpDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListSharedBPDataList({
          healthDataSharePublic,
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListSharedBPDataList = useCallback(
    async ({ page, healthDataSharePublic }) => {
      try {
        const response = await BPDataService.listSharedBPDataList({
          page,
          healthDataSharePublic,
        });
        const data = await response.json();
        if (response.ok) {
          const sharedBpDataList = data.data;
          if (sharedBpDataList?.length || page === 1) {
            listSharedBPDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listSharedBPDataList,
      setError,
    ]
  );

  return {
    handleListSharedBPDataList,
    sharedBpDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetBPData({
  bpData: _bpData = null,
  ignoreLoadOnMount = false,
}) {
  const { bpData, setBPData } = useBPDataStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetBPData(_bpData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetBPData = useCallback(
    async (bpData = null) => {
      try {
        const response = await BPDataService.getBPData(bpData);
        const data = await response.json();
        if (response.ok) {
          const bpData = data.data;
          setBPData(bpData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setBPData, setError]
  );

  return {
    handleGetBPData,
    error,
    setError,
    processing,
    setProcessing,
    bpData,
  };
}

export function useFindBPData({
  bpData: _bpData = null,
  ignoreLoadOnMount = false,
  userDataId,
}) {
  const listKey = generateListKey({ userDataId });
  const {
    bpDataList: _bpDataList,
    setBPData,
    bpData,
  } = useBPDataStore((state) => state);
  const bpDataList = _bpDataList ? _bpDataList[listKey] : _bpDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetBPData } = useGetBPData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindBPData(_bpData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindBPData = useCallback(
    async (bpData = null) => {
      try {
        if (bpDataList?.length) {
          setBPData(bpDataList.find((item) => item.id === bpData?.id));
        } else {
          await handleGetBPData(bpData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [bpDataList, setBPData, handleGetBPData, setError]
  );

  return {
    handleFindBPData,
    error,
    setError,
    processing,
    setProcessing,
    bpData,
  };
}

export function useCreateBPData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: bpData,
    setItem: setBPData,
    itemCreated: bpDataCreated,
    setItemCreated: setBPDataCreated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { addBPData } = useBPDataStore((state) => state);

  const handleCreateBPData = useCallback(
    async (body = null) => {
      try {
        setProcessing(true);
        const response = await BPDataService.createBPData(body);
        const data = await response.json();
        if (response.ok) {
          const newBPData = data.data;
          setBPData(newBPData);
          setBPDataCreated(true);
          addBPData({ key: listKey, item: newBPData });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [setProcessing, setBPData, setBPDataCreated, addBPData, listKey, setError]
  );

  return {
    handleCreateBPData,
    error,
    setError,
    processing,
    setProcessing,
    bpData,
    setBPData,
    bpDataCreated,
    setBPDataCreated,
  };
}

export function useRemoveBPData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: bpData,
    setItem: setBPData,
    itemRemoved: bpDataRemoved,
    setItemRemoved: setBPDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeBPData } = useBPDataStore((state) => state);

  const handleRemoveBPData = useCallback(
    async (bpData = null) => {
      try {
        setProcessing(true);
        const response = await BPDataService.removeBPData(bpData);
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setBPData(bpData);
            setBPDataRemoved(true);
            removeBPData({ key: listKey, item: bpData });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setBPData,
      setBPDataRemoved,
      removeBPData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveBPData,
    error,
    setError,
    processing,
    setProcessing,
    bpData,
    setBPData,
    bpDataRemoved,
    setBPDataRemoved,
  };
}

export function useResetBPDataList(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const { resetBPDataPage: resetPage, clearBPDataList } = useBPDataStore(
    (state) => state
  );

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetBPDataList = useCallback(() => {
    handleResetPage();
    clearBPDataList({ key: listKey });
  }, [handleResetPage, clearBPDataList, listKey]);

  return handleResetBPDataList;
}

export function useResetSharedBPDataList() {
  //const { userDataId } = options ?? {};
  const listKey = generateListKey();
  const { resetSharedBPDataPage: resetPage, clearSharedBPDataList } =
    useBPDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetSharedBPDataList = useCallback(() => {
    handleResetPage();
    clearSharedBPDataList({ key: listKey });
  }, [handleResetPage, clearSharedBPDataList, listKey]);

  return handleResetSharedBPDataList;
}
