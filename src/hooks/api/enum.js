import { useCallback, useReducer } from 'react';
import { produce } from 'immer';

import { EnumService } from 'src/api/app/health';

const initialState = {
  dataChannels: null,
  meals: null,
  healthDataShareRecipientTypes: null,
  healthDataShareScopes: null,
  healthDataShareCategories: null,
  additionalLifeRelationships: null,
  error: null,
};

const types = {
  SET_DATA_CHANNEL: 'SET_DATA_CHANNEL',
  SET_MEAL: 'SET_MEAL',
  SET_HEALTH_DATA_SHARE_RECIPIENT_TYPE: 'SET_HEALTH_DATA_SHARE_RECIPIENT_TYPE',
  SET_HEALTH_DATA_SHARE_SCOPE: 'SET_HEALTH_DATA_SHARE_SCOPE',
  SET_HEALTH_DATA_SHARE_CATEGORY: 'SET_HEALTH_DATA_SHARE_CATEGORY',
  SET_ADDITIONAL_LIFE_RELATIONSHIP: 'SET_ADDITIONAL_LIFE_RELATIONSHIP',
  CLEAR_ENUM_STATE: 'CLEAR_ENUM_STATE',
  SET_ERROR: 'SET_ERROR',
};

const reducer = produce((draft, action) => {
  switch (action.type) {
    case types.SET_DATA_CHANNEL:
      draft.dataChannels = action.payload;
      return;
    case types.SET_MEAL:
      draft.meals = action.payload;
      return;
    case types.SET_HEALTH_DATA_SHARE_RECIPIENT_TYPE:
      draft.healthDataShareRecipientTypes = action.payload;
      return;
    case types.SET_HEALTH_DATA_SHARE_SCOPE:
      draft.healthDataShareScopes = action.payload;
      return;
    case types.SET_HEALTH_DATA_SHARE_CATEGORY:
      draft.healthDataShareCategories = action.payload;
      return;
    case types.SET_ADDITIONAL_LIFE_RELATIONSHIP:
      draft.additionalLifeRelationships = action.payload;
      return;
    case types.SET_ERROR:
      draft.error = action.payload;
      return;
    case types.CLEAR_ENUM_STATE:
      return initialState;
    default:
      return draft;
  }
});

export function useListEnums() {
  const [
    {
      error,
      dataChannels,
      meals,
      healthDataShareRecipientTypes,
      healthDataShareScopes,
      healthDataShareCategories,
      additionalLifeRelationships,
    },
    dispatch,
  ] = useReducer(reducer, initialState);

  const setDataChannel = useCallback((dataChannels) => {
    dispatch({
      type: types.SET_DATA_CHANNEL,
      payload: dataChannels,
    });
  }, []);
  const setMeal = useCallback((meals) => {
    dispatch({
      type: types.SET_MEAL,
      payload: meals,
    });
  }, []);
  const setHealthDataShareRecipientType = useCallback(
    (healthDataShareRecipientTypes) => {
      dispatch({
        type: types.SET_HEALTH_DATA_SHARE_RECIPIENT_TYPE,
        payload: healthDataShareRecipientTypes,
      });
    },
    []
  );
  const setHealthDataShareScope = useCallback((healthDataShareScopes) => {
    dispatch({
      type: types.SET_HEALTH_DATA_SHARE_SCOPE,
      payload: healthDataShareScopes,
    });
  }, []);
  const setHealthDataShareCategory = useCallback(
    (healthDataShareCategories) => {
      dispatch({
        type: types.SET_HEALTH_DATA_SHARE_CATEGORY,
        payload: healthDataShareCategories,
      });
    },
    []
  );
  const setAdditionalLifeRelationship = useCallback(
    (additionalLifeRelationships) => {
      dispatch({
        type: types.SET_ADDITIONAL_LIFE_RELATIONSHIP,
        payload: additionalLifeRelationships,
      });
    },
    []
  );
  const setError = useCallback((error) => {
    dispatch({ type: types.SET_ERROR, payload: error });
  }, []);

  const handleListDataChannel = useCallback(async () => {
    try {
      const response = await EnumService.listDataChannel();
      const data = await response.json();
      if (response.ok) {
        setDataChannel(data.data);
      } else {
        setError(data.error);
      }
    } catch (error) {
      setError(error);
    }
  }, [setError, setDataChannel]);
  const handleListMeal = useCallback(async () => {
    try {
      const response = await EnumService.listMeal();
      const data = await response.json();
      if (response.ok) {
        setMeal(data.data);
      } else {
        setError(data.error);
      }
    } catch (error) {
      setError(error);
    }
  }, [setError, setMeal]);
  const handleListHealthDataShareRecipientType = useCallback(async () => {
    try {
      const response = await EnumService.listHealthDataShareRecipientType();
      const data = await response.json();
      if (response.ok) {
        setHealthDataShareRecipientType(data.data);
      } else {
        setError(data.error);
      }
    } catch (error) {
      setError(error);
    }
  }, [setError, setHealthDataShareRecipientType]);
  const handleListHealthDataShareScope = useCallback(async () => {
    try {
      const response = await EnumService.listHealthDataShareScope();
      const data = await response.json();
      if (response.ok) {
        setHealthDataShareScope(data.data);
      } else {
        setError(data.error);
      }
    } catch (error) {
      setError(error);
    }
  }, [setError, setHealthDataShareScope]);
  const handleListHealthDataShareCategory = useCallback(async () => {
    try {
      const response = await EnumService.listHealthDataShareCategory();
      const data = await response.json();
      if (response.ok) {
        setHealthDataShareCategory(data.data);
      } else {
        setError(data.error);
      }
    } catch (error) {
      setError(error);
    }
  }, [setError, setHealthDataShareCategory]);
  const handleListAdditionalLifeRelationship = useCallback(async () => {
    try {
      const response = await EnumService.listAdditionalLifeRelationship();
      const data = await response.json();
      if (response.ok) {
        setAdditionalLifeRelationship(data.data);
      } else {
        setError(data.error);
      }
    } catch (error) {
      setError(error);
    }
  }, [setError, setAdditionalLifeRelationship]);

  return {
    handleListDataChannel,
    dataChannels,
    setDataChannel,
    handleListMeal,
    meals,
    setMeal,
    handleListHealthDataShareRecipientType,
    healthDataShareRecipientTypes,
    setHealthDataShareRecipientType,
    handleListHealthDataShareScope,
    healthDataShareScopes,
    setHealthDataShareScope,
    handleListHealthDataShareCategory,
    healthDataShareCategories,
    setHealthDataShareCategory,
    handleListAdditionalLifeRelationship,
    additionalLifeRelationships,
    setAdditionalLifeRelationship,
    error,
    setError,
  };
}
