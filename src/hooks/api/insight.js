import { useCallback, useEffect } from 'react';

import { InsightService } from 'src/api/app/health';

import { useInsightStore } from 'src/stores/insight';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListBPDataTimedAverages({
  userId,
  period,
  createdOn,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey({
    userId,
    period,
    createdOn,
  });
  const { bpDataTimedAverages: _bpDataTimedAverages, setBPDataTimedAverages } =
    useInsightStore((state) => state);
  const bpDataTimedAverages = _bpDataTimedAverages
    ? _bpDataTimedAverages[listKey]
    : _bpDataTimedAverages;
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount && !bpDataTimedAverages?.length) {
      (async () => {
        await handleListBPDataTimedAverages({
          userId,
          period,
          createdOn,
        });
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [bpDataTimedAverages?.length, createdOn]);

  const handleListBPDataTimedAverages = useCallback(
    async ({ userId, period, createdOn }) => {
      try {
        const response = await InsightService.listBPDataTimedAverages({
          userId,
          period,
          createdOn,
        });
        const data = await response.json();
        if (response.ok) {
          const bpDataTimedAverages = data.data;
          setBPDataTimedAverages({
            key: listKey,
            item: bpDataTimedAverages,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setBPDataTimedAverages, listKey, setError]
  );

  return {
    handleListBPDataTimedAverages,
    error,
    setError,
    processing,
    setProcessing,
    bpDataTimedAverages,
  };
}

export function useListBPDataCumulativeAverages({
  userId,
  period,
  createdOn,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey({
    userId,
    period,
    createdOn,
  });
  const {
    bpDataCumulativeAverages: _bpDataCumulativeAverages,
    setBPDataCumulativeAverages,
  } = useInsightStore((state) => state);
  const bpDataCumulativeAverages = _bpDataCumulativeAverages
    ? _bpDataCumulativeAverages[listKey]
    : _bpDataCumulativeAverages;
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount && !bpDataCumulativeAverages?.length) {
      (async () => {
        await handleListBPDataCumulativeAverages({
          userId,
          period,
          createdOn,
        });
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [bpDataCumulativeAverages?.length, createdOn]);

  const handleListBPDataCumulativeAverages = useCallback(
    async ({ userId, period, createdOn }) => {
      try {
        const response = await InsightService.listBPDataCumulativeAverages({
          userId,
          period,
          createdOn,
        });
        const data = await response.json();
        if (response.ok) {
          const bpDataCumulativeAverages = data.data;
          setBPDataCumulativeAverages({
            key: listKey,
            item: bpDataCumulativeAverages,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setBPDataCumulativeAverages, listKey, setError]
  );

  return {
    handleListBPDataCumulativeAverages,
    error,
    setError,
    processing,
    setProcessing,
    bpDataCumulativeAverages,
  };
}

export function useListGlucoseDataTimedAverages({
  userId,
  period,
  createdOn,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey({
    userId,
    period,
    createdOn,
  });
  const {
    glucoseDataTimedAverages: _glucoseDataTimedAverages,
    setGlucoseDataTimedAverages,
  } = useInsightStore((state) => state);
  const glucoseDataTimedAverages = _glucoseDataTimedAverages
    ? _glucoseDataTimedAverages[listKey]
    : _glucoseDataTimedAverages;
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount && !glucoseDataTimedAverages?.length) {
      (async () => {
        await handleListGlucoseDataTimedAverages({
          userId,
          period,
          createdOn,
        });
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [glucoseDataTimedAverages?.length, createdOn]);

  const handleListGlucoseDataTimedAverages = useCallback(
    async ({ userId, period, createdOn }) => {
      try {
        const response = await InsightService.listGlucoseDataTimedAverages({
          userId,
          period,
          createdOn,
        });
        const data = await response.json();
        if (response.ok) {
          const glucoseDataTimedAverages = data.data;
          setGlucoseDataTimedAverages({
            key: listKey,
            item: glucoseDataTimedAverages,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setGlucoseDataTimedAverages, listKey, setError]
  );

  return {
    handleListGlucoseDataTimedAverages,
    error,
    setError,
    processing,
    setProcessing,
    glucoseDataTimedAverages,
  };
}

export function useListGlucoseDataCumulativeAverages({
  userId,
  period,
  createdOn,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey({
    userId,
    period,
    createdOn,
  });
  const {
    glucoseDataCumulativeAverages: _glucoseDataCumulativeAverages,
    setGlucoseDataCumulativeAverages,
  } = useInsightStore((state) => state);
  const glucoseDataCumulativeAverages = _glucoseDataCumulativeAverages
    ? _glucoseDataCumulativeAverages[listKey]
    : _glucoseDataCumulativeAverages;
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount && !glucoseDataCumulativeAverages?.length) {
      (async () => {
        await handleListGlucoseDataCumulativeAverages({
          userId,
          period,
          createdOn,
        });
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [glucoseDataCumulativeAverages?.length, createdOn]);

  const handleListGlucoseDataCumulativeAverages = useCallback(
    async ({ userId, period, createdOn }) => {
      try {
        const response = await InsightService.listGlucoseDataCumulativeAverages(
          {
            userId,
            period,
            createdOn,
          }
        );
        const data = await response.json();
        if (response.ok) {
          const glucoseDataCumulativeAverages = data.data;
          setGlucoseDataCumulativeAverages({
            key: listKey,
            item: glucoseDataCumulativeAverages,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setGlucoseDataCumulativeAverages, listKey, setError]
  );

  return {
    handleListGlucoseDataCumulativeAverages,
    error,
    setError,
    processing,
    setProcessing,
    glucoseDataCumulativeAverages,
  };
}

export function useListTemperatureDataTimedAverages({
  userId,
  period,
  createdOn,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey({
    userId,
    period,
    createdOn,
  });
  const {
    temperatureDataTimedAverages: _temperatureDataTimedAverages,
    setTemperatureDataTimedAverages,
  } = useInsightStore((state) => state);
  const temperatureDataTimedAverages = _temperatureDataTimedAverages
    ? _temperatureDataTimedAverages[listKey]
    : _temperatureDataTimedAverages;
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount && !temperatureDataTimedAverages?.length) {
      (async () => {
        await handleListTemperatureDataTimedAverages({
          userId,
          period,
          createdOn,
        });
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [temperatureDataTimedAverages?.length, createdOn]);

  const handleListTemperatureDataTimedAverages = useCallback(
    async ({ userId, period, createdOn }) => {
      try {
        const response = await InsightService.listTemperatureDataTimedAverages({
          userId,
          period,
          createdOn,
        });
        const data = await response.json();
        if (response.ok) {
          const temperatureDataTimedAverages = data.data;
          setTemperatureDataTimedAverages({
            key: listKey,
            item: temperatureDataTimedAverages,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setTemperatureDataTimedAverages, listKey, setError]
  );

  return {
    handleListTemperatureDataTimedAverages,
    error,
    setError,
    processing,
    setProcessing,
    temperatureDataTimedAverages,
  };
}

export function useListTemperatureDataCumulativeAverages({
  userId,
  period,
  createdOn,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey({
    userId,
    period,
    createdOn,
  });
  const {
    temperatureDataCumulativeAverages: _temperatureDataCumulativeAverages,
    setTemperatureDataCumulativeAverages,
  } = useInsightStore((state) => state);
  const temperatureDataCumulativeAverages = _temperatureDataCumulativeAverages
    ? _temperatureDataCumulativeAverages[listKey]
    : _temperatureDataCumulativeAverages;
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount && !temperatureDataCumulativeAverages?.length) {
      (async () => {
        await handleListTemperatureDataCumulativeAverages({
          userId,
          period,
          createdOn,
        });
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [temperatureDataCumulativeAverages?.length, createdOn]);

  const handleListTemperatureDataCumulativeAverages = useCallback(
    async ({ userId, period, createdOn }) => {
      try {
        const response =
          await InsightService.listTemperatureDataCumulativeAverages({
            userId,
            period,
            createdOn,
          });
        const data = await response.json();
        if (response.ok) {
          const temperatureDataCumulativeAverages = data.data;
          setTemperatureDataCumulativeAverages({
            key: listKey,
            item: temperatureDataCumulativeAverages,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setTemperatureDataCumulativeAverages, listKey, setError]
  );

  return {
    handleListTemperatureDataCumulativeAverages,
    error,
    setError,
    processing,
    setProcessing,
    temperatureDataCumulativeAverages,
  };
}

export function useListOxygenDataTimedAverages({
  userId,
  period,
  createdOn,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey({
    userId,
    period,
    createdOn,
  });
  const {
    oxygenDataTimedAverages: _oxygenDataTimedAverages,
    setOxygenDataTimedAverages,
  } = useInsightStore((state) => state);
  const oxygenDataTimedAverages = _oxygenDataTimedAverages
    ? _oxygenDataTimedAverages[listKey]
    : _oxygenDataTimedAverages;
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount && !oxygenDataTimedAverages?.length) {
      (async () => {
        await handleListOxygenDataTimedAverages({
          userId,
          period,
          createdOn,
        });
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [oxygenDataTimedAverages?.length, createdOn]);

  const handleListOxygenDataTimedAverages = useCallback(
    async ({ userId, period, createdOn }) => {
      try {
        const response = await InsightService.listOxygenDataTimedAverages({
          userId,
          period,
          createdOn,
        });
        const data = await response.json();
        if (response.ok) {
          const oxygenDataTimedAverages = data.data;
          setOxygenDataTimedAverages({
            key: listKey,
            item: oxygenDataTimedAverages,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setOxygenDataTimedAverages, listKey, setError]
  );

  return {
    handleListOxygenDataTimedAverages,
    error,
    setError,
    processing,
    setProcessing,
    oxygenDataTimedAverages,
  };
}

export function useListOxygenDataCumulativeAverages({
  userId,
  period,
  createdOn,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey({
    userId,
    period,
    createdOn,
  });
  const {
    oxygenDataCumulativeAverages: _oxygenDataCumulativeAverages,
    setOxygenDataCumulativeAverages,
  } = useInsightStore((state) => state);
  const oxygenDataCumulativeAverages = _oxygenDataCumulativeAverages
    ? _oxygenDataCumulativeAverages[listKey]
    : _oxygenDataCumulativeAverages;
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount && !oxygenDataCumulativeAverages?.length) {
      (async () => {
        await handleListOxygenDataCumulativeAverages({
          userId,
          period,
          createdOn,
        });
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [oxygenDataCumulativeAverages?.length, createdOn]);

  const handleListOxygenDataCumulativeAverages = useCallback(
    async ({ userId, period, createdOn }) => {
      try {
        const response = await InsightService.listOxygenDataCumulativeAverages({
          userId,
          period,
          createdOn,
        });
        const data = await response.json();
        if (response.ok) {
          const oxygenDataCumulativeAverages = data.data;
          setOxygenDataCumulativeAverages({
            key: listKey,
            item: oxygenDataCumulativeAverages,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setOxygenDataCumulativeAverages, listKey, setError]
  );

  return {
    handleListOxygenDataCumulativeAverages,
    error,
    setError,
    processing,
    setProcessing,
    oxygenDataCumulativeAverages,
  };
}

export function useListCholesterolDataTimedAverages({
  userId,
  period,
  createdOn,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey({
    userId,
    period,
    createdOn,
  });
  const {
    cholesterolDataTimedAverages: _cholesterolDataTimedAverages,
    setCholesterolDataTimedAverages,
  } = useInsightStore((state) => state);
  const cholesterolDataTimedAverages = _cholesterolDataTimedAverages
    ? _cholesterolDataTimedAverages[listKey]
    : _cholesterolDataTimedAverages;
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount && !cholesterolDataTimedAverages?.length) {
      (async () => {
        await handleListCholesterolDataTimedAverages({
          userId,
          period,
          createdOn,
        });
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cholesterolDataTimedAverages?.length, createdOn]);

  const handleListCholesterolDataTimedAverages = useCallback(
    async ({ userId, period, createdOn }) => {
      try {
        const response = await InsightService.listCholesterolDataTimedAverages({
          userId,
          period,
          createdOn,
        });
        const data = await response.json();
        if (response.ok) {
          const cholesterolDataTimedAverages = data.data;
          setCholesterolDataTimedAverages({
            key: listKey,
            item: cholesterolDataTimedAverages,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setCholesterolDataTimedAverages, listKey, setError]
  );

  return {
    handleListCholesterolDataTimedAverages,
    error,
    setError,
    processing,
    setProcessing,
    cholesterolDataTimedAverages,
  };
}

export function useListCholesterolDataCumulativeAverages({
  userId,
  period,
  createdOn,
  ignoreLoadOnMount = false,
}) {
  const listKey = generateListKey({
    userId,
    period,
    createdOn,
  });
  const {
    cholesterolDataCumulativeAverages: _cholesterolDataCumulativeAverages,
    setCholesterolDataCumulativeAverages,
  } = useInsightStore((state) => state);
  const cholesterolDataCumulativeAverages = _cholesterolDataCumulativeAverages
    ? _cholesterolDataCumulativeAverages[listKey]
    : _cholesterolDataCumulativeAverages;
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount && !cholesterolDataCumulativeAverages?.length) {
      (async () => {
        await handleListCholesterolDataCumulativeAverages({
          userId,
          period,
          createdOn,
        });
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cholesterolDataCumulativeAverages?.length, createdOn]);

  const handleListCholesterolDataCumulativeAverages = useCallback(
    async ({ userId, period, createdOn }) => {
      try {
        const response =
          await InsightService.listCholesterolDataCumulativeAverages({
            userId,
            period,
            createdOn,
          });
        const data = await response.json();
        if (response.ok) {
          const cholesterolDataCumulativeAverages = data.data;
          setCholesterolDataCumulativeAverages({
            key: listKey,
            item: cholesterolDataCumulativeAverages,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setCholesterolDataCumulativeAverages, listKey, setError]
  );

  return {
    handleListCholesterolDataCumulativeAverages,
    error,
    setError,
    processing,
    setProcessing,
    cholesterolDataCumulativeAverages,
  };
}

export function useResetInsights() {
  const listKey = generateListKey();
  const { clearInsights } = useInsightStore((state) => state);

  const handleResetInsights = useCallback(() => {
    clearInsights({ key: listKey });
  }, [clearInsights, listKey]);

  return handleResetInsights;
}

export function useResetBPDataInsights(options = null) {
  const { userId, period, createdOn } = options ?? {};
  const listKey = generateListKey({ userId, period, createdOn });
  const { clearBPDataTimedAverages, clearBPDataCumulativeAverages } =
    useInsightStore((state) => state);

  const handleResetBPDataInsights = useCallback(() => {
    clearBPDataTimedAverages({ key: listKey });
    clearBPDataCumulativeAverages({ key: listKey });
  }, [clearBPDataCumulativeAverages, clearBPDataTimedAverages, listKey]);

  return handleResetBPDataInsights;
}

export function useResetGlucoseDataInsights(options = null) {
  const { userId, period, createdOn } = options ?? {};
  const listKey = generateListKey({ userId, period, createdOn });
  const { clearGlucoseDataTimedAverages, clearGlucoseDataCumulativeAverages } =
    useInsightStore((state) => state);

  const handleResetGlucoseDataInsights = useCallback(() => {
    clearGlucoseDataTimedAverages({ key: listKey });
    clearGlucoseDataCumulativeAverages({ key: listKey });
  }, [
    clearGlucoseDataCumulativeAverages,
    clearGlucoseDataTimedAverages,
    listKey,
  ]);

  return handleResetGlucoseDataInsights;
}

export function useResetTemperatureDataInsights(options = null) {
  const { userId, period, createdOn } = options ?? {};
  const listKey = generateListKey({ userId, period, createdOn });
  const {
    clearTemperatureDataTimedAverages,
    clearTemperatureDataCumulativeAverages,
  } = useInsightStore((state) => state);

  const handleResetTemperatureDataInsights = useCallback(() => {
    clearTemperatureDataTimedAverages({ key: listKey });
    clearTemperatureDataCumulativeAverages({ key: listKey });
  }, [
    clearTemperatureDataCumulativeAverages,
    clearTemperatureDataTimedAverages,
    listKey,
  ]);

  return handleResetTemperatureDataInsights;
}

export function useResetOxygenDataInsights(options = null) {
  const { userId, period, createdOn } = options ?? {};
  const listKey = generateListKey({ userId, period, createdOn });
  const { clearOxygenDataTimedAverages, clearOxygenDataCumulativeAverages } =
    useInsightStore((state) => state);

  const handleResetOxygenDataInsights = useCallback(() => {
    clearOxygenDataTimedAverages({ key: listKey });
    clearOxygenDataCumulativeAverages({ key: listKey });
  }, [
    clearOxygenDataCumulativeAverages,
    clearOxygenDataTimedAverages,
    listKey,
  ]);

  return handleResetOxygenDataInsights;
}

export function useResetCholesterolDataInsights(options = null) {
  const { userId, period, createdOn } = options ?? {};
  const listKey = generateListKey({ userId, period, createdOn });
  const {
    clearCholesterolDataTimedAverages,
    clearCholesterolDataCumulativeAverages,
  } = useInsightStore((state) => state);

  const handleResetCholesterolDataInsights = useCallback(() => {
    clearCholesterolDataTimedAverages({ key: listKey });
    clearCholesterolDataCumulativeAverages({ key: listKey });
  }, [
    clearCholesterolDataCumulativeAverages,
    clearCholesterolDataTimedAverages,
    listKey,
  ]);

  return handleResetCholesterolDataInsights;
}
