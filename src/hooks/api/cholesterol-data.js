import { useCallback, useEffect } from 'react';

import { CholesterolDataService } from 'src/api/app/health';

import { usePager } from '../page';
import { useCholesterolDataStore } from 'src/stores/cholesterol-data';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListCholesterolDataList(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    cholesterolDataList: _cholesterolDataList,
    cholesterolDataPage: __page,
    setCholesterolDataPage: _setPage,
    cholesterolDataEndPaging: __endPaging,
    setCholesterolDataEndPaging: _setEndPaging,
    resetCholesterolDataPage: resetPage,
    listCholesterolDataList,
  } = useCholesterolDataStore((state) => state);
  const { error, setError } = useItem();
  const cholesterolDataList = _cholesterolDataList
    ? _cholesterolDataList[listKey]
    : _cholesterolDataList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!cholesterolDataList?.length) {
      (async () => {
        await handleListCholesterolDataList({
          page,
          userDataId,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cholesterolDataList?.length]);

  useEffect(() => {
    if (cholesterolDataList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListCholesterolDataList({
          page,
          userDataId,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListCholesterolDataList = useCallback(
    async ({ page, userDataId }) => {
      try {
        const response = await CholesterolDataService.listCholesterolDataList({
          page,
          userDataId,
        });
        const data = await response.json();
        if (response.ok) {
          const cholesterolDataList = data.data;
          if (cholesterolDataList?.length || page === 1) {
            listCholesterolDataList({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listCholesterolDataList,
      setError,
    ]
  );

  return {
    handleListCholesterolDataList,
    cholesterolDataList,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useGetCholesterolData({
  cholesterolData: _cholesterolData = null,
  ignoreLoadOnMount = false,
}) {
  const { cholesterolData, setCholesterolData } = useCholesterolDataStore(
    (state) => state
  );
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetCholesterolData(_cholesterolData);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetCholesterolData = useCallback(
    async (cholesterolData = null) => {
      try {
        const response = await CholesterolDataService.getCholesterolData(
          cholesterolData
        );
        const data = await response.json();
        if (response.ok) {
          const cholesterolData = data.data;
          setCholesterolData(cholesterolData);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setCholesterolData, setError]
  );

  return {
    handleGetCholesterolData,
    error,
    setError,
    processing,
    setProcessing,
    cholesterolData,
  };
}

export function useFindCholesterolData({
  cholesterolData: _cholesterolData = null,
  ignoreLoadOnMount = false,
  userDataId,
}) {
  const listKey = generateListKey({ userDataId });
  const {
    cholesterolDataList: _cholesterolDataList,
    setCholesterolData,
    cholesterolData,
  } = useCholesterolDataStore((state) => state);
  const cholesterolDataList = _cholesterolDataList
    ? _cholesterolDataList[listKey]
    : _cholesterolDataList;
  const { error, setError, processing, setProcessing } = useItem();
  const { handleGetCholesterolData } = useGetCholesterolData({
    ignoreLoadOnMount: true,
  });

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      handleFindCholesterolData(_cholesterolData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFindCholesterolData = useCallback(
    async (cholesterolData = null) => {
      try {
        if (cholesterolDataList?.length) {
          setCholesterolData(
            cholesterolDataList.find((item) => item.id === cholesterolData?.id)
          );
        } else {
          await handleGetCholesterolData(cholesterolData);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      cholesterolDataList,
      setCholesterolData,
      handleGetCholesterolData,
      setError,
    ]
  );

  return {
    handleFindCholesterolData,
    error,
    setError,
    processing,
    setProcessing,
    cholesterolData,
  };
}

export function useCreateCholesterolData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: cholesterolData,
    setItem: setCholesterolData,
    itemCreated: cholesterolDataCreated,
    setItemCreated: setCholesterolDataCreated,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { addCholesterolData } = useCholesterolDataStore((state) => state);

  const handleCreateCholesterolData = useCallback(
    async (body = null) => {
      try {
        setProcessing(true);
        const response = await CholesterolDataService.createCholesterolData(
          body
        );
        const data = await response.json();
        if (response.ok) {
          const newCholesterolData = data.data;
          setCholesterolData(newCholesterolData);
          setCholesterolDataCreated(true);
          addCholesterolData({ key: listKey, item: newCholesterolData });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setCholesterolData,
      setCholesterolDataCreated,
      addCholesterolData,
      listKey,
      setError,
    ]
  );

  return {
    handleCreateCholesterolData,
    error,
    setError,
    processing,
    setProcessing,
    cholesterolData,
    setCholesterolData,
    cholesterolDataCreated,
    setCholesterolDataCreated,
  };
}

export function useRemoveCholesterolData(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const {
    item: cholesterolData,
    setItem: setCholesterolData,
    itemRemoved: cholesterolDataRemoved,
    setItemRemoved: setCholesterolDataRemoved,
    error,
    setError,
    processing,
    setProcessing,
  } = useItem();
  const { removeCholesterolData } = useCholesterolDataStore((state) => state);

  const handleRemoveCholesterolData = useCallback(
    async (cholesterolData = null) => {
      try {
        setProcessing(true);
        const response = await CholesterolDataService.removeCholesterolData(
          cholesterolData
        );
        const data = await response.json();
        if (response.ok) {
          if (data.data) {
            setCholesterolData(cholesterolData);
            setCholesterolDataRemoved(true);
            removeCholesterolData({
              key: listKey,
              item: cholesterolData,
            });
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setProcessing,
      setCholesterolData,
      setCholesterolDataRemoved,
      removeCholesterolData,
      listKey,
      setError,
    ]
  );

  return {
    handleRemoveCholesterolData,
    error,
    setError,
    processing,
    setProcessing,
    cholesterolData,
    setCholesterolData,
    cholesterolDataRemoved,
    setCholesterolDataRemoved,
  };
}

export function useResetCholesterolDataList(options = null) {
  const { userDataId } = options ?? {};
  const listKey = generateListKey({ userDataId });
  const { resetCholesterolDataPage: resetPage, clearCholesterolDataList } =
    useCholesterolDataStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetCholesterolDataList = useCallback(() => {
    handleResetPage();
    clearCholesterolDataList({ key: listKey });
  }, [handleResetPage, clearCholesterolDataList, listKey]);

  return handleResetCholesterolDataList;
}
