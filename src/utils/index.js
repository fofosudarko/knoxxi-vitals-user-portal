import { nanoid } from 'nanoid';
import * as _round from 'lodash.round';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import crypto from 'crypto';

import {
  ZEROES_WITHIN_PHONE_NUMBER_REGEX,
  VALID_PHONE_NUMBER_REGEX,
} from 'src/config';

dayjs.extend(relativeTime);

export function handleNotification(error = null) {
  console.error(error);
  throw error;
}

export function alertWebError(error = null) {
  console.error(error);

  if (isAxiosError(error)) {
    const response = error.response;

    if (response) {
      if (isCKYCError(response.data)) {
        alert(response.data.possibleCause);
      } else {
        alert(
          response.data.error
            ? response.data.error.message
            : 'Oops, Something happened. Please check later.'
        );
      }
    } else {
      alert('Oops, Something happened. Please check later.');
    }
  } else {
    if (isCKYCError(error)) {
      alert(error?.possibleCause);
    } else {
      alert(error?.message);
    }
  }
}

export function readErrorMessage(error = null) {
  let errorMessage;

  console.error(error);

  if (isAxiosError(error)) {
    const response = error.response;

    if (response) {
      if (isCKYCError(response.data)) {
        errorMessage = response.data.possibleCause;
      } else {
        errorMessage = response.data.error
          ? response.data.error.message
          : 'Oops, Something happened. Please check later.';
      }
    } else {
      errorMessage = 'Oops, Something happened. Please check later.';
    }
  } else {
    if (isCKYCError(error)) {
      errorMessage = error?.possibleCause;
    } else if (isRTKQueryError(error)) {
      if (error?.data) {
        if (error?.data.error) {
          errorMessage = error?.data.error.message;
        } else {
          errorMessage = 'Ooops, something bad happened. Please check later.';
        }
      } else if (error?.error) {
        errorMessage = error.error;
      } else {
        errorMessage = 'Ooops, something bad happened. Please check later.';
      }
    } else {
      errorMessage = error?.message;
    }
  }

  return errorMessage;
}

export function isCKYCError(data = null) {
  return data && (data.exception || data.responseCode);
}

export function isRTKQueryError(error = null) {
  return typeof error === 'object' && error && (error?.data || error?.status);
}

export function hasUserSignedUp(UserStatus = null) {
  return UserStatus && ['CONFIRMED'].includes(UserStatus);
}

export function getFolders({ userId = null, rootFolder = null }) {
  if (!userId) {
    userId = '';
  }

  if (!rootFolder) {
    rootFolder = '';
  }

  const userFolder = `${userId}`;
  const bucketFolder = `${rootFolder}/${userId}`;

  return [bucketFolder, userFolder];
}

export function humanizeDate(date = null) {
  let humanizedDate = '';

  if (!date || !(date instanceof Date)) {
    return humanizedDate;
  }

  humanizedDate = new Date(date);

  return humanizedDate.toUTCString();
}

export function humanizeTime(time = null) {
  let humanizedTime = '';

  if (!time) {
    return humanizedTime;
  }

  time = parseFloat(time);
  let quotient,
    i = 0;

  const seconds = 1,
    minutes = 60 * seconds,
    hours = 60 * minutes,
    days = 24 * hours,
    years = 365 * days,
    divisors = [years, days, hours, minutes, seconds],
    unitsFormatMore = ['years', 'days', 'hours', 'minutes', 'seconds'],
    unitsFormatOne = ['year', 'day', 'hour', 'minute', 'second'],
    times = [];

  for (const divisor of divisors) {
    quotient = Math.floor(time / divisor);
    times.push({
      units: quotient <= 1 ? unitsFormatOne[i] : unitsFormatMore[i],
      quotient,
    });
    time = time % divisor;
    i++;
  }

  times.push({ units: 'ms', quotient: _round(time * 1000) });

  if (times.length) {
    humanizedTime = times.reduce((acc, x) => {
      if (x.quotient !== 0) {
        acc.push(`${x.quotient} ${x.units}`);
      }
      return acc;
    }, []);
  }

  return Array.isArray(humanizedTime) ? humanizedTime.join(', ') : '';
}

export function getId() {
  return nanoid();
}

export function removeForwardSlashes(string = null) {
  if (!string) {
    return string;
  }

  return string.replace(/^.+\//g, '');
}

export function convertBytesToNearestForm(value = null) {
  let nearestForm = '';

  if (!value) {
    return nearestForm;
  }

  value = parseInt(value);
  nearestForm = null;
  let quotient,
    i = 0;

  const bytes = 1,
    kilobytes = 1024 * bytes,
    megabytes = 1024 * kilobytes,
    gigabytes = 1024 * megabytes,
    terabytes = 1024 * gigabytes,
    divisors = [terabytes, gigabytes, megabytes, kilobytes, bytes],
    unitsFormat = ['TB', 'GB', 'MB', 'KB', 'B'];

  for (const divisor of divisors) {
    quotient = _round(value / divisor, 1);

    if (quotient >= 1) {
      nearestForm = {};
      nearestForm.units = unitsFormat[i];
      nearestForm.quotient = quotient;
      break;
    }

    i++;
  }

  return nearestForm ? `${nearestForm.quotient} ${nearestForm.units}` : '';
}

export function isVideoCaptureSupported() {
  if (!navigator) {
    return false;
  }

  return 'mediaDevices' in navigator;
}

export function truncateString({
  limit = 32,
  string = null,
  position = 'middle',
}) {
  if (!string) {
    return '';
  }

  if (string.length <= limit) {
    return string;
  }

  const parts = 2,
    quotient = Math.floor(limit / parts);

  if (position === 'middle') {
    string =
      string.substring(0, quotient) +
      '...' +
      string.substring(string.length - quotient, string.length);
  } else if (position === 'end') {
    string = string.substring(0, string.length - quotient) + '...';
  } else if (position === 'start') {
    string = '...' + string.substring(string.length - quotient);
  }

  return string;
}

export function convertDatePropertiesInResponseToISOString({
  response = null,
  properties,
}) {
  if (!response) {
    return response;
  }

  if (!properties || !Array.isArray(properties)) {
    return response;
  }

  for (const property of properties) {
    if ([property] in response) {
      if (response[property] instanceof Date) {
        response[property] = response[property].toISOString();
      }
    }
  }

  return response;
}

export function transformS3ObjectResponse(options = {}) {
  return options;
}

export function convertValueToDurationUnit({
  durationUnit = null,
  value = null,
}) {
  let conversion = -1;

  if (!durationUnit || !value) {
    return conversion;
  }

  switch (durationUnit) {
    case 'SECONDS':
      conversion = value;
      break;
    case 'MINUTES':
      conversion = value * 60;
      break;
    case 'HOURS':
      conversion = value * 60 * 60;
      break;
    case 'DAYS':
      conversion = value * 24 * 60 * 60;
      break;
    case 'WEEKS':
      conversion = value * 7 * 24 * 60 * 60;
      break;
    case 'MONTHS':
      conversion = value * 30 * 24 * 60 * 60;
      break;
    case 'YEARS':
      conversion = value * 12 * 30 * 24 * 60 * 60;
      break;
  }

  return conversion;
}

export function convertDurationUnitToValue({
  durationUnit = null,
  value = null,
}) {
  let conversion = -1;

  if (!durationUnit || !value) {
    return conversion;
  }

  switch (durationUnit) {
    case 'SECONDS':
      conversion = value;
      break;
    case 'MINUTES':
      conversion = value / 60;
      break;
    case 'HOURS':
      conversion = value / (60 * 60);
      break;
    case 'DAYS':
      conversion = value / (24 * 60 * 60);
      break;
    case 'WEEKS':
      conversion = value / (7 * 24 * 60 * 60);
      break;
    case 'MONTHS':
      conversion = value / (30 * 24 * 60 * 60);
      break;
    case 'YEARS':
      conversion = value / (12 * 30 * 24 * 60 * 60);
      break;
  }

  return conversion;
}

export function getEndTime({ startTime = null, duration = null }) {
  let endTime = null;

  if (!startTime || !duration) {
    return endTime;
  }

  startTime = new Date(startTime).getTime() * 0.001;
  endTime = startTime + duration;

  return new Date(endTime * 1000);
}

export function convertDateToUTCString(date = null) {
  let conversion = '';

  if (date instanceof Date) {
    conversion = date.toUTCString();
  } else if (typeof date === 'string' || typeof date === 'number') {
    conversion = new Date(date).toUTCString();
  }

  return conversion;
}

export function convertDateToISOString(date = null, removeZ = true) {
  let conversion = '';

  if (date instanceof Date) {
    conversion = date.toISOString();
  } else if (typeof date === 'string' || typeof date === 'number') {
    conversion = new Date(date).toISOString();
  }

  return removeZ ? conversion.replace('Z', '') : conversion;
}

export function convertToLowerCase(string = null) {
  return string && typeof string === 'string' ? string.toLocaleLowerCase() : '';
}

export function getRelativeTime(date = null) {
  return dayjs(date).fromNow();
}

export function getFormattedDate(date = null) {
  return dayjs(date).format('YYYY-MM-DD');
}

export function isAxiosError(error = null) {
  return !!error?.response;
}

export function removeZeroesWithinPhoneNumber(phoneNumber = '') {
  if (!phoneNumber || typeof phoneNumber !== 'string') {
    return '';
  }

  const matches = phoneNumber.match(ZEROES_WITHIN_PHONE_NUMBER_REGEX);

  if (matches?.length === 4) {
    phoneNumber = matches[1] + matches[3];
  }

  return phoneNumber;
}

export function isValidPhoneNumber(phoneNumber = null) {
  if (!phoneNumber) {
    return false;
  }

  return phoneNumber.match(VALID_PHONE_NUMBER_REGEX);
}

export function prependPlusToPhoneNumber(phoneNumber = null) {
  const plus = '+';

  if (!phoneNumber) {
    return phoneNumber;
  }

  if (!phoneNumber.match(/^\+/)) {
    phoneNumber = plus + phoneNumber;
  }

  return phoneNumber;
}

export function getTimestamp(date = null) {
  return date ? new Date(date).getTime() : null;
}

export function isDateBefore({ firstDate = null, secondDate = null }) {
  if (!(firstDate instanceof Date || secondDate instanceof Date)) {
    return false;
  }

  return firstDate.getTime() < secondDate.getTime();
}

export function isDateAfter({ firstDate = null, secondDate = null }) {
  if (!(firstDate instanceof Date || secondDate instanceof Date)) {
    return false;
  }

  return firstDate.getTime() > secondDate.getTime();
}

export function getDurationContext(context = null) {
  if (!context) {
    return {};
  }

  const {
    startedOn = null,
    duration = null,
    endedOn: _endedOn = null,
    expiredOn = null,
  } = context;

  const isInProgress = isDateAfter({
    firstDate: new Date(),
    secondDate: new Date(startedOn),
  });
  const endedOn = _endedOn
    ? _endedOn
    : getEndTime({ startTime: startedOn, duration });
  const hasStarted = startedOn
      ? isDateAfter({
          firstDate: new Date(),
          secondDate: new Date(startedOn),
        })
      : false,
    hasEnded = endedOn
      ? isDateAfter({
          firstDate: new Date(),
          secondDate: new Date(endedOn),
        })
      : null,
    hasExpired = expiredOn
      ? isDateAfter({
          firstDate: new Date(),
          secondDate: new Date(expiredOn),
        })
      : false;

  return {
    isInProgress,
    endedOn,
    startedOn,
    hasStarted,
    hasEnded,
    hasExpired,
    expiredOn,
  };
}

export function allowCors(fn) {
  return async (req, res) => {
    if (req.method === 'OPTIONS') {
      res.status(200).end();
      return;
    }

    return await fn(req, res);
  };
}

export function generateFullname({
  fullname,
  firstname,
  middlename,
  lastname,
}) {
  let newFullname = '';
  let oldFirstname, oldMiddlename, oldLastname;

  if (fullname) {
    const splittedNames = fullname.split(' ');
    if (splittedNames?.length === 2) {
      [oldFirstname, oldLastname] = splittedNames;
    } else {
      [oldFirstname, oldMiddlename, oldLastname] = splittedNames;
    }
  }

  if (firstname && typeof firstname === 'string') {
    newFullname = firstname;
  } else if (firstname === null || firstname === undefined) {
    if (oldFirstname) {
      newFullname = oldFirstname;
    }
  }

  if (middlename && typeof middlename === 'string') {
    newFullname = `${newFullname} ${middlename}`;
  } else if (middlename === null || middlename === undefined) {
    if (oldMiddlename) {
      newFullname = `${newFullname} ${oldMiddlename}`;
    }
  }

  if (lastname && typeof lastname === 'string') {
    newFullname = `${newFullname} ${lastname}`;
  } else if (lastname === null || lastname === undefined) {
    if (oldLastname) {
      newFullname = `${newFullname} ${oldLastname}`;
    }
  }

  return newFullname.length === 0 ? null : newFullname;
}

export function convertToDate(dateString) {
  return dateString === undefined || typeof dateString !== 'string'
    ? null
    : new Date(dateString);
}

export function getAccountResetActivationCodeKeys({ channel, receiver }) {
  const app = process.env.APP_NAME || '';
  const tokenKey = `${app}-account-reset-activation-code-${channel}-${receiver}-token`,
    secretKey = `${app}-account-reset-activation-code-${channel}-${receiver}-secret`;
  return { tokenKey, secretKey };
}

export function getAccountActivationCodeKeys({ channel, receiver }) {
  const app = process.env.APP_NAME || '';
  const tokenKey = `${app}-account-activation-code-${channel}-${receiver}-token`,
    secretKey = `${app}-account-activation-code-${channel}-${receiver}-secret`;
  return { tokenKey, secretKey };
}

export function isTrue(value = null) {
  return (typeof value !== 'string' ? value?.toString() : value) === 'true';
}

export function convertToNumber(value = null, isFloat = false) {
  const result = isFloat ? Number.parseFloat(value) : Number.parseInt(value);
  return !isNaN(result) ? result : undefined;
}

export function isDev() {
  return process.env.NODE_ENV === 'development';
}

export function round(number, precision = 3) {
  return _round(number, precision);
}

export function humanizeString({
  string: _string = null,
  toUpperCase = false,
  toLowerCase = false,
  capitalize = false,
  delimiter = /_/g,
  replacementDelimiter = ' ',
  firstWord = false,
}) {
  if (!_string) {
    return '';
  }

  let strings = _string
    .replace(delimiter, replacementDelimiter)
    .split(replacementDelimiter);
  let stringArray = [];

  for (const string of strings) {
    if (toUpperCase) {
      stringArray.push(string.toLocaleUpperCase());
    } else if (toLowerCase) {
      stringArray.push(string.toLocaleLowerCase());
    } else if (capitalize) {
      stringArray.push(
        string.charAt(0).toLocaleUpperCase() +
          string.substring(1).toLocaleLowerCase()
      );
    } else {
      stringArray.push(string);
    }
  }

  if (capitalize && firstWord) {
    const previousStringArray = stringArray.slice();
    stringArray = [];
    previousStringArray.slice(1).forEach((string) => {
      stringArray.push(string.toLocaleLowerCase());
    });
    stringArray.unshift(previousStringArray[0]);
  }

  return stringArray.join(replacementDelimiter);
}

export function extractDateString(dateString = null) {
  if (!dateString) {
    return '';
  }

  const match = dateString.match(/(\d{4}-\d{2}-\d{2})/i);

  return match ? match[0] : '';
}

export function getSubstring({
  string = null,
  length = 8,
  toLowerCase = false,
  toUpperCase = false,
}) {
  if (!string || typeof string !== 'string') {
    return string;
  }

  string = string.replace(/-/g, '').substring(0, length);

  if (toLowerCase) {
    string = string.toLocaleLowerCase();
  }

  if (toUpperCase) {
    string = string.toLocaleUpperCase();
  }

  return string;
}

export function convertToBase64(string) {
  return Buffer.from(string).toString('base64');
}

export function convertFromBase64(base64, isRawData = false) {
  return isRawData
    ? Buffer.from(base64, 'base64')
    : Buffer.from(base64, 'base64').toString();
}

export function getTransactionId() {
  function addZeroPadding(value) {
    return !value ? value : `${value <= 9 ? `0${value}` : `${value}`}`;
  }
  const date = new Date();
  return `${date.getFullYear()}${addZeroPadding(
    date.getMonth() + 1
  )}${addZeroPadding(date.getDate())}${addZeroPadding(
    date.getUTCHours()
  )}${addZeroPadding(date.getUTCMinutes())}${addZeroPadding(
    date.getUTCSeconds()
  )}${addZeroPadding(date.getUTCMilliseconds())}`;
}

export function getReferenceId() {
  return generateUUID();
}

function generateUUID() {
  return crypto.randomUUID();
}

export function convertToFloat(value) {
  return Number.parseFloat(value);
}

export function constructS3Endpoint(endpointUrl) {
  const endpoint = new URL(endpointUrl) ?? null;
  return {
    hostname: endpoint?.hostname,
    protocol: endpoint?.protocol,
    port: endpoint?.port,
    query: endpoint?.query,
    path: endpoint?.path,
  };
}

export function generateQuerystring(params = null) {
  let queryString = '';
  if (typeof params !== 'object' || !params) {
    return queryString;
  }
  const queryParams = new URLSearchParams();
  for (const key of Object.keys(params)) {
    const value = params[key];
    if (value) {
      queryParams.set(key, value);
    }
  }
  if (queryParams.toString() !== '') {
    queryString = '?' + queryParams.toString();
  }
  return queryString;
}

export function generateListKey(params = null) {
  let listKey = 'list';
  if (typeof params !== 'object' || !params) {
    return listKey;
  }
  const paramsList = [];
  for (const key of Object.keys(params)) {
    const value = params[key];
    if (value) {
      paramsList.push(value.replace(/\s+/, ''));
    }
  }
  if (paramsList.length) {
    listKey += ':' + paramsList.join(',');
  }
  return listKey;
}

export function cleanupResource(resource) {
  resource = resource.trim();
  return resource.match(/^\/+/) ? resource.replace(/^\/+/, '') : resource;
}

export function getContentUploadListItemDetails(contentUploadListItem) {
  const {
    name,
    contentCategory,
    contentSubCategory,
    file,
    advertPoster,
    advertRate,
  } = contentUploadListItem ?? {};
  const {
    url: advertPosterUrl,
    type: advertPosterType,
    size: advertPosterSize,
    name: advertPosterName,
  } = advertPoster ?? {};
  const { currency: advertRateCurrency, value: advertRateValue } =
    advertRate ?? {};

  return {
    name,
    contentCategory,
    contentSubCategory,
    file,
    advertPosterUrl,
    advertPosterType,
    advertPosterSize,
    advertPosterName,
    advertRateCurrency,
    advertRateValue,
  };
}

// generete list of dates given a certain date, the count of date and it's order
export function getListOfDates(
  date = null,
  count = 0,
  interval = 1,
  order = 'descending'
) {
  let dates = [];
  if (typeof date === 'string' || typeof date === 'number') {
    date = new Date(date);
  } else if (!date || (typeof date === 'object' && !(date instanceof Date))) {
    return dates;
  }
  const dateAsString = (date) =>
    `${date.getUTCFullYear()}-${
      date.getUTCMonth() + 1 < 10
        ? '0' + (date.getUTCMonth() + 1)
        : date.getUTCMonth() + 1
    }-${date.getUTCDate() < 10 ? '0' + date.getUTCDate() : date.getUTCDate()}`;
  const deductor = order === 'descending' ? -1 : 1,
    dayMs = 24 * 60 * 60 * 1000;
  // get date in milliseconds
  const dateMs = date.getTime();
  for (let i = 0; i < count; i++) {
    // multiply day in milliseconds by the current iterator
    // use deductor to get date in milliseconds from time in milliseconds
    const currentTimeMs = dateMs + i * interval * deductor * dayMs;
    // convert to date and append it to dates
    dates.push(dateAsString(new Date(currentTimeMs)));
  }
  return dates;
}

export function getVariantFromHealthDataSeverityName(severityName) {
  let variant = 'primary';
  if (severityName === 'None') {
    variant = 'primary';
  } else if (severityName === 'Normal') {
    variant = 'success';
  } else if (severityName === 'Warning' || severityName === 'Caution') {
    variant = 'warning';
  } else if (severityName === 'Danger' || severityName === 'Emergency') {
    variant = 'danger';
  }
  return variant;
}
