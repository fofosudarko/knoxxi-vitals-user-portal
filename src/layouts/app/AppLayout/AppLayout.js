import PropTypes from 'prop-types';

import { useAuthStore } from 'src/stores/auth';
import { useDeviceDimensions } from 'src/hooks';
import AppHeader from './AppHeader';
import AppMain, { AppMainNav } from './AppMain';

function AppLayout({ children }) {
  const appUser = useAuthStore((state) => state.appUser);
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <>
      <AppMain appUser={appUser}>
        <div className="w-auto">
          <AppHeader
            appUser={appUser}
            SideNav={AppMainNav}
            hideNavbarBrand
            disableNavbarContainerFluid
          />
        </div>
        <div
          style={{
            paddingTop: isLargeDevice ? '60px' : '40px',
          }}
        >
          {children}
        </div>
      </AppMain>
    </>
  );
}

export function getAppLayout(page) {
  return <AppLayout>{page}</AppLayout>;
}

AppLayout.propTypes = {
  children: PropTypes.node,
};
AppLayout.defaultProps = {
  children: null,
};

export default AppLayout;
