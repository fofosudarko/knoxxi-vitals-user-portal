import { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Navbar, Container, Offcanvas } from 'react-bootstrap';
import Link from 'next/link';
import Image from 'next/image';

import { APP_DASHBOARD_LOGO } from 'src/config';
import { useRoutes, useDeviceDimensions } from 'src/hooks';

import { NavButton, NewHOC } from 'src/components/lib';

function SharedHeader({
  appUser,
  SideNav: _SideNav,
  hideNavbarBrand,
  disableNavbarContainerFluid,
}) {
  const { isLargeDevice } = useDeviceDimensions();
  const { useSharedRoute } = useRoutes();
  const { sharedRoute } = useSharedRoute();

  const SideNav = NewHOC(_SideNav);

  return (
    <Navbar expand="lg" className="app-header" sticky="top">
      <Container
        fluid={!disableNavbarContainerFluid}
        style={{ width: isLargeDevice ? '100%' : '100%' }}
        className={`${
          isLargeDevice ? 'd-flex justify-content-around' : undefined
        }`}
      >
        <div>
          <MainNav>
            <SideNav appUser={appUser} />
          </MainNav>
          {!hideNavbarBrand ? (
            <Navbar.Brand className="mx-2">
              <Link href={sharedRoute} as={sharedRoute}>
                <>
                  <Image
                    src={APP_DASHBOARD_LOGO}
                    alt="Knoxxi Logo"
                    height={isLargeDevice ? '28' : '25'}
                    width={isLargeDevice ? '85' : '80'}
                  />
                </>
              </Link>
            </Navbar.Brand>
          ) : (
            <div></div>
          )}
        </div>
      </Container>
    </Navbar>
  );
}

export function SharedHeaderNav() {
  return (
    <div className="w-auto my-2 my-md-0 d-flex justify-content-start justify-content-lg-end"></div>
  );
}

function MainNav({ children }) {
  const [showMainNav, setShowMainNav] = useState(false);

  const handleShowMainNav = useCallback(() => {
    setShowMainNav((state) => !state);
  }, []);

  return (
    <div className="d-block d-lg-none">
      <NavButton onClick={handleShowMainNav} variant="white" />
      <Offcanvas show={showMainNav} onHide={handleShowMainNav}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title></Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body className="app-nav-area-bg">
          <div onClick={handleShowMainNav}>{children}</div>
        </Offcanvas.Body>
      </Offcanvas>
    </div>
  );
}

SharedHeader.propTypes = {
  appUser: PropTypes.object,
  SideNav: PropTypes.func,
  hideNavbarBrand: PropTypes.bool,
  disableNavbarContainerFluid: PropTypes.bool,
};
SharedHeader.defaultProps = {
  appUser: null,
  SideNav: null,
  hideNavbarBrand: false,
  disableNavbarContainerFluid: false,
};
SharedHeaderNav.propTypes = {
  appUser: PropTypes.object,
};
SharedHeaderNav.defaultProps = {
  appUser: null,
};
MainNav.propTypes = {
  children: PropTypes.node,
};
MainNav.defaultProps = {
  children: null,
};

export default SharedHeader;
