import PropTypes from 'prop-types';

import { useDeviceDimensions } from 'src/hooks';
import SharedHeader from './SharedHeader';
import SharedMain, { SharedMainNav } from './SharedMain';

function SharedLayout({ children }) {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <>
      <SharedMain>
        <div className="w-auto">
          <SharedHeader
            SideNav={SharedMainNav}
            hideNavbarBrand
            disableNavbarContainerFluid
          />
        </div>
        <div
          style={{
            paddingTop: isLargeDevice ? '60px' : '40px',
          }}
        >
          {children}
        </div>
      </SharedMain>
    </>
  );
}

export function getSharedLayout(page) {
  return <SharedLayout>{page}</SharedLayout>;
}

SharedLayout.propTypes = {
  children: PropTypes.node,
};
SharedLayout.defaultProps = {
  children: null,
};

export default SharedLayout;
