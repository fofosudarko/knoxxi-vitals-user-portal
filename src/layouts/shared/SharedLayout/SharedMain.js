import { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Nav } from 'react-bootstrap';
import Link from 'next/link';
import Image from 'next/image';
import { useDashboardStore } from 'src/stores/dashboard';

import { useRoutes, useDeviceDimensions } from 'src/hooks';
import { APP_DASHBOARD_LOGO2 } from 'src/config';

import { AppContainer } from 'src/components/lib';

function SharedMain({ children, appUser }) {
  return (
    <AppContainer
      NavArea={<SharedMainNav appUser={appUser} />}
      disablePaddingTop
    >
      {children}
    </AppContainer>
  );
}

export function SharedMainNav() {
  const { isLargeDevice } = useDeviceDimensions();
  const { setMainNav } = useDashboardStore((state) => state);

  const { sharedRoute } = useRoutes().useSharedRoute();

  const handleSelect = useCallback(
    (selectedKey) => {
      if (selectedKey === sharedRoute) {
        setMainNav(sharedRoute);
      }
    },
    [sharedRoute, setMainNav]
  );

  const navItems = useMemo(() => {
    return [];
  }, []);

  return (
    <Nav
      onSelect={handleSelect}
      className="flex-column my-1 vh-100"
      activeKey="home"
    >
      <Nav.Item>
        <Link href={sharedRoute} as={sharedRoute}>
          <div className="pt-3 pb-5 ps-3">
            <Image
              src={APP_DASHBOARD_LOGO2}
              alt="Knoxxi Logo"
              height={isLargeDevice ? '63' : '63'}
              width={isLargeDevice ? '170' : '170'}
            />
          </div>
        </Link>
      </Nav.Item>
      {navItems.map((item, index) => (
        <Nav.Item key={index}>
          <Nav.Link
            eventKey={item.eventKey}
            as="div"
            disabled={item.disabled}
            style={{ cursor: item.disabled ? 'not-allowed' : 'pointer' }}
            className={`${
              item.active ? 'app-nav-active-link' : 'app-nav-inactive-link'
            }`}
          >
            <Link
              href={item.route}
              as={item.route}
              className="text-decoration-none"
            >
              <>
                <div
                  className={`${
                    item.active
                      ? 'app-nav-active-text'
                      : 'app-nav-inactive-text'
                  }`}
                >
                  <div className="app-nav-text text-lowercase">
                    <div className="d-inline-block">{item.Icon}</div>
                    <span className="mx-2">{item.label}</span>
                  </div>
                </div>
              </>
            </Link>
          </Nav.Link>
        </Nav.Item>
      ))}
    </Nav>
  );
}

SharedMain.propTypes = {
  children: PropTypes.node,
  appUser: PropTypes.object,
};
SharedMain.defaultProps = {
  children: null,
  appUser: null,
};

export default SharedMain;
