import PropTypes from 'prop-types';
import { Nav } from 'react-bootstrap';
import Link from 'next/link';

import { useRoutes } from 'src/hooks';

function HealthDataMain({ children }) {
  const {
    router,
    useBPDataListRoute,
    useGlucoseDataListRoute,
    useTemperatureDataListRoute,
    useWeightDataListRoute,
    useWaterDataListRoute,
    useOxygenDataListRoute,
    useCholesterolDataListRoute,
  } = useRoutes();
  const routerPath = router.asPath;
  const { bpDataListRoute } = useBPDataListRoute(),
    { glucoseDataListRoute } = useGlucoseDataListRoute(),
    { temperatureDataListRoute } = useTemperatureDataListRoute(),
    { weightDataListRoute } = useWeightDataListRoute(),
    { waterDataListRoute } = useWaterDataListRoute(),
    { oxygenDataListRoute } = useOxygenDataListRoute(),
    { cholesterolDataListRoute } = useCholesterolDataListRoute();
  const navItems = [
    {
      route: bpDataListRoute,
      label: 'Blood Pressure',
    },
    { route: glucoseDataListRoute, label: 'Glucose' },
    { route: temperatureDataListRoute, label: 'Temperature' },
    { route: weightDataListRoute, label: 'Weight' },
    { route: waterDataListRoute, label: 'Water' },
    { route: oxygenDataListRoute, label: 'Oxygen' },
    { route: cholesterolDataListRoute, label: 'Cholesterol' },
  ];
  return (
    <div>
      <Nav
        fill
        variant="underline"
        defaultActiveKey={bpDataListRoute}
        activeKey={routerPath}
      >
        {navItems.map((item, index) => (
          <Nav.Item key={index}>
            <Nav.Link eventKey={item.route} as="div">
              <Link
                href={item.route}
                as={item.route}
                className="text-decoration-none"
              >
                <>
                  <div>
                    <span className="mx-2">{item.label}</span>
                  </div>
                </>
              </Link>
            </Nav.Link>
          </Nav.Item>
        ))}
      </Nav>
      {children}
    </div>
  );
}

HealthDataMain.propTypes = {
  children: PropTypes.node,
  appUser: PropTypes.object,
};
HealthDataMain.defaultProps = {
  children: null,
  appUser: null,
};

export default HealthDataMain;
