import PropTypes from 'prop-types';

import { useAuthStore } from 'src/stores/auth';
import { useDeviceDimensions } from 'src/hooks';
import HealthDataMain from './HealthDataMain';
import AppLayout from 'src/layouts/app/AppLayout/AppLayout';

function HealthDataLayout({ children }) {
  const appUser = useAuthStore((state) => state.appUser);
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <>
      <HealthDataMain appUser={appUser}>
        <div
          style={{
            paddingTop: isLargeDevice ? '60px' : '40px',
          }}
        >
          {children}
        </div>
      </HealthDataMain>
    </>
  );
}

export function getHealthDataLayout(page) {
  return (
    <AppLayout>
      <HealthDataLayout>{page}</HealthDataLayout>
    </AppLayout>
  );
}

HealthDataLayout.propTypes = {
  children: PropTypes.node,
};
HealthDataLayout.defaultProps = {
  children: null,
};

export default HealthDataLayout;
