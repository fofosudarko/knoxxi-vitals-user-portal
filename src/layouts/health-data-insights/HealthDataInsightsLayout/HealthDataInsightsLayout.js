import PropTypes from 'prop-types';

import { useAuthStore } from 'src/stores/auth';
import { useDeviceDimensions } from 'src/hooks';
import HealthDataInsightsMain from './HealthDataInsightsMain';
import AppLayout from 'src/layouts/app/AppLayout/AppLayout';

function HealthDataInsightsLayout({ children }) {
  const appUser = useAuthStore((state) => state.appUser);
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <>
      <HealthDataInsightsMain appUser={appUser}>
        <div
          style={{
            paddingTop: isLargeDevice ? '60px' : '40px',
          }}
        >
          {children}
        </div>
      </HealthDataInsightsMain>
    </>
  );
}

export function getHealthDataInsightsLayout(page) {
  return (
    <AppLayout>
      <HealthDataInsightsLayout>{page}</HealthDataInsightsLayout>
    </AppLayout>
  );
}

HealthDataInsightsLayout.propTypes = {
  children: PropTypes.node,
};
HealthDataInsightsLayout.defaultProps = {
  children: null,
};

export default HealthDataInsightsLayout;
