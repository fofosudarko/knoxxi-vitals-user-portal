import PropTypes from 'prop-types';
import { Nav } from 'react-bootstrap';
import Link from 'next/link';

import { useRoutes } from 'src/hooks';

function HealthDataInsightsMain({ children }) {
  const {
    router,
    useInsightsBPDataRoute,
    useInsightsGlucoseDataRoute,
    useInsightsTemperatureDataRoute,
    useInsightsOxygenDataRoute,
    useInsightsCholesterolDataRoute,
  } = useRoutes();
  const routerPath = router.asPath;
  const { insightsBPDataRoute } = useInsightsBPDataRoute();
  const { insightsGlucoseDataRoute } = useInsightsGlucoseDataRoute();
  const { insightsTemperatureDataRoute } = useInsightsTemperatureDataRoute();
  const { insightsOxygenDataRoute } = useInsightsOxygenDataRoute();
  const { insightsCholesterolDataRoute } = useInsightsCholesterolDataRoute();
  const navItems = [
    {
      route: insightsBPDataRoute,
      label: 'Blood Pressure',
    },
    {
      route: insightsGlucoseDataRoute,
      label: 'Glucose',
    },
    {
      route: insightsTemperatureDataRoute,
      label: 'Temperature',
    },
    {
      route: insightsOxygenDataRoute,
      label: 'Oxygen',
    },
    {
      route: insightsCholesterolDataRoute,
      label: 'Cholesterol',
    },
  ];
  return (
    <div>
      <Nav
        fill
        variant="underline"
        defaultActiveKey={insightsBPDataRoute}
        activeKey={routerPath}
      >
        {navItems.map((item, index) => (
          <Nav.Item key={index}>
            <Nav.Link eventKey={item.route} as="div">
              <Link
                href={item.route}
                as={item.route}
                className="text-decoration-none"
              >
                <>
                  <div>
                    <span className="mx-2">{item.label}</span>
                  </div>
                </>
              </Link>
            </Nav.Link>
          </Nav.Item>
        ))}
      </Nav>
      {children}
    </div>
  );
}

HealthDataInsightsMain.propTypes = {
  children: PropTypes.node,
  appUser: PropTypes.object,
};
HealthDataInsightsMain.defaultProps = {
  children: null,
  appUser: null,
};

export default HealthDataInsightsMain;
