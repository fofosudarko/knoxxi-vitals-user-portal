import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  additionalLife: null,
  additionalLives: null,
  additionalLifePage: null,
  additionalLifeEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listAdditionalLives(additionalLives) {
    const key = additionalLives.key,
      items = additionalLives.items;
    set((state) => ({
      additionalLives: {
        ...state.additionalLives,
        [key]: Collection.list(
          state.additionalLives ? state.additionalLives[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setAdditionalLife(additionalLife) {
    set({ additionalLife });
  },
  addAdditionalLife(additionalLife) {
    const key = additionalLife.key,
      item = additionalLife.item;
    set((state) => ({
      additionalLives: {
        ...state.additionalLives,
        [key]: Collection.prepend(
          state.additionalLives ? state.additionalLives[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  updateAdditionalLife(additionalLife) {
    const key = additionalLife.key,
      item = additionalLife.item;
    set((state) => ({
      additionalLives: {
        ...state.additionalLives,
        [key]: Collection.update(
          state.additionalLives ? state.additionalLives[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  removeAdditionalLife(additionalLife) {
    const key = additionalLife.key,
      item = additionalLife.item;
    set((state) => ({
      additionalLives: {
        ...state.additionalLives,
        [key]: Collection.remove(
          state.additionalLives ? state.additionalLives[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setAdditionalLifePage(additionalLifePage) {
    const key = additionalLifePage.key,
      item = additionalLifePage.item;
    set((state) => ({
      additionalLifePage: {
        ...state.additionalLifePage,
        [key]: item,
      },
    }));
  },
  setAdditionalLifeEndPaging(additionalLifeEndPaging) {
    const key = additionalLifeEndPaging.key,
      item = additionalLifeEndPaging.item;
    set((state) => ({
      additionalLifeEndPaging: {
        ...state.additionalLifeEndPaging,
        [key]: item,
      },
    }));
  },
  resetAdditionalLifePage(additionalLifePage) {
    const key = additionalLifePage.key;
    set((state) => ({
      additionalLifePage: {
        ...state.additionalLifePage,
        [key]: null,
      },
      additionalLifeEndPaging: {
        ...state.additionalLifeEndPaging,
        [key]: null,
      },
    }));
  },
  clearAdditionalLives(additionalLives) {
    const key = additionalLives.key;
    set((state) => ({
      additionalLives: {
        ...state.additionalLives,
        [key]: null,
      },
    }));
  },
  clearAdditionalLifeState() {
    set({ ...initialState });
  },
});
export const useAdditionalLifeStore = create(storeFn);
