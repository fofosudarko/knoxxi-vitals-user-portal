import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  glucoseData: null,
  glucoseDataList: null,
  glucoseDataPage: null,
  glucoseDataEndPaging: null,
  sharedGlucoseDataList: null,
  sharedGlucoseDataPage: null,
  sharedGlucoseDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listGlucoseDataList(glucoseDataList) {
    const key = glucoseDataList.key,
      items = glucoseDataList.items;
    set((state) => ({
      glucoseDataList: {
        ...state.glucoseDataList,
        [key]: Collection.list(
          state.glucoseDataList ? state.glucoseDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  listSharedGlucoseDataList(sharedGlucoseDataList) {
    const key = sharedGlucoseDataList.key,
      items = sharedGlucoseDataList.items;
    set((state) => ({
      sharedGlucoseDataList: {
        ...state.sharedGlucoseDataList,
        [key]: Collection.list(
          state.sharedGlucoseDataList
            ? state.sharedGlucoseDataList[key] ?? []
            : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setGlucoseData(glucoseData) {
    set({ glucoseData });
  },
  addGlucoseData(glucoseData) {
    const key = glucoseData.key,
      item = glucoseData.item;
    set((state) => ({
      glucoseDataList: {
        ...state.glucoseDataList,
        [key]: Collection.prepend(
          state.glucoseDataList ? state.glucoseDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  updateGlucoseData(glucoseData) {
    const key = glucoseData.key,
      item = glucoseData.item;
    set((state) => ({
      glucoseDataList: {
        ...state.glucoseDataList,
        [key]: Collection.update(
          state.glucoseDataList ? state.glucoseDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  removeGlucoseData(glucoseData) {
    const key = glucoseData.key,
      item = glucoseData.item;
    set((state) => ({
      glucoseDataList: {
        ...state.glucoseDataList,
        [key]: Collection.remove(
          state.glucoseDataList ? state.glucoseDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setGlucoseDataPage(glucoseDataPage) {
    const key = glucoseDataPage.key,
      item = glucoseDataPage.item;
    set((state) => ({
      glucoseDataPage: {
        ...state.glucoseDataPage,
        [key]: item,
      },
    }));
  },
  setSharedGlucoseDataPage(sharedGlucoseDataPage) {
    const key = sharedGlucoseDataPage.key,
      item = sharedGlucoseDataPage.item;
    set((state) => ({
      sharedGlucoseDataPage: {
        ...state.sharedGlucoseDataPage,
        [key]: item,
      },
    }));
  },
  setGlucoseDataEndPaging(glucoseDataEndPaging) {
    const key = glucoseDataEndPaging.key,
      item = glucoseDataEndPaging.item;
    set((state) => ({
      glucoseDataEndPaging: {
        ...state.glucoseDataEndPaging,
        [key]: item,
      },
    }));
  },
  setSharedGlucoseDataEndPaging(sharedGlucoseDataEndPaging) {
    const key = sharedGlucoseDataEndPaging.key,
      item = sharedGlucoseDataEndPaging.item;
    set((state) => ({
      sharedGlucoseDataEndPaging: {
        ...state.sharedGlucoseDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetGlucoseDataPage(glucoseDataPage) {
    const key = glucoseDataPage.key;
    set((state) => ({
      glucoseDataPage: {
        ...state.glucoseDataPage,
        [key]: null,
      },
      glucoseDataEndPaging: {
        ...state.glucoseDataEndPaging,
        [key]: null,
      },
    }));
  },
  resetSharedGlucoseDataPage(sharedGlucoseDataPage) {
    const key = sharedGlucoseDataPage.key;
    set((state) => ({
      sharedGlucoseDataPage: {
        ...state.sharedGlucoseDataPage,
        [key]: null,
      },
      sharedGlucoseDataEndPaging: {
        ...state.sharedGlucoseDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearGlucoseDataList(glucoseDataList) {
    const key = glucoseDataList.key;
    set((state) => ({
      glucoseDataList: {
        ...state.glucoseDataList,
        [key]: null,
      },
    }));
  },
  clearSharedGlucoseDataList(sharedGlucoseDataList) {
    const key = sharedGlucoseDataList.key;
    set((state) => ({
      sharedGlucoseDataList: {
        ...state.sharedGlucoseDataList,
        [key]: null,
      },
    }));
  },
  clearGlucoseDataState() {
    set({ ...initialState });
  },
});
export const useGlucoseDataStore = create(storeFn);
