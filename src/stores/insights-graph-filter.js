import { create } from 'zustand';
import { persist } from 'zustand/middleware';

const initialState = {
  date: new Date(),
  count: 7,
  period: 'Weekly',
  interval: 1,
  order: 'descending',
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  setDate(date) {
    set({ date });
  },
  setCount(count) {
    set({ count });
  },
  setPeriod(period) {
    set({ period });
  },
  setInterval(interval) {
    set({ interval });
  },
  setOrder(order) {
    set({ order });
  },
  clearInsightsGraphFilterState() {
    set({ ...initialState });
  },
});
export const useInsightsGraphFilterStore = create(
  persist(storeFn, { name: 'insights-graph-filter-storage' })
);
