import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  oxygenData: null,
  oxygenDataList: null,
  oxygenDataPage: null,
  oxygenDataEndPaging: null,
  sharedOxygenDataList: null,
  sharedOxygenDataPage: null,
  sharedOxygenDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listOxygenDataList(oxygenDataList) {
    const key = oxygenDataList.key,
      items = oxygenDataList.items;
    set((state) => ({
      oxygenDataList: {
        ...state.oxygenDataList,
        [key]: Collection.list(
          state.oxygenDataList ? state.oxygenDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  listSharedOxygenDataList(sharedOxygenDataList) {
    const key = sharedOxygenDataList.key,
      items = sharedOxygenDataList.items;
    set((state) => ({
      sharedOxygenDataList: {
        ...state.sharedOxygenDataList,
        [key]: Collection.list(
          state.sharedOxygenDataList
            ? state.sharedOxygenDataList[key] ?? []
            : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setOxygenData(oxygenData) {
    set({ oxygenData });
  },
  addOxygenData(oxygenData) {
    const key = oxygenData.key,
      item = oxygenData.item;
    set((state) => ({
      oxygenDataList: {
        ...state.oxygenDataList,
        [key]: Collection.prepend(
          state.oxygenDataList ? state.oxygenDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  removeOxygenData(oxygenData) {
    const key = oxygenData.key,
      item = oxygenData.item;
    set((state) => ({
      oxygenDataList: {
        ...state.oxygenDataList,
        [key]: Collection.remove(
          state.oxygenDataList ? state.oxygenDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setOxygenDataPage(oxygenDataPage) {
    const key = oxygenDataPage.key,
      item = oxygenDataPage.item;
    set((state) => ({
      oxygenDataPage: {
        ...state.oxygenDataPage,
        [key]: item,
      },
    }));
  },
  setSharedOxygenDataPage(sharedOxygenDataPage) {
    const key = sharedOxygenDataPage.key,
      item = sharedOxygenDataPage.item;
    set((state) => ({
      sharedOxygenDataPage: {
        ...state.sharedOxygenDataPage,
        [key]: item,
      },
    }));
  },
  setOxygenDataEndPaging(oxygenDataEndPaging) {
    const key = oxygenDataEndPaging.key,
      item = oxygenDataEndPaging.item;
    set((state) => ({
      oxygenDataEndPaging: {
        ...state.oxygenDataEndPaging,
        [key]: item,
      },
    }));
  },
  setSharedOxygenDataEndPaging(sharedOxygenDataEndPaging) {
    const key = sharedOxygenDataEndPaging.key,
      item = sharedOxygenDataEndPaging.item;
    set((state) => ({
      sharedOxygenDataEndPaging: {
        ...state.sharedOxygenDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetOxygenDataPage(oxygenDataPage) {
    const key = oxygenDataPage.key;
    set((state) => ({
      oxygenDataPage: {
        ...state.oxygenDataPage,
        [key]: null,
      },
      oxygenDataEndPaging: {
        ...state.oxygenDataEndPaging,
        [key]: null,
      },
    }));
  },
  resetSharedOxygenDataPage(sharedOxygenDataPage) {
    const key = sharedOxygenDataPage.key;
    set((state) => ({
      sharedOxygenDataPage: {
        ...state.sharedOxygenDataPage,
        [key]: null,
      },
      sharedOxygenDataEndPaging: {
        ...state.sharedOxygenDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearOxygenDataList(oxygenDataList) {
    const key = oxygenDataList.key;
    set((state) => ({
      oxygenDataList: {
        ...state.oxygenDataList,
        [key]: null,
      },
    }));
  },
  clearSharedOxygenDataList(sharedOxygenDataList) {
    const key = sharedOxygenDataList.key;
    set((state) => ({
      sharedOxygenDataList: {
        ...state.sharedOxygenDataList,
        [key]: null,
      },
    }));
  },
  clearOxygenDataState() {
    set({ ...initialState });
  },
});
export const useOxygenDataStore = create(storeFn);
