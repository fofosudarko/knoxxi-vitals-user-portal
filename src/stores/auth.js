import { create } from 'zustand';
import { persist } from 'zustand/middleware';

const initialState = {
  appUser: null,
  appUserAccessToken: null,
  appUserRegistration: null,
  ckycLogout: null,
  ckycSignIn: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  setAppUser(appUser) {
    set({ appUser });
  },
  setAppUserAccessToken(appUserAccessToken) {
    set({ appUserAccessToken });
  },
  setAppUserRegistration(appUserRegistration) {
    set({ appUserRegistration });
  },
  setCkycLogout(ckycLogout) {
    set({ ckycLogout });
  },
  setCkycSignIn(ckycSignIn) {
    set({ ckycSignIn });
  },
  clearAppUserRegistration() {
    set({
      appUserRegistration: null,
      ckycLogout: null,
      ckycSignIn: null,
    });
  },
  clearAuthState() {
    set({ ...initialState });
  },
});
export const useAuthStore = create(
  persist(storeFn, {
    name: 'auth-storage',
  })
);
