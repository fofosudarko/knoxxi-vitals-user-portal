import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  weightData: null,
  weightDataList: null,
  weightDataPage: null,
  weightDataEndPaging: null,
  sharedWeightDataList: null,
  sharedWeightDataPage: null,
  sharedWeightDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listWeightDataList(weightDataList) {
    const key = weightDataList.key,
      items = weightDataList.items;
    set((state) => ({
      weightDataList: {
        ...state.weightDataList,
        [key]: Collection.list(
          state.weightDataList ? state.weightDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  listSharedWeightDataList(sharedWeightDataList) {
    const key = sharedWeightDataList.key,
      items = sharedWeightDataList.items;
    set((state) => ({
      sharedWeightDataList: {
        ...state.sharedWeightDataList,
        [key]: Collection.list(
          state.sharedWeightDataList
            ? state.sharedWeightDataList[key] ?? []
            : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setWeightData(weightData) {
    set({ weightData });
  },
  addWeightData(weightData) {
    const key = weightData.key,
      item = weightData.item;
    set((state) => ({
      weightDataList: {
        ...state.weightDataList,
        [key]: Collection.prepend(
          state.weightDataList ? state.weightDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  removeWeightData(weightData) {
    const key = weightData.key,
      item = weightData.item;
    set((state) => ({
      weightDataList: {
        ...state.weightDataList,
        [key]: Collection.remove(
          state.weightDataList ? state.weightDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setWeightDataPage(weightDataPage) {
    const key = weightDataPage.key,
      item = weightDataPage.item;
    set((state) => ({
      weightDataPage: {
        ...state.weightDataPage,
        [key]: item,
      },
    }));
  },
  setSharedWeightDataPage(sharedWeightDataPage) {
    const key = sharedWeightDataPage.key,
      item = sharedWeightDataPage.item;
    set((state) => ({
      sharedWeightDataPage: {
        ...state.sharedWeightDataPage,
        [key]: item,
      },
    }));
  },
  setWeightDataEndPaging(weightDataEndPaging) {
    const key = weightDataEndPaging.key,
      item = weightDataEndPaging.item;
    set((state) => ({
      weightDataEndPaging: {
        ...state.weightDataEndPaging,
        [key]: item,
      },
    }));
  },
  setSharedWeightDataEndPaging(sharedWeightDataEndPaging) {
    const key = sharedWeightDataEndPaging.key,
      item = sharedWeightDataEndPaging.item;
    set((state) => ({
      sharedWeightDataEndPaging: {
        ...state.sharedWeightDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetWeightDataPage(weightDataPage) {
    const key = weightDataPage.key;
    set((state) => ({
      weightDataPage: {
        ...state.weightDataPage,
        [key]: null,
      },
      weightDataEndPaging: {
        ...state.weightDataEndPaging,
        [key]: null,
      },
    }));
  },
  resetSharedWeightDataPage(sharedWeightDataPage) {
    const key = sharedWeightDataPage.key;
    set((state) => ({
      sharedWeightDataPage: {
        ...state.sharedWeightDataPage,
        [key]: null,
      },
      sharedWeightDataEndPaging: {
        ...state.sharedWeightDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearWeightDataList(weightDataList) {
    const key = weightDataList.key;
    set((state) => ({
      weightDataList: {
        ...state.weightDataList,
        [key]: null,
      },
    }));
  },
  clearSharedWeightDataList(sharedWeightDataList) {
    const key = sharedWeightDataList.key;
    set((state) => ({
      sharedWeightDataList: {
        ...state.sharedWeightDataList,
        [key]: null,
      },
    }));
  },
  clearWeightDataState() {
    set({ ...initialState });
  },
});
export const useWeightDataStore = create(storeFn);
