import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  cholesterolData: null,
  cholesterolDataList: null,
  cholesterolDataPage: null,
  cholesterolDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listCholesterolDataList(cholesterolDataList) {
    const key = cholesterolDataList.key,
      items = cholesterolDataList.items;
    set((state) => ({
      cholesterolDataList: {
        ...state.cholesterolDataList,
        [key]: Collection.list(
          state.cholesterolDataList ? state.cholesterolDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setCholesterolData(cholesterolData) {
    set({ cholesterolData });
  },
  addCholesterolData(cholesterolData) {
    const key = cholesterolData.key,
      item = cholesterolData.item;
    set((state) => ({
      cholesterolDataList: {
        ...state.cholesterolDataList,
        [key]: Collection.prepend(
          state.cholesterolDataList ? state.cholesterolDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  removeCholesterolData(cholesterolData) {
    const key = cholesterolData.key,
      item = cholesterolData.item;
    set((state) => ({
      cholesterolDataList: {
        ...state.cholesterolDataList,
        [key]: Collection.remove(
          state.cholesterolDataList ? state.cholesterolDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setCholesterolDataPage(cholesterolDataPage) {
    const key = cholesterolDataPage.key,
      item = cholesterolDataPage.item;
    set((state) => ({
      cholesterolDataPage: {
        ...state.cholesterolDataPage,
        [key]: item,
      },
    }));
  },
  setCholesterolDataEndPaging(cholesterolDataEndPaging) {
    const key = cholesterolDataEndPaging.key,
      item = cholesterolDataEndPaging.item;
    set((state) => ({
      cholesterolDataEndPaging: {
        ...state.cholesterolDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetCholesterolDataPage(cholesterolDataPage) {
    const key = cholesterolDataPage.key;
    set((state) => ({
      cholesterolDataPage: {
        ...state.cholesterolDataPage,
        [key]: null,
      },
      cholesterolDataEndPaging: {
        ...state.cholesterolDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearCholesterolDataList(cholesterolDataList) {
    const key = cholesterolDataList.key;
    set((state) => ({
      cholesterolDataList: {
        ...state.cholesterolDataList,
        [key]: null,
      },
    }));
  },
  clearCholesterolDataState() {
    set({ ...initialState });
  },
});
export const useCholesterolDataStore = create(storeFn);
