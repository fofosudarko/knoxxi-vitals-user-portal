import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  waterData: null,
  waterDataList: null,
  waterDataPage: null,
  waterDataEndPaging: null,
  sharedWaterDataList: null,
  sharedWaterDataPage: null,
  sharedWaterDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listWaterDataList(waterDataList) {
    const key = waterDataList.key,
      items = waterDataList.items;
    set((state) => ({
      waterDataList: {
        ...state.waterDataList,
        [key]: Collection.list(
          state.waterDataList ? state.waterDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  listSharedWaterDataList(sharedWaterDataList) {
    const key = sharedWaterDataList.key,
      items = sharedWaterDataList.items;
    set((state) => ({
      sharedWaterDataList: {
        ...state.sharedWaterDataList,
        [key]: Collection.list(
          state.sharedWaterDataList ? state.sharedWaterDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setWaterData(waterData) {
    set({ waterData });
  },
  addWaterData(waterData) {
    const key = waterData.key,
      item = waterData.item;
    set((state) => ({
      waterDataList: {
        ...state.waterDataList,
        [key]: Collection.prepend(
          state.waterDataList ? state.waterDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  removeWaterData(waterData) {
    const key = waterData.key,
      item = waterData.item;
    set((state) => ({
      waterDataList: {
        ...state.waterDataList,
        [key]: Collection.remove(
          state.waterDataList ? state.waterDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setWaterDataPage(waterDataPage) {
    const key = waterDataPage.key,
      item = waterDataPage.item;
    set((state) => ({
      waterDataPage: {
        ...state.waterDataPage,
        [key]: item,
      },
    }));
  },
  setSharedWaterDataPage(sharedWaterDataPage) {
    const key = sharedWaterDataPage.key,
      item = sharedWaterDataPage.item;
    set((state) => ({
      sharedWaterDataPage: {
        ...state.sharedWaterDataPage,
        [key]: item,
      },
    }));
  },
  setWaterDataEndPaging(waterDataEndPaging) {
    const key = waterDataEndPaging.key,
      item = waterDataEndPaging.item;
    set((state) => ({
      waterDataEndPaging: {
        ...state.waterDataEndPaging,
        [key]: item,
      },
    }));
  },
  setSharedWaterDataEndPaging(sharedWaterDataEndPaging) {
    const key = sharedWaterDataEndPaging.key,
      item = sharedWaterDataEndPaging.item;
    set((state) => ({
      sharedWaterDataEndPaging: {
        ...state.sharedWaterDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetWaterDataPage(waterDataPage) {
    const key = waterDataPage.key;
    set((state) => ({
      waterDataPage: {
        ...state.waterDataPage,
        [key]: null,
      },
      waterDataEndPaging: {
        ...state.waterDataEndPaging,
        [key]: null,
      },
    }));
  },
  resetSharedWaterDataPage(sharedWaterDataPage) {
    const key = sharedWaterDataPage.key;
    set((state) => ({
      sharedWaterDataPage: {
        ...state.sharedWaterDataPage,
        [key]: null,
      },
      sharedWaterDataEndPaging: {
        ...state.sharedWaterDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearWaterDataList(waterDataList) {
    const key = waterDataList.key;
    set((state) => ({
      waterDataList: {
        ...state.waterDataList,
        [key]: null,
      },
    }));
  },
  clearSharedWaterDataList(sharedWaterDataList) {
    const key = sharedWaterDataList.key;
    set((state) => ({
      sharedWaterDataList: {
        ...state.sharedWaterDataList,
        [key]: null,
      },
    }));
  },
  clearWaterDataState() {
    set({ ...initialState });
  },
});
export const useWaterDataStore = create(storeFn);
