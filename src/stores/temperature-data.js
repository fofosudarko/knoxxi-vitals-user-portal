import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  temperatureData: null,
  temperatureDataList: null,
  temperatureDataPage: null,
  temperatureDataEndPaging: null,
  sharedTemperatureDataList: null,
  sharedTemperatureDataPage: null,
  sharedTemperatureDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listTemperatureDataList(temperatureDataList) {
    const key = temperatureDataList.key,
      items = temperatureDataList.items;
    set((state) => ({
      temperatureDataList: {
        ...state.temperatureDataList,
        [key]: Collection.list(
          state.temperatureDataList ? state.temperatureDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  listSharedTemperatureDataList(sharedTemperatureDataList) {
    const key = sharedTemperatureDataList.key,
      items = sharedTemperatureDataList.items;
    set((state) => ({
      sharedTemperatureDataList: {
        ...state.sharedTemperatureDataList,
        [key]: Collection.list(
          state.sharedTemperatureDataList
            ? state.sharedTemperatureDataList[key] ?? []
            : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setTemperatureData(temperatureData) {
    set({ temperatureData });
  },
  addTemperatureData(temperatureData) {
    const key = temperatureData.key,
      item = temperatureData.item;
    set((state) => ({
      temperatureDataList: {
        ...state.temperatureDataList,
        [key]: Collection.prepend(
          state.temperatureDataList ? state.temperatureDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  removeTemperatureData(temperatureData) {
    const key = temperatureData.key,
      item = temperatureData.item;
    set((state) => ({
      temperatureDataList: {
        ...state.temperatureDataList,
        [key]: Collection.remove(
          state.temperatureDataList ? state.temperatureDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setTemperatureDataPage(temperatureDataPage) {
    const key = temperatureDataPage.key,
      item = temperatureDataPage.item;
    set((state) => ({
      temperatureDataPage: {
        ...state.temperatureDataPage,
        [key]: item,
      },
    }));
  },
  setSharedTemperatureDataPage(sharedTemperatureDataPage) {
    const key = sharedTemperatureDataPage.key,
      item = sharedTemperatureDataPage.item;
    set((state) => ({
      sharedTemperatureDataPage: {
        ...state.sharedTemperatureDataPage,
        [key]: item,
      },
    }));
  },
  setTemperatureDataEndPaging(temperatureDataEndPaging) {
    const key = temperatureDataEndPaging.key,
      item = temperatureDataEndPaging.item;
    set((state) => ({
      temperatureDataEndPaging: {
        ...state.temperatureDataEndPaging,
        [key]: item,
      },
    }));
  },
  setSharedTemperatureDataEndPaging(sharedTemperatureDataEndPaging) {
    const key = sharedTemperatureDataEndPaging.key,
      item = sharedTemperatureDataEndPaging.item;
    set((state) => ({
      sharedTemperatureDataEndPaging: {
        ...state.sharedTemperatureDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetTemperatureDataPage(temperatureDataPage) {
    const key = temperatureDataPage.key;
    set((state) => ({
      temperatureDataPage: {
        ...state.temperatureDataPage,
        [key]: null,
      },
      temperatureDataEndPaging: {
        ...state.temperatureDataEndPaging,
        [key]: null,
      },
    }));
  },
  resetSharedTemperatureDataPage(sharedTemperatureDataPage) {
    const key = sharedTemperatureDataPage.key;
    set((state) => ({
      sharedTemperatureDataPage: {
        ...state.sharedTemperatureDataPage,
        [key]: null,
      },
      sharedTemperatureDataEndPaging: {
        ...state.sharedTemperatureDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearTemperatureDataList(temperatureDataList) {
    const key = temperatureDataList.key;
    set((state) => ({
      temperatureDataList: {
        ...state.temperatureDataList,
        [key]: null,
      },
    }));
  },
  clearSharedTemperatureDataList(sharedTemperatureDataList) {
    const key = sharedTemperatureDataList.key;
    set((state) => ({
      sharedTemperatureDataList: {
        ...state.sharedTemperatureDataList,
        [key]: null,
      },
    }));
  },
  clearTemperatureDataState() {
    set({ ...initialState });
  },
});
export const useTemperatureDataStore = create(storeFn);
