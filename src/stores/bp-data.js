import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  bpData: null,
  bpDataList: null,
  bpDataPage: null,
  bpDataEndPaging: null,
  sharedBpDataList: null,
  sharedBpDataPage: null,
  sharedBpDataEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listBPDataList(bpDataList) {
    const key = bpDataList.key,
      items = bpDataList.items;
    set((state) => ({
      bpDataList: {
        ...state.bpDataList,
        [key]: Collection.list(
          state.bpDataList ? state.bpDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  listSharedBPDataList(sharedBpDataList) {
    const key = sharedBpDataList.key,
      items = sharedBpDataList.items;
    set((state) => ({
      sharedBpDataList: {
        ...state.sharedBpDataList,
        [key]: Collection.list(
          state.sharedBpDataList ? state.sharedBpDataList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setBPData(bpData) {
    set({ bpData });
  },
  addBPData(bpData) {
    const key = bpData.key,
      item = bpData.item;
    set((state) => ({
      bpDataList: {
        ...state.bpDataList,
        [key]: Collection.prepend(
          state.bpDataList ? state.bpDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  removeBPData(bpData) {
    const key = bpData.key,
      item = bpData.item;
    set((state) => ({
      bpDataList: {
        ...state.bpDataList,
        [key]: Collection.remove(
          state.bpDataList ? state.bpDataList[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setBPDataPage(bpDataPage) {
    const key = bpDataPage.key,
      item = bpDataPage.item;
    set((state) => ({
      bpDataPage: {
        ...state.bpDataPage,
        [key]: item,
      },
    }));
  },
  setSharedBPDataPage(sharedBpDataPage) {
    const key = sharedBpDataPage.key,
      item = sharedBpDataPage.item;
    set((state) => ({
      sharedBpDataPage: {
        ...state.sharedBpDataPage,
        [key]: item,
      },
    }));
  },
  setBPDataEndPaging(bpDataEndPaging) {
    const key = bpDataEndPaging.key,
      item = bpDataEndPaging.item;
    set((state) => ({
      bpDataEndPaging: {
        ...state.bpDataEndPaging,
        [key]: item,
      },
    }));
  },
  setSharedBPDataEndPaging(sharedBpDataEndPaging) {
    const key = sharedBpDataEndPaging.key,
      item = sharedBpDataEndPaging.item;
    set((state) => ({
      sharedBpDataEndPaging: {
        ...state.sharedBpDataEndPaging,
        [key]: item,
      },
    }));
  },
  resetBPDataPage(bpDataPage) {
    const key = bpDataPage.key;
    set((state) => ({
      bpDataPage: {
        ...state.bpDataPage,
        [key]: null,
      },
      bpDataEndPaging: {
        ...state.bpDataEndPaging,
        [key]: null,
      },
    }));
  },
  resetSharedBPDataPage(sharedBpDataPage) {
    const key = sharedBpDataPage.key;
    set((state) => ({
      sharedBpDataPage: {
        ...state.sharedBpDataPage,
        [key]: null,
      },
      sharedBpDataEndPaging: {
        ...state.sharedBpDataEndPaging,
        [key]: null,
      },
    }));
  },
  clearBPDataList(bpDataList) {
    const key = bpDataList.key;
    set((state) => ({
      bpDataList: {
        ...state.bpDataList,
        [key]: null,
      },
    }));
  },
  clearSharedBPDataList(sharedBpDataList) {
    const key = sharedBpDataList.key;
    set((state) => ({
      sharedBpDataList: {
        ...state.sharedBpDataList,
        [key]: null,
      },
    }));
  },
  clearBPDataState() {
    set({ ...initialState });
  },
});
export const useBPDataStore = create(storeFn);
