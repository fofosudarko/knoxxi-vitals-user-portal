import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  healthDataShare: null,
  healthDataSharePublic: null,
  healthDataSharePublicRetrieved: false,
  healthDataShares: null,
  healthDataSharePage: null,
  healthDataShareEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listHealthDataShares(healthDataShares) {
    const key = healthDataShares.key,
      items = healthDataShares.items;
    set((state) => ({
      healthDataShares: {
        ...state.healthDataShares,
        [key]: Collection.list(
          state.healthDataShares ? state.healthDataShares[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setHealthDataShare(healthDataShare) {
    set({ healthDataShare });
  },
  setHealthDataSharePublic(healthDataSharePublic) {
    set({ healthDataSharePublic });
  },
  setHealthDataSharePublicRetrieved(healthDataSharePublicRetrieved) {
    set({ healthDataSharePublicRetrieved });
  },
  addHealthDataShare(healthDataShare) {
    const key = healthDataShare.key,
      item = healthDataShare.item;
    set((state) => ({
      healthDataShares: {
        ...state.healthDataShares,
        [key]: Collection.prepend(
          state.healthDataShares ? state.healthDataShares[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  updateHealthDataShare(healthDataShare) {
    const key = healthDataShare.key,
      item = healthDataShare.item;
    set((state) => ({
      healthDataShares: {
        ...state.healthDataShares,
        [key]: Collection.update(
          state.healthDataShares ? state.healthDataShares[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  removeHealthDataShare(healthDataShare) {
    const key = healthDataShare.key,
      item = healthDataShare.item;
    set((state) => ({
      healthDataShares: {
        ...state.healthDataShares,
        [key]: Collection.remove(
          state.healthDataShares ? state.healthDataShares[key] ?? [] : [],
          item
        ),
      },
    }));
  },
  setHealthDataSharePage(healthDataSharePage) {
    const key = healthDataSharePage.key,
      item = healthDataSharePage.item;
    set((state) => ({
      healthDataSharePage: {
        ...state.healthDataSharePage,
        [key]: item,
      },
    }));
  },
  setHealthDataShareEndPaging(healthDataShareEndPaging) {
    const key = healthDataShareEndPaging.key,
      item = healthDataShareEndPaging.item;
    set((state) => ({
      healthDataShareEndPaging: {
        ...state.healthDataShareEndPaging,
        [key]: item,
      },
    }));
  },
  resetHealthDataSharePage(healthDataSharePage) {
    const key = healthDataSharePage.key;
    set((state) => ({
      healthDataSharePage: {
        ...state.healthDataSharePage,
        [key]: null,
      },
      healthDataShareEndPaging: {
        ...state.healthDataShareEndPaging,
        [key]: null,
      },
    }));
  },
  clearHealthDataShares(healthDataShares) {
    const key = healthDataShares.key;
    set((state) => ({
      healthDataShares: {
        ...state.healthDataShares,
        [key]: null,
      },
    }));
  },
  clearHealthDataShareState() {
    set({ ...initialState });
  },
});
export const useHealthDataShareStore = create(storeFn);
