import { create } from 'zustand';

const initialState = {
  bpDataTimedAverages: null,
  bpDataCumulativeAverages: null,
  glucoseDataTimedAverages: null,
  glucoseDataCumulativeAverages: null,
  temperatureDataTimedAverages: null,
  temperatureDataCumulativeAverages: null,
  oxygenDataTimedAverages: null,
  oxygenDataCumulativeAverages: null,
  cholesterolDataTimedAverages: null,
  cholesterolDataCumulativeAverages: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  setBPDataTimedAverages(bpDataTimedAverages) {
    const key = bpDataTimedAverages.key,
      item = bpDataTimedAverages.item;
    set((state) => ({
      bpDataTimedAverages: {
        ...state.bpDataTimedAverages,
        [key]: item,
      },
    }));
  },
  setBPDataCumulativeAverages(bpDataCumulativeAverages) {
    const key = bpDataCumulativeAverages.key,
      item = bpDataCumulativeAverages.item;
    set((state) => ({
      bpDataCumulativeAverages: {
        ...state.bpDataCumulativeAverages,
        [key]: item,
      },
    }));
  },
  setGlucoseDataTimedAverages(glucoseDataTimedAverages) {
    const key = glucoseDataTimedAverages.key,
      item = glucoseDataTimedAverages.item;
    set((state) => ({
      glucoseDataTimedAverages: {
        ...state.glucoseDataTimedAverages,
        [key]: item,
      },
    }));
  },
  setGlucoseDataCumulativeAverages(glucoseDataCumulativeAverages) {
    const key = glucoseDataCumulativeAverages.key,
      item = glucoseDataCumulativeAverages.item;
    set((state) => ({
      glucoseDataCumulativeAverages: {
        ...state.glucoseDataCumulativeAverages,
        [key]: item,
      },
    }));
  },
  setTemperatureDataTimedAverages(temperatureDataTimedAverages) {
    const key = temperatureDataTimedAverages.key,
      item = temperatureDataTimedAverages.item;
    set((state) => ({
      temperatureDataTimedAverages: {
        ...state.temperatureDataTimedAverages,
        [key]: item,
      },
    }));
  },
  setTemperatureDataCumulativeAverages(temperatureDataCumulativeAverages) {
    const key = temperatureDataCumulativeAverages.key,
      item = temperatureDataCumulativeAverages.item;
    set((state) => ({
      temperatureDataCumulativeAverages: {
        ...state.temperatureDataCumulativeAverages,
        [key]: item,
      },
    }));
  },
  setOxygenDataTimedAverages(oxygenDataTimedAverages) {
    const key = oxygenDataTimedAverages.key,
      item = oxygenDataTimedAverages.item;
    set((state) => ({
      oxygenDataTimedAverages: {
        ...state.oxygenDataTimedAverages,
        [key]: item,
      },
    }));
  },
  setOxygenDataCumulativeAverages(oxygenDataCumulativeAverages) {
    const key = oxygenDataCumulativeAverages.key,
      item = oxygenDataCumulativeAverages.item;
    set((state) => ({
      oxygenDataCumulativeAverages: {
        ...state.oxygenDataCumulativeAverages,
        [key]: item,
      },
    }));
  },
  setCholesterolDataTimedAverages(cholesterolDataTimedAverages) {
    const key = cholesterolDataTimedAverages.key,
      item = cholesterolDataTimedAverages.item;
    set((state) => ({
      cholesterolDataTimedAverages: {
        ...state.cholesterolDataTimedAverages,
        [key]: item,
      },
    }));
  },
  setCholesterolDataCumulativeAverages(cholesterolDataCumulativeAverages) {
    const key = cholesterolDataCumulativeAverages.key,
      item = cholesterolDataCumulativeAverages.item;
    set((state) => ({
      cholesterolDataCumulativeAverages: {
        ...state.cholesterolDataCumulativeAverages,
        [key]: item,
      },
    }));
  },
  clearBPDataTimedAverages(bpDataTimedAverages) {
    const key = bpDataTimedAverages.key;
    set((state) => ({
      bpDataTimedAverages: {
        ...state.bpDataTimedAverages,
        [key]: null,
      },
    }));
  },
  clearBPDataCumulativeAverages(bpDataCumulativeAverages) {
    const key = bpDataCumulativeAverages.key;
    set((state) => ({
      bpDataCumulativeAverages: {
        ...state.bpDataCumulativeAverages,
        [key]: null,
      },
    }));
  },
  clearGlucoseDataTimedAverages(glucoseDataTimedAverages) {
    const key = glucoseDataTimedAverages.key;
    set((state) => ({
      glucoseDataTimedAverages: {
        ...state.glucoseDataTimedAverages,
        [key]: null,
      },
    }));
  },
  clearGlucoseDataCumulativeAverages(glucoseDataCumulativeAverages) {
    const key = glucoseDataCumulativeAverages.key;
    set((state) => ({
      glucoseDataCumulativeAverages: {
        ...state.glucoseDataCumulativeAverages,
        [key]: null,
      },
    }));
  },
  clearTemperatureDataTimedAverages(temperatureDataTimedAverages) {
    const key = temperatureDataTimedAverages.key;
    set((state) => ({
      temperatureDataTimedAverages: {
        ...state.temperatureDataTimedAverages,
        [key]: null,
      },
    }));
  },
  clearTemperatureDataCumulativeAverages(temperatureDataCumulativeAverages) {
    const key = temperatureDataCumulativeAverages.key;
    set((state) => ({
      temperatureDataCumulativeAverages: {
        ...state.temperatureDataCumulativeAverages,
        [key]: null,
      },
    }));
  },
  clearOxygenDataTimedAverages(oxygenDataTimedAverages) {
    const key = oxygenDataTimedAverages.key;
    set((state) => ({
      oxygenDataTimedAverages: {
        ...state.oxygenDataTimedAverages,
        [key]: null,
      },
    }));
  },
  clearOxygenDataCumulativeAverages(oxygenDataCumulativeAverages) {
    const key = oxygenDataCumulativeAverages.key;
    set((state) => ({
      oxygenDataCumulativeAverages: {
        ...state.oxygenDataCumulativeAverages,
        [key]: null,
      },
    }));
  },
  clearCholesterolDataTimedAverages(cholesterolDataTimedAverages) {
    const key = cholesterolDataTimedAverages.key;
    set((state) => ({
      cholesterolDataTimedAverages: {
        ...state.cholesterolDataTimedAverages,
        [key]: null,
      },
    }));
  },
  clearCholesterolDataCumulativeAverages(cholesterolDataCumulativeAverages) {
    const key = cholesterolDataCumulativeAverages.key;
    set((state) => ({
      cholesterolDataCumulativeAverages: {
        ...state.cholesterolDataCumulativeAverages,
        [key]: null,
      },
    }));
  },
  clearInsightState() {
    set({ ...initialState });
  },
});
export const useInsightStore = create(storeFn);
