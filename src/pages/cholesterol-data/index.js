import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataLayout } from 'src/layouts/health-data/HealthDataLayout/HealthDataLayout';
import { CholesterolDataListPage } from 'src/components/cholesterol-data';

export default function CholesterolDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Cholesterol data list</title>
      </Head>
      {appUser && hasMounted ? (
        <CholesterolDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

CholesterolDataList.getLayout = getHealthDataLayout;

CholesterolDataList.propTypes = {};
CholesterolDataList.defaultProps = {};
