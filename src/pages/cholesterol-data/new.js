import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { CholesterolDataNewPage } from 'src/components/cholesterol-data';

export default function CholesterolDataNew() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>New cholesterol data</title>
      </Head>
      {appUser && hasMounted ? (
        <CholesterolDataNewPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

CholesterolDataNew.getLayout = getAppLayout;

CholesterolDataNew.propTypes = {};
CholesterolDataNew.defaultProps = {};
