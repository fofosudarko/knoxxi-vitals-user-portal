import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataLayout } from 'src/layouts/health-data/HealthDataLayout/HealthDataLayout';
import { WaterDataListPage } from 'src/components/water-data';

export default function WaterDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Water data list</title>
      </Head>
      {appUser && hasMounted ? (
        <WaterDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

WaterDataList.getLayout = getHealthDataLayout;

WaterDataList.propTypes = {};
WaterDataList.defaultProps = {};
