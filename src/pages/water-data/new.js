import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { WaterDataNewPage } from 'src/components/water-data';

export default function WaterDataNew() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>New water data</title>
      </Head>
      {appUser && hasMounted ? (
        <WaterDataNewPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

WaterDataNew.getLayout = getAppLayout;

WaterDataNew.propTypes = {};
WaterDataNew.defaultProps = {};
