import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { AdditionalLivesPage } from 'src/components/additional-lives';

export default function AdditionalLives() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Additional lives</title>
      </Head>
      {appUser && hasMounted ? (
        <AdditionalLivesPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

AdditionalLives.getLayout = getAppLayout;

AdditionalLives.propTypes = {};
AdditionalLives.defaultProps = {};
