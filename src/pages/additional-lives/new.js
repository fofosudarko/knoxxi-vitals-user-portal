import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { AdditionalLifeNewPage } from 'src/components/additional-lives';

export default function AdditionalLifeNew() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>New additional life</title>
      </Head>
      {appUser && hasMounted ? (
        <AdditionalLifeNewPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

AdditionalLifeNew.getLayout = getAppLayout;

AdditionalLifeNew.propTypes = {};
AdditionalLifeNew.defaultProps = {};
