import PropTypes from 'prop-types';
import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { useGetAdditionalLife } from 'src/hooks/api';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { AdditionalLifeEditPage } from 'src/components/additional-lives';

export default function AdditionalLifeEdit({ additionalLifeId }) {
  const { appUser, hasMounted } = useAuth();
  const { additionalLife } = useGetAdditionalLife({
    additionalLife: { id: additionalLifeId },
  });

  return (
    <>
      <Head>
        <title>Edit additional life</title>
      </Head>
      {appUser && hasMounted && additionalLife ? (
        <AdditionalLifeEditPage
          appUser={appUser}
          additionalLife={additionalLife}
        />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

AdditionalLifeEdit.getLayout = getAppLayout;

export function getServerSideProps(context) {
  const { additionalLifeId = null } = context.params;
  return { props: { additionalLifeId } };
}

AdditionalLifeEdit.propTypes = {
  additionalLifeId: PropTypes.string,
};
AdditionalLifeEdit.defaultProps = {
  additionalLifeId: undefined,
};
