import { useHomeRedirect } from 'src/hooks';

import { SitePage } from 'src/components/site';

export default function Site() {
  useHomeRedirect();
  return <SitePage />;
}
