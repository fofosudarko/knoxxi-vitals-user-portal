import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { HealthDataSharesPage } from 'src/components/health-data-shares';

export default function HealthDataShares() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Health data shares</title>
      </Head>
      {appUser && hasMounted ? (
        <HealthDataSharesPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

HealthDataShares.getLayout = getAppLayout;

HealthDataShares.propTypes = {};
HealthDataShares.defaultProps = {};
