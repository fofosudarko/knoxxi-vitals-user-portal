import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { WeightDataNewPage } from 'src/components/weight-data';

export default function WeightDataNew() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>New weight data</title>
      </Head>
      {appUser && hasMounted ? (
        <WeightDataNewPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

WeightDataNew.getLayout = getAppLayout;

WeightDataNew.propTypes = {};
WeightDataNew.defaultProps = {};
