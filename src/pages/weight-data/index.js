import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataLayout } from 'src/layouts/health-data/HealthDataLayout/HealthDataLayout';
import { WeightDataListPage } from 'src/components/weight-data';

export default function WeightDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Weight data list</title>
      </Head>
      {appUser && hasMounted ? (
        <WeightDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

WeightDataList.getLayout = getHealthDataLayout;

WeightDataList.propTypes = {};
WeightDataList.defaultProps = {};
