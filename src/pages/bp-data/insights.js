import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { BPDataInsightsGraphPage } from 'src/components/insights';

export default function BPDataInsights() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>BP Data Insights</title>
      </Head>
      {appUser && hasMounted ? (
        <BPDataInsightsGraphPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

BPDataInsights.getLayout = getAppLayout;

BPDataInsights.propTypes = {};
BPDataInsights.defaultProps = {};
