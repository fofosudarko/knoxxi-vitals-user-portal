import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { BPDataNewPage } from 'src/components/bp-data';

export default function BPDataNew() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>New bp data</title>
      </Head>
      {appUser && hasMounted ? (
        <BPDataNewPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

BPDataNew.getLayout = getAppLayout;

BPDataNew.propTypes = {};
BPDataNew.defaultProps = {};
