import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataLayout } from 'src/layouts/health-data/HealthDataLayout/HealthDataLayout';
import { BPDataListPage } from 'src/components/bp-data';

export default function BPDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>BP data list</title>
      </Head>
      {appUser && hasMounted ? (
        <BPDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

BPDataList.getLayout = getHealthDataLayout;

BPDataList.propTypes = {};
BPDataList.defaultProps = {};
