import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataLayout } from 'src/layouts/health-data/HealthDataLayout/HealthDataLayout';
import { GlucoseDataListPage } from 'src/components/glucose-data';

export default function GlucoseDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Glucose data list</title>
      </Head>
      {appUser && hasMounted ? (
        <GlucoseDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

GlucoseDataList.getLayout = getHealthDataLayout;

GlucoseDataList.propTypes = {};
GlucoseDataList.defaultProps = {};
