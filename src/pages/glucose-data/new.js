import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { GlucoseDataNewPage } from 'src/components/glucose-data';

export default function GlucoseDataNew() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>New glucose data</title>
      </Head>
      {appUser && hasMounted ? (
        <GlucoseDataNewPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

GlucoseDataNew.getLayout = getAppLayout;

GlucoseDataNew.propTypes = {};
GlucoseDataNew.defaultProps = {};
