import PropTypes from 'prop-types';
import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { useGetGlucoseData } from 'src/hooks/api';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { GlucoseDataEditPage } from 'src/components/glucose-data';

export default function GlucoseDataEdit({ glucoseDataId }) {
  const { appUser, hasMounted } = useAuth();
  const { glucoseData } = useGetGlucoseData({
    glucoseData: { id: glucoseDataId },
  });

  return (
    <>
      <Head>
        <title>Edit glucose data</title>
      </Head>
      {appUser && hasMounted && glucoseData ? (
        <GlucoseDataEditPage appUser={appUser} glucoseData={glucoseData} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

GlucoseDataEdit.getLayout = getAppLayout;

export function getServerSideProps(context) {
  const { glucoseDataId = null } = context.params;
  return { props: { glucoseDataId } };
}

GlucoseDataEdit.propTypes = {
  glucoseDataId: PropTypes.string,
};
GlucoseDataEdit.defaultProps = {
  glucoseDataId: undefined,
};
