import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataInsightsLayout } from 'src/layouts/health-data-insights/HealthDataInsightsLayout/HealthDataInsightsLayout';
import { OxygenDataInsightsGraphPage } from 'src/components/insights';

export default function OxygenDataInsights() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Oxygen Data Insights</title>
      </Head>
      {appUser && hasMounted ? (
        <OxygenDataInsightsGraphPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

OxygenDataInsights.getLayout = getHealthDataInsightsLayout;

OxygenDataInsights.propTypes = {};
OxygenDataInsights.defaultProps = {};
