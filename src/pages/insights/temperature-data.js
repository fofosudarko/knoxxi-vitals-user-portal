import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataInsightsLayout } from 'src/layouts/health-data-insights/HealthDataInsightsLayout/HealthDataInsightsLayout';
import { TemperatureDataInsightsGraphPage } from 'src/components/insights';

export default function TemperatureDataInsights() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Temperature Data Insights</title>
      </Head>
      {appUser && hasMounted ? (
        <TemperatureDataInsightsGraphPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

TemperatureDataInsights.getLayout = getHealthDataInsightsLayout;

TemperatureDataInsights.propTypes = {};
TemperatureDataInsights.defaultProps = {};
