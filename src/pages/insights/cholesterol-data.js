import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataInsightsLayout } from 'src/layouts/health-data-insights/HealthDataInsightsLayout/HealthDataInsightsLayout';
import { CholesterolDataInsightsGraphPage } from 'src/components/insights';

export default function CholesterolDataInsights() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Cholesterol Data Insights</title>
      </Head>
      {appUser && hasMounted ? (
        <CholesterolDataInsightsGraphPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

CholesterolDataInsights.getLayout = getHealthDataInsightsLayout;

CholesterolDataInsights.propTypes = {};
CholesterolDataInsights.defaultProps = {};
