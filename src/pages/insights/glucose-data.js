import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataInsightsLayout } from 'src/layouts/health-data-insights/HealthDataInsightsLayout/HealthDataInsightsLayout';
import { GlucoseDataInsightsGraphPage } from 'src/components/insights';

export default function GlucoseDataInsights() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Glucose Data Insights</title>
      </Head>
      {appUser && hasMounted ? (
        <GlucoseDataInsightsGraphPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

GlucoseDataInsights.getLayout = getHealthDataInsightsLayout;

GlucoseDataInsights.propTypes = {};
GlucoseDataInsights.defaultProps = {};
