import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataInsightsLayout } from 'src/layouts/health-data-insights/HealthDataInsightsLayout/HealthDataInsightsLayout';
import { BPDataInsightsGraphPage } from 'src/components/insights';

export default function BPDataInsights() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>BP Data Insights</title>
      </Head>
      {appUser && hasMounted ? (
        <BPDataInsightsGraphPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

BPDataInsights.getLayout = getHealthDataInsightsLayout;

BPDataInsights.propTypes = {};
BPDataInsights.defaultProps = {};
