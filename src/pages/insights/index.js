import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { InsightsPage } from 'src/components/init';

export default function InsightsIndex() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Insights</title>
      </Head>
      {appUser && hasMounted ? (
        <InsightsPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

InsightsIndex.getLayout = getAppLayout;
