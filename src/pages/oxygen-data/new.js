import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { OxygenDataNewPage } from 'src/components/oxygen-data';

export default function OxygenDataNew() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>New oxygen data</title>
      </Head>
      {appUser && hasMounted ? (
        <OxygenDataNewPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

OxygenDataNew.getLayout = getAppLayout;

OxygenDataNew.propTypes = {};
OxygenDataNew.defaultProps = {};
