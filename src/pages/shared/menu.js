import Head from 'next/head';

import { useHasMounted } from 'src/hooks';
import { getSharedLayout } from 'src/layouts/shared/SharedLayout/SharedLayout';
import { SharedPage } from 'src/components/init';

export default function SharedIndex() {
  const hasMounted = useHasMounted();

  return (
    <>
      <Head>
        <title>Shared menu</title>
      </Head>
      {hasMounted ? <SharedPage /> : <div>Loading...</div>}
    </>
  );
}

SharedIndex.getLayout = getSharedLayout;
