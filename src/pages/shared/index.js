import Head from 'next/head';

import { useHasMounted } from 'src/hooks';
import { getSharedLayout } from 'src/layouts/shared/SharedLayout/SharedLayout';
import { SharedGetPage } from 'src/components/shared';

export default function SharedGet() {
  const hasMounted = useHasMounted();

  return (
    <>
      <Head>
        <title>New shared data</title>
      </Head>
      {hasMounted ? <SharedGetPage /> : <div>Loading...</div>}
    </>
  );
}

SharedGet.getLayout = getSharedLayout;

SharedGet.propTypes = {};
SharedGet.defaultProps = {};
