import Head from 'next/head';

import { useHasMounted } from 'src/hooks';
import { useGetHealthDataSharePublic } from 'src/hooks/api';
import { getSharedLayout } from 'src/layouts/shared/SharedLayout/SharedLayout';
import { SharedTemperatureDataListPage } from 'src/components/temperature-data';

export default function SharedTemperatureDataList() {
  const hasMounted = useHasMounted();
  const { healthDataSharePublic } = useGetHealthDataSharePublic({
    ignoreLoadOnMount: true,
  });

  return (
    <>
      <Head>
        <title>Shared temperature data list</title>
      </Head>
      {hasMounted ? (
        <SharedTemperatureDataListPage
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

SharedTemperatureDataList.getLayout = getSharedLayout;

SharedTemperatureDataList.propTypes = {};
SharedTemperatureDataList.defaultProps = {};
