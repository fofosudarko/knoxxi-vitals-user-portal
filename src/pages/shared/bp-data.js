import Head from 'next/head';

import { useHasMounted } from 'src/hooks';
import { useGetHealthDataSharePublic } from 'src/hooks/api';
import { getSharedLayout } from 'src/layouts/shared/SharedLayout/SharedLayout';
import { SharedBPDataListPage } from 'src/components/bp-data';

export default function SharedBPDataList() {
  const hasMounted = useHasMounted();
  const { healthDataSharePublic } = useGetHealthDataSharePublic({
    ignoreLoadOnMount: true,
  });

  return (
    <>
      <Head>
        <title>Shared BP data list</title>
      </Head>
      {hasMounted ? (
        <SharedBPDataListPage healthDataSharePublic={healthDataSharePublic} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

SharedBPDataList.getLayout = getSharedLayout;

SharedBPDataList.propTypes = {};
SharedBPDataList.defaultProps = {};
