import Head from 'next/head';

import { useHasMounted } from 'src/hooks';
import { useGetHealthDataSharePublic } from 'src/hooks/api';
import { getSharedLayout } from 'src/layouts/shared/SharedLayout/SharedLayout';
import { SharedGlucoseDataListPage } from 'src/components/glucose-data';

export default function SharedGlucoseDataList() {
  const hasMounted = useHasMounted();
  const { healthDataSharePublic } = useGetHealthDataSharePublic({
    ignoreLoadOnMount: true,
  });

  return (
    <>
      <Head>
        <title>Shared glucose data list</title>
      </Head>
      {hasMounted ? (
        <SharedGlucoseDataListPage
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

SharedGlucoseDataList.getLayout = getSharedLayout;

SharedGlucoseDataList.propTypes = {};
SharedGlucoseDataList.defaultProps = {};
