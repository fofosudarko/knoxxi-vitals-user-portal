import Head from 'next/head';

import { useHasMounted } from 'src/hooks';
import { useGetHealthDataSharePublic } from 'src/hooks/api';
import { getSharedLayout } from 'src/layouts/shared/SharedLayout/SharedLayout';
import { SharedWaterDataListPage } from 'src/components/water-data';

export default function SharedWaterDataList() {
  const hasMounted = useHasMounted();
  const { healthDataSharePublic } = useGetHealthDataSharePublic({
    ignoreLoadOnMount: true,
  });

  return (
    <>
      <Head>
        <title>Shared water data list</title>
      </Head>
      {hasMounted ? (
        <SharedWaterDataListPage
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

SharedWaterDataList.getLayout = getSharedLayout;

SharedWaterDataList.propTypes = {};
SharedWaterDataList.defaultProps = {};
