import Head from 'next/head';

import { useHasMounted } from 'src/hooks';
import { useGetHealthDataSharePublic } from 'src/hooks/api';
import { getSharedLayout } from 'src/layouts/shared/SharedLayout/SharedLayout';
import { SharedWeightDataListPage } from 'src/components/weight-data';

export default function SharedWeightDataList() {
  const hasMounted = useHasMounted();
  const { healthDataSharePublic } = useGetHealthDataSharePublic({
    ignoreLoadOnMount: true,
  });

  return (
    <>
      <Head>
        <title>Shared weight data list</title>
      </Head>
      {hasMounted ? (
        <SharedWeightDataListPage
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

SharedWeightDataList.getLayout = getSharedLayout;

SharedWeightDataList.propTypes = {};
SharedWeightDataList.defaultProps = {};
