import Head from 'next/head';

import { useHasMounted } from 'src/hooks';
import { useGetHealthDataSharePublic } from 'src/hooks/api';
import { getSharedLayout } from 'src/layouts/shared/SharedLayout/SharedLayout';
import { SharedOxygenDataListPage } from 'src/components/oxygen-data';

export default function SharedOxygenDataList() {
  const hasMounted = useHasMounted();
  const { healthDataSharePublic } = useGetHealthDataSharePublic({
    ignoreLoadOnMount: true,
  });

  return (
    <>
      <Head>
        <title>Shared oxygen data list</title>
      </Head>
      {hasMounted ? (
        <SharedOxygenDataListPage
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

SharedOxygenDataList.getLayout = getSharedLayout;

SharedOxygenDataList.propTypes = {};
SharedOxygenDataList.defaultProps = {};
