import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getHealthDataLayout } from 'src/layouts/health-data/HealthDataLayout/HealthDataLayout';
import { TemperatureDataListPage } from 'src/components/temperature-data';

export default function TemperatureDataList() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Temperature data list</title>
      </Head>
      {appUser && hasMounted ? (
        <TemperatureDataListPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

TemperatureDataList.getLayout = getHealthDataLayout;

TemperatureDataList.propTypes = {};
TemperatureDataList.defaultProps = {};
