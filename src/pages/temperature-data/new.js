import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getAppLayout } from 'src/layouts/app/AppLayout/AppLayout';
import { TemperatureDataNewPage } from 'src/components/temperature-data';

export default function TemperatureDataNew() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>New temperature data</title>
      </Head>
      {appUser && hasMounted ? (
        <TemperatureDataNewPage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

TemperatureDataNew.getLayout = getAppLayout;

TemperatureDataNew.propTypes = {};
TemperatureDataNew.defaultProps = {};
