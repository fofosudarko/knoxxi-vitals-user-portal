import { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { YUP_WEIGHT_DATA_USER_WEIGHT_VALIDATOR } from 'src/config/validators';
import { useFormReset, useDeviceDimensions } from 'src/hooks';

import { CancelButton, CreateButton, EditButton } from 'src/components/lib';

function WeightDataInput({ weightData, isEditing, onSubmit, processing }) {
  const weightDataInputSchema = Yup({
    userWeight: YUP_WEIGHT_DATA_USER_WEIGHT_VALIDATOR,
  });
  const { isLargeDevice } = useDeviceDimensions();
  const defaultValues = useMemo(
    () => ({
      userWeight: weightData?.userWeight ?? '',
    }),
    [weightData?.userWeight]
  );

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    reset,
  } = useForm({ defaultValues, resolver: yupResolver(weightDataInputSchema) });

  useFormReset({ values: defaultValues, item: weightData, reset });

  const handleSubmitWeightData = useCallback(
    ({ userWeight }) => {
      const newWeightData = {
        userWeight,
      };

      onSubmit && onSubmit(newWeightData);
    },
    [onSubmit]
  );

  const handleCancelWeightData = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitWeightData)} noValidate>
        <Row xs={{ cols: 1 }}>
          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">User weight(kg)</Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="User weight"
                  {...field}
                  isInvalid={!!errors.userWeight}
                />
              )}
              name="userWeight"
              control={control}
            />
            {errors.userWeight ? (
              <Form.Control.Feedback type="invalid">
                {errors.userWeight.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give the user weight e.g. 120 kg</Form.Text>
          </Form.Group>
        </Row>

        <div className="d-flex flex-column-reverse flex-md-row justify-content-end my-3">
          <div className="my-1 my-sm-0 mx-1">
            <CancelButton
              onClick={handleCancelWeightData}
              autoWidth={isLargeDevice}
              text="Cancel"
            />
          </div>
          <div className="my-1 my-sm-0 mx-1">
            {isEditing ? (
              <EditButton
                type="submit"
                clicked={processing}
                text="Edit weight data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            ) : (
              <CreateButton
                type="submit"
                clicked={processing}
                text="Add weight data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            )}
          </div>
        </div>
      </Form>
    </div>
  );
}

WeightDataInput.propTypes = {
  appUser: PropTypes.object,
  weightData: PropTypes.object,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
};
WeightDataInput.defaultProps = {
  appUser: null,
  weightData: null,
  isEditing: false,
  onSubmit: undefined,
  processing: false,
};

export default WeightDataInput;
