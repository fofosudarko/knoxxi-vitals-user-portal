import PropTypes from 'prop-types';

import { GoBack } from 'src/components/lib';
import { SharedWeightDataListListView } from '../SharedWeightDataListList/SharedWeightDataListList';

function SharedWeightDataListPage({ healthDataSharePublic }) {
  const query = { healthDataSharePublic };
  return (
    <div>
      <div className="justify-between">
        <div className="page-title">Shared weight data</div>
        <GoBack />
      </div>
      <div className="page-content">
        <SharedWeightDataListListView
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedWeightDataListPage.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedWeightDataListPage.defaultProps = {
  healthDataSharePublic: null,
};

export default SharedWeightDataListPage;
