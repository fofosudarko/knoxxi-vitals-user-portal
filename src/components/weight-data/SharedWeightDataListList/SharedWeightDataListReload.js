import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetSharedWeightDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function SharedWeightDataListReload({ useTooltip }) {
  const _handleResetSharedWeightDataList = useResetSharedWeightDataList();

  const handleResetSharedWeightDataList = useCallback(() => {
    _handleResetSharedWeightDataList();
  }, [_handleResetSharedWeightDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetSharedWeightDataList}
      text="Reload shared weight data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

SharedWeightDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  healthDataSharePublic: PropTypes.object,
};
SharedWeightDataListReload.defaultProps = {
  useTooltip: false,
  healthDataSharePublic: null,
};

export default SharedWeightDataListReload;
