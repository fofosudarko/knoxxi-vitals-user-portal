import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListSharedWeightDataList } from 'src/hooks/api';

import SharedWeightDataListReload from './SharedWeightDataListReload';
import {
  SharedWeightDataListList,
  SharedWeightDataListListEmpty,
} from './_SharedWeightDataListList';

function SharedWeightDataListListContainer({ healthDataSharePublic, query }) {
  const {
    sharedWeightDataList,
    error: sharedWeightDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListSharedWeightDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (sharedWeightDataListError) {
      handleNotification(sharedWeightDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, sharedWeightDataListError, setError]);

  const handleLoadMoreSharedWeightDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <SharedWeightDataListList
      sharedWeightDataList={sharedWeightDataList}
      onLoadMore={handleLoadMoreSharedWeightDataList}
      loadingText="Loading shared weight data list..."
      healthDataSharePublic={healthDataSharePublic}
      SharedWeightDataListListEmpty={SharedWeightDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function SharedWeightDataListListView({ healthDataSharePublic, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <SharedWeightDataListReload
          useTooltip
          healthDataSharePublic={healthDataSharePublic}
        />
      </div>
      <div className="my-2">
        <SharedWeightDataListListContainer
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedWeightDataListListContainer.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedWeightDataListListContainer.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};
SharedWeightDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedWeightDataList: PropTypes.array,
  SharedWeightDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedWeightDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared weight data list...',
  onLoadMore: undefined,
  sharedWeightDataList: null,
  SharedWeightDataListListEmpty: null,
  isDisabled: false,
};
SharedWeightDataListListView.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedWeightDataListListView.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};

export default SharedWeightDataListList;
