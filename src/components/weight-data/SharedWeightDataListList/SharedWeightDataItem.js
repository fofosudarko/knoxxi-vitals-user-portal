import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
//import { FaEllipsisV } from 'react-icons/fa';

//import { useDeviceDimensions } from 'src/hooks';
import { humanizeDate } from 'src/utils';

export function useWeightDataDetails(weightData) {
  const { createdOn, userWeight } = weightData ?? {};

  return {
    createdOn,
    userWeight,
  };
}

export function SharedWeightDataGridItem({ weightData }) {
  const { createdOn, userWeight } = useWeightDataDetails(weightData);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Created</div>
            <div className="item-subtitle">
              {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Reading(kg)</div>
            <div className="item-subtitle">
              {userWeight !== undefined ? userWeight : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              {/*<WeightDataItemActions
                weightData={weightData}
                healthDataSharePublic={healthDataSharePublic}
              />*/}
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function SharedWeightDataTableItem({ weightData }) {
  const { createdOn, userWeight } = useWeightDataDetails(weightData);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {userWeight !== undefined ? userWeight : 'N/A'}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          {/*<WeightDataItemActions weightData={weightData} healthDataSharePublic={healthDataSharePublic} />*/}
        </div>
      </td>
    </tr>
  );
}

/*export function WeightDataItemActions({ weightData, healthDataSharePublic }) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item>
            <WeightDataRemove weightData={weightData} healthDataSharePublic={healthDataSharePublic} />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}*/

SharedWeightDataGridItem.propTypes = {
  weightData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedWeightDataGridItem.defaultProps = {
  weightData: null,
  healthDataSharePublic: null,
};
SharedWeightDataTableItem.propTypes = {
  weightData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedWeightDataTableItem.defaultProps = {
  weightData: null,
  healthDataSharePublic: null,
};
/*WeightDataItemActions.propTypes = {
  weightData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
WeightDataItemActions.defaultProps = {
  weightData: null,
  healthDataSharePublic: null,
};*/
