import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import {
  SharedWeightDataTableItem,
  SharedWeightDataGridItem,
} from './SharedWeightDataItem';

export function SharedWeightDataListList({
  sharedWeightDataList,
  loadingText,
  healthDataSharePublic,
  onLoadMore,
  SharedWeightDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!sharedWeightDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(SharedWeightDataListListEmpty);

  return sharedWeightDataList.length ? (
    <div>
      {isLargeDevice ? (
        <SharedWeightDataListListTable
          sharedWeightDataList={sharedWeightDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <SharedWeightDataListListGrid
          sharedWeightDataList={sharedWeightDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      )}

      {sharedWeightDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more shared weight data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty healthDataSharePublic={healthDataSharePublic} />
  );
}

export function SharedWeightDataListListGrid({
  sharedWeightDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {sharedWeightDataList.map((item) => (
          <Col key={item.id}>
            <SharedWeightDataGridItem
              weightData={item}
              healthDataSharePublic={healthDataSharePublic}
            />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function SharedWeightDataListListTable({
  sharedWeightDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">Weight(kg)</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {sharedWeightDataList.map((item, index) => (
            <SharedWeightDataTableItem
              weightData={item}
              healthDataSharePublic={healthDataSharePublic}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function SharedWeightDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no shared weight data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

SharedWeightDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedWeightDataList: PropTypes.array,
  SharedWeightDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedWeightDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared weight data list...',
  onLoadMore: undefined,
  sharedWeightDataList: null,
  SharedWeightDataListListEmpty: null,
  isDisabled: false,
};
SharedWeightDataListListGrid.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedWeightDataList: PropTypes.array,
};
SharedWeightDataListListGrid.defaultProps = {
  healthDataSharePublic: null,
  sharedWeightDataList: null,
};
SharedWeightDataListListTable.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedWeightDataList: PropTypes.array,
};
SharedWeightDataListListTable.defaultProps = {
  healthDataSharePublic: null,
  sharedWeightDataList: null,
};
SharedWeightDataListListEmpty.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedWeightDataListListEmpty.defaultProps = {
  healthDataSharePublic: null,
};
