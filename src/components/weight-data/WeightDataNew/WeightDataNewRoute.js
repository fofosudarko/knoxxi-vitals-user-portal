import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { CreateButton } from 'src/components/lib';

export default function WeightDataNewRoute({ text }) {
  const { handleWeightDataNewRoute } = useRoutes().useWeightDataNewRoute();

  return (
    <CreateButton
      variant="primary"
      textColor="white"
      onClick={handleWeightDataNewRoute}
      text={text}
    />
  );
}

WeightDataNewRoute.propTypes = {
  text: PropTypes.string,
};
WeightDataNewRoute.defaultProps = {
  text: 'New weight data',
};
