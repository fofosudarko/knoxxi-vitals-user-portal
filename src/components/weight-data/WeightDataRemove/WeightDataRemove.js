import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemoveWeightData } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function WeightDataRemove({ weightData, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleRemoveWeightData: _handleRemoveWeightData,
    error,
    setError,
    weightDataRemoved,
    setWeightDataRemoved,
  } = useRemoveWeightData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (weightDataRemoved) {
      handleApiResult();
    }
  }, [weightDataRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification('Weight data removed successfully').getAlertNotification()
    );
    setWeightDataRemoved(false);
  }, [handleNotification, setWeightDataRemoved]);

  const handleRemoveWeightData = useCallback(async () => {
    await _handleRemoveWeightData(weightData);
  }, [_handleRemoveWeightData, weightData]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemoveWeightData}
        variant="white"
        text="Remove weight data"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

WeightDataRemove.propTypes = {
  weightData: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
WeightDataRemove.defaultProps = {
  weightData: null,
  appUser: null,
  useTooltip: false,
};

export default WeightDataRemove;
