import PropTypes from 'prop-types';

import { WeightDataListListView } from '../WeightDataListList/WeightDataListList';

function WeightDataListPage({ appUser }) {
  const account = appUser?.account ?? null;
  const query = { userDataId: account?.customerId };
  return (
    <div>
      <div className="page-title">Weight data</div>
      <div className="page-content">
        <WeightDataListListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

WeightDataListPage.propTypes = {
  appUser: PropTypes.object,
};
WeightDataListPage.defaultProps = {
  appUser: null,
};

export default WeightDataListPage;
