import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListWeightDataList } from 'src/hooks/api';

import WeightDataListReload from './WeightDataListReload';
import WeightDataNewRoute from '../WeightDataNew/WeightDataNewRoute';
import {
  WeightDataListList,
  WeightDataListListEmpty,
} from './_WeightDataListList';

function WeightDataListListContainer({ appUser, query }) {
  const {
    weightDataList,
    error: weightDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListWeightDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (weightDataListError) {
      handleNotification(weightDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, weightDataListError, setError]);

  const handleLoadMoreWeightDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <WeightDataListList
      weightDataList={weightDataList}
      onLoadMore={handleLoadMoreWeightDataList}
      loadingText="Loading weight data list..."
      appUser={appUser}
      WeightDataListListEmpty={WeightDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function WeightDataListListView({ appUser, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end my-1">
        <WeightDataNewRoute />
      </div>
      <div className="d-flex justify-content-end">
        <WeightDataListReload useTooltip appUser={appUser} />
      </div>
      <div className="my-2">
        <WeightDataListListContainer appUser={appUser} query={query} />
      </div>
    </div>
  );
}

WeightDataListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
WeightDataListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
WeightDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  weightDataList: PropTypes.array,
  WeightDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
WeightDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading weight data list...',
  onLoadMore: undefined,
  weightDataList: null,
  WeightDataListListEmpty: null,
  isDisabled: false,
};
WeightDataListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
WeightDataListListView.defaultProps = {
  appUser: null,
  query: null,
};

export default WeightDataListList;
