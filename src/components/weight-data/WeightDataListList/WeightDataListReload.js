import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetWeightDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function WeightDataListReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const _handleResetWeightDataList = useResetWeightDataList({
    userDataId: account?.customerId,
  });

  const handleResetWeightDataList = useCallback(() => {
    _handleResetWeightDataList();
  }, [_handleResetWeightDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetWeightDataList}
      text="Reload weight data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

WeightDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
WeightDataListReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default WeightDataListReload;
