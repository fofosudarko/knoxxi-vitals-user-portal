import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCreateWeightData } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';
import { DEFAULT_PARTNERED_BY_ALIAS, DEFAULT_DATA_CHANNEL } from 'src/config';

import { ContentContainer, GoBack } from 'src/components/lib';
import WeightDataInput from '../WeightDataInput/WeightDataInput';

function WeightDataNewPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { handleWeightDataListRoute } = useRoutes().useWeightDataListRoute();
  const {
    handleCreateWeightData: _handleCreateWeightData,
    error,
    setError,
    processing,
    weightDataCreated,
    setWeightDataCreated,
  } = useCreateWeightData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (weightDataCreated) {
      handleApiResult();
    }
  }, [weightDataCreated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Weight data created successfully'
      ).getSuccessNotification(),
      () => {
        setWeightDataCreated(false);
      }
    );
    handleWeightDataListRoute();
  }, [handleNotification, handleWeightDataListRoute, setWeightDataCreated]);

  const handleCreateWeightData = useCallback(
    async (weightData) => {
      const body = {
        ...weightData,
        kycId: account?.customerId,
        mobileNumber: null,
        partneredByAlias: DEFAULT_PARTNERED_BY_ALIAS,
        dataChannel: DEFAULT_DATA_CHANNEL,
      };
      await _handleCreateWeightData(body);
    },
    [_handleCreateWeightData, account?.customerId]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">New weight data</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <WeightDataInput
          appUser={appUser}
          onSubmit={handleCreateWeightData}
          processing={processing}
        />
      </ContentContainer>
    </div>
  );
}

WeightDataNewPage.propTypes = {
  appUser: PropTypes.object,
};
WeightDataNewPage.defaultProps = {
  appUser: null,
};

export default WeightDataNewPage;
