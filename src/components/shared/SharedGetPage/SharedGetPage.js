import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useGetHealthDataSharePublic } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';

import { ContentContainer } from 'src/components/lib';
import SharedInput from '../SharedInput/SharedInput';

function SharedGetPage({ appUser }) {
  const { handleSharedMenuRoute } = useRoutes().useSharedMenuRoute();
  const {
    handleGetHealthDataSharePublic: _handleGetHealthDataSharePublic,
    error,
    setError,
    processing,
    healthDataSharePublicRetrieved,
    setHealthDataSharePublicRetrieved,
  } = useGetHealthDataSharePublic({ ignoreLoadOnMount: true });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (healthDataSharePublicRetrieved) {
      handleApiResult();
    }
  }, [healthDataSharePublicRetrieved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Health data share retrieved successfully'
      ).getSuccessNotification(),
      () => {
        setHealthDataSharePublicRetrieved(false);
      }
    );
    handleSharedMenuRoute();
  }, [
    handleSharedMenuRoute,
    handleNotification,
    setHealthDataSharePublicRetrieved,
  ]);

  const handleGetHealthDataSharePublic = useCallback(
    async (healthDataShare) => {
      const body = {
        ...healthDataShare,
      };
      await _handleGetHealthDataSharePublic(body);
    },
    [_handleGetHealthDataSharePublic]
  );

  return (
    <div>
      <div className="page-title">New shared data</div>
      <ContentContainer widthClass="w-50">
        <SharedInput
          appUser={appUser}
          onSubmit={handleGetHealthDataSharePublic}
          processing={processing}
        />
      </ContentContainer>
    </div>
  );
}

SharedGetPage.propTypes = {
  appUser: PropTypes.object,
};
SharedGetPage.defaultProps = {
  appUser: null,
};

export default SharedGetPage;
