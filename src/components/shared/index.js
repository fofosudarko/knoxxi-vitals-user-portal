import SharedInput from './SharedInput/SharedInput';
import SharedGetPage from './SharedGetPage/SharedGetPage';

export { SharedInput, SharedGetPage };
