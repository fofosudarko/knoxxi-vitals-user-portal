import { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { YUP_HEALTH_DATA_SHARE_PUBLIC_SECURITY_CODE_VALIDATOR } from 'src/config/validators';
import { useFormReset, useDeviceDimensions } from 'src/hooks';

import { CancelButton, CreateButton, EditButton } from 'src/components/lib';

function SharedInput({
  healthDataSharePublic,
  isEditing,
  onSubmit,
  processing,
}) {
  const healthDataSharePublicInputSchema = Yup({
    securityCode: YUP_HEALTH_DATA_SHARE_PUBLIC_SECURITY_CODE_VALIDATOR,
  });
  const { isLargeDevice } = useDeviceDimensions();
  const defaultValues = useMemo(
    () => ({
      securityCode: healthDataSharePublic?.securityCode ?? '',
    }),
    [healthDataSharePublic?.securityCode]
  );

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(healthDataSharePublicInputSchema),
  });

  useFormReset({ values: defaultValues, item: healthDataSharePublic, reset });

  const handleSubmitHealthDataSharePublic = useCallback(
    ({ securityCode }) => {
      const newHealthDataSharePublic = {
        securityCode,
      };

      onSubmit && onSubmit(newHealthDataSharePublic);
    },
    [onSubmit]
  );

  const handleCancelHealthDataSharePublic = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <div>
      <Form
        onSubmit={handleSubmit(handleSubmitHealthDataSharePublic)}
        noValidate
      >
        <Row xs={{ cols: 1 }}>
          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">
              Share security code
            </Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Security code"
                  {...field}
                  isInvalid={!!errors.securityCode}
                />
              )}
              name="securityCode"
              control={control}
            />
            {errors.securityCode ? (
              <Form.Control.Feedback type="invalid">
                {errors.securityCode.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give the share security code e.g. KS000000</Form.Text>
          </Form.Group>
        </Row>

        <div className="d-flex flex-column-reverse flex-md-row justify-content-end my-3">
          <div className="my-1 my-sm-0 mx-1">
            <CancelButton
              onClick={handleCancelHealthDataSharePublic}
              autoWidth={isLargeDevice}
              text="Cancel"
            />
          </div>
          <div className="my-1 my-sm-0 mx-1">
            {isEditing ? (
              <EditButton
                type="submit"
                clicked={processing}
                text="Edit"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            ) : (
              <CreateButton
                type="submit"
                clicked={processing}
                text="Send security code"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            )}
          </div>
        </div>
      </Form>
    </div>
  );
}

SharedInput.propTypes = {
  appUser: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
};
SharedInput.defaultProps = {
  appUser: null,
  healthDataSharePublic: null,
  isEditing: false,
  onSubmit: undefined,
  processing: false,
};

export default SharedInput;
