import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemoveHealthDataShare } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function HealthDataShareRemove({ healthDataShare, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleRemoveHealthDataShare: _handleRemoveHealthDataShare,
    error,
    setError,
    healthDataShareRemoved,
    setHealthDataShareRemoved,
  } = useRemoveHealthDataShare({ appUser, sharedById: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (healthDataShareRemoved) {
      handleApiResult();
    }
  }, [healthDataShareRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Health data share removed successfully'
      ).getAlertNotification()
    );
    setHealthDataShareRemoved(false);
  }, [handleNotification, setHealthDataShareRemoved]);

  const handleRemoveHealthDataShare = useCallback(async () => {
    await _handleRemoveHealthDataShare(healthDataShare);
  }, [_handleRemoveHealthDataShare, healthDataShare]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemoveHealthDataShare}
        variant="white"
        text="Remove health data share"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

HealthDataShareRemove.propTypes = {
  healthDataShare: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
HealthDataShareRemove.defaultProps = {
  healthDataShare: null,
  appUser: null,
  useTooltip: false,
};

export default HealthDataShareRemove;
