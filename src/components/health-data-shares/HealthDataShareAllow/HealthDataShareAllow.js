import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useAllowHealthDataShare } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { CreateButton } from 'src/components/lib';

function HealthDataShareAllow({ healthDataShare, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleAllowHealthDataShare: _handleAllowHealthDataShare,
    error,
    setError,
    healthDataShareUpdated,
    setHealthDataShareUpdated,
  } = useAllowHealthDataShare({ appUser, sharedById: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (healthDataShareUpdated) {
      handleApiResult();
    }
  }, [healthDataShareUpdated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Health data share allowed successfully'
      ).getSuccessNotification()
    );
    setHealthDataShareUpdated(false);
  }, [handleNotification, setHealthDataShareUpdated]);

  const handleAllowHealthDataShare = useCallback(async () => {
    await _handleAllowHealthDataShare(healthDataShare);
  }, [_handleAllowHealthDataShare, healthDataShare]);

  return (
    <div>
      <CreateButton
        onClick={handleAllowHealthDataShare}
        variant="white"
        text="Allow health data share"
        autoWidth={!useTooltip}
        textNormal
        textColor="success"
        useTooltip={useTooltip}
      />
    </div>
  );
}

HealthDataShareAllow.propTypes = {
  healthDataShare: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
HealthDataShareAllow.defaultProps = {
  healthDataShare: null,
  appUser: null,
  useTooltip: false,
};

export default HealthDataShareAllow;
