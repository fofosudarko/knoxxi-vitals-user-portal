import PropTypes from 'prop-types';

import { HealthDataSharesListView } from '../HealthDataSharesList/HealthDataSharesList';

function HealthDataSharesPage({ appUser }) {
  const account = appUser?.account ?? null;
  const query = { sharedById: account?.customerId };
  return (
    <div>
      <div className="page-title">Health data shares</div>
      <div className="page-content">
        <HealthDataSharesListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

HealthDataSharesPage.propTypes = {
  appUser: PropTypes.object,
};
HealthDataSharesPage.defaultProps = {
  appUser: null,
};

export default HealthDataSharesPage;
