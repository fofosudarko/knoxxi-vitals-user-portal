import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCancelHealthDataShare } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function HealthDataShareCancel({ healthDataShare, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleCancelHealthDataShare: _handleCancelHealthDataShare,
    error,
    setError,
    healthDataShareUpdated,
    setHealthDataShareUpdated,
  } = useCancelHealthDataShare({ appUser, sharedById: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (healthDataShareUpdated) {
      handleApiResult();
    }
  }, [healthDataShareUpdated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Health data share cancelled successfully'
      ).getAlertNotification()
    );
    setHealthDataShareUpdated(false);
  }, [handleNotification, setHealthDataShareUpdated]);

  const handleCancelHealthDataShare = useCallback(async () => {
    await _handleCancelHealthDataShare(healthDataShare);
  }, [_handleCancelHealthDataShare, healthDataShare]);

  return (
    <div>
      <RemoveButton
        onClick={handleCancelHealthDataShare}
        variant="white"
        text="Cancel health data share"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

HealthDataShareCancel.propTypes = {
  healthDataShare: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
HealthDataShareCancel.defaultProps = {
  healthDataShare: null,
  appUser: null,
  useTooltip: false,
};

export default HealthDataShareCancel;
