import PropTypes from 'prop-types';
import { Dropdown, Row, Col } from 'react-bootstrap';
import { FaEllipsisV } from 'react-icons/fa';

import { useDeviceDimensions } from 'src/hooks';
import { humanizeDate } from 'src/utils';

import HealthDataShareRemove from '../HealthDataShareRemove/HealthDataShareRemove';
import HealthDataShareCancel from '../HealthDataShareCancel/HealthDataShareCancel';
import HealthDataShareAllow from '../HealthDataShareAllow/HealthDataShareAllow';

export function useHealthDataShareDetails(healthDataShare) {
  const {
    createdOn,
    recipientName,
    recipientType,
    scope,
    category,
    nextDuration,
    nextDurationStartedOn,
    nextDurationEndedOn,
    pastDuration,
    pastDurationStartedOn,
    pastDurationEndedOn,
    securityCode,
    status,
  } = healthDataShare ?? {};

  return {
    createdOn,
    recipientName,
    recipientType,
    scope,
    category,
    nextDuration,
    nextDurationStartedOn,
    nextDurationEndedOn,
    pastDuration,
    pastDurationStartedOn,
    pastDurationEndedOn,
    securityCode,
    status,
  };
}

export function HealthDataShareGridItem({ healthDataShare, appUser }) {
  const {
    createdOn,
    recipientName,
    recipientType,
    scope,
    category,
    nextDuration,
    nextDurationStartedOn,
    nextDurationEndedOn,
    pastDuration,
    pastDurationStartedOn,
    pastDurationEndedOn,
    securityCode,
    status,
  } = useHealthDataShareDetails(healthDataShare);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Created</div>
            <div className="item-subtitle">
              {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Recipient name</div>
            <div className="item-subtitle">
              {recipientName !== undefined ? recipientName : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Recipient type</div>
            <div className="item-subtitle">
              {recipientType !== undefined ? recipientType : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Scope</div>
            <div className="item-subtitle">{scope ? scope : 'N/A'}</div>
          </Col>
          <Col>
            <div className="item-title">Category</div>
            <div className="item-subtitle">{category ? category : 'N/A'}</div>
          </Col>
          <Col>
            <div className="item-title">Next duration</div>
            <div className="item-subtitle">
              {nextDuration ? nextDuration : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Next duration started on</div>
            <div className="item-subtitle">
              {nextDurationStartedOn
                ? humanizeDate(new Date(nextDurationStartedOn))
                : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Next duration ended on</div>
            <div className="item-subtitle">
              {nextDurationEndedOn
                ? humanizeDate(new Date(nextDurationEndedOn))
                : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Past duration</div>
            <div className="item-subtitle">
              {pastDuration ? pastDuration : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Past duration started on</div>
            <div className="item-subtitle">
              {pastDurationStartedOn
                ? humanizeDate(new Date(pastDurationStartedOn))
                : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Past duration ended on</div>
            <div className="item-subtitle">
              {pastDurationEndedOn
                ? humanizeDate(new Date(pastDurationEndedOn))
                : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Security code</div>
            <div className="item-subtitle">
              {securityCode ? securityCode : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Status</div>
            <div className="item-subtitle">{status ? status : 'N/A'}</div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              <HealthDataShareItemActions
                healthDataShare={healthDataShare}
                appUser={appUser}
              />
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function HealthDataShareTableItem({ healthDataShare, appUser }) {
  const {
    createdOn,
    recipientName,
    recipientType,
    scope,
    category,
    nextDuration,
    nextDurationStartedOn,
    nextDurationEndedOn,
    pastDuration,
    pastDurationStartedOn,
    pastDurationEndedOn,
    securityCode,
    status,
  } = useHealthDataShareDetails(healthDataShare);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {recipientName !== undefined ? recipientName : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {recipientType !== undefined ? recipientType : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {scope ? scope : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {category ? category : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {nextDuration ? nextDuration : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {nextDurationStartedOn
            ? humanizeDate(new Date(nextDurationStartedOn))
            : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {nextDurationEndedOn
            ? humanizeDate(new Date(nextDurationEndedOn))
            : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {pastDuration ? pastDuration : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {pastDurationStartedOn
            ? humanizeDate(new Date(pastDurationStartedOn))
            : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {pastDurationEndedOn
            ? humanizeDate(new Date(pastDurationEndedOn))
            : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {securityCode ? securityCode : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {status ? status : 'N/A'}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          <HealthDataShareItemActions
            healthDataShare={healthDataShare}
            appUser={appUser}
          />
        </div>
      </td>
    </tr>
  );
}

export function HealthDataShareItemActions({ healthDataShare, appUser }) {
  const { status } = useHealthDataShareDetails(healthDataShare);
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          {status === 'ALLOWED' ? (
            <Dropdown.Item>
              <HealthDataShareCancel
                healthDataShare={healthDataShare}
                appUser={appUser}
              />
            </Dropdown.Item>
          ) : status === 'CANCELLED' ? (
            <Dropdown.Item>
              <HealthDataShareAllow
                healthDataShare={healthDataShare}
                appUser={appUser}
              />
            </Dropdown.Item>
          ) : null}
          <Dropdown.Item>
            <HealthDataShareRemove
              healthDataShare={healthDataShare}
              appUser={appUser}
            />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

HealthDataShareGridItem.propTypes = {
  healthDataShare: PropTypes.object,
  appUser: PropTypes.object,
};
HealthDataShareGridItem.defaultProps = {
  healthDataShare: null,
  appUser: null,
};
HealthDataShareTableItem.propTypes = {
  healthDataShare: PropTypes.object,
  appUser: PropTypes.object,
};
HealthDataShareTableItem.defaultProps = {
  healthDataShare: null,
  appUser: null,
};
HealthDataShareItemActions.propTypes = {
  healthDataShare: PropTypes.object,
  appUser: PropTypes.object,
};
HealthDataShareItemActions.defaultProps = {
  healthDataShare: null,
  appUser: null,
};
