import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetHealthDataShares } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function HealthDataSharesReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const _handleResetHealthDataShares = useResetHealthDataShares({
    sharedById: account?.customerId,
  });

  const handleResetHealthDataShares = useCallback(() => {
    _handleResetHealthDataShares();
  }, [_handleResetHealthDataShares]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetHealthDataShares}
      text="Reload health data shares"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

HealthDataSharesReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
HealthDataSharesReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default HealthDataSharesReload;
