import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import {
  HealthDataShareTableItem,
  HealthDataShareGridItem,
} from './HealthDataShareItem';

export function HealthDataSharesList({
  healthDataShares,
  loadingText,
  appUser,
  onLoadMore,
  HealthDataSharesListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!healthDataShares) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(HealthDataSharesListEmpty);

  return healthDataShares.length ? (
    <div>
      {isLargeDevice ? (
        <HealthDataSharesListTable
          healthDataShares={healthDataShares}
          appUser={appUser}
        />
      ) : (
        <HealthDataSharesListGrid
          healthDataShares={healthDataShares}
          appUser={appUser}
        />
      )}

      {healthDataShares.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more health data shares"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function HealthDataSharesListGrid({ healthDataShares, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {healthDataShares.map((item) => (
          <Col key={item.id}>
            <HealthDataShareGridItem healthDataShare={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function HealthDataSharesListTable({ healthDataShares, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">Recipient name</th>
            <th className="list-table-head-cell">Recipient type</th>
            <th className="list-table-head-cell">Scope</th>
            <th className="list-table-head-cell">Category</th>
            <th className="list-table-head-cell">Next duration</th>
            <th className="list-table-head-cell">Next duration started on</th>
            <th className="list-table-head-cell">Next duration ended on</th>
            <th className="list-table-head-cell">Past duration</th>
            <th className="list-table-head-cell">Past duration started on</th>
            <th className="list-table-head-cell">Past duration ended on</th>
            <th className="list-table-head-cell">Security code</th>
            <th className="list-table-head-cell">Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {healthDataShares.map((item, index) => (
            <HealthDataShareTableItem
              healthDataShare={item}
              appUser={appUser}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function HealthDataSharesListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there are no health data shares yet.
          </div>
        </div>
      </div>
    </div>
  );
}

HealthDataSharesList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  healthDataShares: PropTypes.array,
  HealthDataSharesListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
HealthDataSharesList.defaultProps = {
  appUser: null,
  loadingText: 'Loading health data shares...',
  onLoadMore: undefined,
  healthDataShares: null,
  HealthDataSharesListEmpty: null,
  isDisabled: false,
};
HealthDataSharesListGrid.propTypes = {
  appUser: PropTypes.object,
  healthDataShares: PropTypes.array,
};
HealthDataSharesListGrid.defaultProps = {
  appUser: null,
  healthDataShares: null,
};
HealthDataSharesListTable.propTypes = {
  appUser: PropTypes.object,
  healthDataShares: PropTypes.array,
};
HealthDataSharesListTable.defaultProps = {
  appUser: null,
  healthDataShares: null,
};
HealthDataSharesListEmpty.propTypes = {
  appUser: PropTypes.object,
};
HealthDataSharesListEmpty.defaultProps = {
  appUser: null,
};
