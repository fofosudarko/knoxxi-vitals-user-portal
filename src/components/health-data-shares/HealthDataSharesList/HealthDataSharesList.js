import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListHealthDataShares, useSearchItems } from 'src/hooks/api';

import HealthDataSharesReload from './HealthDataSharesReload';
import {
  HealthDataSharesList,
  HealthDataSharesListEmpty,
} from './_HealthDataSharesList';
import HealthDataSharesSearch from './HealthDataSharesSearch';

function HealthDataSharesListContainer({ appUser, query }) {
  const {
    healthDataShares,
    error: healthDataSharesError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListHealthDataShares({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (healthDataSharesError) {
      handleNotification(healthDataSharesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, healthDataSharesError, setError]);

  const handleLoadMoreHealthDataShares = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <HealthDataSharesList
      healthDataShares={healthDataShares}
      onLoadMore={handleLoadMoreHealthDataShares}
      loadingText="Loading health data shares..."
      appUser={appUser}
      HealthDataSharesListEmpty={HealthDataSharesListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function HealthDataSharesListView({ appUser, query }) {
  const { isSearch, handleSearch } = useSearchItems();
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <HealthDataSharesReload useTooltip appUser={appUser} />
      </div>
      <div>
        <HealthDataSharesSearch
          query={query}
          onSearch={handleSearch}
          appUser={appUser}
        />
      </div>
      {!isSearch ? (
        <div className="my-2">
          <HealthDataSharesListContainer appUser={appUser} query={query} />
        </div>
      ) : null}
    </div>
  );
}

HealthDataSharesListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
HealthDataSharesListContainer.defaultProps = {
  appUser: null,
  query: null,
};
HealthDataSharesList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  healthDataShares: PropTypes.array,
  HealthDataSharesListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
HealthDataSharesList.defaultProps = {
  appUser: null,
  loadingText: 'Loading health data shares...',
  onLoadMore: undefined,
  healthDataShares: null,
  HealthDataSharesListEmpty: null,
  isDisabled: false,
};
HealthDataSharesListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
HealthDataSharesListView.defaultProps = {
  appUser: null,
  query: null,
};

export default HealthDataSharesList;
