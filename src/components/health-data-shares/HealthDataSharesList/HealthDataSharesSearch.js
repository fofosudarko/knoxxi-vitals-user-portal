import { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { useNotificationHandler } from 'src/hooks';
import { useSelectItems } from 'src/hooks/api';

import { FilterContainer } from 'src/components/lib';
import {
  HealthDataSharesList,
  HealthDataSharesListEmpty,
} from './_HealthDataSharesList';
import {
  HealthDataShareScopeSelect,
  HealthDataShareRecipientTypeSelect,
  HealthDataShareCategorySelect,
} from 'src/components/enums';

function HealthDataSharesSearch({ query, onSearch, appUser }) {
  const [{ category, scope = null, recipientType = null }, setSearchTerms] =
    useState({});

  useEffect(() => {
    onSearch && onSearch(category || scope || recipientType);
  }, [onSearch, scope, category, recipientType]);

  const handleSubmitSearchTerms = useCallback((searchTerms) => {
    setSearchTerms(searchTerms);
  }, []);

  return (
    <div>
      <FilterContainer>
        <HealthDataSharesSearchInput onSubmit={handleSubmitSearchTerms} />
      </FilterContainer>
      {category || scope || recipientType ? (
        <div className="my-2">
          <HealthDataSharesSearchOutput
            category={category}
            scope={scope}
            recipientType={recipientType}
            query={query}
            appUser={appUser}
          />
        </div>
      ) : null}
    </div>
  );
}

function HealthDataSharesSearchInput({ onSubmit }) {
  const healthDataSharesSearchInputSchema = Yup({});

  const {
    formState: { errors },
    control,
    watch,
  } = useForm({
    resolver: yupResolver(healthDataSharesSearchInputSchema),
  });

  const watchedCategory = watch('category');
  const watchedScope = watch('scope');
  const watchedRecipientType = watch('recipientType');

  useEffect(() => {
    const category = watchedCategory,
      scope = watchedScope,
      recipientType = watchedRecipientType;
    onSubmit && onSubmit({ category, scope, recipientType });
  }, [onSubmit, watchedScope, watchedRecipientType, watchedCategory]);

  return (
    <div>
      <Row xs={{ cols: 1 }} className="gx-2">
        <Col xs={{ span: 12 }} lg={{ span: 4 }}>
          <Form.Group className="my-1">
            <Controller
              name="scope"
              control={control}
              render={({ field }) => (
                <HealthDataShareScopeSelect
                  hideLabel
                  isClearable
                  placeholder="Search by scope"
                  helpText=""
                  errors={errors}
                  {...field}
                />
              )}
            />
          </Form.Group>
        </Col>
        <Col xs={{ span: 12 }} lg={{ span: 4 }}>
          <Form.Group className="my-1">
            <Controller
              name="recipientType"
              control={control}
              render={({ field }) => (
                <HealthDataShareRecipientTypeSelect
                  hideLabel
                  placeholder="Search by recipient type"
                  helpText=""
                  errors={errors}
                  isClearable
                  {...field}
                />
              )}
            />
          </Form.Group>
        </Col>
        <Col xs={{ span: 12 }} lg={{ span: 4 }}>
          <Form.Group className="my-1">
            <Controller
              name="category"
              control={control}
              render={({ field }) => (
                <HealthDataShareCategorySelect
                  hideLabel
                  placeholder="Search by category"
                  helpText=""
                  errors={errors}
                  isClearable
                  {...field}
                />
              )}
            />
          </Form.Group>
        </Col>
      </Row>
    </div>
  );
}

function HealthDataSharesSearchOutput({
  category,
  scope,
  recipientType,
  query,
  appUser,
}) {
  const account = appUser?.account ?? null;
  const {
    error,
    setError,
    healthDataShares,
    setHealthDataShares,
    handleListHealthDataShares,
  } = useSelectItems();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    (async () => {
      await handleListHealthDataShares({
        ...query,
        category,
        scope,
        recipientType,
        sharedById: account?.customerId,
      });
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query, category, scope, recipientType]);

  useEffect(() => {
    return () => {
      setHealthDataShares(null);
    };
  }, [setHealthDataShares]);

  return (
    <HealthDataSharesList
      healthDataShares={healthDataShares}
      loadingText="Searching content personalizations..."
      HealthDataSharesListEmpty={HealthDataSharesListEmpty}
      isSearch
      appUser={appUser}
    />
  );
}

HealthDataSharesSearch.propTypes = {
  onSearch: PropTypes.func,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
HealthDataSharesSearch.defaultProps = {
  onSearch: undefined,
  query: null,
  appUser: null,
};
HealthDataSharesSearchOutput.propTypes = {
  mobileNumber: PropTypes.string,
  category: PropTypes.string,
  scope: PropTypes.string,
  recipientType: PropTypes.string,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
HealthDataSharesSearchOutput.defaultProps = {
  mobileNumber: undefined,
  category: undefined,
  scope: undefined,
  recipientType: undefined,
  query: null,
  appUser: null,
};
HealthDataSharesSearchInput.propTypes = {
  appUser: PropTypes.object,
  onSubmit: PropTypes.func,
};
HealthDataSharesSearchInput.defaultProps = {
  appUser: null,
  onSubmit: undefined,
};

export default HealthDataSharesSearch;
