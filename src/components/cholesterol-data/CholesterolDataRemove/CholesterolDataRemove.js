import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemoveCholesterolData } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function CholesterolDataRemove({ cholesterolData, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleRemoveCholesterolData: _handleRemoveCholesterolData,
    error,
    setError,
    cholesterolDataRemoved,
    setCholesterolDataRemoved,
  } = useRemoveCholesterolData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (cholesterolDataRemoved) {
      handleApiResult();
    }
  }, [cholesterolDataRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Cholesterol data removed successfully'
      ).getAlertNotification()
    );
    setCholesterolDataRemoved(false);
  }, [handleNotification, setCholesterolDataRemoved]);

  const handleRemoveCholesterolData = useCallback(async () => {
    await _handleRemoveCholesterolData(cholesterolData);
  }, [_handleRemoveCholesterolData, cholesterolData]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemoveCholesterolData}
        variant="white"
        text="Remove cholesterol data"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

CholesterolDataRemove.propTypes = {
  cholesterolData: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
CholesterolDataRemove.defaultProps = {
  cholesterolData: null,
  appUser: null,
  useTooltip: false,
};

export default CholesterolDataRemove;
