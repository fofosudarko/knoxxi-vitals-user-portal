import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName, round } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function CholesterolDataCumulativeAveragesLdlCholesterolNumberCard({
  cholesterolDataCumulativeAveragesLdlCholesterol,
  label,
  onClick,
}) {
  const ldlCholesterolReading =
    cholesterolDataCumulativeAveragesLdlCholesterol?.ldlCholesterolReading !==
    undefined
      ? round(
          cholesterolDataCumulativeAveragesLdlCholesterol?.ldlCholesterolReading,
          2
        )
      : 'N/A';
  const ldlCholesterolDataSeverity =
    cholesterolDataCumulativeAveragesLdlCholesterol?.ldlCholesterolDataSeverity ??
    {};
  return (
    <NumberCard
      value={ldlCholesterolReading}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(
        ldlCholesterolDataSeverity.name
      )}
      onClick={onClick}
    />
  );
}

CholesterolDataCumulativeAveragesLdlCholesterolNumberCard.propTypes = {
  cholesterolDataCumulativeAveragesLdlCholesterol: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
CholesterolDataCumulativeAveragesLdlCholesterolNumberCard.defaultProps = {
  cholesterolDataCumulativeAveragesLdlCholesterol: null,
  label: null,
  onClick: undefined,
};

export default CholesterolDataCumulativeAveragesLdlCholesterolNumberCard;
