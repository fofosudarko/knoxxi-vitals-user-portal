import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName, round } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function CholesterolDataCumulativeAveragesHdlCholesterolNumberCard({
  cholesterolDataCumulativeAveragesHdlCholesterol,
  label,
  onClick,
}) {
  const hdlCholesterolReading =
    cholesterolDataCumulativeAveragesHdlCholesterol?.hdlCholesterolReading !==
    undefined
      ? round(
          cholesterolDataCumulativeAveragesHdlCholesterol.hdlCholesterolReading,
          2
        )
      : 'N/A';
  const hdlCholesterolDataSeverity =
    cholesterolDataCumulativeAveragesHdlCholesterol?.hdlCholesterolDataSeverity ??
    {};
  return (
    <NumberCard
      value={hdlCholesterolReading}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(
        hdlCholesterolDataSeverity.name
      )}
      onClick={onClick}
    />
  );
}

CholesterolDataCumulativeAveragesHdlCholesterolNumberCard.propTypes = {
  cholesterolDataCumulativeAveragesHdlCholesterol: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
CholesterolDataCumulativeAveragesHdlCholesterolNumberCard.defaultProps = {
  cholesterolDataCumulativeAveragesHdlCholesterol: null,
  label: null,
  onClick: undefined,
};

export default CholesterolDataCumulativeAveragesHdlCholesterolNumberCard;
