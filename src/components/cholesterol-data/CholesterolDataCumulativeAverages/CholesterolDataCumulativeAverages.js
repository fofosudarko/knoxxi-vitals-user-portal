import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { useNotificationHandler } from 'src/hooks';
import { useListCholesterolDataCumulativeAverages } from 'src/hooks/api';

import CholesterolDataCumulativeAveragesTotalCholesterolNumberCard from './CholesterolDataCumulativeAveragesTotalCholesterolNumberCard';
import CholesterolDataCumulativeAveragesLdlCholesterolNumberCard from './CholesterolDataCumulativeAveragesLdlCholesterolNumberCard';
import CholesterolDataCumulativeAveragesHdlCholesterolNumberCard from './CholesterolDataCumulativeAveragesHdlCholesterolNumberCard';
import CholesterolDataCumulativeAveragesTriglyceridesCholesterolNumberCard from './CholesterolDataCumulativeAveragesTriglyceridesCholesterolNumberCard';

function CholesterolDataCumulativeAveragesContainer({
  appUser,
  query,
  label,
  isCard,
  onClick,
}) {
  const {
    cholesterolDataCumulativeAverages,
    error: cholesterolDataCumulativeAveragesError,
    setError,
  } = useListCholesterolDataCumulativeAverages({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (cholesterolDataCumulativeAveragesError) {
      handleNotification(cholesterolDataCumulativeAveragesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, cholesterolDataCumulativeAveragesError, setError]);

  return (
    <CholesterolDataCumulativeAverages
      cholesterolDataCumulativeAverages={cholesterolDataCumulativeAverages}
      label={label}
      appUser={appUser}
      isDisabled={
        cholesterolDataCumulativeAverages &&
        cholesterolDataCumulativeAverages.length === 0
      }
      query={query}
      isCard={isCard}
      onClick={onClick}
    />
  );
}

function CholesterolDataCumulativeAverages({
  cholesterolDataCumulativeAverages,
  appUser,
  isDisabled,
  isCard,
  onClick,
}) {
  let cholesterolDataCumulativeAveragesTotalCholesterol = null,
    cholesterolDataCumulativeAveragesLdlCholesterol = null,
    cholesterolDataCumulativeAveragesHdlCholesterol = null,
    cholesterolDataCumulativeAveragesTriglyceridesCholesterol = null;

  if (cholesterolDataCumulativeAverages?.length) {
    const {
      totalCholesterolReading,
      ldlCholesterolReading,
      hdlCholesterolReading,
      triglyceridesCholesterolReading,
      totalCholesterolDataInterpretation,
      totalCholesterolDataSeverity,
      ldlCholesterolDataInterpretation,
      ldlCholesterolDataSeverity,
      hdlCholesterolDataInterpretation,
      hdlCholesterolDataSeverity,
      triglyceridesCholesterolDataInterpretation,
      triglyceridesCholesterolDataSeverity,
    } = cholesterolDataCumulativeAverages[0] ?? {};
    cholesterolDataCumulativeAveragesTotalCholesterol = {
      totalCholesterolReading,
      totalCholesterolDataInterpretation,
      totalCholesterolDataSeverity,
    };
    cholesterolDataCumulativeAveragesLdlCholesterol = {
      ldlCholesterolReading,
      ldlCholesterolDataInterpretation,
      ldlCholesterolDataSeverity,
    };
    cholesterolDataCumulativeAveragesHdlCholesterol = {
      hdlCholesterolReading,
      hdlCholesterolDataInterpretation,
      hdlCholesterolDataSeverity,
    };
    cholesterolDataCumulativeAveragesTriglyceridesCholesterol = {
      triglyceridesCholesterolReading,
      triglyceridesCholesterolDataInterpretation,
      triglyceridesCholesterolDataSeverity,
    };
  }

  return (
    <div>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 2 }}
        className="gx-3 gy-3"
      >
        <Col>
          {isCard ? (
            <CholesterolDataCumulativeAveragesTotalCholesterolNumberCard
              isDisabled={isDisabled}
              cholesterolDataCumulativeAveragesTotalCholesterol={
                cholesterolDataCumulativeAveragesTotalCholesterol
              }
              label="Total cholesterol(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <CholesterolDataCumulativeAveragesLdlCholesterolNumberCard
              isDisabled={isDisabled}
              cholesterolDataCumulativeAveragesLdlCholesterol={
                cholesterolDataCumulativeAveragesLdlCholesterol
              }
              label="LDL cholesterol(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <CholesterolDataCumulativeAveragesHdlCholesterolNumberCard
              isDisabled={isDisabled}
              cholesterolDataCumulativeAveragesHdlCholesterol={
                cholesterolDataCumulativeAveragesHdlCholesterol
              }
              label="HDL cholesterol(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <CholesterolDataCumulativeAveragesTriglyceridesCholesterolNumberCard
              isDisabled={isDisabled}
              cholesterolDataCumulativeAveragesTriglyceridesCholesterol={
                cholesterolDataCumulativeAveragesTriglyceridesCholesterol
              }
              label="Triglycerides cholesterol(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
      </Row>
    </div>
  );
}

export function CholesterolDataCumulativeAveragesNumberCardView({
  appUser,
  query,
  label,
  onClick,
}) {
  return (
    <div className="d-block">
      <CholesterolDataCumulativeAveragesContainer
        appUser={appUser}
        query={query}
        label={label}
        isCard
        onClick={onClick}
      />
    </div>
  );
}

CholesterolDataCumulativeAveragesContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
CholesterolDataCumulativeAveragesContainer.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  isCard: false,
  onClick: undefined,
};
CholesterolDataCumulativeAverages.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  query: PropTypes.object,
  cholesterolDataCumulativeAverages: PropTypes.array,
  isDisabled: PropTypes.bool,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
CholesterolDataCumulativeAverages.defaultProps = {
  appUser: null,
  label: undefined,
  query: null,
  cholesterolDataCumulativeAverages: undefined,
  isDisabled: false,
  isCard: false,
  onClick: undefined,
};
CholesterolDataCumulativeAveragesNumberCardView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
CholesterolDataCumulativeAveragesNumberCardView.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  onClick: undefined,
};

export default CholesterolDataCumulativeAverages;
