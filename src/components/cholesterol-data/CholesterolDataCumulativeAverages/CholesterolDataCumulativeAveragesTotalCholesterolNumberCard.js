import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName, round } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function CholesterolDataCumulativeAveragesTotalCholesterolNumberCard({
  cholesterolDataCumulativeAveragesTotalCholesterol,
  label,
  onClick,
}) {
  const totalCholesterolReading =
    cholesterolDataCumulativeAveragesTotalCholesterol?.totalCholesterolReading !==
    undefined
      ? round(
          cholesterolDataCumulativeAveragesTotalCholesterol.totalCholesterolReading,
          2
        )
      : 'N/A';
  const totalCholesterolDataSeverity =
    cholesterolDataCumulativeAveragesTotalCholesterol?.totalCholesterolDataSeverity ??
    {};
  return (
    <NumberCard
      value={totalCholesterolReading}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(
        totalCholesterolDataSeverity.name
      )}
      onClick={onClick}
    />
  );
}

CholesterolDataCumulativeAveragesTotalCholesterolNumberCard.propTypes = {
  cholesterolDataCumulativeAveragesTotalCholesterol: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
CholesterolDataCumulativeAveragesTotalCholesterolNumberCard.defaultProps = {
  cholesterolDataCumulativeAveragesTotalCholesterol: null,
  label: null,
  onClick: undefined,
};

export default CholesterolDataCumulativeAveragesTotalCholesterolNumberCard;
