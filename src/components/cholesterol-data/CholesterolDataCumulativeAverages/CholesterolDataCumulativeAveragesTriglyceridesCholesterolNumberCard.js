import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName, round } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function CholesterolDataCumulativeAveragesTriglyceridesCholesterolNumberCard({
  cholesterolDataCumulativeAveragesTriglyceridesCholesterol,
  label,
  onClick,
}) {
  const triglyceridesCholesterolReading =
    cholesterolDataCumulativeAveragesTriglyceridesCholesterol?.triglyceridesCholesterolReading !==
    undefined
      ? round(
          cholesterolDataCumulativeAveragesTriglyceridesCholesterol.triglyceridesCholesterolReading,
          2
        )
      : 'N/A';
  const triglyceridesCholesterolDataSeverity =
    cholesterolDataCumulativeAveragesTriglyceridesCholesterol?.triglyceridesCholesterolDataSeverity ??
    {};
  return (
    <NumberCard
      value={triglyceridesCholesterolReading}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(
        triglyceridesCholesterolDataSeverity.name
      )}
      onClick={onClick}
    />
  );
}

CholesterolDataCumulativeAveragesTriglyceridesCholesterolNumberCard.propTypes =
  {
    cholesterolDataCumulativeAveragesTriglyceridesCholesterol: PropTypes.object,
    label: PropTypes.string,
    onClick: PropTypes.func,
  };
CholesterolDataCumulativeAveragesTriglyceridesCholesterolNumberCard.defaultProps =
  {
    cholesterolDataCumulativeAveragesTriglyceridesCholesterol: null,
    label: null,
    onClick: undefined,
  };

export default CholesterolDataCumulativeAveragesTriglyceridesCholesterolNumberCard;
