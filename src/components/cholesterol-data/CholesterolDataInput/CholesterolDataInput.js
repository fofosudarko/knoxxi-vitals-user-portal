import { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import {
  YUP_CHOLESTEROL_DATA_TOTAL_CHOLESTEROL_READING_VALIDATOR,
  YUP_CHOLESTEROL_DATA_HDL_CHOLESTEROL_READING_VALIDATOR,
  YUP_CHOLESTEROL_DATA_LDL_CHOLESTEROL_READING_VALIDATOR,
  YUP_CHOLESTEROL_DATA_TRIGLYCERIDES_CHOLESTEROL_READING_VALIDATOR,
} from 'src/config/validators';
import { useFormReset, useDeviceDimensions } from 'src/hooks';

import { CancelButton, CreateButton, EditButton } from 'src/components/lib';

function CholesterolDataInput({
  cholesterolData,
  isEditing,
  onSubmit,
  processing,
}) {
  const cholesterolDataInputSchema = Yup({
    totalCholesterolReading:
      YUP_CHOLESTEROL_DATA_TOTAL_CHOLESTEROL_READING_VALIDATOR,
    hdlCholesterolReading:
      YUP_CHOLESTEROL_DATA_HDL_CHOLESTEROL_READING_VALIDATOR,
    ldlCholesterolReading:
      YUP_CHOLESTEROL_DATA_LDL_CHOLESTEROL_READING_VALIDATOR,
    triglyceridesCholesterolReading:
      YUP_CHOLESTEROL_DATA_TRIGLYCERIDES_CHOLESTEROL_READING_VALIDATOR,
  });
  const { isLargeDevice } = useDeviceDimensions();
  const defaultValues = useMemo(
    () => ({
      totalCholesterolReading: cholesterolData?.totalCholesterolReading ?? '',
      hdlCholesterolReading: cholesterolData?.hdlCholesterolReading ?? '',
      ldlCholesterolReading: cholesterolData?.ldlCholesterolReading ?? '',
      triglyceridesCholesterolReading:
        cholesterolData?.triglyceridesCholesterolReading ?? '',
    }),
    [
      cholesterolData?.totalCholesterolReading,
      cholesterolData?.hdlCholesterolReading,
      cholesterolData?.ldlCholesterolReading,
      cholesterolData?.triglyceridesCholesterolReading,
    ]
  );

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(cholesterolDataInputSchema),
  });

  useFormReset({ values: defaultValues, item: cholesterolData, reset });

  const handleSubmitCholesterolData = useCallback(
    ({
      totalCholesterolReading,
      ldlCholesterolReading,
      hdlCholesterolReading,
      triglyceridesCholesterolReading,
    }) => {
      const newCholesterolData = {
        totalCholesterolReading,
        ldlCholesterolReading,
        hdlCholesterolReading,
        triglyceridesCholesterolReading,
      };

      onSubmit && onSubmit(newCholesterolData);
    },
    [onSubmit]
  );

  const handleCancelCholesterolData = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitCholesterolData)} noValidate>
        <Row xs={{ cols: 1 }}>
          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">
              Total cholesterol reading(mmol/L)
            </Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Total cholesterol reading"
                  {...field}
                  isInvalid={!!errors.totalCholesterolReading}
                />
              )}
              name="totalCholesterolReading"
              control={control}
            />
            {errors.totalCholesterolReading ? (
              <Form.Control.Feedback type="invalid">
                {errors.totalCholesterolReading.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>
              Give the total cholesterol reading e.g. 12 mmol/L
            </Form.Text>
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">
              HDL cholesterol reading(mmol/L)
            </Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="HDL cholesterol reading"
                  {...field}
                  isInvalid={!!errors.hdlCholesterolReading}
                />
              )}
              name="hdlCholesterolReading"
              control={control}
            />
            {errors.hdlCholesterolReading ? (
              <Form.Control.Feedback type="invalid">
                {errors.hdlCholesterolReading.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>
              Give the HDL cholesterol reading e.g. 12 mmol/L
            </Form.Text>
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">
              LDL cholesterol reading(mmol/L)
            </Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="LDL cholesterol reading"
                  {...field}
                  isInvalid={!!errors.ldlCholesterolReading}
                />
              )}
              name="ldlCholesterolReading"
              control={control}
            />
            {errors.ldlCholesterolReading ? (
              <Form.Control.Feedback type="invalid">
                {errors.ldlCholesterolReading.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>
              Give the LDL cholesterol reading e.g. 12 mmol/L
            </Form.Text>
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">
              Triglycerides cholesterol reading(mmol/L)
            </Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Triglycerides cholesterol reading"
                  {...field}
                  isInvalid={!!errors.triglyceridesCholesterolReading}
                />
              )}
              name="triglyceridesCholesterolReading"
              control={control}
            />
            {errors.triglyceridesCholesterolReading ? (
              <Form.Control.Feedback type="invalid">
                {errors.triglyceridesCholesterolReading.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>
              Give the triglycerides cholesterol reading e.g. 12 mmol/L
            </Form.Text>
          </Form.Group>
        </Row>

        <div className="d-flex flex-column-reverse flex-md-row justify-content-end my-3">
          <div className="my-1 my-sm-0 mx-1">
            <CancelButton
              onClick={handleCancelCholesterolData}
              autoWidth={isLargeDevice}
              text="Cancel"
            />
          </div>
          <div className="my-1 my-sm-0 mx-1">
            {isEditing ? (
              <EditButton
                type="submit"
                clicked={processing}
                text="Edit cholesterol data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            ) : (
              <CreateButton
                type="submit"
                clicked={processing}
                text="Add cholesterol data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            )}
          </div>
        </div>
      </Form>
    </div>
  );
}

CholesterolDataInput.propTypes = {
  appUser: PropTypes.object,
  cholesterolData: PropTypes.object,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
};
CholesterolDataInput.defaultProps = {
  appUser: null,
  cholesterolData: null,
  isEditing: false,
  onSubmit: undefined,
  processing: false,
};

export default CholesterolDataInput;
