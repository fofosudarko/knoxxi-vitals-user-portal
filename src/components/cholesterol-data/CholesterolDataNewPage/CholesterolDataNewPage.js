import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCreateCholesterolData } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';
import { DEFAULT_PARTNERED_BY_ALIAS, DEFAULT_DATA_CHANNEL } from 'src/config';

import { ContentContainer, GoBack } from 'src/components/lib';
import CholesterolDataInput from '../CholesterolDataInput/CholesterolDataInput';

function CholesterolDataNewPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { handleCholesterolDataListRoute } =
    useRoutes().useCholesterolDataListRoute();
  const {
    handleCreateCholesterolData: _handleCreateCholesterolData,
    error,
    setError,
    processing,
    cholesterolDataCreated,
    setCholesterolDataCreated,
  } = useCreateCholesterolData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (cholesterolDataCreated) {
      handleApiResult();
    }
  }, [cholesterolDataCreated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Cholesterol data created successfully'
      ).getSuccessNotification(),
      () => {
        setCholesterolDataCreated(false);
      }
    );
    handleCholesterolDataListRoute();
  }, [
    handleCholesterolDataListRoute,
    handleNotification,
    setCholesterolDataCreated,
  ]);

  const handleCreateCholesterolData = useCallback(
    async (cholesterolData) => {
      const body = {
        ...cholesterolData,
        kycId: account?.customerId,
        mobileNumber: null,
        partneredByAlias: DEFAULT_PARTNERED_BY_ALIAS,
        dataChannel: DEFAULT_DATA_CHANNEL,
      };
      await _handleCreateCholesterolData(body);
    },
    [_handleCreateCholesterolData, account?.customerId]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">New cholesterol data</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <CholesterolDataInput
          appUser={appUser}
          onSubmit={handleCreateCholesterolData}
          processing={processing}
        />
      </ContentContainer>
    </div>
  );
}

CholesterolDataNewPage.propTypes = {
  appUser: PropTypes.object,
};
CholesterolDataNewPage.defaultProps = {
  appUser: null,
};

export default CholesterolDataNewPage;
