import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { CreateButton } from 'src/components/lib';

export default function CholesterolDataNewRoute({ text }) {
  const { handleCholesterolDataNewRoute } =
    useRoutes().useCholesterolDataNewRoute();

  return (
    <CreateButton
      variant="primary"
      textColor="white"
      onClick={handleCholesterolDataNewRoute}
      text={text}
    />
  );
}

CholesterolDataNewRoute.propTypes = {
  text: PropTypes.string,
};
CholesterolDataNewRoute.defaultProps = {
  text: 'New cholesterol data',
};
