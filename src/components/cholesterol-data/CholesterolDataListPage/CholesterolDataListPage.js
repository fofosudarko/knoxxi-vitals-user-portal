import PropTypes from 'prop-types';

import { CholesterolDataListListView } from '../CholesterolDataListList/CholesterolDataListList';

function CholesterolDataListPage({ appUser }) {
  const account = appUser?.account ?? null;
  const query = { userDataId: account?.customerId };
  return (
    <div>
      <div className="page-title">Cholesterol data</div>
      <div className="page-content">
        <CholesterolDataListListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

CholesterolDataListPage.propTypes = {
  appUser: PropTypes.object,
};
CholesterolDataListPage.defaultProps = {
  appUser: null,
};

export default CholesterolDataListPage;
