import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListCholesterolDataList } from 'src/hooks/api';

import CholesterolDataListReload from './CholesterolDataListReload';
import CholesterolDataNewRoute from '../CholesterolDataNew/CholesterolDataNewRoute';
import {
  CholesterolDataListList,
  CholesterolDataListListEmpty,
} from './_CholesterolDataListList';

function CholesterolDataListListContainer({ appUser, query }) {
  const {
    cholesterolDataList,
    error: cholesterolDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListCholesterolDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (cholesterolDataListError) {
      handleNotification(cholesterolDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, cholesterolDataListError, setError]);

  const handleLoadMoreCholesterolDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <CholesterolDataListList
      cholesterolDataList={cholesterolDataList}
      onLoadMore={handleLoadMoreCholesterolDataList}
      loadingText="Loading cholesterol data list..."
      appUser={appUser}
      CholesterolDataListListEmpty={CholesterolDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function CholesterolDataListListView({ appUser, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end my-1">
        <CholesterolDataNewRoute />
      </div>
      <div className="d-flex justify-content-end">
        <CholesterolDataListReload useTooltip appUser={appUser} />
      </div>
      <div className="my-2">
        <CholesterolDataListListContainer appUser={appUser} query={query} />
      </div>
    </div>
  );
}

CholesterolDataListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
CholesterolDataListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
CholesterolDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  cholesterolDataList: PropTypes.array,
  CholesterolDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
CholesterolDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading cholesterol data list...',
  onLoadMore: undefined,
  cholesterolDataList: null,
  CholesterolDataListListEmpty: null,
  isDisabled: false,
};
CholesterolDataListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
CholesterolDataListListView.defaultProps = {
  appUser: null,
  query: null,
};

export default CholesterolDataListList;
