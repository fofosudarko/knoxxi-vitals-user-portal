import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetCholesterolDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function CholesterolDataListReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const _handleResetCholesterolDataList = useResetCholesterolDataList({
    userDataId: account?.customerId,
  });

  const handleResetCholesterolDataList = useCallback(() => {
    _handleResetCholesterolDataList();
  }, [_handleResetCholesterolDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetCholesterolDataList}
      text="Reload cholesterol data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

CholesterolDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
CholesterolDataListReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default CholesterolDataListReload;
