import PropTypes from 'prop-types';
import { Dropdown, Row, Col, Badge } from 'react-bootstrap';
import { FaEllipsisV } from 'react-icons/fa';

import { useDeviceDimensions } from 'src/hooks';
import { humanizeDate, getVariantFromHealthDataSeverityName } from 'src/utils';

import CholesterolDataRemove from '../CholesterolDataRemove/CholesterolDataRemove';

export function useCholesterolDataDetails(cholesterolData) {
  const {
    createdOn,
    totalCholesterolReading,
    hdlCholesterolReading,
    ldlCholesterolReading,
    triglyceridesCholesterolReading,
    totalCholesterolDataSeverity,
    hdlCholesterolDataSeverity,
    ldlCholesterolDataSeverity,
    triglyceridesCholesterolDataSeverity,
    totalCholesterolDataInterpretation = null,
    hdlCholesterolDataInterpretation = null,
    ldlCholesterolDataInterpretation = null,
    triglyceridesCholesterolDataInterpretation = null,
  } = cholesterolData ?? {};
  const {
    name: totalCholesterolDataInterpretationName,
    description: totalCholesterolDataInterpretationDescription,
    explanation: totalCholesterolDataInterpretationExplanation,
  } = totalCholesterolDataInterpretation ?? {};
  const {
    name: hdlCholesterolDataInterpretationName,
    description: hdlCholesterolDataInterpretationDescription,
    explanation: hdlCholesterolDataInterpretationExplanation,
  } = hdlCholesterolDataInterpretation ?? {};
  const {
    name: ldlCholesterolDataInterpretationName,
    description: ldlCholesterolDataInterpretationDescription,
    explanation: ldlCholesterolDataInterpretationExplanation,
  } = ldlCholesterolDataInterpretation ?? {};
  const {
    name: triglyceridesCholesterolDataInterpretationName,
    description: triglyceridesCholesterolDataInterpretationDescription,
    explanation: triglyceridesCholesterolDataInterpretationExplanation,
  } = triglyceridesCholesterolDataInterpretation ?? {};

  return {
    createdOn,
    totalCholesterolReading,
    hdlCholesterolReading,
    ldlCholesterolReading,
    triglyceridesCholesterolReading,
    totalCholesterolDataSeverity,
    hdlCholesterolDataSeverity,
    ldlCholesterolDataSeverity,
    triglyceridesCholesterolDataSeverity,
    totalCholesterolDataInterpretationName,
    totalCholesterolDataInterpretationDescription,
    totalCholesterolDataInterpretationExplanation,
    hdlCholesterolDataInterpretationName,
    hdlCholesterolDataInterpretationDescription,
    hdlCholesterolDataInterpretationExplanation,
    ldlCholesterolDataInterpretationName,
    ldlCholesterolDataInterpretationDescription,
    ldlCholesterolDataInterpretationExplanation,
    triglyceridesCholesterolDataInterpretationName,
    triglyceridesCholesterolDataInterpretationDescription,
    triglyceridesCholesterolDataInterpretationExplanation,
  };
}

export function CholesterolDataGridItem({ cholesterolData, appUser }) {
  const {
    createdOn,
    totalCholesterolReading,
    hdlCholesterolReading,
    ldlCholesterolReading,
    triglyceridesCholesterolReading,
    totalCholesterolDataSeverity,
    hdlCholesterolDataSeverity,
    ldlCholesterolDataSeverity,
    triglyceridesCholesterolDataSeverity,
    totalCholesterolDataInterpretationName,
    totalCholesterolDataInterpretationDescription,
    totalCholesterolDataInterpretationExplanation,
    hdlCholesterolDataInterpretationName,
    hdlCholesterolDataInterpretationDescription,
    hdlCholesterolDataInterpretationExplanation,
    ldlCholesterolDataInterpretationName,
    ldlCholesterolDataInterpretationDescription,
    ldlCholesterolDataInterpretationExplanation,
    triglyceridesCholesterolDataInterpretationName,
    triglyceridesCholesterolDataInterpretationDescription,
    triglyceridesCholesterolDataInterpretationExplanation,
  } = useCholesterolDataDetails(cholesterolData);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Created</div>
            <div className="item-subtitle">
              {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Total cholesterol reading(mmol/L)</div>
            <div className="item-subtitle fw-bold">
              <span style={{ color: totalCholesterolDataSeverity.colorCode }}>
                {totalCholesterolReading !== undefined
                  ? totalCholesterolReading
                  : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">Severity(Total cholesterol)</div>
            <div>
              <Badge
                bg={getVariantFromHealthDataSeverityName(
                  totalCholesterolDataSeverity.name
                )}
              >
                {totalCholesterolDataSeverity.name}
              </Badge>
            </div>
          </Col>
          <Col>
            <div className="item-title">Interpretation(Total cholesterol)</div>
            <div className="item-subtitle">
              {totalCholesterolDataInterpretationName !== undefined ? (
                <span>
                  <span
                    style={{ color: totalCholesterolDataSeverity.colorCode }}
                  >
                    <div className="text-truncate">
                      {totalCholesterolDataInterpretationName}
                    </div>
                  </span>
                  : {totalCholesterolDataInterpretationDescription},{' '}
                  {totalCholesterolDataInterpretationExplanation}
                </span>
              ) : (
                'N/A'
              )}
            </div>
          </Col>
          <Col>
            <div className="item-title">HDL cholesterol reading(mmol/L)</div>
            <div className="item-subtitle fw-bold">
              <span style={{ color: hdlCholesterolDataSeverity.colorCode }}>
                {hdlCholesterolReading !== undefined
                  ? hdlCholesterolReading
                  : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">Severity(HDL cholesterol)</div>
            <div>
              <Badge
                bg={getVariantFromHealthDataSeverityName(
                  hdlCholesterolDataSeverity.name
                )}
              >
                {hdlCholesterolDataSeverity.name}
              </Badge>
            </div>
          </Col>
          <Col>
            <div className="item-title">Interpretation(HDL cholesterol)</div>
            <div className="item-subtitle">
              {hdlCholesterolDataInterpretationName !== undefined ? (
                <span>
                  <span
                    style={{
                      color: hdlCholesterolDataSeverity.colorCode,
                    }}
                  >
                    <div className="text-truncate">
                      {hdlCholesterolDataInterpretationName}
                    </div>
                  </span>
                  : {hdlCholesterolDataInterpretationDescription},{' '}
                  {hdlCholesterolDataInterpretationExplanation}
                </span>
              ) : (
                'N/A'
              )}
            </div>
          </Col>
          <Col>
            <div className="item-title">LDL cholesterol reading(mmol/L)</div>
            <div className="item-subtitle fw-bold">
              <span style={{ color: ldlCholesterolDataSeverity.colorCode }}>
                {ldlCholesterolReading !== undefined
                  ? ldlCholesterolReading
                  : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">Severity(LDL cholesterol)</div>
            <div>
              <Badge
                bg={getVariantFromHealthDataSeverityName(
                  ldlCholesterolDataSeverity.name
                )}
              >
                {ldlCholesterolDataSeverity.name}
              </Badge>
            </div>
          </Col>
          <Col>
            <div className="item-title">Interpretation(LDL cholesterol)</div>
            <div className="item-subtitle">
              {ldlCholesterolDataInterpretationName !== undefined ? (
                <span>
                  <span style={{ color: ldlCholesterolDataSeverity.colorCode }}>
                    <div className="text-truncate">
                      {ldlCholesterolDataInterpretationName}
                    </div>
                  </span>
                  : {ldlCholesterolDataInterpretationDescription},{' '}
                  {ldlCholesterolDataInterpretationExplanation}
                </span>
              ) : (
                'N/A'
              )}
            </div>
          </Col>
          <Col>
            <div className="item-title">
              Triglycerides cholesterol reading(mmol/L)
            </div>
            <div className="item-subtitle fw-bold">
              <span
                style={{
                  color: triglyceridesCholesterolDataSeverity.colorCode,
                }}
              >
                {triglyceridesCholesterolReading !== undefined
                  ? triglyceridesCholesterolReading
                  : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">
              Severity(Triglycerides cholesterol)
            </div>
            <div>
              <Badge
                bg={getVariantFromHealthDataSeverityName(
                  triglyceridesCholesterolDataSeverity.name
                )}
              >
                {triglyceridesCholesterolDataSeverity.name}
              </Badge>
            </div>
          </Col>
          <Col>
            <div className="item-title">
              Interpretation(Triglycerides cholesterol)
            </div>
            <div className="item-subtitle">
              {triglyceridesCholesterolDataInterpretationName !== undefined ? (
                <span>
                  <span
                    style={{
                      color: triglyceridesCholesterolDataSeverity.colorCode,
                    }}
                  >
                    <div className="text-truncate">
                      {triglyceridesCholesterolDataInterpretationName}
                    </div>
                  </span>
                  : {triglyceridesCholesterolDataInterpretationDescription},{' '}
                  {triglyceridesCholesterolDataInterpretationExplanation}
                </span>
              ) : (
                'N/A'
              )}
            </div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              <CholesterolDataItemActions
                cholesterolData={cholesterolData}
                appUser={appUser}
              />
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function CholesterolDataTableItem({ cholesterolData, appUser }) {
  const {
    createdOn,
    totalCholesterolReading,
    hdlCholesterolReading,
    ldlCholesterolReading,
    triglyceridesCholesterolReading,
    totalCholesterolDataSeverity,
    hdlCholesterolDataSeverity,
    ldlCholesterolDataSeverity,
    triglyceridesCholesterolDataSeverity,
    totalCholesterolDataInterpretationName,
    totalCholesterolDataInterpretationDescription,
    totalCholesterolDataInterpretationExplanation,
    hdlCholesterolDataInterpretationName,
    hdlCholesterolDataInterpretationDescription,
    hdlCholesterolDataInterpretationExplanation,
    ldlCholesterolDataInterpretationName,
    ldlCholesterolDataInterpretationDescription,
    ldlCholesterolDataInterpretationExplanation,
    triglyceridesCholesterolDataInterpretationName,
    triglyceridesCholesterolDataInterpretationDescription,
    triglyceridesCholesterolDataInterpretationExplanation,
  } = useCholesterolDataDetails(cholesterolData);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span style={{ color: totalCholesterolDataSeverity.colorCode }}>
            {totalCholesterolReading !== undefined
              ? totalCholesterolReading
              : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <Badge
          bg={getVariantFromHealthDataSeverityName(
            totalCholesterolDataSeverity.name
          )}
        >
          {totalCholesterolDataSeverity.name}
        </Badge>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {totalCholesterolDataInterpretationName !== undefined ? (
            <span>
              <span style={{ color: totalCholesterolDataSeverity.colorCode }}>
                {totalCholesterolDataInterpretationName}
              </span>
              : {totalCholesterolDataInterpretationDescription},{' '}
              {totalCholesterolDataInterpretationExplanation}
            </span>
          ) : (
            'N/A'
          )}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span style={{ color: hdlCholesterolDataSeverity.colorCode }}>
            {hdlCholesterolReading !== undefined
              ? hdlCholesterolReading
              : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <Badge
          bg={getVariantFromHealthDataSeverityName(
            hdlCholesterolDataSeverity.name
          )}
        >
          {hdlCholesterolDataSeverity.name}
        </Badge>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {hdlCholesterolDataInterpretationName !== undefined ? (
            <span>
              <span style={{ color: hdlCholesterolDataSeverity.colorCode }}>
                {hdlCholesterolDataInterpretationName}
              </span>
              : {hdlCholesterolDataInterpretationDescription},{' '}
              {hdlCholesterolDataInterpretationExplanation}
            </span>
          ) : (
            'N/A'
          )}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span style={{ color: ldlCholesterolDataSeverity.colorCode }}>
            {ldlCholesterolReading !== undefined
              ? ldlCholesterolReading
              : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <Badge
          bg={getVariantFromHealthDataSeverityName(
            ldlCholesterolDataSeverity.name
          )}
        >
          {ldlCholesterolDataSeverity.name}
        </Badge>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {ldlCholesterolDataInterpretationName !== undefined ? (
            <span>
              <span style={{ color: ldlCholesterolDataSeverity.colorCode }}>
                {ldlCholesterolDataInterpretationName}
              </span>
              : {ldlCholesterolDataInterpretationDescription},{' '}
              {ldlCholesterolDataInterpretationExplanation}
            </span>
          ) : (
            'N/A'
          )}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span
            style={{ color: triglyceridesCholesterolDataSeverity.colorCode }}
          >
            {triglyceridesCholesterolReading !== undefined
              ? triglyceridesCholesterolReading
              : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <Badge
          bg={getVariantFromHealthDataSeverityName(
            triglyceridesCholesterolDataSeverity.name
          )}
        >
          {triglyceridesCholesterolDataSeverity.name}
        </Badge>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {triglyceridesCholesterolDataInterpretationName !== undefined ? (
            <span>
              <span
                style={{
                  color: triglyceridesCholesterolDataSeverity.colorCode,
                }}
              >
                {triglyceridesCholesterolDataInterpretationName}
              </span>
              : {triglyceridesCholesterolDataInterpretationDescription},{' '}
              {triglyceridesCholesterolDataInterpretationExplanation}
            </span>
          ) : (
            'N/A'
          )}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          <CholesterolDataItemActions
            cholesterolData={cholesterolData}
            appUser={appUser}
          />
        </div>
      </td>
    </tr>
  );
}

export function CholesterolDataItemActions({ cholesterolData, appUser }) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item>
            <CholesterolDataRemove
              cholesterolData={cholesterolData}
              appUser={appUser}
            />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

CholesterolDataGridItem.propTypes = {
  cholesterolData: PropTypes.object,
  appUser: PropTypes.object,
};
CholesterolDataGridItem.defaultProps = {
  cholesterolData: null,
  appUser: null,
};
CholesterolDataTableItem.propTypes = {
  cholesterolData: PropTypes.object,
  appUser: PropTypes.object,
};
CholesterolDataTableItem.defaultProps = {
  cholesterolData: null,
  appUser: null,
};
CholesterolDataItemActions.propTypes = {
  cholesterolData: PropTypes.object,
  appUser: PropTypes.object,
};
CholesterolDataItemActions.defaultProps = {
  cholesterolData: null,
  appUser: null,
};
