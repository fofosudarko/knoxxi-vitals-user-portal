import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function CholesterolDataTimedAveragesTriglyceridesCholesterolListChartCard({
  cholesterolDataTimedAveragesTriglyceridesCholesterolList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          cholesterolDataTimedAveragesTriglyceridesCholesterolList?.map(
            (item) => item.createdOn
          ) ?? [],
        datasets: [
          {
            label,
            data:
              cholesterolDataTimedAveragesTriglyceridesCholesterolList?.map(
                (item) => item.triglyceridesCholesterolReading
              ) ?? [],
            backgroundColor:
              cholesterolDataTimedAveragesTriglyceridesCholesterolList?.map(
                (item) => item.triglyceridesCholesterolDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [cholesterolDataTimedAveragesTriglyceridesCholesterolList, label]);
  return cholesterolDataTimedAveragesTriglyceridesCholesterolList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

CholesterolDataTimedAveragesTriglyceridesCholesterolListChartCard.propTypes = {
  cholesterolDataTimedAveragesTriglyceridesCholesterolList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
CholesterolDataTimedAveragesTriglyceridesCholesterolListChartCard.defaultProps =
  {
    cholesterolDataTimedAveragesTriglyceridesCholesterolList: null,
    appUser: null,
    label: null,
    isDisabled: false,
    onClick: undefined,
  };

export default CholesterolDataTimedAveragesTriglyceridesCholesterolListChartCard;
