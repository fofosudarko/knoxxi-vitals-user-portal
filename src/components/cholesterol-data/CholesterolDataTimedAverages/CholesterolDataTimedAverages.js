import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { useNotificationHandler } from 'src/hooks';
import { useListCholesterolDataTimedAverages } from 'src/hooks/api';

import CholesterolDataTimedAveragesTotalCholesterolListChartCard from './CholesterolDataTimedAveragesTotalCholesterolListChartCard';
import CholesterolDataTimedAveragesLdlCholesterolListChartCard from './CholesterolDataTimedAveragesLdlCholesterolListChartCard';
import CholesterolDataTimedAveragesHdlCholesterolListChartCard from './CholesterolDataTimedAveragesHdlCholesterolListChartCard';
import CholesterolDataTimedAveragesTriglyceridesCholesterolListChartCard from './CholesterolDataTimedAveragesTriglyceridesCholesterolListChartCard';

function CholesterolDataTimedAveragesContainer({
  appUser,
  query,
  label,
  isCard,
  onClick,
}) {
  const {
    cholesterolDataTimedAverages,
    error: cholesterolDataTimedAveragesError,
    setError,
  } = useListCholesterolDataTimedAverages({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (cholesterolDataTimedAveragesError) {
      handleNotification(cholesterolDataTimedAveragesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, cholesterolDataTimedAveragesError, setError]);

  return (
    <CholesterolDataTimedAverages
      cholesterolDataTimedAverages={cholesterolDataTimedAverages}
      label={label}
      appUser={appUser}
      isDisabled={
        cholesterolDataTimedAverages &&
        cholesterolDataTimedAverages.length === 0
      }
      query={query}
      isCard={isCard}
      onClick={onClick}
    />
  );
}

function CholesterolDataTimedAverages({
  cholesterolDataTimedAverages,
  appUser,
  isDisabled,
  isCard,
  onClick,
}) {
  const cholesterolDataTimedAveragesTotalCholesterolList = [],
    cholesterolDataTimedAveragesLdlCholesterolList = [],
    cholesterolDataTimedAveragesHdlCholesterolList = [],
    cholesterolDataTimedAveragesTriglyceridesCholesterolList = [];

  cholesterolDataTimedAverages?.forEach((cholesterolDataTimedAverage) => {
    const {
      createdOn,
      totalCholesterolReading,
      ldlCholesterolReading,
      hdlCholesterolReading,
      triglyceridesCholesterolReading,
      totalCholesterolDataInterpretation,
      totalCholesterolDataSeverity,
      ldlCholesterolDataInterpretation,
      ldlCholesterolDataSeverity,
      hdlCholesterolDataInterpretation,
      hdlCholesterolDataSeverity,
      triglyceridesCholesterolDataInterpretation,
      triglyceridesCholesterolDataSeverity,
    } = cholesterolDataTimedAverage;
    cholesterolDataTimedAveragesTotalCholesterolList.push({
      createdOn,
      totalCholesterolReading,
      totalCholesterolDataInterpretation,
      totalCholesterolDataSeverity,
    });
    cholesterolDataTimedAveragesLdlCholesterolList.push({
      createdOn,
      ldlCholesterolReading,
      ldlCholesterolDataInterpretation,
      ldlCholesterolDataSeverity,
    });
    cholesterolDataTimedAveragesHdlCholesterolList.push({
      createdOn,
      hdlCholesterolReading,
      hdlCholesterolDataInterpretation,
      hdlCholesterolDataSeverity,
    });
    cholesterolDataTimedAveragesTriglyceridesCholesterolList.push({
      createdOn,
      triglyceridesCholesterolReading,
      triglyceridesCholesterolDataInterpretation,
      triglyceridesCholesterolDataSeverity,
    });
  });

  return (
    <div>
      <Row
        xs={{ cols: 1 }}
        //md={{ cols: 2 }}
        //lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        <Col>
          {isCard ? (
            <CholesterolDataTimedAveragesTotalCholesterolListChartCard
              isDisabled={isDisabled}
              cholesterolDataTimedAveragesTotalCholesterolList={
                cholesterolDataTimedAveragesTotalCholesterolList
              }
              label="Total cholesterol(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <CholesterolDataTimedAveragesLdlCholesterolListChartCard
              isDisabled={isDisabled}
              cholesterolDataTimedAveragesLdlCholesterolList={
                cholesterolDataTimedAveragesLdlCholesterolList
              }
              label="LDL cholesterol(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <CholesterolDataTimedAveragesHdlCholesterolListChartCard
              isDisabled={isDisabled}
              cholesterolDataTimedAveragesHdlCholesterolList={
                cholesterolDataTimedAveragesHdlCholesterolList
              }
              label="HDL cholesterol(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <CholesterolDataTimedAveragesTriglyceridesCholesterolListChartCard
              isDisabled={isDisabled}
              cholesterolDataTimedAveragesTriglyceridesCholesterolList={
                cholesterolDataTimedAveragesTriglyceridesCholesterolList
              }
              label="Triglycerides cholesterol(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
      </Row>
    </div>
  );
}

export function CholesterolDataTimedAveragesChartCardView({
  appUser,
  query,
  label,
  onClick,
}) {
  return (
    <div className="d-block">
      <CholesterolDataTimedAveragesContainer
        appUser={appUser}
        query={query}
        label={label}
        isCard
        onClick={onClick}
      />
    </div>
  );
}

CholesterolDataTimedAveragesContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
CholesterolDataTimedAveragesContainer.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  isCard: false,
  onClick: undefined,
};
CholesterolDataTimedAverages.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  query: PropTypes.object,
  cholesterolDataTimedAverages: PropTypes.array,
  isDisabled: PropTypes.bool,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
CholesterolDataTimedAverages.defaultProps = {
  appUser: null,
  label: undefined,
  query: null,
  cholesterolDataTimedAverages: undefined,
  isDisabled: false,
  isCard: false,
  onClick: undefined,
};
CholesterolDataTimedAveragesChartCardView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
CholesterolDataTimedAveragesChartCardView.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  onClick: undefined,
};

export default CholesterolDataTimedAverages;
