import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function CholesterolDataTimedAveragesTotalCholesterolListChartCard({
  cholesterolDataTimedAveragesTotalCholesterolList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          cholesterolDataTimedAveragesTotalCholesterolList?.map(
            (item) => item.createdOn
          ) ?? [],
        datasets: [
          {
            label,
            data:
              cholesterolDataTimedAveragesTotalCholesterolList?.map(
                (item) => item.totalCholesterolReading
              ) ?? [],
            backgroundColor:
              cholesterolDataTimedAveragesTotalCholesterolList?.map(
                (item) => item.totalCholesterolDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [cholesterolDataTimedAveragesTotalCholesterolList, label]);
  return cholesterolDataTimedAveragesTotalCholesterolList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

CholesterolDataTimedAveragesTotalCholesterolListChartCard.propTypes = {
  cholesterolDataTimedAveragesTotalCholesterolList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
CholesterolDataTimedAveragesTotalCholesterolListChartCard.defaultProps = {
  cholesterolDataTimedAveragesTotalCholesterolList: null,
  appUser: null,
  label: null,
  isDisabled: false,
  onClick: undefined,
};

export default CholesterolDataTimedAveragesTotalCholesterolListChartCard;
