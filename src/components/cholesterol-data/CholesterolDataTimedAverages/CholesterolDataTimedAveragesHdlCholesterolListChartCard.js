import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function CholesterolDataTimedAveragesHdlCholesterolListChartCard({
  cholesterolDataTimedAveragesHdlCholesterolList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          cholesterolDataTimedAveragesHdlCholesterolList?.map(
            (item) => item.createdOn
          ) ?? [],
        datasets: [
          {
            label,
            data:
              cholesterolDataTimedAveragesHdlCholesterolList?.map(
                (item) => item.hdlCholesterolReading
              ) ?? [],
            backgroundColor:
              cholesterolDataTimedAveragesHdlCholesterolList?.map(
                (item) => item.hdlCholesterolDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [cholesterolDataTimedAveragesHdlCholesterolList, label]);
  return cholesterolDataTimedAveragesHdlCholesterolList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

CholesterolDataTimedAveragesHdlCholesterolListChartCard.propTypes = {
  cholesterolDataTimedAveragesHdlCholesterolList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
CholesterolDataTimedAveragesHdlCholesterolListChartCard.defaultProps = {
  cholesterolDataTimedAveragesHdlCholesterolList: null,
  appUser: null,
  label: null,
  isDisabled: false,
  onClick: undefined,
};

export default CholesterolDataTimedAveragesHdlCholesterolListChartCard;
