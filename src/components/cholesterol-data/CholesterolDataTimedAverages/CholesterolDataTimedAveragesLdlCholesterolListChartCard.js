import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function CholesterolDataTimedAveragesLdlCholesterolListChartCard({
  cholesterolDataTimedAveragesLdlCholesterolList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          cholesterolDataTimedAveragesLdlCholesterolList?.map(
            (item) => item.createdOn
          ) ?? [],
        datasets: [
          {
            label,
            data:
              cholesterolDataTimedAveragesLdlCholesterolList?.map(
                (item) => item.ldlCholesterolReading
              ) ?? [],
            backgroundColor:
              cholesterolDataTimedAveragesLdlCholesterolList?.map(
                (item) => item.ldlCholesterolDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [cholesterolDataTimedAveragesLdlCholesterolList, label]);
  return cholesterolDataTimedAveragesLdlCholesterolList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

CholesterolDataTimedAveragesLdlCholesterolListChartCard.propTypes = {
  cholesterolDataTimedAveragesLdlCholesterolList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
CholesterolDataTimedAveragesLdlCholesterolListChartCard.defaultProps = {
  cholesterolDataTimedAveragesLdlCholesterolList: null,
  appUser: null,
  label: null,
  isDisabled: false,
  onClick: undefined,
};

export default CholesterolDataTimedAveragesLdlCholesterolListChartCard;
