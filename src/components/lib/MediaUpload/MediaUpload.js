import { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import MediaUploadCreate from './MediaUploadCreate';
import MediaUploadEdit from './MediaUploadEdit';
import MediaUploadShow from './MediaUploadShow';
import MediaUploadRemove from './MediaUploadRemove';

function MediaUpload({
  media: _media,
  onSubmit,
  onRemove,
  createText,
  createLabel,
  editText,
  editLabel,
  removeText,
  mediaHeight,
  mediaWidth,
  notCentered,
  fullWidth,
}) {
  const [media, setMedia] = useState(null);

  useEffect(() => {
    if (_media) {
      setMedia(_media);
    }

    return () => {
      setMedia(null);
    };
  }, [_media]);

  useEffect(() => {
    if (media) {
      onSubmit && onSubmit(media);
    }
  }, [media, onSubmit]);

  const handleReceiveMedia = useCallback((media) => {
    setMedia(media);
  }, []);

  const handleRemoveMedia = useCallback(() => {
    onRemove && onRemove();
    setMedia(null);
  }, [onRemove]);

  return (
    <div className="my-3">
      {media ? (
        <>
          <MediaUploadShow
            media={media}
            mediaHeight={mediaHeight}
            mediaWidth={mediaWidth}
          />
          <MediaUploadEdit
            media={media}
            onReceiveMedia={handleReceiveMedia}
            editLabel={editLabel}
            editText={editText}
          />
          <MediaUploadRemove
            media={media}
            onRemoveMedia={handleRemoveMedia}
            removeText={removeText}
          />
        </>
      ) : (
        <MediaUploadCreate
          onReceiveMedia={handleReceiveMedia}
          createText={createText}
          notCentered={notCentered}
          fullWidth={fullWidth}
          createLabel={createLabel}
        />
      )}
    </div>
  );
}

MediaUpload.propTypes = {
  media: PropTypes.object,
  onSubmit: PropTypes.func,
  onRemove: PropTypes.func,
  createText: PropTypes.string,
  createLabel: PropTypes.string,
  editText: PropTypes.string,
  editLabel: PropTypes.string,
  removeText: PropTypes.string,
  mediaHeight: PropTypes.number,
  mediaWidth: PropTypes.number,
  notCentered: PropTypes.bool,
  fullWidth: PropTypes.bool,
};
MediaUpload.defaultProps = {
  media: null,
  onSubmit: undefined,
  onRemove: undefined,
  createText: 'Upload new media',
  createLabel: 'Upload new media',
  editText: 'Upload new media',
  editLabel: 'Upload new media',
  removeText: 'Upload new media',
  mediaHeight: 400,
  mediaWidth: 400,
  notCentered: false,
  fullWidth: false,
};

export default MediaUpload;
