import { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCreateMedia } from 'src/hooks/api';

import { CreateButton } from 'src/components/lib';
import MediaUploadInput from './MediaUploadInput';

function MediaUploadCreate({
  createText,
  createLabel,
  onReceiveMedia,
  notCentered,
  fullWidth,
}) {
  const {
    handleCreateMedia,
    processing,
    error,
    setError,
    media,
    setMedia,
    mediaCreated,
    setMediaCreated,
  } = useCreateMedia();
  const handleNotification = useNotificationHandler();
  let handleHide;

  const [showMediaUploadCreate, setShowMediaUploadCreate] = useState(false);

  useEffect(() => {
    if (error) {
      handleNotification(error);
      handleHide();
    }
    return () => {
      setError(null);
    };
  }, [error, handleNotification, handleHide, setError]);

  useEffect(() => {
    if (media && mediaCreated) {
      handleHide();
    }
  }, [mediaCreated, handleHide, media]);

  const handleShowMediaUploadCreate = useCallback(() => {
    setShowMediaUploadCreate((state) => !state);
  }, []);

  handleHide = useCallback(() => {
    onReceiveMedia && onReceiveMedia(media);
    setShowMediaUploadCreate(false);
    setMediaCreated(false);
    setMedia(null);
  }, [media, onReceiveMedia, setMedia, setMediaCreated]);

  const handleMediaUploadCreate = useCallback(
    async (files) => {
      const formData = new FormData();
      formData.append('media', files[0]);
      await handleCreateMedia(formData);
    },
    [handleCreateMedia]
  );

  return (
    <div>
      {showMediaUploadCreate ? (
        <MediaUploadInput
          onSubmit={handleMediaUploadCreate}
          processing={processing}
          label={createLabel}
        />
      ) : (
        <div
          className={`${
            !fullWidth
              ? !notCentered
                ? 'justify-center'
                : 'justify-start'
              : null
          }`}
        >
          <CreateButton
            variant="light"
            onClick={handleShowMediaUploadCreate}
            textColor="dark"
            text={createText}
          />
        </div>
      )}
    </div>
  );
}

MediaUploadCreate.propTypes = {
  createText: PropTypes.string,
  onReceiveMedia: PropTypes.func,
  notCentered: PropTypes.bool,
  fullWidth: PropTypes.bool,
  createLabel: PropTypes.string,
};
MediaUploadCreate.defaultProps = {
  createText: 'Upload new media',
  onReceiveMedia: undefined,
  notCentered: false,
  fullWidth: false,
  createLabel: 'Create media',
};

export default MediaUploadCreate;
