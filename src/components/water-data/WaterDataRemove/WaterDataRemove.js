import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemoveWaterData } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function WaterDataRemove({ waterData, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleRemoveWaterData: _handleRemoveWaterData,
    error,
    setError,
    waterDataRemoved,
    setWaterDataRemoved,
  } = useRemoveWaterData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (waterDataRemoved) {
      handleApiResult();
    }
  }, [waterDataRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification('Water data removed successfully').getAlertNotification()
    );
    setWaterDataRemoved(false);
  }, [handleNotification, setWaterDataRemoved]);

  const handleRemoveWaterData = useCallback(async () => {
    await _handleRemoveWaterData(waterData);
  }, [_handleRemoveWaterData, waterData]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemoveWaterData}
        variant="white"
        text="Remove water data"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

WaterDataRemove.propTypes = {
  waterData: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
WaterDataRemove.defaultProps = {
  waterData: null,
  appUser: null,
  useTooltip: false,
};

export default WaterDataRemove;
