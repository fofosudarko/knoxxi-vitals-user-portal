import PropTypes from 'prop-types';

import { GoBack } from 'src/components/lib';
import { SharedWaterDataListListView } from '../SharedWaterDataListList/SharedWaterDataListList';

function SharedWaterDataListPage({ healthDataSharePublic }) {
  const query = { healthDataSharePublic };
  return (
    <div>
      <div className="justify-between">
        <div className="page-title">Shared water data</div>
        <GoBack />
      </div>
      <div className="page-content">
        <SharedWaterDataListListView
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedWaterDataListPage.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedWaterDataListPage.defaultProps = {
  healthDataSharePublic: null,
};

export default SharedWaterDataListPage;
