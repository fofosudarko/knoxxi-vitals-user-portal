import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCreateWaterData } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';
import { DEFAULT_PARTNERED_BY_ALIAS, DEFAULT_DATA_CHANNEL } from 'src/config';

import { ContentContainer, GoBack } from 'src/components/lib';
import WaterDataInput from '../WaterDataInput/WaterDataInput';

function WaterDataNewPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { handleWaterDataListRoute } = useRoutes().useWaterDataListRoute();
  const {
    handleCreateWaterData: _handleCreateWaterData,
    error,
    setError,
    processing,
    waterDataCreated,
    setWaterDataCreated,
  } = useCreateWaterData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (waterDataCreated) {
      handleApiResult();
    }
  }, [waterDataCreated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Water data created successfully'
      ).getSuccessNotification(),
      () => {
        setWaterDataCreated(false);
      }
    );
    handleWaterDataListRoute();
  }, [handleNotification, handleWaterDataListRoute, setWaterDataCreated]);

  const handleCreateWaterData = useCallback(
    async (waterData) => {
      const body = {
        ...waterData,
        kycId: account?.customerId,
        mobileNumber: null,
        partneredByAlias: DEFAULT_PARTNERED_BY_ALIAS,
        dataChannel: DEFAULT_DATA_CHANNEL,
      };
      await _handleCreateWaterData(body);
    },
    [_handleCreateWaterData, account?.customerId]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">New water data</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <WaterDataInput
          appUser={appUser}
          onSubmit={handleCreateWaterData}
          processing={processing}
        />
      </ContentContainer>
    </div>
  );
}

WaterDataNewPage.propTypes = {
  appUser: PropTypes.object,
};
WaterDataNewPage.defaultProps = {
  appUser: null,
};

export default WaterDataNewPage;
