import { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { YUP_WATER_DATA_READING_VALIDATOR } from 'src/config/validators';
import { useFormReset, useDeviceDimensions } from 'src/hooks';

import { CancelButton, CreateButton, EditButton } from 'src/components/lib';

function WaterDataInput({ waterData, isEditing, onSubmit, processing }) {
  const waterDataInputSchema = Yup({
    reading: YUP_WATER_DATA_READING_VALIDATOR,
  });
  const { isLargeDevice } = useDeviceDimensions();
  const defaultValues = useMemo(
    () => ({
      reading: waterData?.reading ?? '',
    }),
    [waterData?.reading]
  );

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    reset,
  } = useForm({ defaultValues, resolver: yupResolver(waterDataInputSchema) });

  useFormReset({ values: defaultValues, item: waterData, reset });

  const handleSubmitWaterData = useCallback(
    ({ reading, pulse, diastolic }) => {
      const newWaterData = {
        reading,
        pulse,
        diastolic,
      };

      onSubmit && onSubmit(newWaterData);
    },
    [onSubmit]
  );

  const handleCancelWaterData = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitWaterData)} noValidate>
        <Row xs={{ cols: 1 }}>
          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">Reading(ml)</Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Reading"
                  {...field}
                  isInvalid={!!errors.reading}
                />
              )}
              name="reading"
              control={control}
            />
            {errors.reading ? (
              <Form.Control.Feedback type="invalid">
                {errors.reading.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give the reading e.g. 120 ml</Form.Text>
          </Form.Group>
        </Row>

        <div className="d-flex flex-column-reverse flex-md-row justify-content-end my-3">
          <div className="my-1 my-sm-0 mx-1">
            <CancelButton
              onClick={handleCancelWaterData}
              autoWidth={isLargeDevice}
              text="Cancel"
            />
          </div>
          <div className="my-1 my-sm-0 mx-1">
            {isEditing ? (
              <EditButton
                type="submit"
                clicked={processing}
                text="Edit water data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            ) : (
              <CreateButton
                type="submit"
                clicked={processing}
                text="Add water data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            )}
          </div>
        </div>
      </Form>
    </div>
  );
}

WaterDataInput.propTypes = {
  appUser: PropTypes.object,
  waterData: PropTypes.object,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
};
WaterDataInput.defaultProps = {
  appUser: null,
  waterData: null,
  isEditing: false,
  onSubmit: undefined,
  processing: false,
};

export default WaterDataInput;
