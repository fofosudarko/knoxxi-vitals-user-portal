import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetWaterDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function WaterDataListReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const _handleResetWaterDataList = useResetWaterDataList({
    userDataId: account?.customerId,
  });

  const handleResetWaterDataList = useCallback(() => {
    _handleResetWaterDataList();
  }, [_handleResetWaterDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetWaterDataList}
      text="Reload water data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

WaterDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
WaterDataListReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default WaterDataListReload;
