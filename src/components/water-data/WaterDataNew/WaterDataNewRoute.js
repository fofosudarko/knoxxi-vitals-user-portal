import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { CreateButton } from 'src/components/lib';

export default function WaterDataNewRoute({ text }) {
  const { handleWaterDataNewRoute } = useRoutes().useWaterDataNewRoute();

  return (
    <CreateButton
      variant="primary"
      textColor="white"
      onClick={handleWaterDataNewRoute}
      text={text}
    />
  );
}

WaterDataNewRoute.propTypes = {
  text: PropTypes.string,
};
WaterDataNewRoute.defaultProps = {
  text: 'New water data',
};
