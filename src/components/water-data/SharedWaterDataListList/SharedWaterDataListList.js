import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListSharedWaterDataList } from 'src/hooks/api';

import SharedWaterDataListReload from './SharedWaterDataListReload';
import {
  SharedWaterDataListList,
  SharedWaterDataListListEmpty,
} from './_SharedWaterDataListList';

function SharedWaterDataListListContainer({ healthDataSharePublic, query }) {
  const {
    sharedWaterDataList,
    error: sharedWaterDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListSharedWaterDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (sharedWaterDataListError) {
      handleNotification(sharedWaterDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, sharedWaterDataListError, setError]);

  const handleLoadMoreSharedWaterDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <SharedWaterDataListList
      sharedWaterDataList={sharedWaterDataList}
      onLoadMore={handleLoadMoreSharedWaterDataList}
      loadingText="Loading shared water data list..."
      healthDataSharePublic={healthDataSharePublic}
      SharedWaterDataListListEmpty={SharedWaterDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function SharedWaterDataListListView({ healthDataSharePublic, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <SharedWaterDataListReload
          useTooltip
          healthDataSharePublic={healthDataSharePublic}
        />
      </div>
      <div className="my-2">
        <SharedWaterDataListListContainer
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedWaterDataListListContainer.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedWaterDataListListContainer.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};
SharedWaterDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedWaterDataList: PropTypes.array,
  SharedWaterDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedWaterDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared water data list...',
  onLoadMore: undefined,
  sharedWaterDataList: null,
  SharedWaterDataListListEmpty: null,
  isDisabled: false,
};
SharedWaterDataListListView.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedWaterDataListListView.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};

export default SharedWaterDataListList;
