import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import {
  SharedWaterDataTableItem,
  SharedWaterDataGridItem,
} from './SharedWaterDataItem';

export function SharedWaterDataListList({
  sharedWaterDataList,
  loadingText,
  healthDataSharePublic,
  onLoadMore,
  SharedWaterDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!sharedWaterDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(SharedWaterDataListListEmpty);

  return sharedWaterDataList.length ? (
    <div>
      {isLargeDevice ? (
        <SharedWaterDataListListTable
          sharedWaterDataList={sharedWaterDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <SharedWaterDataListListGrid
          sharedWaterDataList={sharedWaterDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      )}

      {sharedWaterDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more shared water data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty healthDataSharePublic={healthDataSharePublic} />
  );
}

export function SharedWaterDataListListGrid({
  sharedWaterDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {sharedWaterDataList.map((item) => (
          <Col key={item.id}>
            <SharedWaterDataGridItem
              waterData={item}
              healthDataSharePublic={healthDataSharePublic}
            />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function SharedWaterDataListListTable({
  sharedWaterDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">Reading(ml)</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {sharedWaterDataList.map((item, index) => (
            <SharedWaterDataTableItem
              waterData={item}
              healthDataSharePublic={healthDataSharePublic}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function SharedWaterDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no shared water data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

SharedWaterDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedWaterDataList: PropTypes.array,
  SharedWaterDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedWaterDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared water data list...',
  onLoadMore: undefined,
  sharedWaterDataList: null,
  SharedWaterDataListListEmpty: null,
  isDisabled: false,
};
SharedWaterDataListListGrid.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedWaterDataList: PropTypes.array,
};
SharedWaterDataListListGrid.defaultProps = {
  healthDataSharePublic: null,
  sharedWaterDataList: null,
};
SharedWaterDataListListTable.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedWaterDataList: PropTypes.array,
};
SharedWaterDataListListTable.defaultProps = {
  healthDataSharePublic: null,
  sharedWaterDataList: null,
};
SharedWaterDataListListEmpty.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedWaterDataListListEmpty.defaultProps = {
  healthDataSharePublic: null,
};
