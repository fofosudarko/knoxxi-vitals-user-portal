import PropTypes from 'prop-types';
import { Dropdown, Row, Col } from 'react-bootstrap';
import { FaEllipsisV } from 'react-icons/fa';

import { useDeviceDimensions } from 'src/hooks';
import { humanizeDate } from 'src/utils';

import WaterDataRemove from '../WaterDataRemove/WaterDataRemove';

export function useWaterDataDetails(waterData) {
  const { createdOn, reading } = waterData ?? {};

  return {
    createdOn,
    reading,
  };
}

export function SharedWaterDataGridItem({ waterData, healthDataSharePublic }) {
  const { createdOn, reading } = useWaterDataDetails(waterData);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Created</div>
            <div className="item-subtitle">
              {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Reading(ml)</div>
            <div className="item-subtitle">
              {reading !== undefined ? reading : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              <WaterDataItemActions
                waterData={waterData}
                healthDataSharePublic={healthDataSharePublic}
              />
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function SharedWaterDataTableItem({ waterData, healthDataSharePublic }) {
  const { createdOn, reading } = useWaterDataDetails(waterData);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {reading !== undefined ? reading : 'N/A'}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          <WaterDataItemActions
            waterData={waterData}
            healthDataSharePublic={healthDataSharePublic}
          />
        </div>
      </td>
    </tr>
  );
}

export function WaterDataItemActions({ waterData, healthDataSharePublic }) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item>
            <WaterDataRemove
              waterData={waterData}
              healthDataSharePublic={healthDataSharePublic}
            />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

SharedWaterDataGridItem.propTypes = {
  waterData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedWaterDataGridItem.defaultProps = {
  waterData: null,
  healthDataSharePublic: null,
};
SharedWaterDataTableItem.propTypes = {
  waterData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedWaterDataTableItem.defaultProps = {
  waterData: null,
  healthDataSharePublic: null,
};
WaterDataItemActions.propTypes = {
  waterData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
WaterDataItemActions.defaultProps = {
  waterData: null,
  healthDataSharePublic: null,
};
