import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetSharedWaterDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function SharedWaterDataListReload({ useTooltip }) {
  const _handleResetSharedWaterDataList = useResetSharedWaterDataList();

  const handleResetSharedWaterDataList = useCallback(() => {
    _handleResetSharedWaterDataList();
  }, [_handleResetSharedWaterDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetSharedWaterDataList}
      text="Reload shared water data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

SharedWaterDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  healthDataSharePublic: PropTypes.object,
};
SharedWaterDataListReload.defaultProps = {
  useTooltip: false,
  healthDataSharePublic: null,
};

export default SharedWaterDataListReload;
