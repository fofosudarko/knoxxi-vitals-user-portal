import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { MdBloodtype } from 'react-icons/md';
import { GiSugarCane } from 'react-icons/gi';
import { FaTemperatureFull } from 'react-icons/fa6';
import { FiWind } from 'react-icons/fi';
import { GiButter } from 'react-icons/gi';

import { useRoutes } from 'src/hooks';

import { MenuCard } from 'src/components/lib';

function InsightsMenu({ appUser }) {
  const insightsItems = [
    {
      Icon: <MdBloodtype size={200} className="text-primary" />,
      label: 'Blood pressure',
    },
    {
      Icon: <GiSugarCane size={200} className="text-primary" />,
      label: 'Glucose',
    },
    {
      Icon: <FaTemperatureFull size={200} className="text-primary" />,
      label: 'Temperature',
    },
    {
      Icon: <FiWind size={200} className="text-primary" />,
      label: 'Oxygen',
    },
    {
      Icon: <GiButter size={200} className="text-primary" />,
      label: 'Cholesterol',
    },
  ];

  return (
    <div className="mt-2">
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        {insightsItems.map((item, index) => (
          <Col key={index}>
            <InsightsMenuItem
              Icon={item.Icon}
              label={item.label}
              appUser={appUser}
            />
          </Col>
        ))}
      </Row>
    </div>
  );
}

export function InsightsMenuItem({ label, Icon }) {
  const {
    useInsightsBPDataRoute,
    useInsightsGlucoseDataRoute,
    useInsightsTemperatureDataRoute,
    useInsightsOxygenDataRoute,
    useInsightsCholesterolDataRoute,
  } = useRoutes();
  const { handleInsightsBPDataRoute } = useInsightsBPDataRoute();
  const { handleInsightsGlucoseDataRoute } = useInsightsGlucoseDataRoute();
  const { handleInsightsTemperatureDataRoute } =
    useInsightsTemperatureDataRoute();
  const { handleInsightsOxygenDataRoute } = useInsightsOxygenDataRoute();
  const { handleInsightsCholesterolDataRoute } =
    useInsightsCholesterolDataRoute();
  const handleApiResult = useMemo(() => {
    let handler;
    switch (label) {
      case 'Blood pressure':
        handler = handleInsightsBPDataRoute;
        break;
      case 'Glucose':
        handler = handleInsightsGlucoseDataRoute;
        break;
      case 'Temperature':
        handler = handleInsightsTemperatureDataRoute;
        break;
      case 'Oxygen':
        handler = handleInsightsOxygenDataRoute;
        break;
      case 'Cholesterol':
        handler = handleInsightsCholesterolDataRoute;
        break;
      default:
        handler = undefined;
    }
    return handler;
  }, [
    handleInsightsBPDataRoute,
    handleInsightsCholesterolDataRoute,
    handleInsightsGlucoseDataRoute,
    handleInsightsOxygenDataRoute,
    handleInsightsTemperatureDataRoute,
    label,
  ]);

  return (
    <div style={{ cursor: 'pointer' }}>
      <MenuCard onClick={handleApiResult} label={label} Icon={Icon} />
    </div>
  );
}

InsightsMenu.propTypes = {
  appUser: PropTypes.object,
};
InsightsMenu.defaultProps = {
  appUser: null,
};
InsightsMenuItem.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  Icon: PropTypes.node,
};
InsightsMenuItem.defaultProps = {
  appUser: null,
  label: 'Exercise',
  Icon: 'running',
};

export default InsightsMenu;
