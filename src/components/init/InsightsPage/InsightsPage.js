import PropTypes from 'prop-types';

import InsightsMenu from '../InsightsMenu/InsightsMenu';

function InsightsPage({ appUser }) {
  return (
    <div>
      <div className="page-title">Insights</div>
      <div className="page-content">
        <InsightsMenu appUser={appUser} />
      </div>
    </div>
  );
}

InsightsPage.propTypes = {
  appUser: PropTypes.object,
};
InsightsPage.defaultProps = {
  appUser: null,
};

export default InsightsPage;
