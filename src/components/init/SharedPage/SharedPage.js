import PropTypes from 'prop-types';

import { GoBack } from 'src/components/lib';

import SharedMenu from '../SharedMenu/SharedMenu';

function SharedPage({ appUser }) {
  return (
    <div>
      <div className="justify-between">
        <div className="page-title">Shared menu</div>
        <GoBack />
      </div>
      <div className="page-content">
        <SharedMenu appUser={appUser} />
      </div>
    </div>
  );
}

SharedPage.propTypes = {
  appUser: PropTypes.object,
};
SharedPage.defaultProps = {
  appUser: null,
};

export default SharedPage;
