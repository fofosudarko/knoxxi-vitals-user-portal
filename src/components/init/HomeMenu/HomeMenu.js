import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { MdBloodtype } from 'react-icons/md';
import { GiSugarCane } from 'react-icons/gi';
import { FaWeightHanging } from 'react-icons/fa';
import { FaTemperatureFull, FaGlassWater } from 'react-icons/fa6';
import { FiWind } from 'react-icons/fi';
import { GiButter } from 'react-icons/gi';

import { useRoutes } from 'src/hooks';

import { MenuCard } from 'src/components/lib';

function HomeMenu({ appUser }) {
  const homeItems = [
    {
      Icon: <MdBloodtype size={200} className="text-primary" />,
      label: 'Blood pressure',
    },
    {
      Icon: <GiSugarCane size={200} className="text-primary" />,
      label: 'Glucose',
    },
    {
      Icon: <FaTemperatureFull size={200} className="text-primary" />,
      label: 'Temperature',
    },
    {
      Icon: <FaWeightHanging size={200} className="text-primary" />,
      label: 'Weight',
    },
    {
      Icon: <FaGlassWater size={200} className="text-primary" />,
      label: 'Water',
    },
    {
      Icon: <FiWind size={200} className="text-primary" />,
      label: 'Oxygen',
    },
    {
      Icon: <GiButter size={200} className="text-primary" />,
      label: 'Cholesterol',
    },
  ];

  return (
    <div className="mt-2">
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        {homeItems.map((item, index) => (
          <Col key={index}>
            <HomeMenuItem
              Icon={item.Icon}
              label={item.label}
              appUser={appUser}
            />
          </Col>
        ))}
      </Row>
    </div>
  );
}

export function HomeMenuItem({ label, Icon }) {
  const {
    useBPDataListRoute,
    useGlucoseDataListRoute,
    useTemperatureDataListRoute,
    useWeightDataListRoute,
    useWaterDataListRoute,
    useOxygenDataListRoute,
    useCholesterolDataListRoute,
  } = useRoutes();
  const { handleBPDataListRoute } = useBPDataListRoute();
  const { handleGlucoseDataListRoute } = useGlucoseDataListRoute();
  const { handleTemperatureDataListRoute } = useTemperatureDataListRoute();
  const { handleWeightDataListRoute } = useWeightDataListRoute();
  const { handleWaterDataListRoute } = useWaterDataListRoute();
  const { handleOxygenDataListRoute } = useOxygenDataListRoute();
  const { handleCholesterolDataListRoute } = useCholesterolDataListRoute();

  const handleApiResult = useMemo(() => {
    let handler;
    switch (label) {
      case 'Blood pressure':
        handler = handleBPDataListRoute;
        break;
      case 'Glucose':
        handler = handleGlucoseDataListRoute;
        break;
      case 'Temperature':
        handler = handleTemperatureDataListRoute;
        break;
      case 'Weight':
        handler = handleWeightDataListRoute;
        break;
      case 'Water':
        handler = handleWaterDataListRoute;
        break;
      case 'Oxygen':
        handler = handleOxygenDataListRoute;
        break;
      case 'Cholesterol':
        handler = handleCholesterolDataListRoute;
        break;
      default:
        handler = undefined;
    }
    return handler;
  }, [
    handleTemperatureDataListRoute,
    handleBPDataListRoute,
    handleGlucoseDataListRoute,
    handleWeightDataListRoute,
    handleWaterDataListRoute,
    handleOxygenDataListRoute,
    handleCholesterolDataListRoute,
    label,
  ]);

  return (
    <div style={{ cursor: 'pointer' }}>
      <MenuCard onClick={handleApiResult} label={label} Icon={Icon} />
    </div>
  );
}

HomeMenu.propTypes = {
  appUser: PropTypes.object,
};
HomeMenu.defaultProps = {
  appUser: null,
};
HomeMenuItem.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  Icon: PropTypes.node,
};
HomeMenuItem.defaultProps = {
  appUser: null,
  label: 'Exercise',
  Icon: 'running',
};

export default HomeMenu;
