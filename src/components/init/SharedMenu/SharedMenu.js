import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { MdBloodtype } from 'react-icons/md';
import { GiSugarCane } from 'react-icons/gi';
import { FaWeightHanging } from 'react-icons/fa';
import { FaTemperatureFull, FaGlassWater } from 'react-icons/fa6';
import { FiWind } from 'react-icons/fi';

import { useRoutes } from 'src/hooks';

import { MenuCard } from 'src/components/lib';

function SharedMenu({ appUser }) {
  const homeItems = [
    {
      Icon: <MdBloodtype size={200} className="text-primary" />,
      label: 'Blood pressure',
    },
    {
      Icon: <GiSugarCane size={200} className="text-primary" />,
      label: 'Glucose',
    },
    {
      Icon: <FaTemperatureFull size={200} className="text-primary" />,
      label: 'Temperature',
    },
    {
      Icon: <FaWeightHanging size={200} className="text-primary" />,
      label: 'Weight',
    },
    {
      Icon: <FaGlassWater size={200} className="text-primary" />,
      label: 'Water',
    },
    {
      Icon: <FiWind size={200} className="text-primary" />,
      label: 'Oxygen',
    },
  ];

  return (
    <div className="mt-2">
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        {homeItems.map((item, index) => (
          <Col key={index}>
            <SharedMenuItem
              Icon={item.Icon}
              label={item.label}
              appUser={appUser}
            />
          </Col>
        ))}
      </Row>
    </div>
  );
}

export function SharedMenuItem({ label, Icon }) {
  const {
    useSharedBPDataListRoute,
    useSharedGlucoseDataListRoute,
    useSharedTemperatureDataListRoute,
    useSharedWeightDataListRoute,
    useSharedWaterDataListRoute,
    useSharedOxygenDataListRoute,
  } = useRoutes();
  const { handleSharedBPDataListRoute } = useSharedBPDataListRoute();
  const { handleSharedGlucoseDataListRoute } = useSharedGlucoseDataListRoute();
  const { handleSharedTemperatureDataListRoute } =
    useSharedTemperatureDataListRoute();
  const { handleSharedWeightDataListRoute } = useSharedWeightDataListRoute();
  const { handleSharedWaterDataListRoute } = useSharedWaterDataListRoute();
  const { handleSharedOxygenDataListRoute } = useSharedOxygenDataListRoute();

  const handleApiResult = useMemo(() => {
    let handler;
    switch (label) {
      case 'Blood pressure':
        handler = handleSharedBPDataListRoute;
        break;
      case 'Glucose':
        handler = handleSharedGlucoseDataListRoute;
        break;
      case 'Temperature':
        handler = handleSharedTemperatureDataListRoute;
        break;
      case 'Weight':
        handler = handleSharedWeightDataListRoute;
        break;
      case 'Water':
        handler = handleSharedWaterDataListRoute;
        break;
      case 'Oxygen':
        handler = handleSharedOxygenDataListRoute;
        break;
      default:
        handler = undefined;
    }
    return handler;
  }, [
    handleSharedTemperatureDataListRoute,
    handleSharedBPDataListRoute,
    handleSharedGlucoseDataListRoute,
    handleSharedWeightDataListRoute,
    handleSharedWaterDataListRoute,
    handleSharedOxygenDataListRoute,
    label,
  ]);

  return (
    <div style={{ cursor: 'pointer' }}>
      <MenuCard onClick={handleApiResult} label={label} Icon={Icon} />
    </div>
  );
}

SharedMenu.propTypes = {
  appUser: PropTypes.object,
};
SharedMenu.defaultProps = {
  appUser: null,
};
SharedMenuItem.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  Icon: PropTypes.node,
};
SharedMenuItem.defaultProps = {
  appUser: null,
  label: 'Exercise',
  Icon: 'running',
};

export default SharedMenu;
