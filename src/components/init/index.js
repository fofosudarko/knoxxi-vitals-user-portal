import HomePage from './HomePage/HomePage';
import HomeMenu from './HomeMenu/HomeMenu';
import InsightsPage from './InsightsPage/InsightsPage';
import InsightsMenu from './InsightsMenu/InsightsMenu';
import SharedPage from './SharedPage/SharedPage';
import SharedMenu from './SharedMenu/SharedMenu';

export {
  HomePage,
  HomeMenu,
  InsightsPage,
  InsightsMenu,
  SharedPage,
  SharedMenu,
};
