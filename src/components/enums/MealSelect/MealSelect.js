import { forwardRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListEnums } from 'src/hooks/api';
import { humanizeString } from 'src/utils';

import { ItemSelect } from 'src/components/lib';

function getValueFromMeal(meal = null) {
  if (!meal) {
    return '';
  }

  return typeof meal === 'object' ? meal.name ?? null : meal;
}

function getMealOptionItem(meal = null) {
  if (!meal) {
    return {};
  }

  const value = getValueFromMeal(meal);

  return {
    value,
    label: humanizeString({ string: value, capitalize: true }),
  };
}

const MealSelect = forwardRef(function MealSelect(
  {
    name,
    onChange,
    onBlur,
    errors,
    value,
    onFocus,
    isClearable,
    isDisabled,
    placeholder,
    helpText,
    hideLabel,
    label,
    labelClass,
  },
  ref
) {
  const selectProps = {
    name,
    onBlur,
    errors,
    value,
    onFocus,
    isClearable,
    onChange,
    isDisabled,
    placeholder,
    helpText,
    hideLabel,
    label,
    labelClass,
  };
  const { error, setError, meals, setMeal, handleListMeal } = useListEnums();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (!meals?.length) {
      (async () => {
        await handleListMeal();
      })();
    }
  }, [handleListMeal, meals]);

  useEffect(() => {
    return () => {
      setMeal(null);
    };
  }, [setMeal]);

  return (
    <ItemSelect
      {...selectProps}
      ref={ref}
      items={meals}
      getOptionItem={getMealOptionItem}
      isSync
      isRawValue
    />
  );
});

MealSelect.displayName = 'MealSelect';

MealSelect.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  value: PropTypes.any,
  isClearable: PropTypes.bool,
  isDisabled: PropTypes.bool,
  helpText: PropTypes.string,
  placeholder: PropTypes.string,
  errors: PropTypes.object,
  hideLabel: PropTypes.bool,
  label: PropTypes.string,
  labelClass: PropTypes.string,
};
MealSelect.defaultProps = {
  name: undefined,
  onChange: undefined,
  onBlur: undefined,
  onFocus: undefined,
  value: undefined,
  isClearable: false,
  isDisabled: false,
  helpText: undefined,
  placeholder: undefined,
  errors: null,
  hideLabel: false,
  label: undefined,
  labelClass: undefined,
};

export default MealSelect;
