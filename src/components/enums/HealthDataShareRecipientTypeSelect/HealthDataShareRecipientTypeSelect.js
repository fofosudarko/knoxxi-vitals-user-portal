import { forwardRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListEnums } from 'src/hooks/api';
import { humanizeString } from 'src/utils';

import { ItemSelect } from 'src/components/lib';

function getValueFromHealthDataShareRecipientType(
  healthDataShareRecipientType = null
) {
  if (!healthDataShareRecipientType) {
    return '';
  }

  return typeof healthDataShareRecipientType === 'object'
    ? healthDataShareRecipientType.name ?? null
    : healthDataShareRecipientType;
}

function getHealthDataShareRecipientTypeOptionItem(
  healthDataShareRecipientType = null
) {
  if (!healthDataShareRecipientType) {
    return {};
  }

  const value = getValueFromHealthDataShareRecipientType(
    healthDataShareRecipientType
  );

  return {
    value,
    label: humanizeString({ string: value, capitalize: true }),
  };
}

const HealthDataShareRecipientTypeSelect = forwardRef(
  function HealthDataShareRecipientTypeSelect(
    {
      name,
      onChange,
      onBlur,
      errors,
      value,
      onFocus,
      isClearable,
      isDisabled,
      placeholder,
      helpText,
      hideLabel,
      label,
      labelClass,
    },
    ref
  ) {
    const selectProps = {
      name,
      onBlur,
      errors,
      value,
      onFocus,
      isClearable,
      onChange,
      isDisabled,
      placeholder,
      helpText,
      hideLabel,
      label,
      labelClass,
    };
    const {
      error,
      setError,
      healthDataShareRecipientTypes,
      setHealthDataShareRecipientType,
      handleListHealthDataShareRecipientType,
    } = useListEnums();
    const handleNotification = useNotificationHandler();

    useEffect(() => {
      if (error) {
        handleNotification(error);
      }

      return () => {
        setError(null);
      };
    }, [error, handleNotification, setError]);

    useEffect(() => {
      if (!healthDataShareRecipientTypes?.length) {
        (async () => {
          await handleListHealthDataShareRecipientType();
        })();
      }
    }, [handleListHealthDataShareRecipientType, healthDataShareRecipientTypes]);

    useEffect(() => {
      return () => {
        setHealthDataShareRecipientType(null);
      };
    }, [setHealthDataShareRecipientType]);

    return (
      <ItemSelect
        {...selectProps}
        ref={ref}
        items={healthDataShareRecipientTypes}
        getOptionItem={getHealthDataShareRecipientTypeOptionItem}
        isSync
        isRawValue
      />
    );
  }
);

HealthDataShareRecipientTypeSelect.displayName =
  'HealthDataShareRecipientTypeSelect';

HealthDataShareRecipientTypeSelect.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  value: PropTypes.any,
  isClearable: PropTypes.bool,
  isDisabled: PropTypes.bool,
  helpText: PropTypes.string,
  placeholder: PropTypes.string,
  errors: PropTypes.object,
  hideLabel: PropTypes.bool,
  label: PropTypes.string,
  labelClass: PropTypes.string,
};
HealthDataShareRecipientTypeSelect.defaultProps = {
  name: undefined,
  onChange: undefined,
  onBlur: undefined,
  onFocus: undefined,
  value: undefined,
  isClearable: false,
  isDisabled: false,
  helpText: undefined,
  placeholder: undefined,
  errors: null,
  hideLabel: false,
  label: undefined,
  labelClass: undefined,
};

export default HealthDataShareRecipientTypeSelect;
