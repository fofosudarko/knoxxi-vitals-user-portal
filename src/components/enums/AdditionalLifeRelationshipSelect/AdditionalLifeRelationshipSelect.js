import { forwardRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListEnums } from 'src/hooks/api';
import { humanizeString } from 'src/utils';

import { ItemSelect } from 'src/components/lib';

function getValueFromAdditionalLifeRelationship(
  additionalLifeRelationship = null
) {
  if (!additionalLifeRelationship) {
    return '';
  }

  return typeof additionalLifeRelationship === 'object'
    ? additionalLifeRelationship.name ?? null
    : additionalLifeRelationship;
}

function getAdditionalLifeRelationshipOptionItem(
  additionalLifeRelationship = null
) {
  if (!additionalLifeRelationship) {
    return {};
  }

  const value = getValueFromAdditionalLifeRelationship(
    additionalLifeRelationship
  );

  return {
    value,
    label: humanizeString({ string: value, capitalize: true }),
  };
}

const AdditionalLifeRelationshipSelect = forwardRef(
  function AdditionalLifeRelationshipSelect(
    {
      name,
      onChange,
      onBlur,
      errors,
      value,
      onFocus,
      isClearable,
      isDisabled,
      placeholder,
      helpText,
      hideLabel,
      label,
      labelClass,
    },
    ref
  ) {
    const selectProps = {
      name,
      onBlur,
      errors,
      value,
      onFocus,
      isClearable,
      onChange,
      isDisabled,
      placeholder,
      helpText,
      hideLabel,
      label,
      labelClass,
    };
    const {
      error,
      setError,
      additionalLifeRelationships,
      setAdditionalLifeRelationship,
      handleListAdditionalLifeRelationship,
    } = useListEnums();
    const handleNotification = useNotificationHandler();

    useEffect(() => {
      if (error) {
        handleNotification(error);
      }

      return () => {
        setError(null);
      };
    }, [error, handleNotification, setError]);

    useEffect(() => {
      if (!additionalLifeRelationships?.length) {
        (async () => {
          await handleListAdditionalLifeRelationship();
        })();
      }
    }, [handleListAdditionalLifeRelationship, additionalLifeRelationships]);

    useEffect(() => {
      return () => {
        setAdditionalLifeRelationship(null);
      };
    }, [setAdditionalLifeRelationship]);

    return (
      <ItemSelect
        {...selectProps}
        ref={ref}
        items={additionalLifeRelationships}
        getOptionItem={getAdditionalLifeRelationshipOptionItem}
        isSync
        isRawValue
      />
    );
  }
);

AdditionalLifeRelationshipSelect.displayName =
  'AdditionalLifeRelationshipSelect';

AdditionalLifeRelationshipSelect.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  value: PropTypes.any,
  isClearable: PropTypes.bool,
  isDisabled: PropTypes.bool,
  helpText: PropTypes.string,
  placeholder: PropTypes.string,
  errors: PropTypes.object,
  hideLabel: PropTypes.bool,
  label: PropTypes.string,
  labelClass: PropTypes.string,
};
AdditionalLifeRelationshipSelect.defaultProps = {
  name: undefined,
  onChange: undefined,
  onBlur: undefined,
  onFocus: undefined,
  value: undefined,
  isClearable: false,
  isDisabled: false,
  helpText: undefined,
  placeholder: undefined,
  errors: null,
  hideLabel: false,
  label: undefined,
  labelClass: undefined,
};

export default AdditionalLifeRelationshipSelect;
