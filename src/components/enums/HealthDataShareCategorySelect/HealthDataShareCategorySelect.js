import { forwardRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListEnums } from 'src/hooks/api';
import { humanizeString } from 'src/utils';

import { ItemSelect } from 'src/components/lib';

function getValueFromHealthDataShareCategory(healthDataShareCategory = null) {
  if (!healthDataShareCategory) {
    return '';
  }

  return typeof healthDataShareCategory === 'object'
    ? healthDataShareCategory.name ?? null
    : healthDataShareCategory;
}

function getHealthDataShareCategoryOptionItem(healthDataShareCategory = null) {
  if (!healthDataShareCategory) {
    return {};
  }

  const value = getValueFromHealthDataShareCategory(healthDataShareCategory);

  return {
    value,
    label: humanizeString({ string: value, capitalize: true }),
  };
}

const HealthDataShareCategorySelect = forwardRef(
  function HealthDataShareCategorySelect(
    {
      name,
      onChange,
      onBlur,
      errors,
      value,
      onFocus,
      isClearable,
      isDisabled,
      placeholder,
      helpText,
      hideLabel,
      label,
      labelClass,
    },
    ref
  ) {
    const selectProps = {
      name,
      onBlur,
      errors,
      value,
      onFocus,
      isClearable,
      onChange,
      isDisabled,
      placeholder,
      helpText,
      hideLabel,
      label,
      labelClass,
    };
    const {
      error,
      setError,
      healthDataShareCategories,
      setHealthDataShareCategory,
      handleListHealthDataShareCategory,
    } = useListEnums();
    const handleNotification = useNotificationHandler();

    useEffect(() => {
      if (error) {
        handleNotification(error);
      }

      return () => {
        setError(null);
      };
    }, [error, handleNotification, setError]);

    useEffect(() => {
      if (!healthDataShareCategories?.length) {
        (async () => {
          await handleListHealthDataShareCategory();
        })();
      }
    }, [handleListHealthDataShareCategory, healthDataShareCategories]);

    useEffect(() => {
      return () => {
        setHealthDataShareCategory(null);
      };
    }, [setHealthDataShareCategory]);

    return (
      <ItemSelect
        {...selectProps}
        ref={ref}
        items={healthDataShareCategories}
        getOptionItem={getHealthDataShareCategoryOptionItem}
        isSync
        isRawValue
      />
    );
  }
);

HealthDataShareCategorySelect.displayName = 'HealthDataShareCategorySelect';

HealthDataShareCategorySelect.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  value: PropTypes.any,
  isClearable: PropTypes.bool,
  isDisabled: PropTypes.bool,
  helpText: PropTypes.string,
  placeholder: PropTypes.string,
  errors: PropTypes.object,
  hideLabel: PropTypes.bool,
  label: PropTypes.string,
  labelClass: PropTypes.string,
};
HealthDataShareCategorySelect.defaultProps = {
  name: undefined,
  onChange: undefined,
  onBlur: undefined,
  onFocus: undefined,
  value: undefined,
  isClearable: false,
  isDisabled: false,
  helpText: undefined,
  placeholder: undefined,
  errors: null,
  hideLabel: false,
  label: undefined,
  labelClass: undefined,
};

export default HealthDataShareCategorySelect;
