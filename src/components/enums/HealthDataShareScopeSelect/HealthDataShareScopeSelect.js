import { forwardRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListEnums } from 'src/hooks/api';
import { humanizeString } from 'src/utils';

import { ItemSelect } from 'src/components/lib';

function getValueFromHealthDataShareScope(healthDataShareScope = null) {
  if (!healthDataShareScope) {
    return '';
  }

  return typeof healthDataShareScope === 'object'
    ? healthDataShareScope.name ?? null
    : healthDataShareScope;
}

function getHealthDataShareScopeOptionItem(healthDataShareScope = null) {
  if (!healthDataShareScope) {
    return {};
  }

  const value = getValueFromHealthDataShareScope(healthDataShareScope);

  return {
    value,
    label: humanizeString({ string: value, capitalize: true }),
  };
}

const HealthDataShareScopeSelect = forwardRef(
  function HealthDataShareScopeSelect(
    {
      name,
      onChange,
      onBlur,
      errors,
      value,
      onFocus,
      isClearable,
      isDisabled,
      placeholder,
      helpText,
      hideLabel,
      label,
      labelClass,
    },
    ref
  ) {
    const selectProps = {
      name,
      onBlur,
      errors,
      value,
      onFocus,
      isClearable,
      onChange,
      isDisabled,
      placeholder,
      helpText,
      hideLabel,
      label,
      labelClass,
    };
    const {
      error,
      setError,
      healthDataShareScopes,
      setHealthDataShareScope,
      handleListHealthDataShareScope,
    } = useListEnums();
    const handleNotification = useNotificationHandler();

    useEffect(() => {
      if (error) {
        handleNotification(error);
      }

      return () => {
        setError(null);
      };
    }, [error, handleNotification, setError]);

    useEffect(() => {
      if (!healthDataShareScopes?.length) {
        (async () => {
          await handleListHealthDataShareScope();
        })();
      }
    }, [handleListHealthDataShareScope, healthDataShareScopes]);

    useEffect(() => {
      return () => {
        setHealthDataShareScope(null);
      };
    }, [setHealthDataShareScope]);

    return (
      <ItemSelect
        {...selectProps}
        ref={ref}
        items={healthDataShareScopes}
        getOptionItem={getHealthDataShareScopeOptionItem}
        isSync
        isRawValue
      />
    );
  }
);

HealthDataShareScopeSelect.displayName = 'HealthDataShareScopeSelect';

HealthDataShareScopeSelect.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  value: PropTypes.any,
  isClearable: PropTypes.bool,
  isDisabled: PropTypes.bool,
  helpText: PropTypes.string,
  placeholder: PropTypes.string,
  errors: PropTypes.object,
  hideLabel: PropTypes.bool,
  label: PropTypes.string,
  labelClass: PropTypes.string,
};
HealthDataShareScopeSelect.defaultProps = {
  name: undefined,
  onChange: undefined,
  onBlur: undefined,
  onFocus: undefined,
  value: undefined,
  isClearable: false,
  isDisabled: false,
  helpText: undefined,
  placeholder: undefined,
  errors: null,
  hideLabel: false,
  label: undefined,
  labelClass: undefined,
};

export default HealthDataShareScopeSelect;
