import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useUpdateGlucoseData } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';
import { DEFAULT_PARTNERED_BY_ALIAS, DEFAULT_DATA_CHANNEL } from 'src/config';

import { ContentContainer, GoBack } from 'src/components/lib';
import GlucoseDataInput from '../GlucoseDataInput/GlucoseDataInput';

function GlucoseDataEditPage({ appUser, glucoseData }) {
  const account = appUser?.account ?? null;
  const { handleGlucoseDataListRoute } = useRoutes().useGlucoseDataListRoute();
  const {
    handleUpdateGlucoseData: _handleUpdateGlucoseData,
    error,
    processing,
    setError,
    glucoseDataUpdated,
    setGlucoseDataUpdated,
  } = useUpdateGlucoseData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (glucoseDataUpdated) {
      handleApiResult();
    }
  }, [handleApiResult, glucoseDataUpdated]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Glucose data updated successfully'
      ).getNormalNotification(),
      () => {
        setGlucoseDataUpdated(false);
      }
    );
    handleGlucoseDataListRoute();
  }, [handleNotification, handleGlucoseDataListRoute, setGlucoseDataUpdated]);

  const handleUpdateGlucoseData = useCallback(
    async (updatedGlucoseData) => {
      const body = {
        ...updatedGlucoseData,
        kycId: account?.customerId,
        mobileNumber: null,
        partneredByAlias: DEFAULT_PARTNERED_BY_ALIAS,
        dataChannel: DEFAULT_DATA_CHANNEL,
      };
      await _handleUpdateGlucoseData(glucoseData, body);
    },
    [_handleUpdateGlucoseData, account?.customerId, glucoseData]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">Edit glucose data</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <GlucoseDataInput
          appUser={appUser}
          onSubmit={handleUpdateGlucoseData}
          isEditing
          processing={processing}
          glucoseData={glucoseData}
        />
      </ContentContainer>
    </div>
  );
}

GlucoseDataEditPage.propTypes = {
  appUser: PropTypes.object,
  glucoseData: PropTypes.object,
};
GlucoseDataEditPage.defaultProps = {
  appUser: null,
  glucoseData: null,
};

export default GlucoseDataEditPage;
