import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemoveGlucoseData } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function GlucoseDataRemove({ glucoseData, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleRemoveGlucoseData: _handleRemoveGlucoseData,
    error,
    setError,
    glucoseDataRemoved,
    setGlucoseDataRemoved,
  } = useRemoveGlucoseData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (glucoseDataRemoved) {
      handleApiResult();
    }
  }, [glucoseDataRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Glucose data removed successfully'
      ).getAlertNotification()
    );
    setGlucoseDataRemoved(false);
  }, [handleNotification, setGlucoseDataRemoved]);

  const handleRemoveGlucoseData = useCallback(async () => {
    await _handleRemoveGlucoseData(glucoseData);
  }, [_handleRemoveGlucoseData, glucoseData]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemoveGlucoseData}
        variant="white"
        text="Remove glucose data"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

GlucoseDataRemove.propTypes = {
  glucoseData: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
GlucoseDataRemove.defaultProps = {
  glucoseData: null,
  appUser: null,
  useTooltip: false,
};

export default GlucoseDataRemove;
