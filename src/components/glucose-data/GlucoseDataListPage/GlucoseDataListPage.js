import PropTypes from 'prop-types';

import { GlucoseDataListListView } from '../GlucoseDataListList/GlucoseDataListList';

function GlucoseDataListPage({ appUser }) {
  const account = appUser?.account ?? null;
  const query = { userDataId: account?.customerId };
  return (
    <div>
      <div className="page-title">Glucose data</div>
      <div className="page-content">
        <GlucoseDataListListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

GlucoseDataListPage.propTypes = {
  appUser: PropTypes.object,
};
GlucoseDataListPage.defaultProps = {
  appUser: null,
};

export default GlucoseDataListPage;
