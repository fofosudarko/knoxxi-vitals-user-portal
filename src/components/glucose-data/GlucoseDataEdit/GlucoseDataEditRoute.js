import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { EditButton } from 'src/components/lib';

export default function GlucoseDataEditRoute({ useTooltip, glucoseData }) {
  const { handleGlucoseDataEditRoute } =
    useRoutes().useGlucoseDataEditRoute(glucoseData);

  return (
    <EditButton
      onClick={handleGlucoseDataEditRoute}
      variant="transparent"
      text="Edit glucose data"
      textColor="black"
      textNormal
      autoWidth={!useTooltip}
      useTooltip={useTooltip}
    />
  );
}

GlucoseDataEditRoute.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
  glucoseData: PropTypes.object,
};
GlucoseDataEditRoute.defaultProps = {
  useTooltip: false,
  appUser: null,
  glucoseData: null,
};
