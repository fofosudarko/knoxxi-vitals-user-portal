import PropTypes from 'prop-types';

import { GoBack } from 'src/components/lib';
import { SharedGlucoseDataListListView } from '../SharedGlucoseDataListList/SharedGlucoseDataListList';

function SharedGlucoseDataListPage({ healthDataSharePublic }) {
  const query = { healthDataSharePublic };
  return (
    <div>
      <div className="justify-between">
        <div className="page-title">Shared glucose data</div>
        <GoBack />
      </div>
      <div className="page-content">
        <SharedGlucoseDataListListView
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedGlucoseDataListPage.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedGlucoseDataListPage.defaultProps = {
  healthDataSharePublic: null,
};

export default SharedGlucoseDataListPage;
