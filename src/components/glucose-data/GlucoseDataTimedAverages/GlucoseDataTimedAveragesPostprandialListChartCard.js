import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function GlucoseDataTimedAveragesPostprandialListChartCard({
  glucoseDataTimedAveragesPostprandialList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          glucoseDataTimedAveragesPostprandialList?.map(
            (item) => item.createdOn
          ) ?? [],
        datasets: [
          {
            label,
            data:
              glucoseDataTimedAveragesPostprandialList?.map(
                (item) => item.postprandialReading
              ) ?? [],
            backgroundColor:
              glucoseDataTimedAveragesPostprandialList?.map(
                (item) => item.postprandialGlucoseDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [glucoseDataTimedAveragesPostprandialList, label]);
  return glucoseDataTimedAveragesPostprandialList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

GlucoseDataTimedAveragesPostprandialListChartCard.propTypes = {
  glucoseDataTimedAveragesPostprandialList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
GlucoseDataTimedAveragesPostprandialListChartCard.defaultProps = {
  glucoseDataTimedAveragesPostprandialList: null,
  appUser: null,
  label: null,
  isDisabled: false,
  onClick: undefined,
};

export default GlucoseDataTimedAveragesPostprandialListChartCard;
