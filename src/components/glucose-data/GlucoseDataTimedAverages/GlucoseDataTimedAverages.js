import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { useNotificationHandler } from 'src/hooks';
import { useListGlucoseDataTimedAverages } from 'src/hooks/api';

import GlucoseDataTimedAveragesFastingListChartCard from './GlucoseDataTimedAveragesFastingListChartCard';
import GlucoseDataTimedAveragesPostprandialListChartCard from './GlucoseDataTimedAveragesPostprandialListChartCard';

function GlucoseDataTimedAveragesContainer({
  appUser,
  query,
  label,
  isCard,
  onClick,
}) {
  const {
    glucoseDataTimedAverages,
    error: glucoseDataTimedAveragesError,
    setError,
  } = useListGlucoseDataTimedAverages({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (glucoseDataTimedAveragesError) {
      handleNotification(glucoseDataTimedAveragesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, glucoseDataTimedAveragesError, setError]);

  return (
    <GlucoseDataTimedAverages
      glucoseDataTimedAverages={glucoseDataTimedAverages}
      label={label}
      appUser={appUser}
      isDisabled={
        glucoseDataTimedAverages && glucoseDataTimedAverages.length === 0
      }
      query={query}
      isCard={isCard}
      onClick={onClick}
    />
  );
}

function GlucoseDataTimedAverages({
  glucoseDataTimedAverages,
  appUser,
  isDisabled,
  isCard,
  onClick,
}) {
  const glucoseDataTimedAveragesFastingList = [],
    glucoseDataTimedAveragesPostprandialList = [];

  glucoseDataTimedAverages?.forEach((glucoseDataTimedAverage) => {
    const {
      createdOn,
      fastingReading,
      postprandialReading,
      fastingGlucoseDataInterpretation,
      fastingGlucoseDataSeverity,
      postprandialGlucoseDataInterpretation,
      postprandialGlucoseDataSeverity,
    } = glucoseDataTimedAverage;
    glucoseDataTimedAveragesFastingList.push({
      createdOn,
      fastingReading,
      fastingGlucoseDataInterpretation,
      fastingGlucoseDataSeverity,
    });
    glucoseDataTimedAveragesPostprandialList.push({
      createdOn,
      postprandialReading,
      postprandialGlucoseDataInterpretation,
      postprandialGlucoseDataSeverity,
    });
  });

  return (
    <div>
      <Row
        xs={{ cols: 1 }}
        //md={{ cols: 2 }}
        //lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        <Col>
          {isCard ? (
            <GlucoseDataTimedAveragesFastingListChartCard
              isDisabled={isDisabled}
              glucoseDataTimedAveragesFastingList={
                glucoseDataTimedAveragesFastingList
              }
              label="Reading before meals(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <GlucoseDataTimedAveragesPostprandialListChartCard
              isDisabled={isDisabled}
              glucoseDataTimedAveragesPostprandialList={
                glucoseDataTimedAveragesPostprandialList
              }
              label="Reading after meals(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
      </Row>
    </div>
  );
}

export function GlucoseDataTimedAveragesChartCardView({
  appUser,
  query,
  label,
  onClick,
}) {
  return (
    <div className="d-block">
      <GlucoseDataTimedAveragesContainer
        appUser={appUser}
        query={query}
        label={label}
        isCard
        onClick={onClick}
      />
    </div>
  );
}

GlucoseDataTimedAveragesContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
GlucoseDataTimedAveragesContainer.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  isCard: false,
  onClick: undefined,
};
GlucoseDataTimedAverages.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  query: PropTypes.object,
  glucoseDataTimedAverages: PropTypes.array,
  isDisabled: PropTypes.bool,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
GlucoseDataTimedAverages.defaultProps = {
  appUser: null,
  label: undefined,
  query: null,
  glucoseDataTimedAverages: undefined,
  isDisabled: false,
  isCard: false,
  onClick: undefined,
};
GlucoseDataTimedAveragesChartCardView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
GlucoseDataTimedAveragesChartCardView.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  onClick: undefined,
};

export default GlucoseDataTimedAverages;
