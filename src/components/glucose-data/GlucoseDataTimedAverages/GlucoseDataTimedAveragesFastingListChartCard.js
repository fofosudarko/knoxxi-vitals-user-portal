import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function GlucoseDataTimedAveragesFastingListChartCard({
  glucoseDataTimedAveragesFastingList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          glucoseDataTimedAveragesFastingList?.map((item) => item.createdOn) ??
          [],
        datasets: [
          {
            label,
            data:
              glucoseDataTimedAveragesFastingList?.map(
                (item) => item.fastingReading
              ) ?? [],
            backgroundColor:
              glucoseDataTimedAveragesFastingList?.map(
                (item) => item.fastingGlucoseDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [glucoseDataTimedAveragesFastingList, label]);
  return glucoseDataTimedAveragesFastingList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

GlucoseDataTimedAveragesFastingListChartCard.propTypes = {
  glucoseDataTimedAveragesFastingList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
GlucoseDataTimedAveragesFastingListChartCard.defaultProps = {
  glucoseDataTimedAveragesFastingList: null,
  appUser: null,
  label: null,
  isDisabled: false,
  onClick: undefined,
};

export default GlucoseDataTimedAveragesFastingListChartCard;
