import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import { GlucoseDataTableItem, GlucoseDataGridItem } from './GlucoseDataItem';

export function GlucoseDataListList({
  glucoseDataList,
  loadingText,
  appUser,
  onLoadMore,
  GlucoseDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!glucoseDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(GlucoseDataListListEmpty);

  return glucoseDataList.length ? (
    <div>
      {isLargeDevice ? (
        <GlucoseDataListListTable
          glucoseDataList={glucoseDataList}
          appUser={appUser}
        />
      ) : (
        <GlucoseDataListListGrid
          glucoseDataList={glucoseDataList}
          appUser={appUser}
        />
      )}

      {glucoseDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more glucose data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function GlucoseDataListListGrid({ glucoseDataList, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {glucoseDataList.map((item) => (
          <Col key={item.id}>
            <GlucoseDataGridItem glucoseData={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function GlucoseDataListListTable({ glucoseDataList, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">
              Reading before meals(mmol/L)
            </th>
            <th className="list-table-head-cell">Severity</th>
            <th className="list-table-head-cell">Interpretation</th>
            <th className="list-table-head-cell">
              Reading after meals(mmol/L)
            </th>
            <th className="list-table-head-cell">Severity</th>
            <th className="list-table-head-cell">Interpretation</th>
            <th className="list-table-head-cell">Meal</th>
            <th className="list-table-head-cell">Remarks</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {glucoseDataList.map((item, index) => (
            <GlucoseDataTableItem
              glucoseData={item}
              appUser={appUser}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function GlucoseDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no glucose data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

GlucoseDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  glucoseDataList: PropTypes.array,
  GlucoseDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
GlucoseDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading glucose data list...',
  onLoadMore: undefined,
  glucoseDataList: null,
  GlucoseDataListListEmpty: null,
  isDisabled: false,
};
GlucoseDataListListGrid.propTypes = {
  appUser: PropTypes.object,
  glucoseDataList: PropTypes.array,
};
GlucoseDataListListGrid.defaultProps = {
  appUser: null,
  glucoseDataList: null,
};
GlucoseDataListListTable.propTypes = {
  appUser: PropTypes.object,
  glucoseDataList: PropTypes.array,
};
GlucoseDataListListTable.defaultProps = {
  appUser: null,
  glucoseDataList: null,
};
GlucoseDataListListEmpty.propTypes = {
  appUser: PropTypes.object,
};
GlucoseDataListListEmpty.defaultProps = {
  appUser: null,
};
