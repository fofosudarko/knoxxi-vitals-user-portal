import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetGlucoseDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function GlucoseDataListReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const _handleResetGlucoseDataList = useResetGlucoseDataList({
    userDataId: account?.customerId,
  });

  const handleResetGlucoseDataList = useCallback(() => {
    _handleResetGlucoseDataList();
  }, [_handleResetGlucoseDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetGlucoseDataList}
      text="Reload glucose data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

GlucoseDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
GlucoseDataListReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default GlucoseDataListReload;
