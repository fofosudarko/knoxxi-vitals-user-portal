import { useCallback, useState, useMemo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import {
  YUP_GLUCOSE_DATA_FASTING_READING_VALIDATOR,
  YUP_GLUCOSE_DATA_POSTPRANDIAL_READING_VALIDATOR,
  YUP_MEAL_VALIDATOR,
  YUP_REMARKS_VALIDATOR,
} from 'src/config/validators';
import {
  useFormReset,
  useDeviceDimensions,
  useNotificationHandler,
} from 'src/hooks';
import { humanizeString } from 'src/utils';

import { CancelButton, CreateButton, EditButton } from 'src/components/lib';
import { MealSelect } from 'src/components/enums';

function GlucoseDataInput({ glucoseData, isEditing, onSubmit, processing }) {
  const [meal, setMeal] = useState(glucoseData?.meal ?? 'meals');
  const [shouldInputReadings, setShouldInputReadings] = useState();
  const [shouldInputFastingReading, setShouldInputFastingReading] = useState();
  const [shouldInputPostprandialReading, setShouldInputPostprandialReading] =
    useState();
  const glucoseDataInputSchema = Yup(
    shouldInputReadings
      ? shouldInputFastingReading && shouldInputPostprandialReading
        ? {
            fastingReading: YUP_GLUCOSE_DATA_FASTING_READING_VALIDATOR,
            postprandialReading:
              YUP_GLUCOSE_DATA_POSTPRANDIAL_READING_VALIDATOR,
            meal: YUP_MEAL_VALIDATOR,
            remarks: YUP_REMARKS_VALIDATOR,
          }
        : shouldInputFastingReading
        ? {
            fastingReading: YUP_GLUCOSE_DATA_FASTING_READING_VALIDATOR,
            meal: YUP_MEAL_VALIDATOR,
            remarks: YUP_REMARKS_VALIDATOR,
          }
        : shouldInputPostprandialReading
        ? {
            postprandialReading:
              YUP_GLUCOSE_DATA_POSTPRANDIAL_READING_VALIDATOR,
            meal: YUP_MEAL_VALIDATOR,
            remarks: YUP_REMARKS_VALIDATOR,
          }
        : {
            meal: YUP_MEAL_VALIDATOR,
            remarks: YUP_REMARKS_VALIDATOR,
          }
      : {
          meal: YUP_MEAL_VALIDATOR,
          remarks: YUP_REMARKS_VALIDATOR,
        }
  );
  const { isLargeDevice } = useDeviceDimensions();
  const defaultValues = useMemo(
    () => ({
      fastingReading: glucoseData?.fastingReading ?? '',
      postprandialReading: glucoseData?.postprandialReading ?? '',
      meal: glucoseData?.meal ?? '',
      remarks: glucoseData?.remarks ?? '',
      inputReadings: shouldInputReadings,
      inputFastingReading: shouldInputFastingReading,
      inputPostprandialReading: shouldInputPostprandialReading,
    }),
    [
      glucoseData?.fastingReading,
      glucoseData?.postprandialReading,
      glucoseData?.meal,
      glucoseData?.remarks,
      shouldInputReadings,
      shouldInputFastingReading,
      shouldInputPostprandialReading,
    ]
  );

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    reset,
    watch,
  } = useForm({ defaultValues, resolver: yupResolver(glucoseDataInputSchema) });
  const handleNotification = useNotificationHandler();

  const watchedMeal = watch('meal');
  const watchedInputReadings = watch('inputReadings');
  const watchedInputFastingReading = watch('inputFastingReading');
  const watchedInputPostprandialReading = watch('inputPostprandialReading');

  useFormReset({ values: defaultValues, item: glucoseData, reset });

  useEffect(() => {
    if (watchedMeal !== undefined) {
      setMeal(watchedMeal === '' ? 'meals' : watchedMeal);
    }

    if (watchedInputReadings !== undefined) {
      setShouldInputReadings(watchedInputReadings);
    }

    if (watchedInputFastingReading !== undefined) {
      setShouldInputFastingReading(watchedInputFastingReading);
    }

    if (watchedInputPostprandialReading !== undefined) {
      setShouldInputPostprandialReading(watchedInputPostprandialReading);
    }
  }, [
    watchedInputFastingReading,
    watchedInputPostprandialReading,
    watchedInputReadings,
    watchedMeal,
  ]);

  const handleSubmitGlucoseData = useCallback(
    ({ postprandialReading, fastingReading, meal, remarks }) => {
      if (
        meal === undefined &&
        shouldInputReadings &&
        !postprandialReading &&
        !fastingReading
      ) {
        handleNotification(
          new Error(
            'Meal or at least one glucose data (fasting/postprandial) must exist'
          )
        );
        return;
      }
      const newGlucoseData = {
        postprandialReading,
        fastingReading,
        meal,
        remarks,
      };

      onSubmit && onSubmit(newGlucoseData);
    },
    [handleNotification, onSubmit, shouldInputReadings]
  );

  const handleCancelGlucoseData = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitGlucoseData)} noValidate>
        <Row xs={{ cols: 1 }}>
          <Form.Group className="my-1" as={Col}>
            <Controller
              name="meal"
              control={control}
              render={({ field }) => (
                <MealSelect
                  label="Meal"
                  labelClass="main-form-label"
                  placeholder="Meal"
                  helpText="Select meal. E.g BREAKFAST"
                  errors={errors}
                  {...field}
                />
              )}
            />
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">Remarks</Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  as="textarea"
                  rows={15}
                  placeholder="Remarks"
                  {...field}
                  isInvalid={!!errors.remarks}
                />
              )}
              name="remarks"
              control={control}
            />
            {errors.remarks ? (
              <Form.Control.Feedback type="invalid">
                {errors.remarks.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give any remarks e.g. Ate oats and omelette</Form.Text>
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Controller
              render={({ field }) => (
                <Form.Check
                  type="checkbox"
                  {...field}
                  label={`Would you like to ${
                    isEditing ? 'edit' : 'input'
                  } your glucose data readings?`}
                />
              )}
              name="inputReadings"
              control={control}
            />
          </Form.Group>

          {shouldInputReadings ? (
            <>
              <Form.Group className="my-1" as={Col}>
                <Controller
                  render={({ field }) => (
                    <Form.Check
                      type="checkbox"
                      {...field}
                      label={`Would you like to ${
                        isEditing ? 'edit' : 'input'
                      } your reading before ${humanizeString({
                        string: meal,
                        toLowerCase: true,
                      })}?`}
                    />
                  )}
                  name="inputFastingReading"
                  control={control}
                />
              </Form.Group>
              {shouldInputFastingReading ? (
                <Form.Group className="my-1" as={Col}>
                  <Form.Label className="main-form-label">
                    Reading before{' '}
                    {humanizeString({ string: meal, toLowerCase: true })}
                    (mmol/L)
                  </Form.Label>
                  <Controller
                    render={({ field }) => (
                      <Form.Control
                        type="text"
                        placeholder={`Reading before ${humanizeString({
                          string: meal,
                          toLowerCase: true,
                        })}`}
                        {...field}
                        isInvalid={!!errors.fastingReading}
                      />
                    )}
                    name="fastingReading"
                    control={control}
                  />
                  {errors.fastingReading ? (
                    <Form.Control.Feedback type="invalid">
                      {errors.fastingReading.message}
                    </Form.Control.Feedback>
                  ) : null}
                  <Form.Text>
                    Give the reading before{' '}
                    {humanizeString({ string: meal, toLowerCase: true })} e.g. 5
                    mmol/L
                  </Form.Text>
                </Form.Group>
              ) : null}

              <Form.Group className="my-1" as={Col}>
                <Controller
                  render={({ field }) => (
                    <Form.Check
                      type="checkbox"
                      {...field}
                      label={`Would you like to ${
                        isEditing ? 'edit' : 'input'
                      } your reading 2 hours after ${humanizeString({
                        string: meal,
                        toLowerCase: true,
                      })}?`}
                    />
                  )}
                  name="inputPostprandialReading"
                  control={control}
                />
              </Form.Group>
              {shouldInputPostprandialReading ? (
                <Form.Group className="my-1" as={Col}>
                  <Form.Label className="main-form-label">
                    Reading 2 hours after{' '}
                    {humanizeString({ string: meal, toLowerCase: true })}
                    (mmol/L)
                  </Form.Label>
                  <Controller
                    render={({ field }) => (
                      <Form.Control
                        type="text"
                        placeholder={`Reading 2 hours after ${humanizeString({
                          string: meal,
                          toLowerCase: true,
                        })}`}
                        {...field}
                        isInvalid={!!errors.postprandialReading}
                      />
                    )}
                    name="postprandialReading"
                    control={control}
                  />
                  {errors.postprandialReading ? (
                    <Form.Control.Feedback type="invalid">
                      {errors.postprandialReading.message}
                    </Form.Control.Feedback>
                  ) : null}
                  <Form.Text>
                    Give the reading 2 hours after{' '}
                    {humanizeString({ string: meal, toLowerCase: true })} e.g. 5
                    mmol/L
                  </Form.Text>
                </Form.Group>
              ) : null}
            </>
          ) : null}
        </Row>

        <div className="d-flex flex-column-reverse flex-md-row justify-content-end my-3">
          <div className="my-1 my-sm-0 mx-1">
            <CancelButton
              onClick={handleCancelGlucoseData}
              autoWidth={isLargeDevice}
              text="Cancel"
            />
          </div>
          <div className="my-1 my-sm-0 mx-1">
            {isEditing ? (
              <EditButton
                type="submit"
                clicked={processing}
                text="Edit glucose data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            ) : (
              <CreateButton
                type="submit"
                clicked={processing}
                text="Add glucose data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            )}
          </div>
        </div>
      </Form>
    </div>
  );
}

GlucoseDataInput.propTypes = {
  appUser: PropTypes.object,
  glucoseData: PropTypes.object,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
};
GlucoseDataInput.defaultProps = {
  appUser: null,
  glucoseData: null,
  isEditing: false,
  onSubmit: undefined,
  processing: false,
};

export default GlucoseDataInput;
