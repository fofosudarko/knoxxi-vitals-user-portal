import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCreateGlucoseData } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';
import { DEFAULT_PARTNERED_BY_ALIAS, DEFAULT_DATA_CHANNEL } from 'src/config';

import { ContentContainer, GoBack } from 'src/components/lib';
import GlucoseDataInput from '../GlucoseDataInput/GlucoseDataInput';

function GlucoseDataNewPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { handleGlucoseDataListRoute } = useRoutes().useGlucoseDataListRoute();
  const {
    handleCreateGlucoseData: _handleCreateGlucoseData,
    error,
    setError,
    processing,
    glucoseDataCreated,
    setGlucoseDataCreated,
  } = useCreateGlucoseData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (glucoseDataCreated) {
      handleApiResult();
    }
  }, [glucoseDataCreated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Glucose data created successfully'
      ).getSuccessNotification(),
      () => {
        setGlucoseDataCreated(false);
      }
    );
    handleGlucoseDataListRoute();
  }, [handleGlucoseDataListRoute, handleNotification, setGlucoseDataCreated]);

  const handleCreateGlucoseData = useCallback(
    async (glucoseData) => {
      const body = {
        ...glucoseData,
        kycId: account?.customerId,
        mobileNumber: null,
        partneredByAlias: DEFAULT_PARTNERED_BY_ALIAS,
        dataChannel: DEFAULT_DATA_CHANNEL,
      };
      await _handleCreateGlucoseData(body);
    },
    [_handleCreateGlucoseData, account?.customerId]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">New glucose data</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <GlucoseDataInput
          appUser={appUser}
          onSubmit={handleCreateGlucoseData}
          processing={processing}
        />
      </ContentContainer>
    </div>
  );
}

GlucoseDataNewPage.propTypes = {
  appUser: PropTypes.object,
};
GlucoseDataNewPage.defaultProps = {
  appUser: null,
};

export default GlucoseDataNewPage;
