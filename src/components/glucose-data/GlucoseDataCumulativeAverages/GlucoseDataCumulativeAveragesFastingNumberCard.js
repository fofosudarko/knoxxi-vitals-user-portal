import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName, round } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function GlucoseDataCumulativeAveragesFastingNumberCard({
  glucoseDataCumulativeAveragesFasting,
  label,
  onClick,
}) {
  const fastingReading =
    glucoseDataCumulativeAveragesFasting?.fastingReading !== undefined
      ? round(glucoseDataCumulativeAveragesFasting.fastingReading, 2)
      : 'N/A';
  const fastingGlucoseDataSeverity =
    glucoseDataCumulativeAveragesFasting?.fastingGlucoseDataSeverity ?? {};
  return (
    <NumberCard
      value={fastingReading}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(
        fastingGlucoseDataSeverity.name
      )}
      onClick={onClick}
    />
  );
}

GlucoseDataCumulativeAveragesFastingNumberCard.propTypes = {
  glucoseDataCumulativeAveragesFasting: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
GlucoseDataCumulativeAveragesFastingNumberCard.defaultProps = {
  glucoseDataCumulativeAveragesFasting: null,
  label: null,
  onClick: undefined,
};

export default GlucoseDataCumulativeAveragesFastingNumberCard;
