import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName, round } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function GlucoseDataCumulativeAveragesPostprandialNumberCard({
  glucoseDataCumulativeAveragesPostprandial,
  label,
  onClick,
}) {
  const postprandialReading =
    glucoseDataCumulativeAveragesPostprandial?.postprandialReading !== undefined
      ? round(glucoseDataCumulativeAveragesPostprandial.postprandialReading, 2)
      : 'N/A';
  const postprandialGlucoseDataSeverity =
    glucoseDataCumulativeAveragesPostprandial?.postprandialGlucoseDataSeverity ??
    {};
  return (
    <NumberCard
      value={postprandialReading}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(
        postprandialGlucoseDataSeverity.name
      )}
      onClick={onClick}
    />
  );
}

GlucoseDataCumulativeAveragesPostprandialNumberCard.propTypes = {
  glucoseDataCumulativeAveragesPostprandial: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
GlucoseDataCumulativeAveragesPostprandialNumberCard.defaultProps = {
  glucoseDataCumulativeAveragesPostprandial: null,
  label: null,
  onClick: undefined,
};

export default GlucoseDataCumulativeAveragesPostprandialNumberCard;
