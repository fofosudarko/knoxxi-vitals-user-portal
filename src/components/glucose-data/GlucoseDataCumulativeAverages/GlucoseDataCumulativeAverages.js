import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { useNotificationHandler } from 'src/hooks';
import { useListGlucoseDataCumulativeAverages } from 'src/hooks/api';

import GlucoseDataCumulativeAveragesFastingNumberCard from './GlucoseDataCumulativeAveragesFastingNumberCard';
import GlucoseDataCumulativeAveragesPostprandialNumberCard from './GlucoseDataCumulativeAveragesPostprandialNumberCard';

function GlucoseDataCumulativeAveragesContainer({
  appUser,
  query,
  label,
  isCard,
  onClick,
}) {
  const {
    glucoseDataCumulativeAverages,
    error: glucoseDataCumulativeAveragesError,
    setError,
  } = useListGlucoseDataCumulativeAverages({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (glucoseDataCumulativeAveragesError) {
      handleNotification(glucoseDataCumulativeAveragesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, glucoseDataCumulativeAveragesError, setError]);

  return (
    <GlucoseDataCumulativeAverages
      glucoseDataCumulativeAverages={glucoseDataCumulativeAverages}
      label={label}
      appUser={appUser}
      isDisabled={
        glucoseDataCumulativeAverages &&
        glucoseDataCumulativeAverages.length === 0
      }
      query={query}
      isCard={isCard}
      onClick={onClick}
    />
  );
}

function GlucoseDataCumulativeAverages({
  glucoseDataCumulativeAverages,
  appUser,
  isDisabled,
  isCard,
  onClick,
}) {
  let glucoseDataCumulativeAveragesFasting = null,
    glucoseDataCumulativeAveragesPostprandial = null;

  if (glucoseDataCumulativeAverages?.length) {
    const {
      fastingReading,
      postprandialReading,
      fastingGlucoseDataInterpretation,
      fastingGlucoseDataSeverity,
      postprandialGlucoseDataInterpretation,
      postprandialGlucoseDataSeverity,
    } = glucoseDataCumulativeAverages[0] ?? {};
    glucoseDataCumulativeAveragesFasting = {
      fastingReading,
      fastingGlucoseDataInterpretation,
      fastingGlucoseDataSeverity,
    };
    glucoseDataCumulativeAveragesPostprandial = {
      postprandialReading,
      postprandialGlucoseDataInterpretation,
      postprandialGlucoseDataSeverity,
    };
  }

  return (
    <div>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 2 }}
        className="gx-3 gy-3"
      >
        <Col>
          {isCard ? (
            <GlucoseDataCumulativeAveragesFastingNumberCard
              isDisabled={isDisabled}
              glucoseDataCumulativeAveragesFasting={
                glucoseDataCumulativeAveragesFasting
              }
              label="Reading before meals(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <GlucoseDataCumulativeAveragesPostprandialNumberCard
              isDisabled={isDisabled}
              glucoseDataCumulativeAveragesPostprandial={
                glucoseDataCumulativeAveragesPostprandial
              }
              label="Reading after meals(mmol/L)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
      </Row>
    </div>
  );
}

export function GlucoseDataCumulativeAveragesNumberCardView({
  appUser,
  query,
  label,
  onClick,
}) {
  return (
    <div className="d-block">
      <GlucoseDataCumulativeAveragesContainer
        appUser={appUser}
        query={query}
        label={label}
        isCard
        onClick={onClick}
      />
    </div>
  );
}

GlucoseDataCumulativeAveragesContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
GlucoseDataCumulativeAveragesContainer.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  isCard: false,
  onClick: undefined,
};
GlucoseDataCumulativeAverages.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  query: PropTypes.object,
  glucoseDataCumulativeAverages: PropTypes.array,
  isDisabled: PropTypes.bool,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
GlucoseDataCumulativeAverages.defaultProps = {
  appUser: null,
  label: undefined,
  query: null,
  glucoseDataCumulativeAverages: undefined,
  isDisabled: false,
  isCard: false,
  onClick: undefined,
};
GlucoseDataCumulativeAveragesNumberCardView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
GlucoseDataCumulativeAveragesNumberCardView.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  onClick: undefined,
};

export default GlucoseDataCumulativeAverages;
