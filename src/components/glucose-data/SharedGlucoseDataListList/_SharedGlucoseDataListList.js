import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import {
  SharedGlucoseDataTableItem,
  SharedGlucoseDataGridItem,
} from './SharedGlucoseDataItem';

export function SharedGlucoseDataListList({
  sharedGlucoseDataList,
  loadingText,
  healthDataSharePublic,
  onLoadMore,
  SharedGlucoseDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!sharedGlucoseDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(SharedGlucoseDataListListEmpty);

  return sharedGlucoseDataList.length ? (
    <div>
      {isLargeDevice ? (
        <SharedGlucoseDataListListTable
          sharedGlucoseDataList={sharedGlucoseDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <SharedGlucoseDataListListGrid
          sharedGlucoseDataList={sharedGlucoseDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      )}

      {sharedGlucoseDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more shared glucose data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty healthDataSharePublic={healthDataSharePublic} />
  );
}

export function SharedGlucoseDataListListGrid({
  sharedGlucoseDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {sharedGlucoseDataList.map((item) => (
          <Col key={item.id}>
            <SharedGlucoseDataGridItem
              glucoseData={item}
              healthDataSharePublic={healthDataSharePublic}
            />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function SharedGlucoseDataListListTable({
  sharedGlucoseDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">
              Reading before meals(mmol/L)
            </th>
            <th className="list-table-head-cell">Severity</th>
            <th className="list-table-head-cell">Interpretation</th>
            <th className="list-table-head-cell">
              Reading after meals(mmol/L)
            </th>
            <th className="list-table-head-cell">Severity</th>
            <th className="list-table-head-cell">Interpretation</th>
            <th className="list-table-head-cell">Meal</th>
            <th className="list-table-head-cell">Remarks</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {sharedGlucoseDataList.map((item, index) => (
            <SharedGlucoseDataTableItem
              glucoseData={item}
              healthDataSharePublic={healthDataSharePublic}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function SharedGlucoseDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no shared glucose data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

SharedGlucoseDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedGlucoseDataList: PropTypes.array,
  SharedGlucoseDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedGlucoseDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared glucose data list...',
  onLoadMore: undefined,
  sharedGlucoseDataList: null,
  SharedGlucoseDataListListEmpty: null,
  isDisabled: false,
};
SharedGlucoseDataListListGrid.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedGlucoseDataList: PropTypes.array,
};
SharedGlucoseDataListListGrid.defaultProps = {
  healthDataSharePublic: null,
  sharedGlucoseDataList: null,
};
SharedGlucoseDataListListTable.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedGlucoseDataList: PropTypes.array,
};
SharedGlucoseDataListListTable.defaultProps = {
  healthDataSharePublic: null,
  sharedGlucoseDataList: null,
};
SharedGlucoseDataListListEmpty.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedGlucoseDataListListEmpty.defaultProps = {
  healthDataSharePublic: null,
};
