import PropTypes from 'prop-types';
import { Row, Col, Badge } from 'react-bootstrap';
//import { FaEllipsisV } from 'react-icons/fa';

//import { useDeviceDimensions } from 'src/hooks';
import { humanizeDate, getVariantFromHealthDataSeverityName } from 'src/utils';

export function useGlucoseDataDetails(glucoseData) {
  const {
    createdOn,
    fastingReading,
    postprandialReading,
    fastingGlucoseDataSeverity,
    postprandialGlucoseDataSeverity,
    fastingGlucoseDataInterpretation = null,
    postprandialGlucoseDataInterpretation = null,
    meal,
    remarks,
  } = glucoseData ?? {};
  const {
    name: fastingGlucoseDataInterpretationName,
    description: fastingGlucoseDataInterpretationDescription,
    explanation: fastingGlucoseDataInterpretationExplanation,
  } = fastingGlucoseDataInterpretation ?? {};
  const {
    name: postprandialGlucoseDataInterpretationName,
    description: postprandialGlucoseDataInterpretationDescription,
    explanation: postprandialGlucoseDataInterpretationExplanation,
  } = postprandialGlucoseDataInterpretation ?? {};

  return {
    createdOn,
    fastingReading,
    postprandialReading,
    fastingGlucoseDataSeverity,
    postprandialGlucoseDataSeverity,
    fastingGlucoseDataInterpretationName,
    fastingGlucoseDataInterpretationDescription,
    fastingGlucoseDataInterpretationExplanation,
    postprandialGlucoseDataInterpretationName,
    postprandialGlucoseDataInterpretationDescription,
    postprandialGlucoseDataInterpretationExplanation,
    meal,
    remarks,
  };
}

export function SharedGlucoseDataGridItem({ glucoseData }) {
  const {
    createdOn,
    fastingReading,
    postprandialReading,
    fastingGlucoseDataSeverity,
    postprandialGlucoseDataSeverity,
    fastingGlucoseDataInterpretationName,
    fastingGlucoseDataInterpretationDescription,
    fastingGlucoseDataInterpretationExplanation,
    postprandialGlucoseDataInterpretationName,
    postprandialGlucoseDataInterpretationDescription,
    postprandialGlucoseDataInterpretationExplanation,
    meal,
    remarks,
  } = useGlucoseDataDetails(glucoseData);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Created</div>
            <div className="item-subtitle">
              {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Fasting reading(mmol/L)</div>
            <div className="item-subtitle fw-bold">
              <span style={{ color: fastingGlucoseDataSeverity.colorCode }}>
                {fastingReading !== undefined ? fastingReading : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">Severity(Fasting)</div>
            <div>
              <Badge
                bg={getVariantFromHealthDataSeverityName(
                  fastingGlucoseDataSeverity.name
                )}
              >
                {fastingGlucoseDataSeverity.name}
              </Badge>
            </div>
          </Col>
          <Col>
            <div className="item-title">Interpretation(Fasting)</div>
            <div className="item-subtitle">
              {fastingGlucoseDataInterpretationName !== undefined ? (
                <span>
                  <span style={{ color: fastingGlucoseDataSeverity.colorCode }}>
                    {fastingGlucoseDataInterpretationName}
                  </span>
                  : {fastingGlucoseDataInterpretationDescription},{' '}
                  {fastingGlucoseDataInterpretationExplanation}
                </span>
              ) : (
                'N/A'
              )}
            </div>
          </Col>
          <Col>
            <div className="item-title">Postprandial reading(mmol/L)</div>
            <div className="item-subtitle fw-bold">
              <span
                style={{ color: postprandialGlucoseDataSeverity.colorCode }}
              >
                {postprandialReading !== undefined
                  ? postprandialReading
                  : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">Severity(Postprandial)</div>
            <div>
              <Badge
                bg={getVariantFromHealthDataSeverityName(
                  postprandialGlucoseDataSeverity.name
                )}
              >
                {postprandialGlucoseDataSeverity.name}
              </Badge>
            </div>
          </Col>
          <Col>
            <div className="item-title">Interpretation(Postprandial)</div>
            <div className="item-subtitle">
              <div>
                {postprandialGlucoseDataInterpretationName !== undefined ? (
                  <span>
                    <span
                      style={{
                        color: postprandialGlucoseDataSeverity.colorCode,
                      }}
                    >
                      {postprandialGlucoseDataInterpretationName}
                    </span>
                    : {postprandialGlucoseDataInterpretationDescription},{' '}
                    {postprandialGlucoseDataInterpretationExplanation}
                  </span>
                ) : (
                  'N/A'
                )}
              </div>
            </div>
          </Col>
          <Col>
            <div className="item-title">Meal</div>
            <div className="item-subtitle">{meal ? meal : 'N/A'}</div>
          </Col>
          <Col>
            <div className="item-title">Remarks</div>
            <div className="item-subtitle">{remarks ? remarks : 'N/A'}</div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              {/*<GlucoseDataItemActions
                glucoseData={glucoseData}
                healthDataSharePublic={healthDataSharePublic}
              />*/}
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function SharedGlucoseDataTableItem({ glucoseData }) {
  const {
    createdOn,
    fastingReading,
    postprandialReading,
    fastingGlucoseDataSeverity,
    postprandialGlucoseDataSeverity,
    fastingGlucoseDataInterpretationName,
    fastingGlucoseDataInterpretationDescription,
    fastingGlucoseDataInterpretationExplanation,
    postprandialGlucoseDataInterpretationName,
    postprandialGlucoseDataInterpretationDescription,
    postprandialGlucoseDataInterpretationExplanation,
    meal,
    remarks,
  } = useGlucoseDataDetails(glucoseData);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span style={{ color: fastingGlucoseDataSeverity.colorCode }}>
            {fastingReading !== undefined ? fastingReading : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <Badge
          bg={getVariantFromHealthDataSeverityName(
            fastingGlucoseDataSeverity.name
          )}
        >
          {fastingGlucoseDataSeverity.name}
        </Badge>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {fastingGlucoseDataInterpretationName !== undefined ? (
            <span>
              <span style={{ color: fastingGlucoseDataSeverity.colorCode }}>
                {fastingGlucoseDataInterpretationName}
              </span>
              : {fastingGlucoseDataInterpretationDescription},{' '}
              {fastingGlucoseDataInterpretationExplanation}
            </span>
          ) : (
            'N/A'
          )}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span style={{ color: postprandialGlucoseDataSeverity.colorCode }}>
            {postprandialReading !== undefined ? postprandialReading : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <Badge
          bg={getVariantFromHealthDataSeverityName(
            postprandialGlucoseDataSeverity.name
          )}
        >
          {postprandialGlucoseDataSeverity.name}
        </Badge>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {postprandialGlucoseDataInterpretationName !== undefined ? (
            <span>
              <span
                style={{ color: postprandialGlucoseDataSeverity.colorCode }}
              >
                {postprandialGlucoseDataInterpretationName}
              </span>
              : {postprandialGlucoseDataInterpretationDescription},{' '}
              {postprandialGlucoseDataInterpretationExplanation}
            </span>
          ) : (
            'N/A'
          )}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {meal ? meal : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {remarks ? remarks : 'N/A'}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          {/*<GlucoseDataItemActions glucoseData={glucoseData} healthDataSharePublic={healthDataSharePublic} />*/}
        </div>
      </td>
    </tr>
  );
}

/*export function GlucoseDataItemActions({ glucoseData, healthDataSharePublic }) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item>
            <GlucoseDataEditRoute glucoseData={glucoseData} healthDataSharePublic={healthDataSharePublic} />
          </Dropdown.Item>
          <Dropdown.Item>
            <GlucoseDataRemove glucoseData={glucoseData} healthDataSharePublic={healthDataSharePublic} />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}*/

SharedGlucoseDataGridItem.propTypes = {
  glucoseData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedGlucoseDataGridItem.defaultProps = {
  glucoseData: null,
  healthDataSharePublic: null,
};
SharedGlucoseDataTableItem.propTypes = {
  glucoseData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedGlucoseDataTableItem.defaultProps = {
  glucoseData: null,
  healthDataSharePublic: null,
};
/*GlucoseDataItemActions.propTypes = {
  glucoseData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
GlucoseDataItemActions.defaultProps = {
  glucoseData: null,
  healthDataSharePublic: null,
};*/
