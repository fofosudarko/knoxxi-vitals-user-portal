import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetSharedGlucoseDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function SharedGlucoseDataListReload({ useTooltip }) {
  const _handleResetSharedGlucoseDataList = useResetSharedGlucoseDataList();

  const handleResetSharedGlucoseDataList = useCallback(() => {
    _handleResetSharedGlucoseDataList();
  }, [_handleResetSharedGlucoseDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetSharedGlucoseDataList}
      text="Reload shared glucose data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

SharedGlucoseDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  healthDataSharePublic: PropTypes.object,
};
SharedGlucoseDataListReload.defaultProps = {
  useTooltip: false,
  healthDataSharePublic: null,
};

export default SharedGlucoseDataListReload;
