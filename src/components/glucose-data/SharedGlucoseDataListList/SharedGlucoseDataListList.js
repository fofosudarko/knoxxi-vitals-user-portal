import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListSharedGlucoseDataList } from 'src/hooks/api';

import SharedGlucoseDataListReload from './SharedGlucoseDataListReload';
import {
  SharedGlucoseDataListList,
  SharedGlucoseDataListListEmpty,
} from './_SharedGlucoseDataListList';

function SharedGlucoseDataListListContainer({ healthDataSharePublic, query }) {
  const {
    sharedGlucoseDataList,
    error: sharedGlucoseDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListSharedGlucoseDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (sharedGlucoseDataListError) {
      handleNotification(sharedGlucoseDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, sharedGlucoseDataListError, setError]);

  const handleLoadMoreSharedGlucoseDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <SharedGlucoseDataListList
      sharedGlucoseDataList={sharedGlucoseDataList}
      onLoadMore={handleLoadMoreSharedGlucoseDataList}
      loadingText="Loading shared glucose data list..."
      healthDataSharePublic={healthDataSharePublic}
      SharedGlucoseDataListListEmpty={SharedGlucoseDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function SharedGlucoseDataListListView({
  healthDataSharePublic,
  query,
}) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <SharedGlucoseDataListReload
          useTooltip
          healthDataSharePublic={healthDataSharePublic}
        />
      </div>
      <div className="my-2">
        <SharedGlucoseDataListListContainer
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedGlucoseDataListListContainer.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedGlucoseDataListListContainer.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};
SharedGlucoseDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedGlucoseDataList: PropTypes.array,
  SharedGlucoseDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedGlucoseDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared glucose data list...',
  onLoadMore: undefined,
  sharedGlucoseDataList: null,
  SharedGlucoseDataListListEmpty: null,
  isDisabled: false,
};
SharedGlucoseDataListListView.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedGlucoseDataListListView.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};

export default SharedGlucoseDataListList;
