import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { CreateButton } from 'src/components/lib';

export default function GlucoseDataNewRoute({ text }) {
  const { handleGlucoseDataNewRoute } = useRoutes().useGlucoseDataNewRoute();

  return (
    <CreateButton
      variant="primary"
      textColor="white"
      onClick={handleGlucoseDataNewRoute}
      text={text}
    />
  );
}

GlucoseDataNewRoute.propTypes = {
  text: PropTypes.string,
};
GlucoseDataNewRoute.defaultProps = {
  text: 'New glucose data',
};
