import { Container, Row, Col, Button } from 'react-bootstrap';
import Image from 'next/image';

import { APP_LOGO } from 'src/config';
import { useRoutes, useDeviceDimensions } from 'src/hooks';

function SitePage() {
  const { isSmallDevice, isMediumDevice, isLargeDevice } =
    useDeviceDimensions();
  const { handleSignInRoute } = useRoutes().useSignInRoute();

  return (
    <Container fluid className="main-site">
      <div className="mx-auto d-flex align-items-center justify-content-center vh-100">
        <div
          className={`${
            isSmallDevice
              ? 'w-100'
              : isMediumDevice
              ? 'w-75'
              : isLargeDevice
              ? 'w-25'
              : 'w-100'
          } bg-white p-4 p-md-5 rounded shadow`}
        >
          <div className="w-100 d-flex justify-content-center">
            <Image
              src={APP_LOGO}
              alt="Knoxxi Logo"
              width={isLargeDevice ? 200 : 100}
              height={isLargeDevice ? 70 : 35}
            />
          </div>
          <div style={{ marginTop: '25px', marginBottom: '25px' }}>
            <p className="text-primary text-center fs-3 fw-normal text-lowercase">
              Knoxxi Vitals User Portal
            </p>
          </div>
          <Row xs={{ cols: 1 }}>
            <Col>
              <Button
                className="w-100 my-1 my-lg-0 text-white"
                size="lg"
                variant="primary"
                onClick={handleSignInRoute}
              >
                <div>
                  <span className="fw-bold fs-5">Sign in</span>
                </div>
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    </Container>
  );
}

export default SitePage;
