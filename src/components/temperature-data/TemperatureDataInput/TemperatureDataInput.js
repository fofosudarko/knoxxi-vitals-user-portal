import { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { YUP_TEMPERATURE_DATA_READING_VALIDATOR } from 'src/config/validators';
import { useFormReset, useDeviceDimensions } from 'src/hooks';

import { CancelButton, CreateButton, EditButton } from 'src/components/lib';

function TemperatureDataInput({
  temperatureData,
  isEditing,
  onSubmit,
  processing,
}) {
  const temperatureDataInputSchema = Yup({
    reading: YUP_TEMPERATURE_DATA_READING_VALIDATOR,
  });
  const { isLargeDevice } = useDeviceDimensions();
  const defaultValues = useMemo(
    () => ({
      reading: temperatureData?.reading ?? '',
    }),
    [temperatureData?.reading]
  );

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(temperatureDataInputSchema),
  });

  useFormReset({ values: defaultValues, item: temperatureData, reset });

  const handleSubmitTemperatureData = useCallback(
    ({ reading, pulse, diastolic }) => {
      const newTemperatureData = {
        reading,
        pulse,
        diastolic,
      };

      onSubmit && onSubmit(newTemperatureData);
    },
    [onSubmit]
  );

  const handleCancelTemperatureData = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitTemperatureData)} noValidate>
        <Row xs={{ cols: 1 }}>
          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">
              Reading(<sup>o</sup>C)
            </Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Reading"
                  {...field}
                  isInvalid={!!errors.reading}
                />
              )}
              name="reading"
              control={control}
            />
            {errors.reading ? (
              <Form.Control.Feedback type="invalid">
                {errors.reading.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>
              Give the reading e.g. 32.3<sup>o</sup>C
            </Form.Text>
          </Form.Group>
        </Row>

        <div className="d-flex flex-column-reverse flex-md-row justify-content-end my-3">
          <div className="my-1 my-sm-0 mx-1">
            <CancelButton
              onClick={handleCancelTemperatureData}
              autoWidth={isLargeDevice}
              text="Cancel"
            />
          </div>
          <div className="my-1 my-sm-0 mx-1">
            {isEditing ? (
              <EditButton
                type="submit"
                clicked={processing}
                text="Edit temperature data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            ) : (
              <CreateButton
                type="submit"
                clicked={processing}
                text="Add temperature data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            )}
          </div>
        </div>
      </Form>
    </div>
  );
}

TemperatureDataInput.propTypes = {
  appUser: PropTypes.object,
  temperatureData: PropTypes.object,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
};
TemperatureDataInput.defaultProps = {
  appUser: null,
  temperatureData: null,
  isEditing: false,
  onSubmit: undefined,
  processing: false,
};

export default TemperatureDataInput;
