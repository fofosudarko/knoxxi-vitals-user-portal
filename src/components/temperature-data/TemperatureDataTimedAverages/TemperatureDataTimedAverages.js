import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { useNotificationHandler } from 'src/hooks';
import { useListTemperatureDataTimedAverages } from 'src/hooks/api';

import TemperatureDataTimedAveragesListChartCard from './TemperatureDataTimedAveragesListChartCard';

function TemperatureDataTimedAveragesContainer({
  appUser,
  query,
  label,
  isCard,
  onClick,
}) {
  const {
    temperatureDataTimedAverages,
    error: temperatureDataTimedAveragesError,
    setError,
  } = useListTemperatureDataTimedAverages({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (temperatureDataTimedAveragesError) {
      handleNotification(temperatureDataTimedAveragesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, temperatureDataTimedAveragesError, setError]);

  return (
    <TemperatureDataTimedAverages
      temperatureDataTimedAverages={temperatureDataTimedAverages}
      label={label}
      appUser={appUser}
      isDisabled={
        temperatureDataTimedAverages &&
        temperatureDataTimedAverages.length === 0
      }
      query={query}
      isCard={isCard}
      onClick={onClick}
    />
  );
}

function TemperatureDataTimedAverages({
  temperatureDataTimedAverages,
  appUser,
  isDisabled,
  isCard,
  onClick,
}) {
  const temperatureDataTimedAveragesList = [];

  temperatureDataTimedAverages?.forEach((temperatureDataTimedAverage) => {
    const {
      createdOn,
      reading,
      temperatureDataInterpretation,
      temperatureDataSeverity,
    } = temperatureDataTimedAverage;
    temperatureDataTimedAveragesList.push({
      createdOn,
      reading,
      temperatureDataInterpretation,
      temperatureDataSeverity,
    });
  });

  return (
    <div>
      <Row
        xs={{ cols: 1 }}
        //md={{ cols: 2 }}
        //lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        <Col>
          {isCard ? (
            <TemperatureDataTimedAveragesListChartCard
              isDisabled={isDisabled}
              temperatureDataTimedAveragesList={
                temperatureDataTimedAveragesList
              }
              label="Reading(C)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
      </Row>
    </div>
  );
}

export function TemperatureDataTimedAveragesChartCardView({
  appUser,
  query,
  label,
  onClick,
}) {
  return (
    <div className="d-block">
      <TemperatureDataTimedAveragesContainer
        appUser={appUser}
        query={query}
        label={label}
        isCard
        onClick={onClick}
      />
    </div>
  );
}

TemperatureDataTimedAveragesContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
TemperatureDataTimedAveragesContainer.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  isCard: false,
  onClick: undefined,
};
TemperatureDataTimedAverages.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  query: PropTypes.object,
  temperatureDataTimedAverages: PropTypes.array,
  isDisabled: PropTypes.bool,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
TemperatureDataTimedAverages.defaultProps = {
  appUser: null,
  label: undefined,
  query: null,
  temperatureDataTimedAverages: undefined,
  isDisabled: false,
  isCard: false,
  onClick: undefined,
};
TemperatureDataTimedAveragesChartCardView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
TemperatureDataTimedAveragesChartCardView.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  onClick: undefined,
};

export default TemperatureDataTimedAverages;
