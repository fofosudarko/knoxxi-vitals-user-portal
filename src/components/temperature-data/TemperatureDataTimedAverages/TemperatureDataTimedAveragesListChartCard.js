import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function TemperatureDataTimedAveragesListChartCard({
  temperatureDataTimedAveragesList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          temperatureDataTimedAveragesList?.map((item) => item.createdOn) ?? [],
        datasets: [
          {
            label,
            data:
              temperatureDataTimedAveragesList?.map((item) => item.reading) ??
              [],
            backgroundColor:
              temperatureDataTimedAveragesList?.map(
                (item) => item.temperatureDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [temperatureDataTimedAveragesList, label]);
  return temperatureDataTimedAveragesList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

TemperatureDataTimedAveragesListChartCard.propTypes = {
  temperatureDataTimedAveragesList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
TemperatureDataTimedAveragesListChartCard.defaultProps = {
  temperatureDataTimedAveragesList: null,
  appUser: null,
  label: null,
  isDisabled: false,
  onClick: undefined,
};

export default TemperatureDataTimedAveragesListChartCard;
