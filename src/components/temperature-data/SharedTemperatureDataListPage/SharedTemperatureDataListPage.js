import PropTypes from 'prop-types';

import { GoBack } from 'src/components/lib';
import { SharedTemperatureDataListListView } from '../SharedTemperatureDataListList/SharedTemperatureDataListList';

function SharedTemperatureDataListPage({ healthDataSharePublic }) {
  const query = { healthDataSharePublic };
  return (
    <div>
      <div className="justify-between">
        <div className="page-title">Shared temperature data</div>
        <GoBack />
      </div>
      <div className="page-content">
        <SharedTemperatureDataListListView
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedTemperatureDataListPage.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedTemperatureDataListPage.defaultProps = {
  healthDataSharePublic: null,
};

export default SharedTemperatureDataListPage;
