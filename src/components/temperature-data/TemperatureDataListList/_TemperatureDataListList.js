import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import {
  TemperatureDataTableItem,
  TemperatureDataGridItem,
} from './TemperatureDataItem';

export function TemperatureDataListList({
  temperatureDataList,
  loadingText,
  appUser,
  onLoadMore,
  TemperatureDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!temperatureDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(TemperatureDataListListEmpty);

  return temperatureDataList.length ? (
    <div>
      {isLargeDevice ? (
        <TemperatureDataListListTable
          temperatureDataList={temperatureDataList}
          appUser={appUser}
        />
      ) : (
        <TemperatureDataListListGrid
          temperatureDataList={temperatureDataList}
          appUser={appUser}
        />
      )}

      {temperatureDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more temperature data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function TemperatureDataListListGrid({ temperatureDataList, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {temperatureDataList.map((item) => (
          <Col key={item.id}>
            <TemperatureDataGridItem temperatureData={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function TemperatureDataListListTable({ temperatureDataList, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">
              Reading(<sup>o</sup>C)
            </th>
            <th className="list-table-head-cell">Severity</th>
            <th className="list-table-head-cell">Interpretation</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {temperatureDataList.map((item, index) => (
            <TemperatureDataTableItem
              temperatureData={item}
              appUser={appUser}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function TemperatureDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no temperature data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

TemperatureDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  temperatureDataList: PropTypes.array,
  TemperatureDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
TemperatureDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading temperature data list...',
  onLoadMore: undefined,
  temperatureDataList: null,
  TemperatureDataListListEmpty: null,
  isDisabled: false,
};
TemperatureDataListListGrid.propTypes = {
  appUser: PropTypes.object,
  temperatureDataList: PropTypes.array,
};
TemperatureDataListListGrid.defaultProps = {
  appUser: null,
  temperatureDataList: null,
};
TemperatureDataListListTable.propTypes = {
  appUser: PropTypes.object,
  temperatureDataList: PropTypes.array,
};
TemperatureDataListListTable.defaultProps = {
  appUser: null,
  temperatureDataList: null,
};
TemperatureDataListListEmpty.propTypes = {
  appUser: PropTypes.object,
};
TemperatureDataListListEmpty.defaultProps = {
  appUser: null,
};
