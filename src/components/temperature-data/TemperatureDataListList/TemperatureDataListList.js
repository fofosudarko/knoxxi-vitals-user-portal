import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListTemperatureDataList } from 'src/hooks/api';

import TemperatureDataListReload from './TemperatureDataListReload';
import TemperatureDataNewRoute from '../TemperatureDataNew/TemperatureDataNewRoute';
import {
  TemperatureDataListList,
  TemperatureDataListListEmpty,
} from './_TemperatureDataListList';

function TemperatureDataListListContainer({ appUser, query }) {
  const {
    temperatureDataList,
    error: temperatureDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListTemperatureDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (temperatureDataListError) {
      handleNotification(temperatureDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, temperatureDataListError, setError]);

  const handleLoadMoreTemperatureDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <TemperatureDataListList
      temperatureDataList={temperatureDataList}
      onLoadMore={handleLoadMoreTemperatureDataList}
      loadingText="Loading temperature data list..."
      appUser={appUser}
      TemperatureDataListListEmpty={TemperatureDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function TemperatureDataListListView({ appUser, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end my-1">
        <TemperatureDataNewRoute />
      </div>
      <div className="d-flex justify-content-end">
        <TemperatureDataListReload useTooltip appUser={appUser} />
      </div>
      <div className="my-2">
        <TemperatureDataListListContainer appUser={appUser} query={query} />
      </div>
    </div>
  );
}

TemperatureDataListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
TemperatureDataListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
TemperatureDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  temperatureDataList: PropTypes.array,
  TemperatureDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
TemperatureDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading temperature data list...',
  onLoadMore: undefined,
  temperatureDataList: null,
  TemperatureDataListListEmpty: null,
  isDisabled: false,
};
TemperatureDataListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
TemperatureDataListListView.defaultProps = {
  appUser: null,
  query: null,
};

export default TemperatureDataListList;
