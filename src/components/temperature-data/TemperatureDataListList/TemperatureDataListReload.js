import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetTemperatureDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function TemperatureDataListReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const _handleResetTemperatureDataList = useResetTemperatureDataList({
    userDataId: account?.customerId,
  });

  const handleResetTemperatureDataList = useCallback(() => {
    _handleResetTemperatureDataList();
  }, [_handleResetTemperatureDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetTemperatureDataList}
      text="Reload temperature data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

TemperatureDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
TemperatureDataListReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default TemperatureDataListReload;
