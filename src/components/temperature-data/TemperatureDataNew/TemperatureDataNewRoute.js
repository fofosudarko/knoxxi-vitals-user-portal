import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { CreateButton } from 'src/components/lib';

export default function TemperatureDataNewRoute({ text }) {
  const { handleTemperatureDataNewRoute } =
    useRoutes().useTemperatureDataNewRoute();

  return (
    <CreateButton
      variant="primary"
      textColor="white"
      onClick={handleTemperatureDataNewRoute}
      text={text}
    />
  );
}

TemperatureDataNewRoute.propTypes = {
  text: PropTypes.string,
};
TemperatureDataNewRoute.defaultProps = {
  text: 'New temperature data',
};
