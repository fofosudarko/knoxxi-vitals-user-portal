import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemoveTemperatureData } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function TemperatureDataRemove({ temperatureData, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleRemoveTemperatureData: _handleRemoveTemperatureData,
    error,
    setError,
    temperatureDataRemoved,
    setTemperatureDataRemoved,
  } = useRemoveTemperatureData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (temperatureDataRemoved) {
      handleApiResult();
    }
  }, [temperatureDataRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Temperature data removed successfully'
      ).getAlertNotification()
    );
    setTemperatureDataRemoved(false);
  }, [handleNotification, setTemperatureDataRemoved]);

  const handleRemoveTemperatureData = useCallback(async () => {
    await _handleRemoveTemperatureData(temperatureData);
  }, [_handleRemoveTemperatureData, temperatureData]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemoveTemperatureData}
        variant="white"
        text="Remove temperature data"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

TemperatureDataRemove.propTypes = {
  temperatureData: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
TemperatureDataRemove.defaultProps = {
  temperatureData: null,
  appUser: null,
  useTooltip: false,
};

export default TemperatureDataRemove;
