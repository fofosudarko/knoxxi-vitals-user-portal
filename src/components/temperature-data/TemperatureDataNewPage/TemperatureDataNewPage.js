import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCreateTemperatureData } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';
import { DEFAULT_PARTNERED_BY_ALIAS, DEFAULT_DATA_CHANNEL } from 'src/config';

import { ContentContainer, GoBack } from 'src/components/lib';
import TemperatureDataInput from '../TemperatureDataInput/TemperatureDataInput';

function TemperatureDataNewPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { handleTemperatureDataListRoute } =
    useRoutes().useTemperatureDataListRoute();
  const {
    handleCreateTemperatureData: _handleCreateTemperatureData,
    error,
    setError,
    processing,
    temperatureDataCreated,
    setTemperatureDataCreated,
  } = useCreateTemperatureData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (temperatureDataCreated) {
      handleApiResult();
    }
  }, [temperatureDataCreated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Temperature data created successfully'
      ).getSuccessNotification(),
      () => {
        setTemperatureDataCreated(false);
      }
    );
    handleTemperatureDataListRoute();
  }, [
    handleTemperatureDataListRoute,
    handleNotification,
    setTemperatureDataCreated,
  ]);

  const handleCreateTemperatureData = useCallback(
    async (temperatureData) => {
      const body = {
        ...temperatureData,
        kycId: account?.customerId,
        mobileNumber: null,
        partneredByAlias: DEFAULT_PARTNERED_BY_ALIAS,
        dataChannel: DEFAULT_DATA_CHANNEL,
      };
      await _handleCreateTemperatureData(body);
    },
    [_handleCreateTemperatureData, account?.customerId]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">New temperature data</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <TemperatureDataInput
          appUser={appUser}
          onSubmit={handleCreateTemperatureData}
          processing={processing}
        />
      </ContentContainer>
    </div>
  );
}

TemperatureDataNewPage.propTypes = {
  appUser: PropTypes.object,
};
TemperatureDataNewPage.defaultProps = {
  appUser: null,
};

export default TemperatureDataNewPage;
