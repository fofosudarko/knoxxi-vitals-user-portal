import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListSharedTemperatureDataList } from 'src/hooks/api';

import SharedTemperatureDataListReload from './SharedTemperatureDataListReload';
import {
  SharedTemperatureDataListList,
  SharedTemperatureDataListListEmpty,
} from './_SharedTemperatureDataListList';

function SharedTemperatureDataListListContainer({
  healthDataSharePublic,
  query,
}) {
  const {
    sharedTemperatureDataList,
    error: sharedTemperatureDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListSharedTemperatureDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (sharedTemperatureDataListError) {
      handleNotification(sharedTemperatureDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, sharedTemperatureDataListError, setError]);

  const handleLoadMoreSharedTemperatureDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <SharedTemperatureDataListList
      sharedTemperatureDataList={sharedTemperatureDataList}
      onLoadMore={handleLoadMoreSharedTemperatureDataList}
      loadingText="Loading shared temperature data list..."
      healthDataSharePublic={healthDataSharePublic}
      SharedTemperatureDataListListEmpty={SharedTemperatureDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function SharedTemperatureDataListListView({
  healthDataSharePublic,
  query,
}) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <SharedTemperatureDataListReload
          useTooltip
          healthDataSharePublic={healthDataSharePublic}
        />
      </div>
      <div className="my-2">
        <SharedTemperatureDataListListContainer
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedTemperatureDataListListContainer.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedTemperatureDataListListContainer.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};
SharedTemperatureDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedTemperatureDataList: PropTypes.array,
  SharedTemperatureDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedTemperatureDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared temperature data list...',
  onLoadMore: undefined,
  sharedTemperatureDataList: null,
  SharedTemperatureDataListListEmpty: null,
  isDisabled: false,
};
SharedTemperatureDataListListView.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedTemperatureDataListListView.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};

export default SharedTemperatureDataListList;
