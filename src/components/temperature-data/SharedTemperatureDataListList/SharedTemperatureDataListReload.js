import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetSharedTemperatureDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function SharedTemperatureDataListReload({ useTooltip }) {
  const _handleResetSharedTemperatureDataList =
    useResetSharedTemperatureDataList();

  const handleResetSharedTemperatureDataList = useCallback(() => {
    _handleResetSharedTemperatureDataList();
  }, [_handleResetSharedTemperatureDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetSharedTemperatureDataList}
      text="Reload shared temperature data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

SharedTemperatureDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  healthDataSharePublic: PropTypes.object,
};
SharedTemperatureDataListReload.defaultProps = {
  useTooltip: false,
  healthDataSharePublic: null,
};

export default SharedTemperatureDataListReload;
