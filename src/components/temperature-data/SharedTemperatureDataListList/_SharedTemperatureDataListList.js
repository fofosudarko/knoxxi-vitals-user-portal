import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import {
  SharedTemperatureDataTableItem,
  SharedTemperatureDataGridItem,
} from './SharedTemperatureDataItem';

export function SharedTemperatureDataListList({
  sharedTemperatureDataList,
  loadingText,
  healthDataSharePublic,
  onLoadMore,
  SharedTemperatureDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!sharedTemperatureDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(SharedTemperatureDataListListEmpty);

  return sharedTemperatureDataList.length ? (
    <div>
      {isLargeDevice ? (
        <SharedTemperatureDataListListTable
          sharedTemperatureDataList={sharedTemperatureDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <SharedTemperatureDataListListGrid
          sharedTemperatureDataList={sharedTemperatureDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      )}

      {sharedTemperatureDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more shared temperature data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty healthDataSharePublic={healthDataSharePublic} />
  );
}

export function SharedTemperatureDataListListGrid({
  sharedTemperatureDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {sharedTemperatureDataList.map((item) => (
          <Col key={item.id}>
            <SharedTemperatureDataGridItem
              temperatureData={item}
              healthDataSharePublic={healthDataSharePublic}
            />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function SharedTemperatureDataListListTable({
  sharedTemperatureDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">
              Reading(<sup>o</sup>C)
            </th>
            <th className="list-table-head-cell">Severity</th>
            <th className="list-table-head-cell">Interpretation</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {sharedTemperatureDataList.map((item, index) => (
            <SharedTemperatureDataTableItem
              temperatureData={item}
              healthDataSharePublic={healthDataSharePublic}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function SharedTemperatureDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no shared temperature data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

SharedTemperatureDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedTemperatureDataList: PropTypes.array,
  SharedTemperatureDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedTemperatureDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared temperature data list...',
  onLoadMore: undefined,
  sharedTemperatureDataList: null,
  SharedTemperatureDataListListEmpty: null,
  isDisabled: false,
};
SharedTemperatureDataListListGrid.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedTemperatureDataList: PropTypes.array,
};
SharedTemperatureDataListListGrid.defaultProps = {
  healthDataSharePublic: null,
  sharedTemperatureDataList: null,
};
SharedTemperatureDataListListTable.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedTemperatureDataList: PropTypes.array,
};
SharedTemperatureDataListListTable.defaultProps = {
  healthDataSharePublic: null,
  sharedTemperatureDataList: null,
};
SharedTemperatureDataListListEmpty.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedTemperatureDataListListEmpty.defaultProps = {
  healthDataSharePublic: null,
};
