import PropTypes from 'prop-types';
import { Row, Col, Badge } from 'react-bootstrap';
//import { FaEllipsisV } from 'react-icons/fa';

//import { useDeviceDimensions } from 'src/hooks';
import { humanizeDate, getVariantFromHealthDataSeverityName } from 'src/utils';

export function useTemperatureDataDetails(temperatureData) {
  const {
    createdOn,
    reading,
    temperatureDataSeverity,
    temperatureDataInterpretation = null,
  } = temperatureData ?? {};
  const {
    name: temperatureDataInterpretationName,
    description: temperatureDataInterpretationDescription,
    explanation: temperatureDataInterpretationExplanation,
  } = temperatureDataInterpretation ?? {};

  return {
    createdOn,
    reading,
    temperatureDataSeverity,
    temperatureDataInterpretationName,
    temperatureDataInterpretationDescription,
    temperatureDataInterpretationExplanation,
  };
}

export function SharedTemperatureDataGridItem({ temperatureData }) {
  const {
    createdOn,
    reading,
    temperatureDataSeverity,
    temperatureDataInterpretationName,
    temperatureDataInterpretationDescription,
    temperatureDataInterpretationExplanation,
  } = useTemperatureDataDetails(temperatureData);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Created</div>
            <div className="item-subtitle">
              {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">
              Reading(<sup>O</sup>C)
            </div>
            <div className="item-subtitle fw-bold">
              <span style={{ color: temperatureDataSeverity.colorCode }}>
                {reading !== undefined ? reading : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">Severity</div>
            <Badge
              bg={getVariantFromHealthDataSeverityName(
                temperatureDataSeverity.name
              )}
            >
              {temperatureDataSeverity.name}
            </Badge>
          </Col>
          <Col>
            <div className="item-title">Interpretation</div>
            <div className="item-subtitle">
              {temperatureDataInterpretationName !== undefined ? (
                <span>
                  <span style={{ color: temperatureDataSeverity.colorCode }}>
                    {temperatureDataInterpretationName}
                  </span>
                  : {temperatureDataInterpretationDescription},{' '}
                  {temperatureDataInterpretationExplanation}
                </span>
              ) : (
                'N/A'
              )}
            </div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              {/*<TemperatureDataItemActions
                temperatureData={temperatureData}
                healthDataSharePublic={healthDataSharePublic}
              />*/}
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function SharedTemperatureDataTableItem({ temperatureData }) {
  const {
    createdOn,
    reading,
    temperatureDataSeverity,
    temperatureDataInterpretationName,
    temperatureDataInterpretationDescription,
    temperatureDataInterpretationExplanation,
  } = useTemperatureDataDetails(temperatureData);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span style={{ color: temperatureDataSeverity.colorCode }}>
            {reading !== undefined ? reading : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <Badge
          bg={getVariantFromHealthDataSeverityName(
            temperatureDataSeverity.name
          )}
        >
          {temperatureDataSeverity.name}
        </Badge>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {temperatureDataInterpretationName !== undefined ? (
            <span>
              <span style={{ color: temperatureDataSeverity.colorCode }}>
                {temperatureDataInterpretationName}
              </span>
              : {temperatureDataInterpretationDescription},{' '}
              {temperatureDataInterpretationExplanation}
            </span>
          ) : (
            'N/A'
          )}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          {/*<TemperatureDataItemActions
            temperatureData={temperatureData}
            healthDataSharePublic={healthDataSharePublic}
          />*/}
        </div>
      </td>
    </tr>
  );
}

/*export function TemperatureDataItemActions({
  temperatureData,
  healthDataSharePublic,
}) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item>
            <TemperatureDataRemove
              temperatureData={temperatureData}
              healthDataSharePublic={healthDataSharePublic}
            />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}*/

SharedTemperatureDataGridItem.propTypes = {
  temperatureData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedTemperatureDataGridItem.defaultProps = {
  temperatureData: null,
  healthDataSharePublic: null,
};
SharedTemperatureDataTableItem.propTypes = {
  temperatureData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedTemperatureDataTableItem.defaultProps = {
  temperatureData: null,
  healthDataSharePublic: null,
};
/*TemperatureDataItemActions.propTypes = {
  temperatureData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
TemperatureDataItemActions.defaultProps = {
  temperatureData: null,
  healthDataSharePublic: null,
};*/
