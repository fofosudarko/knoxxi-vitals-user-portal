import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName, round } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function TemperatureDataCumulativeAveragesNumberCard({
  temperatureDataCumulativeAverages,
  label,
  onClick,
}) {
  const reading =
    temperatureDataCumulativeAverages?.reading !== undefined
      ? round(temperatureDataCumulativeAverages?.reading, 2)
      : 'N/A';
  const temperatureDataSeverity =
    temperatureDataCumulativeAverages?.temperatureDataSeverity ?? {};
  return (
    <NumberCard
      value={reading}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(
        temperatureDataSeverity.name
      )}
      onClick={onClick}
    />
  );
}

TemperatureDataCumulativeAveragesNumberCard.propTypes = {
  temperatureDataCumulativeAverages: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
TemperatureDataCumulativeAveragesNumberCard.defaultProps = {
  temperatureDataCumulativeAverages: null,
  label: null,
  onClick: undefined,
};

export default TemperatureDataCumulativeAveragesNumberCard;
