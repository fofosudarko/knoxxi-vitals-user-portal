import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { useNotificationHandler } from 'src/hooks';
import { useListTemperatureDataCumulativeAverages } from 'src/hooks/api';

import TemperatureDataCumulativeAveragesNumberCard from './TemperatureDataCumulativeAveragesNumberCard';

function TemperatureDataCumulativeAveragesContainer({
  appUser,
  query,
  label,
  isCard,
  onClick,
}) {
  const {
    temperatureDataCumulativeAverages,
    error: temperatureDataCumulativeAveragesError,
    setError,
  } = useListTemperatureDataCumulativeAverages({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (temperatureDataCumulativeAveragesError) {
      handleNotification(temperatureDataCumulativeAveragesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, temperatureDataCumulativeAveragesError, setError]);

  return (
    <TemperatureDataCumulativeAverages
      temperatureDataCumulativeAverages={temperatureDataCumulativeAverages}
      label={label}
      appUser={appUser}
      isDisabled={
        temperatureDataCumulativeAverages &&
        temperatureDataCumulativeAverages.length === 0
      }
      query={query}
      isCard={isCard}
      onClick={onClick}
    />
  );
}

function TemperatureDataCumulativeAverages({
  temperatureDataCumulativeAverages: _temperatureDataCumulativeAverages,
  appUser,
  isDisabled,
  isCard,
  onClick,
}) {
  let temperatureDataCumulativeAverages = null;

  if (_temperatureDataCumulativeAverages?.length) {
    const { reading, temperatureDataInterpretation, temperatureDataSeverity } =
      _temperatureDataCumulativeAverages[0] ?? {};
    temperatureDataCumulativeAverages = {
      reading,
      temperatureDataInterpretation,
      temperatureDataSeverity,
    };
  }

  return (
    <div>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 1 }}
        lg={{ cols: 1 }}
        className="gx-3 gy-3"
      >
        <Col>
          {isCard ? (
            <TemperatureDataCumulativeAveragesNumberCard
              isDisabled={isDisabled}
              temperatureDataCumulativeAverages={
                temperatureDataCumulativeAverages
              }
              label="Reading(C)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
      </Row>
    </div>
  );
}

export function TemperatureDataCumulativeAveragesNumberCardView({
  appUser,
  query,
  label,
  onClick,
}) {
  return (
    <div className="d-block">
      <TemperatureDataCumulativeAveragesContainer
        appUser={appUser}
        query={query}
        label={label}
        isCard
        onClick={onClick}
      />
    </div>
  );
}

TemperatureDataCumulativeAveragesContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
TemperatureDataCumulativeAveragesContainer.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  isCard: false,
  onClick: undefined,
};
TemperatureDataCumulativeAverages.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  query: PropTypes.object,
  temperatureDataCumulativeAverages: PropTypes.array,
  isDisabled: PropTypes.bool,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
TemperatureDataCumulativeAverages.defaultProps = {
  appUser: null,
  label: undefined,
  query: null,
  temperatureDataCumulativeAverages: undefined,
  isDisabled: false,
  isCard: false,
  onClick: undefined,
};
TemperatureDataCumulativeAveragesNumberCardView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
TemperatureDataCumulativeAveragesNumberCardView.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  onClick: undefined,
};

export default TemperatureDataCumulativeAverages;
