import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function BPDataCumulativeAveragesSystolicNumberCard({
  bpDataCumulativeAveragesSystolic,
  label,
  onClick,
}) {
  const systolic = bpDataCumulativeAveragesSystolic?.systolic ?? 'N/A';
  const bpDataSeverity = bpDataCumulativeAveragesSystolic?.bpDataSeverity ?? {};
  return (
    <NumberCard
      value={systolic}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(bpDataSeverity.name)}
      onClick={onClick}
    />
  );
}

BPDataCumulativeAveragesSystolicNumberCard.propTypes = {
  bpDataCumulativeAveragesSystolic: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
BPDataCumulativeAveragesSystolicNumberCard.defaultProps = {
  bpDataCumulativeAveragesSystolic: null,
  label: null,
  onClick: undefined,
};

export default BPDataCumulativeAveragesSystolicNumberCard;
