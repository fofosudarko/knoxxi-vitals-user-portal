import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function BPDataCumulativeAveragesDiastolicNumberCard({
  bpDataCumulativeAveragesDiastolic,
  label,
  onClick,
}) {
  const diastolic = bpDataCumulativeAveragesDiastolic?.diastolic ?? 'N/A';
  const bpDataSeverity =
    bpDataCumulativeAveragesDiastolic?.bpDataSeverity ?? {};
  return (
    <NumberCard
      value={diastolic}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(bpDataSeverity.name)}
      onClick={onClick}
    />
  );
}

BPDataCumulativeAveragesDiastolicNumberCard.propTypes = {
  bpDataCumulativeAveragesDiastolic: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
BPDataCumulativeAveragesDiastolicNumberCard.defaultProps = {
  bpDataCumulativeAveragesDiastolic: null,
  label: null,
  onClick: undefined,
};

export default BPDataCumulativeAveragesDiastolicNumberCard;
