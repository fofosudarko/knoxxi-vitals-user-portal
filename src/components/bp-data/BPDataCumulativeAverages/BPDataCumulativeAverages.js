import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { useNotificationHandler } from 'src/hooks';
import { useListBPDataCumulativeAverages } from 'src/hooks/api';

import BPDataCumulativeAveragesSystolicNumberCard from './BPDataCumulativeAveragesSystolicNumberCard';
import BPDataCumulativeAveragesDiastolicNumberCard from './BPDataCumulativeAveragesDiastolicNumberCard';
import BPDataCumulativeAveragesPulseNumberCard from './BPDataCumulativeAveragesPulseNumberCard';

function BPDataCumulativeAveragesContainer({
  appUser,
  query,
  label,
  isCard,
  onClick,
}) {
  const {
    bpDataCumulativeAverages,
    error: bpDataCumulativeAveragesError,
    setError,
  } = useListBPDataCumulativeAverages({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (bpDataCumulativeAveragesError) {
      handleNotification(bpDataCumulativeAveragesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, bpDataCumulativeAveragesError, setError]);

  return (
    <BPDataCumulativeAverages
      bpDataCumulativeAverages={bpDataCumulativeAverages}
      label={label}
      appUser={appUser}
      isDisabled={
        bpDataCumulativeAverages && bpDataCumulativeAverages.length === 0
      }
      query={query}
      isCard={isCard}
      onClick={onClick}
    />
  );
}

function BPDataCumulativeAverages({
  bpDataCumulativeAverages,
  appUser,
  isDisabled,
  isCard,
  onClick,
}) {
  let bpDataCumulativeAveragesSystolic = null,
    bpDataCumulativeAveragesDiastolic = null,
    bpDataCumulativeAveragesPulse = null;

  if (bpDataCumulativeAverages?.length) {
    const { systolic, diastolic, pulse, bpDataInterpretation, bpDataSeverity } =
      bpDataCumulativeAverages[0] ?? {};
    bpDataCumulativeAveragesSystolic = {
      systolic,
      bpDataInterpretation,
      bpDataSeverity,
    };
    bpDataCumulativeAveragesDiastolic = {
      diastolic,
      bpDataInterpretation,
      bpDataSeverity,
    };
    bpDataCumulativeAveragesPulse = {
      pulse,
      bpDataInterpretation,
      bpDataSeverity,
    };
  }

  return (
    <div>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        <Col>
          {isCard ? (
            <BPDataCumulativeAveragesSystolicNumberCard
              isDisabled={isDisabled}
              bpDataCumulativeAveragesSystolic={
                bpDataCumulativeAveragesSystolic
              }
              label="Systolic(mmHg)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <BPDataCumulativeAveragesDiastolicNumberCard
              isDisabled={isDisabled}
              bpDataCumulativeAveragesDiastolic={
                bpDataCumulativeAveragesDiastolic
              }
              label="Diastolic(mmHg)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <BPDataCumulativeAveragesPulseNumberCard
              isDisabled={isDisabled}
              bpDataCumulativeAveragesPulse={bpDataCumulativeAveragesPulse}
              label="Pulse(bpm)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
      </Row>
    </div>
  );
}

export function BPDataCumulativeAveragesNumberCardView({
  appUser,
  query,
  label,
  onClick,
}) {
  return (
    <div className="d-block">
      <BPDataCumulativeAveragesContainer
        appUser={appUser}
        query={query}
        label={label}
        isCard
        onClick={onClick}
      />
    </div>
  );
}

BPDataCumulativeAveragesContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
BPDataCumulativeAveragesContainer.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  isCard: false,
  onClick: undefined,
};
BPDataCumulativeAverages.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  query: PropTypes.object,
  bpDataCumulativeAverages: PropTypes.array,
  isDisabled: PropTypes.bool,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
BPDataCumulativeAverages.defaultProps = {
  appUser: null,
  label: undefined,
  query: null,
  bpDataCumulativeAverages: undefined,
  isDisabled: false,
  isCard: false,
  onClick: undefined,
};
BPDataCumulativeAveragesNumberCardView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
BPDataCumulativeAveragesNumberCardView.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  onClick: undefined,
};

export default BPDataCumulativeAverages;
