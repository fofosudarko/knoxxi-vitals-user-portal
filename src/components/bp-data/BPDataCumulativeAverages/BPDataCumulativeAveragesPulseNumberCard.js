import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function BPDataCumulativeAveragesPulseNumberCard({
  bpDataCumulativeAveragesPulse,
  label,
  onClick,
}) {
  const pulse = bpDataCumulativeAveragesPulse?.pulse ?? 'N/A';
  const bpDataSeverity = bpDataCumulativeAveragesPulse?.bpDataSeverity ?? {};
  return (
    <NumberCard
      value={pulse}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(bpDataSeverity.name)}
      onClick={onClick}
    />
  );
}

BPDataCumulativeAveragesPulseNumberCard.propTypes = {
  bpDataCumulativeAveragesPulse: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
BPDataCumulativeAveragesPulseNumberCard.defaultProps = {
  bpDataCumulativeAveragesPulse: null,
  label: null,
  onClick: undefined,
};

export default BPDataCumulativeAveragesPulseNumberCard;
