import { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import {
  YUP_BP_DATA_SYSTOLIC_VALIDATOR,
  YUP_BP_DATA_DIASTOLIC_VALIDATOR,
  YUP_BP_DATA_PULSE_VALIDATOR,
} from 'src/config/validators';
import { useFormReset, useDeviceDimensions } from 'src/hooks';

import { CancelButton, CreateButton, EditButton } from 'src/components/lib';

function BPDataInput({ bpData, isEditing, onSubmit, processing }) {
  const bpDataInputSchema = Yup({
    systolic: YUP_BP_DATA_SYSTOLIC_VALIDATOR,
    diastolic: YUP_BP_DATA_DIASTOLIC_VALIDATOR,
    pulse: YUP_BP_DATA_PULSE_VALIDATOR,
  });
  const { isLargeDevice } = useDeviceDimensions();
  const defaultValues = useMemo(
    () => ({
      systolic: bpData?.systolic ?? '',
      diastolic: bpData?.diastolic ?? '',
      pulse: bpData?.pulse ?? '',
    }),
    [bpData?.systolic, bpData?.pulse, bpData?.diastolic]
  );

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    reset,
  } = useForm({ defaultValues, resolver: yupResolver(bpDataInputSchema) });

  useFormReset({ values: defaultValues, item: bpData, reset });

  const handleSubmitBPData = useCallback(
    ({ systolic, pulse, diastolic }) => {
      const newBPData = {
        systolic,
        pulse,
        diastolic,
      };

      onSubmit && onSubmit(newBPData);
    },
    [onSubmit]
  );

  const handleCancelBPData = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitBPData)} noValidate>
        <Row xs={{ cols: 1 }}>
          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">
              Systolic pressure(mmHg)
            </Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Systolic pressure"
                  {...field}
                  isInvalid={!!errors.systolic}
                  isClearable
                />
              )}
              name="systolic"
              control={control}
            />
            {errors.systolic ? (
              <Form.Control.Feedback type="invalid">
                {errors.systolic.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>
              Give the systolic pressure reading e.g. 120 mmHg
            </Form.Text>
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">
              Diastolic pressure(mmHg)
            </Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Diastolic pressure"
                  {...field}
                  isInvalid={!!errors.diastolic}
                />
              )}
              name="diastolic"
              control={control}
            />
            {errors.diastolic ? (
              <Form.Control.Feedback type="invalid">
                {errors.diastolic.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>
              Give the diastolic pressure reading e.g. 80 mmHg
            </Form.Text>
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">Pulse(bpm)</Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Pulse"
                  {...field}
                  isInvalid={!!errors.pulse}
                />
              )}
              name="pulse"
              control={control}
            />
            {errors.pulse ? (
              <Form.Control.Feedback type="invalid">
                {errors.pulse.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give the pulse reading e.g. 72 bpm</Form.Text>
          </Form.Group>
        </Row>

        <div className="d-flex flex-column-reverse flex-md-row justify-content-end my-3">
          <div className="my-1 my-sm-0 mx-1">
            <CancelButton
              onClick={handleCancelBPData}
              autoWidth={isLargeDevice}
              text="Cancel"
            />
          </div>
          <div className="my-1 my-sm-0 mx-1">
            {isEditing ? (
              <EditButton
                type="submit"
                clicked={processing}
                text="Edit bp data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            ) : (
              <CreateButton
                type="submit"
                clicked={processing}
                text="Add bp data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            )}
          </div>
        </div>
      </Form>
    </div>
  );
}

BPDataInput.propTypes = {
  appUser: PropTypes.object,
  bpData: PropTypes.object,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
};
BPDataInput.defaultProps = {
  appUser: null,
  bpData: null,
  isEditing: false,
  onSubmit: undefined,
  processing: false,
};

export default BPDataInput;
