import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemoveBPData } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function BPDataRemove({ bpData, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleRemoveBPData: _handleRemoveBPData,
    error,
    setError,
    bpDataRemoved,
    setBPDataRemoved,
  } = useRemoveBPData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (bpDataRemoved) {
      handleApiResult();
    }
  }, [bpDataRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification('BP data removed successfully').getAlertNotification()
    );
    setBPDataRemoved(false);
  }, [handleNotification, setBPDataRemoved]);

  const handleRemoveBPData = useCallback(async () => {
    await _handleRemoveBPData(bpData);
  }, [_handleRemoveBPData, bpData]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemoveBPData}
        variant="white"
        text="Remove bp data"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

BPDataRemove.propTypes = {
  bpData: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
BPDataRemove.defaultProps = {
  bpData: null,
  appUser: null,
  useTooltip: false,
};

export default BPDataRemove;
