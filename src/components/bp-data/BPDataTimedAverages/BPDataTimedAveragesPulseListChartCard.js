import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function BPDataTimedAveragesPulseListChartCard({
  bpDataTimedAveragesPulseList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          bpDataTimedAveragesPulseList?.map((item) => item.createdOn) ?? [],
        datasets: [
          {
            label,
            data: bpDataTimedAveragesPulseList?.map((item) => item.pulse) ?? [],
            backgroundColor:
              bpDataTimedAveragesPulseList?.map(
                (item) => item.bpDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [bpDataTimedAveragesPulseList, label]);
  return bpDataTimedAveragesPulseList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

BPDataTimedAveragesPulseListChartCard.propTypes = {
  bpDataTimedAveragesPulseList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
BPDataTimedAveragesPulseListChartCard.defaultProps = {
  bpDataTimedAveragesPulseList: null,
  appUser: null,
  label: null,
  isDisabled: false,
  onClick: undefined,
};

export default BPDataTimedAveragesPulseListChartCard;
