import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { useNotificationHandler } from 'src/hooks';
import { useListBPDataTimedAverages } from 'src/hooks/api';

import BPDataTimedAveragesSystolicListChartCard from './BPDataTimedAveragesSystolicListChartCard';
import BPDataTimedAveragesDiastolicListChartCard from './BPDataTimedAveragesDiastolicListChartCard';
import BPDataTimedAveragesPulseListChartCard from './BPDataTimedAveragesPulseListChartCard';

function BPDataTimedAveragesContainer({
  appUser,
  query,
  label,
  isCard,
  onClick,
}) {
  const {
    bpDataTimedAverages,
    error: bpDataTimedAveragesError,
    setError,
  } = useListBPDataTimedAverages({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (bpDataTimedAveragesError) {
      handleNotification(bpDataTimedAveragesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, bpDataTimedAveragesError, setError]);

  return (
    <BPDataTimedAverages
      bpDataTimedAverages={bpDataTimedAverages}
      label={label}
      appUser={appUser}
      isDisabled={bpDataTimedAverages && bpDataTimedAverages.length === 0}
      query={query}
      isCard={isCard}
      onClick={onClick}
    />
  );
}

function BPDataTimedAverages({
  bpDataTimedAverages,
  appUser,
  isDisabled,
  isCard,
  onClick,
}) {
  const bpDataTimedAveragesSystolicList = [],
    bpDataTimedAveragesDiastolicList = [],
    bpDataTimedAveragesPulseList = [];

  bpDataTimedAverages?.forEach((bpDataTimedAverage) => {
    const {
      createdOn,
      systolic,
      diastolic,
      pulse,
      bpDataInterpretation,
      bpDataSeverity,
    } = bpDataTimedAverage;
    bpDataTimedAveragesSystolicList.push({
      createdOn,
      systolic,
      bpDataInterpretation,
      bpDataSeverity,
    });
    bpDataTimedAveragesDiastolicList.push({
      createdOn,
      diastolic,
      bpDataInterpretation,
      bpDataSeverity,
    });
    bpDataTimedAveragesPulseList.push({
      createdOn,
      pulse,
      bpDataInterpretation,
      bpDataSeverity,
    });
  });

  return (
    <div>
      <Row
        xs={{ cols: 1 }}
        //md={{ cols: 2 }}
        //lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        <Col>
          {isCard ? (
            <BPDataTimedAveragesSystolicListChartCard
              isDisabled={isDisabled}
              bpDataTimedAveragesSystolicList={bpDataTimedAveragesSystolicList}
              label="Systolic(mmHg)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <BPDataTimedAveragesDiastolicListChartCard
              isDisabled={isDisabled}
              bpDataTimedAveragesDiastolicList={
                bpDataTimedAveragesDiastolicList
              }
              label="Diastolic(mmHg)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
        <Col>
          {isCard ? (
            <BPDataTimedAveragesPulseListChartCard
              isDisabled={isDisabled}
              bpDataTimedAveragesPulseList={bpDataTimedAveragesPulseList}
              label="Pulse(bpm)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
      </Row>
    </div>
  );
}

export function BPDataTimedAveragesChartCardView({
  appUser,
  query,
  label,
  onClick,
}) {
  return (
    <div className="d-block">
      <BPDataTimedAveragesContainer
        appUser={appUser}
        query={query}
        label={label}
        isCard
        onClick={onClick}
      />
    </div>
  );
}

BPDataTimedAveragesContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
BPDataTimedAveragesContainer.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  isCard: false,
  onClick: undefined,
};
BPDataTimedAverages.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  query: PropTypes.object,
  bpDataTimedAverages: PropTypes.array,
  isDisabled: PropTypes.bool,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
BPDataTimedAverages.defaultProps = {
  appUser: null,
  label: undefined,
  query: null,
  bpDataTimedAverages: undefined,
  isDisabled: false,
  isCard: false,
  onClick: undefined,
};
BPDataTimedAveragesChartCardView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
BPDataTimedAveragesChartCardView.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  onClick: undefined,
};

export default BPDataTimedAverages;
