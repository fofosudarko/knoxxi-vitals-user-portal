import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function BPDataTimedAveragesSystolicListChartCard({
  bpDataTimedAveragesSystolicList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          bpDataTimedAveragesSystolicList?.map((item) => item.createdOn) ?? [],
        datasets: [
          {
            label,
            data:
              bpDataTimedAveragesSystolicList?.map((item) => item.systolic) ??
              [],
            backgroundColor:
              bpDataTimedAveragesSystolicList?.map(
                (item) => item.bpDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [bpDataTimedAveragesSystolicList, label]);
  return bpDataTimedAveragesSystolicList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

BPDataTimedAveragesSystolicListChartCard.propTypes = {
  bpDataTimedAveragesSystolicList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
BPDataTimedAveragesSystolicListChartCard.defaultProps = {
  bpDataTimedAveragesSystolicList: null,
  appUser: null,
  label: null,
  isDisabled: false,
  onClick: undefined,
};

export default BPDataTimedAveragesSystolicListChartCard;
