import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function BPDataTimedAveragesDiastolicListChartCard({
  bpDataTimedAveragesDiastolicList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          bpDataTimedAveragesDiastolicList?.map((item) => item.createdOn) ?? [],
        datasets: [
          {
            label,
            data:
              bpDataTimedAveragesDiastolicList?.map((item) => item.diastolic) ??
              [],
            backgroundColor:
              bpDataTimedAveragesDiastolicList?.map(
                (item) => item.bpDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [bpDataTimedAveragesDiastolicList, label]);
  return bpDataTimedAveragesDiastolicList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

BPDataTimedAveragesDiastolicListChartCard.propTypes = {
  bpDataTimedAveragesDiastolicList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
BPDataTimedAveragesDiastolicListChartCard.defaultProps = {
  bpDataTimedAveragesDiastolicList: null,
  appUser: null,
  label: null,
  isDisabled: false,
  onClick: undefined,
};

export default BPDataTimedAveragesDiastolicListChartCard;
