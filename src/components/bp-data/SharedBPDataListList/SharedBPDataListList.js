import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListSharedBPDataList } from 'src/hooks/api';

import SharedBPDataListReload from './SharedBPDataListReload';
import {
  SharedBPDataListList,
  SharedBPDataListListEmpty,
} from './_SharedBPDataListList';

function SharedBPDataListListContainer({ healthDataSharePublic, query }) {
  const {
    sharedBpDataList,
    error: sharedBpDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListSharedBPDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (sharedBpDataListError) {
      handleNotification(sharedBpDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, sharedBpDataListError, setError]);

  const handleLoadMoreSharedBPDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <SharedBPDataListList
      sharedBpDataList={sharedBpDataList}
      onLoadMore={handleLoadMoreSharedBPDataList}
      loadingText="Loading shared BP data list..."
      healthDataSharePublic={healthDataSharePublic}
      SharedBPDataListListEmpty={SharedBPDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function SharedBPDataListListView({ healthDataSharePublic, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <SharedBPDataListReload
          useTooltip
          healthDataSharePublic={healthDataSharePublic}
        />
      </div>
      <div className="my-2">
        <SharedBPDataListListContainer
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedBPDataListListContainer.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedBPDataListListContainer.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};
SharedBPDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedBpDataList: PropTypes.array,
  SharedBPDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedBPDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared BP data list...',
  onLoadMore: undefined,
  sharedBpDataList: null,
  SharedBPDataListListEmpty: null,
  isDisabled: false,
};
SharedBPDataListListView.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedBPDataListListView.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};

export default SharedBPDataListList;
