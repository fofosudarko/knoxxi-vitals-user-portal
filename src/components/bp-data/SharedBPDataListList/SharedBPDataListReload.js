import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetSharedBPDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function SharedBPDataListReload({ useTooltip }) {
  const _handleResetSharedBPDataList = useResetSharedBPDataList();

  const handleResetSharedBPDataList = useCallback(() => {
    _handleResetSharedBPDataList();
  }, [_handleResetSharedBPDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetSharedBPDataList}
      text="Reload shared BP data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

SharedBPDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  healthDataSharePublic: PropTypes.object,
};
SharedBPDataListReload.defaultProps = {
  useTooltip: false,
  healthDataSharePublic: null,
};

export default SharedBPDataListReload;
