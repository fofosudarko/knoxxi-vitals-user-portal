import PropTypes from 'prop-types';
import { Row, Col, Badge } from 'react-bootstrap';
//import { FaEllipsisV } from 'react-icons/fa';

//import { useDeviceDimensions } from 'src/hooks';
import { humanizeDate, getVariantFromHealthDataSeverityName } from 'src/utils';

export function useSharedBPDataDetails(bpData) {
  const {
    createdOn,
    systolic,
    diastolic,
    pulse,
    bpDataSeverity,
    bpDataInterpretation = null,
  } = bpData ?? {};
  const {
    name: bpDataInterpretationName,
    description: bpDataInterpretationDescription,
    explanation: bpDataInterpretationExplanation,
  } = bpDataInterpretation ?? {};

  return {
    createdOn,
    systolic,
    diastolic,
    pulse,
    bpDataSeverity,
    bpDataInterpretationName,
    bpDataInterpretationDescription,
    bpDataInterpretationExplanation,
  };
}

export function SharedBPDataGridItem({ bpData }) {
  const {
    createdOn,
    systolic,
    diastolic,
    pulse,
    bpDataSeverity,
    bpDataInterpretationName,
    bpDataInterpretationDescription,
    bpDataInterpretationExplanation,
  } = useSharedBPDataDetails(bpData);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Created</div>
            <div className="item-subtitle">
              {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Systolic(mmHg)</div>
            <div className="item-subtitle fw-bold">
              <span style={{ color: bpDataSeverity.colorCode }}>
                {systolic !== undefined ? systolic : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">Diastolic(mmHg)</div>
            <div className="item-subtitle fw-bold">
              <span style={{ color: bpDataSeverity.colorCode }}>
                {diastolic !== undefined ? diastolic : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">Pulse(bpm)</div>
            <div className="item-subtitle fw-bold">
              <span style={{ color: bpDataSeverity.colorCode }}>
                {pulse !== undefined ? pulse : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">Severity</div>
            <Badge
              bg={getVariantFromHealthDataSeverityName(bpDataSeverity.name)}
            >
              {bpDataSeverity.name}
            </Badge>
          </Col>
          <Col>
            <div className="item-title">Intepretation</div>
            <div className="item-subtitle">
              {bpDataInterpretationName !== undefined ? (
                <span>
                  <span style={{ color: bpDataSeverity.colorCode }}>
                    {bpDataInterpretationName}
                  </span>
                  : {bpDataInterpretationDescription},{' '}
                  {bpDataInterpretationExplanation}
                </span>
              ) : (
                'N/A'
              )}
            </div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              {/*<SharedBPDataItemActions
                bpData={bpData}
                healthDataSharePublic={healthDataSharePublic}
              />*/}
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function SharedBPDataTableItem({ bpData }) {
  const {
    createdOn,
    systolic,
    diastolic,
    pulse,
    bpDataSeverity,
    bpDataInterpretationName,
    bpDataInterpretationDescription,
    bpDataInterpretationExplanation,
  } = useSharedBPDataDetails(bpData);

  return (
    <tr className="list-table-row-border">
      <td>
        <div className="item-body" style={{ cursor: 'pointer' }}>
          {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span style={{ color: bpDataSeverity.colorCode }}>
            {systolic !== undefined ? systolic : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span style={{ color: bpDataSeverity.colorCode }}>
            {diastolic !== undefined ? diastolic : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span style={{ color: bpDataSeverity.colorCode }}>
            {pulse !== undefined ? pulse : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <Badge bg={getVariantFromHealthDataSeverityName(bpDataSeverity.name)}>
          {bpDataSeverity.name}
        </Badge>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {bpDataInterpretationName !== undefined ? (
            <span>
              <span style={{ color: bpDataSeverity.colorCode }}>
                {bpDataInterpretationName}
              </span>
              : {bpDataInterpretationDescription},{' '}
              {bpDataInterpretationExplanation}
            </span>
          ) : (
            'N/A'
          )}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          {/*<SharedBPDataItemActions
            bpData={bpData}
            healthDataSharePublic={healthDataSharePublic}
          />*/}
        </div>
      </td>
    </tr>
  );
}

/*export function SharedBPDataItemActions({ bpData, healthDataSharePublic }) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item>
            <SharedBPDataRemove
              bpData={bpData}
              healthDataSharePublic={healthDataSharePublic}
            />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}*/

SharedBPDataGridItem.propTypes = {
  bpData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedBPDataGridItem.defaultProps = {
  bpData: null,
  healthDataSharePublic: null,
};
SharedBPDataTableItem.propTypes = {
  bpData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedBPDataTableItem.defaultProps = {
  bpData: null,
  healthDataSharePublic: null,
};
/*SharedBPDataItemActions.propTypes = {
  bpData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedBPDataItemActions.defaultProps = {
  bpData: null,
  healthDataSharePublic: null,
};*/
