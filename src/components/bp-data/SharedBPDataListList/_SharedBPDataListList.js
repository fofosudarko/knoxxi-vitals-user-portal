import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import {
  SharedBPDataTableItem,
  SharedBPDataGridItem,
} from './SharedBPDataItem';

export function SharedBPDataListList({
  sharedBpDataList,
  loadingText,
  healthDataSharePublic,
  onLoadMore,
  SharedBPDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!sharedBpDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(SharedBPDataListListEmpty);

  return sharedBpDataList.length ? (
    <div>
      {isLargeDevice ? (
        <SharedBPDataListListTable
          sharedBpDataList={sharedBpDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <SharedBPDataListListGrid
          sharedBpDataList={sharedBpDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      )}

      {sharedBpDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more shared BP data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty healthDataSharePublic={healthDataSharePublic} />
  );
}

export function SharedBPDataListListGrid({
  sharedBpDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {sharedBpDataList.map((item) => (
          <Col key={item.id}>
            <SharedBPDataGridItem
              bpData={item}
              healthDataSharePublic={healthDataSharePublic}
            />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function SharedBPDataListListTable({
  sharedBpDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">Systolic(mmHg)</th>
            <th className="list-table-head-cell">Diastolic(mmHg)</th>
            <th className="list-table-head-cell">Pulse(bpm)</th>
            <th className="list-table-head-cell">Severity</th>
            <th className="list-table-head-cell">Interpretation</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {sharedBpDataList.map((item, index) => (
            <SharedBPDataTableItem
              bpData={item}
              healthDataSharePublic={healthDataSharePublic}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function SharedBPDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no shared BP data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

SharedBPDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedBpDataList: PropTypes.array,
  SharedBPDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedBPDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared BP data list...',
  onLoadMore: undefined,
  sharedBpDataList: null,
  SharedBPDataListListEmpty: null,
  isDisabled: false,
};
SharedBPDataListListGrid.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedBpDataList: PropTypes.array,
};
SharedBPDataListListGrid.defaultProps = {
  healthDataSharePublic: null,
  sharedBpDataList: null,
};
SharedBPDataListListTable.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedBpDataList: PropTypes.array,
};
SharedBPDataListListTable.defaultProps = {
  healthDataSharePublic: null,
  sharedBpDataList: null,
};
SharedBPDataListListEmpty.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedBPDataListListEmpty.defaultProps = {
  healthDataSharePublic: null,
};
