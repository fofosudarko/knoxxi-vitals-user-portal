import PropTypes from 'prop-types';

import { GoBack } from 'src/components/lib';
import { SharedBPDataListListView } from '../SharedBPDataListList/SharedBPDataListList';

function SharedBPDataListPage({ healthDataSharePublic }) {
  const query = { healthDataSharePublic };
  return (
    <div>
      <div className="justify-between">
        <div className="page-title">Shared BP data</div>
        <GoBack />
      </div>
      <div className="page-content">
        <SharedBPDataListListView
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedBPDataListPage.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedBPDataListPage.defaultProps = {
  healthDataSharePublic: null,
};

export default SharedBPDataListPage;
