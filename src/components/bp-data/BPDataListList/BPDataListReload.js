import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetBPDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function BPDataListReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const _handleResetBPDataList = useResetBPDataList({
    userDataId: account?.customerId,
  });

  const handleResetBPDataList = useCallback(() => {
    _handleResetBPDataList();
  }, [_handleResetBPDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetBPDataList}
      text="Reload BP data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

BPDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
BPDataListReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default BPDataListReload;
