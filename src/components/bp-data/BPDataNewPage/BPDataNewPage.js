import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCreateBPData } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';
import { DEFAULT_PARTNERED_BY_ALIAS, DEFAULT_DATA_CHANNEL } from 'src/config';

import { ContentContainer, GoBack } from 'src/components/lib';
import BPDataInput from '../BPDataInput/BPDataInput';

function BPDataNewPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { handleBPDataListRoute } = useRoutes().useBPDataListRoute();
  const {
    handleCreateBPData: _handleCreateBPData,
    error,
    setError,
    processing,
    bpDataCreated,
    setBPDataCreated,
  } = useCreateBPData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (bpDataCreated) {
      handleApiResult();
    }
  }, [bpDataCreated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification('BP data created successfully').getSuccessNotification(),
      () => {
        setBPDataCreated(false);
      }
    );
    handleBPDataListRoute();
  }, [handleBPDataListRoute, handleNotification, setBPDataCreated]);

  const handleCreateBPData = useCallback(
    async (bpData) => {
      const body = {
        ...bpData,
        kycId: account?.customerId,
        mobileNumber: null,
        partneredByAlias: DEFAULT_PARTNERED_BY_ALIAS,
        dataChannel: DEFAULT_DATA_CHANNEL,
      };
      await _handleCreateBPData(body);
    },
    [_handleCreateBPData, account?.customerId]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">New bp data</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <BPDataInput
          appUser={appUser}
          onSubmit={handleCreateBPData}
          processing={processing}
        />
      </ContentContainer>
    </div>
  );
}

BPDataNewPage.propTypes = {
  appUser: PropTypes.object,
};
BPDataNewPage.defaultProps = {
  appUser: null,
};

export default BPDataNewPage;
