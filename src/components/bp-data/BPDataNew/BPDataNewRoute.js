import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { CreateButton } from 'src/components/lib';

export default function BPDataNewRoute({ text }) {
  const { handleBPDataNewRoute } = useRoutes().useBPDataNewRoute();

  return (
    <CreateButton
      variant="primary"
      textColor="white"
      onClick={handleBPDataNewRoute}
      text={text}
    />
  );
}

BPDataNewRoute.propTypes = {
  text: PropTypes.string,
};
BPDataNewRoute.defaultProps = {
  text: 'New bp data',
};
