import { useState, useEffect, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Card } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import {
  YUP_PASSWORD_VALIDATOR,
  YUP_PHONE_NUMBER_VALIDATOR,
} from 'src/config/validators';
import { CKYC_SERVICE_RESPONSE_CODE } from 'src/config';
import {
  alertWebError,
  getId,
  isCKYCError,
  isAxiosError,
  isValidPhoneNumber,
  prependPlusToPhoneNumber,
} from 'src/utils';
import { useRoutes, useNotificationHandler } from 'src/hooks';
import { useCKYCSignIn, useCKYCProcessSignIn } from 'src/hooks/api';

import { AppPhoneInput } from 'src/components/lib';

function SignInPage({ appUser }) {
  const signInSchema = Yup({
    mobileNumber: YUP_PHONE_NUMBER_VALIDATOR,
    password: YUP_PASSWORD_VALIDATOR,
  });

  const [processing, setProcessing] = useState(false);
  const serviceErrorRef = useRef(null);

  const {
    handleSubmit,
    control,
    formState: { errors, isValid },
    setError,
  } = useForm({ resolver: yupResolver(signInSchema) });
  const { handleHomeRoute } = useRoutes().useHomeRoute();
  const { handleSignInRoute } = useRoutes().useSignInRoute();

  const {
    handleCKYCSignIn,
    error: ckycSignInError,
    setError: setCkycSignInError,
    ckycSignIn,
    setCkycSignIn,
  } = useCKYCSignIn();
  const {
    handleClearAppUserRegistration,
    handleCKYCProcessSignIn,
    error: ckycProcessSignInError,
    setError: setCkycProcessSignInError,
  } = useCKYCProcessSignIn({ appUser });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (ckycSignInError || ckycProcessSignInError) {
      setProcessing(false);
    }

    if (ckycSignInError) {
      if (isAxiosError(ckycSignInError)) {
        const data = ckycSignInError.response.data;
        if (isCKYCError(data)) {
          if (data.responseCode === CKYC_SERVICE_RESPONSE_CODE.C004) {
            handleSignInRoute();
          } else if (data.responseCode === CKYC_SERVICE_RESPONSE_CODE.C006) {
            handleSignInRoute();
          }
        }
      }

      handleNotification(ckycSignInError);
    }

    if (ckycProcessSignInError) {
      handleNotification(ckycProcessSignInError);
    }

    return () => {
      setCkycSignInError(null);
      setCkycProcessSignInError(null);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    ckycProcessSignInError,
    ckycSignInError,
    setCkycProcessSignInError,
    setCkycSignInError,
  ]);

  useEffect(() => {
    if (ckycSignIn) {
      (async () => {
        await handleCKYCProcessSignIn(ckycSignIn);
      })();
    }

    if (appUser?.account) {
      handleClearAppUserRegistration();
      handleHomeRoute();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [appUser, ckycSignIn]);

  const handleSignIn = useCallback(
    async ({ mobileNumber, password }) => {
      try {
        if (mobileNumber && !isValidPhoneNumber(mobileNumber)) {
          setError('mobileNumber', {
            type: 'manual',
            message: 'Invalid mobile number. Must be at least 12 digits long',
          });
          return;
        }

        setProcessing(true);
        setCkycSignIn(null);
        serviceErrorRef.current = null;
        mobileNumber = prependPlusToPhoneNumber(mobileNumber);
        const signInBody = {
          latitude: '',
          longitude: '',
          mobileNo: mobileNumber,
          password: password,
          requestId: getId(),
          tenant: 'DFS',
        };
        await handleCKYCSignIn(signInBody);
      } catch (error) {
        alertWebError(error);
        setProcessing(false);
      }
    },
    [handleCKYCSignIn, setCkycSignIn, setError]
  );

  return (
    <div>
      <Card>
        <Card.Body>
          <Form onSubmit={handleSubmit(handleSignIn)}>
            <Form.Group className="my-1">
              <Form.Label>mobile number</Form.Label>
              <Controller
                name="mobileNumber"
                control={control}
                render={({ field }) => (
                  <AppPhoneInput
                    errors={errors}
                    componentProps={{
                      inputProps: {
                        type: 'tel',
                        placeholder: 'Your mobile number',
                        autoComplete: 'tel',
                        className: 'w-100 mobile-number-padding form-control',
                      },
                      country: 'gh',
                      regions: ['africa'],
                      prefix: '+',
                    }}
                    {...field}
                  />
                )}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>password</Form.Label>
              <Controller
                render={({ field }) => (
                  <Form.Control
                    {...field}
                    type="password"
                    autoComplete="password"
                    isInvalid={!!errors.password}
                    placeholder="Your password"
                  />
                )}
                control={control}
                name="password"
              />

              {errors.password && (
                <Form.Control.Feedback type="invalid">
                  {errors.password.message}
                </Form.Control.Feedback>
              )}
            </Form.Group>

            <div className="my-4">
              <Button
                type="submit"
                variant="primary"
                className="w-100 fw-bold text-white"
                disabled={!isValid || processing}
              >
                {processing ? `Signing in...` : `Sign in`}
              </Button>
            </div>
          </Form>
        </Card.Body>
      </Card>
    </div>
  );
}

SignInPage.propTypes = {
  appUser: PropTypes.object,
};
SignInPage.defaultProps = {
  appUser: null,
};

export default SignInPage;
