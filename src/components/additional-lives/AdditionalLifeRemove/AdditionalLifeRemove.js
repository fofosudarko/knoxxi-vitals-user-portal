import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemoveAdditionalLife } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function AdditionalLifeRemove({ additionalLife, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleRemoveAdditionalLife: _handleRemoveAdditionalLife,
    error,
    setError,
    additionalLifeRemoved,
    setAdditionalLifeRemoved,
  } = useRemoveAdditionalLife({ appUser, enteredById: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (additionalLifeRemoved) {
      handleApiResult();
    }
  }, [additionalLifeRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Additional life removed successfully'
      ).getAlertNotification()
    );
    setAdditionalLifeRemoved(false);
  }, [handleNotification, setAdditionalLifeRemoved]);

  const handleRemoveAdditionalLife = useCallback(async () => {
    await _handleRemoveAdditionalLife(additionalLife);
  }, [_handleRemoveAdditionalLife, additionalLife]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemoveAdditionalLife}
        variant="white"
        text="Remove additional life"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

AdditionalLifeRemove.propTypes = {
  additionalLife: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
AdditionalLifeRemove.defaultProps = {
  additionalLife: null,
  appUser: null,
  useTooltip: false,
};

export default AdditionalLifeRemove;
