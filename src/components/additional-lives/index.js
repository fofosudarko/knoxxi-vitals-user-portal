import AdditionalLivesList, {
  AdditionalLivesListView,
} from './AdditionalLivesList/AdditionalLivesList';
import { AdditionalLifeItemActions } from './AdditionalLivesList/AdditionalLifeItem';
import AdditionalLifeRemove from './AdditionalLifeRemove/AdditionalLifeRemove';
import AdditionalLivesPage from './AdditionalLivesPage/AdditionalLivesPage';
import AdditionalLifeInput from './AdditionalLifeInput/AdditionalLifeInput';
import AdditionalLifeNewRoute from './AdditionalLifeNew/AdditionalLifeNewRoute';
import AdditionalLifeNewPage from './AdditionalLifeNewPage/AdditionalLifeNewPage';
import AdditionalLifeEditRoute from './AdditionalLifeEdit/AdditionalLifeEditRoute';
import AdditionalLifeEditPage from './AdditionalLifeEditPage/AdditionalLifeEditPage';

export {
  AdditionalLivesList,
  AdditionalLifeRemove,
  AdditionalLivesPage,
  AdditionalLivesListView,
  AdditionalLifeItemActions,
  AdditionalLifeInput,
  AdditionalLifeNewRoute,
  AdditionalLifeNewPage,
  AdditionalLifeEditRoute,
  AdditionalLifeEditPage,
};
