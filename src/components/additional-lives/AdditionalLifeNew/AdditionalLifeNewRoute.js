import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { CreateButton } from 'src/components/lib';

export default function AdditionalLifeNewRoute({ text }) {
  const { handleAdditionalLifeNewRoute } =
    useRoutes().useAdditionalLifeNewRoute();

  return (
    <CreateButton
      variant="primary"
      textColor="white"
      onClick={handleAdditionalLifeNewRoute}
      text={text}
    />
  );
}

AdditionalLifeNewRoute.propTypes = {
  text: PropTypes.string,
};
AdditionalLifeNewRoute.defaultProps = {
  text: 'New additional life',
};
