import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { EditButton } from 'src/components/lib';

export default function AdditionalLifeEditRoute({
  useTooltip,
  additionalLife,
}) {
  const { handleAdditionalLifeEditRoute } =
    useRoutes().useAdditionalLifeEditRoute(additionalLife);

  return (
    <EditButton
      onClick={handleAdditionalLifeEditRoute}
      variant="transparent"
      text="Edit additional life"
      textColor="black"
      textNormal
      autoWidth={!useTooltip}
      useTooltip={useTooltip}
    />
  );
}

AdditionalLifeEditRoute.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
  additionalLife: PropTypes.object,
};
AdditionalLifeEditRoute.defaultProps = {
  useTooltip: false,
  appUser: null,
  additionalLife: null,
};
