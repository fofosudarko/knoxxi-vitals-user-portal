import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import {
  AdditionalLifeTableItem,
  AdditionalLifeGridItem,
} from './AdditionalLifeItem';

export function AdditionalLivesList({
  additionalLives,
  loadingText,
  appUser,
  onLoadMore,
  AdditionalLivesListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!additionalLives) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(AdditionalLivesListEmpty);

  return additionalLives.length ? (
    <div>
      {isLargeDevice ? (
        <AdditionalLivesListTable
          additionalLives={additionalLives}
          appUser={appUser}
        />
      ) : (
        <AdditionalLivesListGrid
          additionalLives={additionalLives}
          appUser={appUser}
        />
      )}

      {additionalLives.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more additional lives"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function AdditionalLivesListGrid({ additionalLives, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {additionalLives.map((item) => (
          <Col key={item.id}>
            <AdditionalLifeGridItem additionalLife={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function AdditionalLivesListTable({ additionalLives, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">Date of birth</th>
            <th className="list-table-head-cell">Phone number</th>
            <th className="list-table-head-cell">Relationship</th>
            <th className="list-table-head-cell">Name</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {additionalLives.map((item, index) => (
            <AdditionalLifeTableItem
              additionalLife={item}
              appUser={appUser}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function AdditionalLivesListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no additional lives yet.
          </div>
        </div>
      </div>
    </div>
  );
}

AdditionalLivesList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  additionalLives: PropTypes.array,
  AdditionalLivesListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
AdditionalLivesList.defaultProps = {
  appUser: null,
  loadingText: 'Loading additional lives...',
  onLoadMore: undefined,
  additionalLives: null,
  AdditionalLivesListEmpty: null,
  isDisabled: false,
};
AdditionalLivesListGrid.propTypes = {
  appUser: PropTypes.object,
  additionalLives: PropTypes.array,
};
AdditionalLivesListGrid.defaultProps = {
  appUser: null,
  additionalLives: null,
};
AdditionalLivesListTable.propTypes = {
  appUser: PropTypes.object,
  additionalLives: PropTypes.array,
};
AdditionalLivesListTable.defaultProps = {
  appUser: null,
  additionalLives: null,
};
AdditionalLivesListEmpty.propTypes = {
  appUser: PropTypes.object,
};
AdditionalLivesListEmpty.defaultProps = {
  appUser: null,
};
