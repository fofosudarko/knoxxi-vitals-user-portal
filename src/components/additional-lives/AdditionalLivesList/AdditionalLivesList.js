import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListAdditionalLives } from 'src/hooks/api';

import AdditionalLivesReload from './AdditionalLivesReload';
import AdditionalLifeNewRoute from '../AdditionalLifeNew/AdditionalLifeNewRoute';
import {
  AdditionalLivesList,
  AdditionalLivesListEmpty,
} from './_AdditionalLivesList';

function AdditionalLivesListContainer({ appUser, query }) {
  const {
    additionalLives,
    error: additionalLivesError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListAdditionalLives({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (additionalLivesError) {
      handleNotification(additionalLivesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, additionalLivesError, setError]);

  const handleLoadMoreAdditionalLives = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <AdditionalLivesList
      additionalLives={additionalLives}
      onLoadMore={handleLoadMoreAdditionalLives}
      loadingText="Loading additional lives..."
      appUser={appUser}
      AdditionalLivesListEmpty={AdditionalLivesListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function AdditionalLivesListView({ appUser, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end my-1">
        <AdditionalLifeNewRoute />
      </div>
      <div className="d-flex justify-content-end">
        <AdditionalLivesReload useTooltip appUser={appUser} />
      </div>
      <div className="my-2">
        <AdditionalLivesListContainer appUser={appUser} query={query} />
      </div>
    </div>
  );
}

AdditionalLivesListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
AdditionalLivesListContainer.defaultProps = {
  appUser: null,
  query: null,
};
AdditionalLivesList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  additionalLives: PropTypes.array,
  AdditionalLivesListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
AdditionalLivesList.defaultProps = {
  appUser: null,
  loadingText: 'Loading additional lives...',
  onLoadMore: undefined,
  additionalLives: null,
  AdditionalLivesListEmpty: null,
  isDisabled: false,
};
AdditionalLivesListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
AdditionalLivesListView.defaultProps = {
  appUser: null,
  query: null,
};

export default AdditionalLivesList;
