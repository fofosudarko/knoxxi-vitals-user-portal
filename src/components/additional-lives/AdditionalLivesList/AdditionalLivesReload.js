import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetAdditionalLives } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function AdditionalLivesReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const _handleResetAdditionalLives = useResetAdditionalLives({
    enteredById: account?.customerId,
  });

  const handleResetAdditionalLives = useCallback(() => {
    _handleResetAdditionalLives();
  }, [_handleResetAdditionalLives]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetAdditionalLives}
      text="Reload additional lives"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

AdditionalLivesReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
AdditionalLivesReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default AdditionalLivesReload;
