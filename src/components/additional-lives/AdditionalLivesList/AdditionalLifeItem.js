import PropTypes from 'prop-types';
import { Dropdown, Row, Col } from 'react-bootstrap';
import { FaEllipsisV } from 'react-icons/fa';

import { useDeviceDimensions } from 'src/hooks';
import { humanizeDate } from 'src/utils';

import AdditionalLifeRemove from '../AdditionalLifeRemove/AdditionalLifeRemove';
import AdditionalLifeEditRoute from '../AdditionalLifeEdit/AdditionalLifeEditRoute';

export function useAdditionalLifeDetails(additionalLife) {
  const { createdOn, dob, phoneNumber, relationship, name } =
    additionalLife ?? {};

  return {
    createdOn,
    dob,
    phoneNumber,
    relationship,
    name,
  };
}

export function AdditionalLifeGridItem({ additionalLife, appUser }) {
  const { createdOn, dob, phoneNumber, relationship, name } =
    useAdditionalLifeDetails(additionalLife);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Created</div>
            <div className="item-subtitle">
              {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Date of birth</div>
            <div className="item-subtitle fw-bold">
              {dob ? humanizeDate(new Date(dob)) : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Phone number</div>
            <div className="item-subtitle fw-bold">
              {phoneNumber !== undefined ? phoneNumber : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Relationship</div>
            <div className="item-subtitle">
              {relationship ? relationship : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">Name</div>
            <div className="item-subtitle">{name ? name : 'N/A'}</div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              <AdditionalLifeItemActions
                additionalLife={additionalLife}
                appUser={appUser}
              />
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function AdditionalLifeTableItem({ additionalLife, appUser }) {
  const { createdOn, dob, phoneNumber, relationship, name } =
    useAdditionalLifeDetails(additionalLife);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {dob ? humanizeDate(new Date(dob)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {phoneNumber !== undefined ? phoneNumber : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {relationship ? relationship : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {name ? name : 'N/A'}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          <AdditionalLifeItemActions
            additionalLife={additionalLife}
            appUser={appUser}
          />
        </div>
      </td>
    </tr>
  );
}

export function AdditionalLifeItemActions({ additionalLife, appUser }) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item>
            <AdditionalLifeEditRoute
              additionalLife={additionalLife}
              appUser={appUser}
            />
          </Dropdown.Item>
          <Dropdown.Item>
            <AdditionalLifeRemove
              additionalLife={additionalLife}
              appUser={appUser}
            />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

AdditionalLifeGridItem.propTypes = {
  additionalLife: PropTypes.object,
  appUser: PropTypes.object,
};
AdditionalLifeGridItem.defaultProps = {
  additionalLife: null,
  appUser: null,
};
AdditionalLifeTableItem.propTypes = {
  additionalLife: PropTypes.object,
  appUser: PropTypes.object,
};
AdditionalLifeTableItem.defaultProps = {
  additionalLife: null,
  appUser: null,
};
AdditionalLifeItemActions.propTypes = {
  additionalLife: PropTypes.object,
  appUser: PropTypes.object,
};
AdditionalLifeItemActions.defaultProps = {
  additionalLife: null,
  appUser: null,
};
