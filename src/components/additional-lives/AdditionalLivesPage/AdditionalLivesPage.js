import PropTypes from 'prop-types';

import { AdditionalLivesListView } from '../AdditionalLivesList/AdditionalLivesList';

function AdditionalLivesPage({ appUser }) {
  const account = appUser?.account ?? null;
  const query = { enteredById: account?.customerId };
  return (
    <div>
      <div className="page-title">Additional lives</div>
      <div className="page-content">
        <AdditionalLivesListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

AdditionalLivesPage.propTypes = {
  appUser: PropTypes.object,
};
AdditionalLivesPage.defaultProps = {
  appUser: null,
};

export default AdditionalLivesPage;
