import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCreateAdditionalLife } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';

import { ContentContainer, GoBack } from 'src/components/lib';
import AdditionalLifeInput from '../AdditionalLifeInput/AdditionalLifeInput';

function AdditionalLifeNewPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { handleAdditionalLivesRoute } = useRoutes().useAdditionalLivesRoute();
  const {
    handleCreateAdditionalLife: _handleCreateAdditionalLife,
    error,
    setError,
    processing,
    additionalLifeCreated,
    setAdditionalLifeCreated,
  } = useCreateAdditionalLife({ appUser, enteredById: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (additionalLifeCreated) {
      handleApiResult();
    }
  }, [additionalLifeCreated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Additional life created successfully'
      ).getSuccessNotification(),
      () => {
        setAdditionalLifeCreated(false);
      }
    );
    handleAdditionalLivesRoute();
  }, [
    handleAdditionalLivesRoute,
    handleNotification,
    setAdditionalLifeCreated,
  ]);

  const handleCreateAdditionalLife = useCallback(
    async (additionalLife) => {
      const body = {
        ...additionalLife,
        kycId: account?.customerId,
      };
      await _handleCreateAdditionalLife(body);
    },
    [_handleCreateAdditionalLife, account?.customerId]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">New additional life</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <AdditionalLifeInput
          appUser={appUser}
          onSubmit={handleCreateAdditionalLife}
          processing={processing}
        />
      </ContentContainer>
    </div>
  );
}

AdditionalLifeNewPage.propTypes = {
  appUser: PropTypes.object,
};
AdditionalLifeNewPage.defaultProps = {
  appUser: null,
};

export default AdditionalLifeNewPage;
