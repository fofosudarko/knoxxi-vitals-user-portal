import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useUpdateAdditionalLife } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';

import { ContentContainer, GoBack } from 'src/components/lib';
import AdditionalLifeInput from '../AdditionalLifeInput/AdditionalLifeInput';

function AdditionalLifeEditPage({ appUser, additionalLife }) {
  const account = appUser?.account ?? null;
  const { handleAdditionalLivesRoute } = useRoutes().useAdditionalLivesRoute();
  const {
    handleUpdateAdditionalLife: _handleUpdateAdditionalLife,
    error,
    processing,
    setError,
    additionalLifeUpdated,
    setAdditionalLifeUpdated,
  } = useUpdateAdditionalLife({ appUser, enteredById: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (additionalLifeUpdated) {
      handleApiResult();
    }
  }, [handleApiResult, additionalLifeUpdated]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Additional life updated successfully'
      ).getNormalNotification(),
      () => {
        setAdditionalLifeUpdated(false);
      }
    );
    handleAdditionalLivesRoute();
  }, [
    handleNotification,
    handleAdditionalLivesRoute,
    setAdditionalLifeUpdated,
  ]);

  const handleUpdateAdditionalLife = useCallback(
    async (updatedAdditionalLife) => {
      const body = {
        ...updatedAdditionalLife,
        kycId: account?.customerId,
      };
      await _handleUpdateAdditionalLife(additionalLife, body);
    },
    [_handleUpdateAdditionalLife, account?.customerId, additionalLife]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">Edit additional life</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <AdditionalLifeInput
          appUser={appUser}
          onSubmit={handleUpdateAdditionalLife}
          isEditing
          processing={processing}
          additionalLife={additionalLife}
        />
      </ContentContainer>
    </div>
  );
}

AdditionalLifeEditPage.propTypes = {
  appUser: PropTypes.object,
  additionalLife: PropTypes.object,
};
AdditionalLifeEditPage.defaultProps = {
  appUser: null,
  additionalLife: null,
};

export default AdditionalLifeEditPage;
