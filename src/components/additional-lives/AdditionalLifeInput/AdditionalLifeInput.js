import { useCallback, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import {
  YUP_DATE_OF_BIRTH_VALIDATOR,
  YUP_PHONE_NUMBER_VALIDATOR,
  YUP_ADDITIONAL_LIFE_RELATIONSHIP_VALIDATOR,
  YUP_GENERAL_NAME_VALIDATOR,
} from 'src/config/validators';
import { useFormReset, useDeviceDimensions } from 'src/hooks';
import {
  isValidPhoneNumber,
  prependPlusToPhoneNumber,
  getFormattedDate,
} from 'src/utils';

import {
  CancelButton,
  CreateButton,
  EditButton,
  AppPhoneInput,
} from 'src/components/lib';
import { AdditionalLifeRelationshipSelect } from 'src/components/enums';

function AdditionalLifeInput({
  additionalLife,
  isEditing,
  onSubmit,
  processing,
}) {
  useState();
  const additionalLifeInputSchema = Yup({
    dob: YUP_DATE_OF_BIRTH_VALIDATOR,
    phoneNumber: YUP_PHONE_NUMBER_VALIDATOR,
    relationship: YUP_ADDITIONAL_LIFE_RELATIONSHIP_VALIDATOR,
    name: YUP_GENERAL_NAME_VALIDATOR,
  });
  const { isLargeDevice } = useDeviceDimensions();
  const defaultValues = useMemo(
    () => ({
      dob: additionalLife?.dob
        ? getFormattedDate(new Date(additionalLife?.dob))
        : '',
      phoneNumber: additionalLife?.phoneNumber ?? '',
      relationship: additionalLife?.relationship ?? '',
      name: additionalLife?.name ?? '',
    }),
    [
      additionalLife?.dob,
      additionalLife?.phoneNumber,
      additionalLife?.relationship,
      additionalLife?.name,
    ]
  );

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    reset,
    setError,
  } = useForm({
    defaultValues,
    resolver: yupResolver(additionalLifeInputSchema),
  });
  useFormReset({ values: defaultValues, item: additionalLife, reset });

  const handleSubmitAdditionalLife = useCallback(
    ({ phoneNumber, dob, relationship, name }) => {
      if (phoneNumber && !isValidPhoneNumber(phoneNumber)) {
        setError('phoneNumber', {
          type: 'manual',
          message: 'Invalid phone number. Must be at least 12 digits long',
        });
        return;
      }

      const newAdditionalLife = {
        phoneNumber: prependPlusToPhoneNumber(phoneNumber),
        dob,
        relationship,
        name,
      };

      onSubmit && onSubmit(newAdditionalLife);
    },
    [onSubmit, setError]
  );

  const handleCancelAdditionalLife = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitAdditionalLife)} noValidate>
        <Row xs={{ cols: 1 }}>
          <Form.Group className="my-1" as={Col}>
            <Controller
              name="relationship"
              control={control}
              render={({ field }) => (
                <AdditionalLifeRelationshipSelect
                  label="Relationship"
                  labelClass="main-form-label"
                  placeholder="Relationship"
                  helpText="Select relationship. E.g Mother"
                  errors={errors}
                  {...field}
                />
              )}
            />
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">Name</Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Name"
                  {...field}
                  isInvalid={!!errors.name}
                />
              )}
              name="name"
              control={control}
            />
            {errors.name ? (
              <Form.Control.Feedback type="invalid">
                {errors.name.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give the name e.g. Akosua Sagaa</Form.Text>
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">Date of birth</Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="date"
                  placeholder="Date of birth"
                  {...field}
                  isInvalid={!!errors.dob}
                />
              )}
              name="dob"
              control={control}
            />
            {errors.dob ? (
              <Form.Control.Feedback type="invalid">
                {errors.dob.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give the date of birth e.g. 2000-01-01</Form.Text>
          </Form.Group>

          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">Phone number</Form.Label>
            <Controller
              name="phoneNumber"
              control={control}
              render={({ field }) => (
                <AppPhoneInput
                  errors={errors}
                  componentProps={{
                    inputProps: {
                      type: 'tel',
                      placeholder: 'Your phone number',
                      autoComplete: 'tel',
                      className: 'w-100 mobile-number-padding form-control',
                    },
                    country: 'gh',
                    regions: ['africa'],
                    prefix: '+',
                  }}
                  {...field}
                />
              )}
            />
            <Form.Text>Give the phone number</Form.Text>
          </Form.Group>
        </Row>

        <div className="d-flex flex-column-reverse flex-md-row justify-content-end my-3">
          <div className="my-1 my-sm-0 mx-1">
            <CancelButton
              onClick={handleCancelAdditionalLife}
              autoWidth={isLargeDevice}
              text="Cancel"
            />
          </div>
          <div className="my-1 my-sm-0 mx-1">
            {isEditing ? (
              <EditButton
                type="submit"
                clicked={processing}
                text="Edit additional life"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            ) : (
              <CreateButton
                type="submit"
                clicked={processing}
                text="Add additional life"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            )}
          </div>
        </div>
      </Form>
    </div>
  );
}

AdditionalLifeInput.propTypes = {
  appUser: PropTypes.object,
  additionalLife: PropTypes.object,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
};
AdditionalLifeInput.defaultProps = {
  appUser: null,
  additionalLife: null,
  isEditing: false,
  onSubmit: undefined,
  processing: false,
};

export default AdditionalLifeInput;
