import { useEffect, useCallback, useState, useContext, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import {
  //SELECT_DATE_COUNT_OPTIONS,
  //SELECT_DATE_INTERVAL_OPTIONS,
  //SELECT_DATE_ORDER_OPTIONS,
  SELECT_DATE_PERIOD_OPTIONS,
  SELECT_DATE_COUNT_PERIOD_MAP,
} from 'src/config';

import { InsightsGraphFilterContext } from 'src/context';
import { FilterContainer, AppDateTimePicker } from 'src/components/lib';

function InsightsGraphFilter({ onSearch }) {
  const {
    date: _date,
    count: _count,
    period: _period,
    interval: _interval,
    order: _order,
    setDate,
    setCount,
    setPeriod,
    setInterval,
    setOrder,
  } = useContext(InsightsGraphFilterContext);
  const [{ date, count, period, interval, order }, setSearchTerms] = useState({
    date: _date,
    count: _count,
    period: _period,
    interval: _interval,
    order: _order,
  });

  useEffect(() => {
    date !== undefined && setDate(date);
    count !== undefined && setCount(count);
    period !== undefined && setPeriod(period);
    interval !== undefined && setInterval(interval);
    order !== undefined && setOrder(order);
  }, [
    onSearch,
    count,
    date,
    interval,
    setDate,
    setCount,
    setInterval,
    setOrder,
    order,
    period,
    setPeriod,
  ]);

  const handleSubmitSearchTerms = useCallback((searchTerms) => {
    setSearchTerms(searchTerms);
  }, []);

  return (
    <div className="mb-2">
      <FilterContainer>
        <InsightsGraphFilterInput
          onSubmit={handleSubmitSearchTerms}
          filterValues={{ date, count, period, interval, order }}
        />
      </FilterContainer>
    </div>
  );
}

function InsightsGraphFilterInput({ onSubmit, filterValues }) {
  const insightsGraphFilterInputSchema = Yup({});

  const defaultValues = useMemo(() => {
    return {
      date: filterValues?.date,
      count: filterValues?.count,
      period: filterValues?.period,
      interval: filterValues?.interval,
      order: filterValues?.order,
    };
  }, [
    filterValues?.count,
    filterValues?.date,
    filterValues?.interval,
    filterValues?.order,
    filterValues?.period,
  ]);

  const {
    formState: { errors },
    control,
    watch,
  } = useForm({
    resolver: yupResolver(insightsGraphFilterInputSchema),
    defaultValues,
  });

  const watchedDate = watch('date');
  const watchedCount = watch('count');
  const watchedPeriod = watch('period');
  const watchedInterval = watch('interval');
  const watchedOrder = watch('order');

  useEffect(() => {
    const date = watchedDate,
      count =
        watchedPeriod !== undefined
          ? SELECT_DATE_COUNT_PERIOD_MAP[watchedPeriod]
          : watchedCount,
      period = watchedPeriod,
      interval = watchedInterval,
      order = watchedOrder;
    onSubmit && onSubmit({ date, count, period, interval, order });
  }, [
    onSubmit,
    watchedCount,
    watchedInterval,
    watchedDate,
    watchedOrder,
    watchedPeriod,
  ]);

  //const selectCountOptions = SELECT_DATE_COUNT_OPTIONS,
  //  selectIntervalOptions = SELECT_DATE_INTERVAL_OPTIONS,
  //  selectOrderOptions = SELECT_DATE_ORDER_OPTIONS
  const selectPeriodOptions = SELECT_DATE_PERIOD_OPTIONS;

  return (
    <div>
      <Row xs={{ cols: 1 }} className="gx-2">
        <Col xs={{ span: 12 }} lg={{ span: 6 }}>
          <Form.Group className="my-1">
            <Form.Label>Filter date</Form.Label>
            <Controller
              render={({ field }) => {
                return <AppDateTimePicker {...field} className="w-100" />;
              }}
              name="date"
              control={control}
            />
          </Form.Group>
          {errors.date ? (
            <Form.Control.Feedback type="invalid">
              {errors.date.message}
            </Form.Control.Feedback>
          ) : null}
        </Col>
        {/*<Col xs={{ span: 12 }} lg={{ span: 3 }}>
          <Form.Group className="my-1">
            <Form.Label>Date count</Form.Label>
            <Controller
              name="count"
              control={control}
              render={({ field }) => (
                <Form.Select
                  placeholder="Filter by count"
                  {...field}
                  isInvalid={!!errors.count}
                >
                  {selectCountOptions.map((item, index) => (
                    <option value={item} key={index}>
                      {item}
                    </option>
                  ))}
                </Form.Select>
              )}
            />
            {errors.count ? (
              <Form.Control.Feedback type="invalid">
                {errors.count.message}
              </Form.Control.Feedback>
            ) : null}
          </Form.Group>
        </Col>*/}
        <Col xs={{ span: 12 }} lg={{ span: 6 }}>
          <Form.Group className="my-1">
            <Form.Label>Date period</Form.Label>
            <Controller
              name="period"
              control={control}
              render={({ field }) => (
                <Form.Select
                  placeholder="Filter by period"
                  {...field}
                  isInvalid={!!errors.period}
                >
                  {selectPeriodOptions.map((item, index) => (
                    <option value={item} key={index}>
                      {item}
                    </option>
                  ))}
                </Form.Select>
              )}
            />
            {errors.period ? (
              <Form.Control.Feedback type="invalid">
                {errors.period.message}
              </Form.Control.Feedback>
            ) : null}
          </Form.Group>
        </Col>
        {/*<Col xs={{ span: 12 }} lg={{ span: 3 }}>
          <Form.Group className="my-1">
            <Form.Label>Date interval</Form.Label>
            <Controller
              name="interval"
              control={control}
              render={({ field }) => (
                <Form.Select
                  placeholder="Filter by interval"
                  {...field}
                  isInvalid={!!errors.interval}
                >
                  {selectIntervalOptions.map((item, index) => (
                    <option value={item} key={index}>
                      {item}
                    </option>
                  ))}
                </Form.Select>
              )}
            />
            {errors.interval ? (
              <Form.Control.Feedback type="invalid">
                {errors.interval.message}
              </Form.Control.Feedback>
            ) : null}
          </Form.Group>
        </Col>
        <Col xs={{ span: 12 }} lg={{ span: 3 }}>
          <Form.Group className="my-1">
            <Form.Label>Date order</Form.Label>
            <Controller
              name="order"
              control={control}
              render={({ field }) => (
                <Form.Select
                  placeholder="Filter by order"
                  {...field}
                  isInvalid={!!errors.order}
                >
                  {selectOrderOptions.map((item, index) => (
                    <option value={item} key={index}>
                      {item}
                    </option>
                  ))}
                </Form.Select>
              )}
            />
            {errors.order ? (
              <Form.Control.Feedback type="invalid">
                {errors.order.message}
              </Form.Control.Feedback>
            ) : null}
          </Form.Group>
        </Col>*/}
      </Row>
    </div>
  );
}

InsightsGraphFilter.propTypes = {
  onSearch: PropTypes.func,
  query: PropTypes.object,
  appUser: PropTypes.object,
};
InsightsGraphFilter.defaultProps = {
  onSearch: undefined,
  query: null,
  appUser: null,
};
InsightsGraphFilterInput.propTypes = {
  appUser: PropTypes.object,
  onSubmit: PropTypes.func,
  filterValues: PropTypes.object,
};
InsightsGraphFilterInput.defaultProps = {
  appUser: null,
  onSubmit: undefined,
  filterValues: null,
};

export default InsightsGraphFilter;
