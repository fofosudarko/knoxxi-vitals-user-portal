import PropTypes from 'prop-types';

import CholesterolDataCumulativeAveragesAllGraphPage from './CholesterolDataCumulativeAveragesAllGraphPage';

function CholesterolDataCumulativeAveragesGraphPage({ appUser }) {
  return <CholesterolDataCumulativeAveragesAllGraphPage appUser={appUser} />;
}

CholesterolDataCumulativeAveragesGraphPage.propTypes = {
  appUser: PropTypes.object,
};
CholesterolDataCumulativeAveragesGraphPage.defaultProps = {
  appUser: null,
};

export default CholesterolDataCumulativeAveragesGraphPage;
