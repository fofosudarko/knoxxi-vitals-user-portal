import PropTypes from 'prop-types';
import { Stack } from 'react-bootstrap';

import { InsightsGraphFilterProvider } from 'src/context';
import CholesterolDataTimedAveragesGraphPage from './CholesterolDataTimedAveragesGraphPage';
import CholesterolDataCumulativeAveragesGraphPage from './CholesterolDataCumulativeAveragesGraphPage';
import InsightsGraphFilter from '../../InsightsGraphFilter';
import CholesterolDataInsightsGraphReload from './CholesterolDataInsightsGraphReload';

function CholesterolDataInsightsGraphPage({ appUser }) {
  return (
    <div>
      <div className="page-title">Cholesterol Data Insights</div>
      <div className="page-content">
        <InsightsGraphFilterProvider>
          <InsightsGraphFilter />
          <div className="d-flex justify-content-end">
            <CholesterolDataInsightsGraphReload useTooltip appUser={appUser} />
          </div>
          <Stack gap={2}>
            <CholesterolDataCumulativeAveragesGraphPage appUser={appUser} />
            <CholesterolDataTimedAveragesGraphPage appUser={appUser} />
          </Stack>
        </InsightsGraphFilterProvider>
      </div>
    </div>
  );
}

CholesterolDataInsightsGraphPage.propTypes = {
  appUser: PropTypes.object,
};
CholesterolDataInsightsGraphPage.defaultProps = {
  appUser: null,
};

export default CholesterolDataInsightsGraphPage;
