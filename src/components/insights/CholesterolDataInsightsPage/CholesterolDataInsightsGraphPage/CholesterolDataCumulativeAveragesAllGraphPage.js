import { useContext } from 'react';
import PropTypes from 'prop-types';

import { getListOfDates } from 'src/utils';

import { InsightsGraphFilterContext } from 'src/context';
import { CholesterolDataCumulativeAveragesNumberCardView } from 'src/components/cholesterol-data';

function CholesterolDataCumulativeAveragesAllGraphPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { date, count, period, interval, order } = useContext(
    InsightsGraphFilterContext
  );
  const listOfDates = getListOfDates(date, count, interval, order);
  const item = {
    label: 'All',
    query: {
      userId: account?.customerId,
      contentCategory: undefined,
      createdOn: listOfDates.join(','),
      period: 'Specific dates',
    },
    appUser,
    handleClick: undefined,
  };

  return (
    <div>
      <div className="page-subtitle">
        {period} cumulative average cholesterol
      </div>
      <CholesterolDataCumulativeAveragesNumberCardView
        label={item.label}
        query={item.query}
        appUser={item.appUser}
        onClick={item.handleClick}
      />
    </div>
  );
}

CholesterolDataCumulativeAveragesAllGraphPage.propTypes = {
  appUser: PropTypes.object,
  onClick: PropTypes.func,
};
CholesterolDataCumulativeAveragesAllGraphPage.defaultProps = {
  appUser: null,
  onClick: undefined,
};

export default CholesterolDataCumulativeAveragesAllGraphPage;
