import PropTypes from 'prop-types';

import CholesterolDataTimedAveragesAllGraphPage from './CholesterolDataTimedAveragesAllGraphPage';

function CholesterolDataTimedAveragesGraphPage({ appUser }) {
  return <CholesterolDataTimedAveragesAllGraphPage appUser={appUser} />;
}

CholesterolDataTimedAveragesGraphPage.propTypes = {
  appUser: PropTypes.object,
};
CholesterolDataTimedAveragesGraphPage.defaultProps = {
  appUser: null,
};

export default CholesterolDataTimedAveragesGraphPage;
