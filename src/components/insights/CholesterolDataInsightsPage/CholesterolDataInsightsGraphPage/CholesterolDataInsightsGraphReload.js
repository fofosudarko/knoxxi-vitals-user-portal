import { useCallback, useContext } from 'react';
import PropTypes from 'prop-types';

import { getListOfDates } from 'src/utils';
import { useResetCholesterolDataInsights } from 'src/hooks/api';

import { InsightsGraphFilterContext } from 'src/context';
import { RefreshButton } from 'src/components/lib';

function CholesterolDataInsightsGraphReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const { date, count, interval, order } = useContext(
    InsightsGraphFilterContext
  );
  const listOfDates = getListOfDates(date, count, interval, order);

  const _handleResetCholesterolDataInsights = useResetCholesterolDataInsights({
    userId: account?.customerId,
    period: 'Specific dates',
    createdOn: listOfDates.join(','),
  });

  const handleResetCholesterolDataInsights = useCallback(() => {
    _handleResetCholesterolDataInsights();
  }, [_handleResetCholesterolDataInsights]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetCholesterolDataInsights}
      text="Reload cholesterol data insights"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

CholesterolDataInsightsGraphReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
CholesterolDataInsightsGraphReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default CholesterolDataInsightsGraphReload;
