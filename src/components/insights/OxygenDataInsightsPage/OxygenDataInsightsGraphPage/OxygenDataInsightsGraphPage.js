import PropTypes from 'prop-types';
import { Stack } from 'react-bootstrap';

import { InsightsGraphFilterProvider } from 'src/context';
import OxygenDataTimedAveragesGraphPage from './OxygenDataTimedAveragesGraphPage';
import OxygenDataCumulativeAveragesGraphPage from './OxygenDataCumulativeAveragesGraphPage';
import InsightsGraphFilter from '../../InsightsGraphFilter';
import OxygenDataInsightsGraphReload from './OxygenDataInsightsGraphReload';

function OxygenDataInsightsGraphPage({ appUser }) {
  return (
    <div>
      <div className="page-title">Oxygen Data Insights</div>
      <div className="page-content">
        <InsightsGraphFilterProvider>
          <InsightsGraphFilter />
          <div className="d-flex justify-content-end">
            <OxygenDataInsightsGraphReload useTooltip appUser={appUser} />
          </div>
          <Stack gap={2}>
            <OxygenDataCumulativeAveragesGraphPage appUser={appUser} />
            <OxygenDataTimedAveragesGraphPage appUser={appUser} />
          </Stack>
        </InsightsGraphFilterProvider>
      </div>
    </div>
  );
}

OxygenDataInsightsGraphPage.propTypes = {
  appUser: PropTypes.object,
};
OxygenDataInsightsGraphPage.defaultProps = {
  appUser: null,
};

export default OxygenDataInsightsGraphPage;
