import PropTypes from 'prop-types';

import OxygenDataTimedAveragesAllGraphPage from './OxygenDataTimedAveragesAllGraphPage';

function OxygenDataTimedAveragesGraphPage({ appUser }) {
  return <OxygenDataTimedAveragesAllGraphPage appUser={appUser} />;
}

OxygenDataTimedAveragesGraphPage.propTypes = {
  appUser: PropTypes.object,
};
OxygenDataTimedAveragesGraphPage.defaultProps = {
  appUser: null,
};

export default OxygenDataTimedAveragesGraphPage;
