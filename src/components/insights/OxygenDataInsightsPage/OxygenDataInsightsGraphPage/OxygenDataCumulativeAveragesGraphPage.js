import PropTypes from 'prop-types';

import OxygenDataCumulativeAveragesAllGraphPage from './OxygenDataCumulativeAveragesAllGraphPage';

function OxygenDataCumulativeAveragesGraphPage({ appUser }) {
  return <OxygenDataCumulativeAveragesAllGraphPage appUser={appUser} />;
}

OxygenDataCumulativeAveragesGraphPage.propTypes = {
  appUser: PropTypes.object,
};
OxygenDataCumulativeAveragesGraphPage.defaultProps = {
  appUser: null,
};

export default OxygenDataCumulativeAveragesGraphPage;
