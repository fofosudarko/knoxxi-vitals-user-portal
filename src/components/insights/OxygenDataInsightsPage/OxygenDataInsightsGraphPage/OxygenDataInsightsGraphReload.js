import { useCallback, useContext } from 'react';
import PropTypes from 'prop-types';

import { getListOfDates } from 'src/utils';
import { useResetOxygenDataInsights } from 'src/hooks/api';

import { InsightsGraphFilterContext } from 'src/context';
import { RefreshButton } from 'src/components/lib';

function OxygenDataInsightsGraphReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const { date, count, interval, order } = useContext(
    InsightsGraphFilterContext
  );
  const listOfDates = getListOfDates(date, count, interval, order);

  const _handleResetOxygenDataInsights = useResetOxygenDataInsights({
    userId: account?.customerId,
    period: 'Specific dates',
    createdOn: listOfDates.join(','),
  });

  const handleResetOxygenDataInsights = useCallback(() => {
    _handleResetOxygenDataInsights();
  }, [_handleResetOxygenDataInsights]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetOxygenDataInsights}
      text="Reload oxygen data insights"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

OxygenDataInsightsGraphReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
OxygenDataInsightsGraphReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default OxygenDataInsightsGraphReload;
