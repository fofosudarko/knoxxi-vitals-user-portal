import { useContext } from 'react';
import PropTypes from 'prop-types';

import { getListOfDates } from 'src/utils';

import { InsightsGraphFilterContext } from 'src/context';
import { GlucoseDataCumulativeAveragesNumberCardView } from 'src/components/glucose-data';

function GlucoseDataCumulativeAveragesAllGraphPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { date, count, interval, period, order } = useContext(
    InsightsGraphFilterContext
  );
  const listOfDates = getListOfDates(date, count, interval, order);
  const item = {
    label: 'All',
    query: {
      userId: account?.customerId,
      contentCategory: undefined,
      createdOn: listOfDates.join(','),
      period: 'Specific dates',
    },
    appUser,
    handleClick: undefined,
  };

  return (
    <div>
      <div className="page-subtitle">
        {period} cumulative average blood glucose
      </div>
      <GlucoseDataCumulativeAveragesNumberCardView
        label={item.label}
        query={item.query}
        appUser={item.appUser}
        onClick={item.handleClick}
      />
    </div>
  );
}

GlucoseDataCumulativeAveragesAllGraphPage.propTypes = {
  appUser: PropTypes.object,
  onClick: PropTypes.func,
};
GlucoseDataCumulativeAveragesAllGraphPage.defaultProps = {
  appUser: null,
  onClick: undefined,
};

export default GlucoseDataCumulativeAveragesAllGraphPage;
