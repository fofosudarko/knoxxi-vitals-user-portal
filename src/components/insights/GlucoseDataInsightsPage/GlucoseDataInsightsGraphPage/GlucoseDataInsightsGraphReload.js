import { useCallback, useContext } from 'react';
import PropTypes from 'prop-types';

import { getListOfDates } from 'src/utils';
import { useResetGlucoseDataInsights } from 'src/hooks/api';

import { InsightsGraphFilterContext } from 'src/context';
import { RefreshButton } from 'src/components/lib';

function GlucoseDataInsightsGraphReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const { date, count, interval, order } = useContext(
    InsightsGraphFilterContext
  );
  const listOfDates = getListOfDates(date, count, interval, order);

  const _handleResetGlucoseDataInsights = useResetGlucoseDataInsights({
    userId: account?.customerId,
    period: 'Specific dates',
    createdOn: listOfDates.join(','),
  });

  const handleResetGlucoseDataInsights = useCallback(() => {
    _handleResetGlucoseDataInsights();
  }, [_handleResetGlucoseDataInsights]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetGlucoseDataInsights}
      text="Reload glucose data insights"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

GlucoseDataInsightsGraphReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
GlucoseDataInsightsGraphReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default GlucoseDataInsightsGraphReload;
