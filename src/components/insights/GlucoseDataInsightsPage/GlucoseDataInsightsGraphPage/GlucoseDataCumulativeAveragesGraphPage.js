import PropTypes from 'prop-types';

import GlucoseDataCumulativeAveragesAllGraphPage from './GlucoseDataCumulativeAveragesAllGraphPage';

function GlucoseDataCumulativeAveragesGraphPage({ appUser }) {
  return <GlucoseDataCumulativeAveragesAllGraphPage appUser={appUser} />;
}

GlucoseDataCumulativeAveragesGraphPage.propTypes = {
  appUser: PropTypes.object,
};
GlucoseDataCumulativeAveragesGraphPage.defaultProps = {
  appUser: null,
};

export default GlucoseDataCumulativeAveragesGraphPage;
