import PropTypes from 'prop-types';

import GlucoseDataTimedAveragesAllGraphPage from './GlucoseDataTimedAveragesAllGraphPage';

function GlucoseDataTimedAveragesGraphPage({ appUser }) {
  return <GlucoseDataTimedAveragesAllGraphPage appUser={appUser} />;
}

GlucoseDataTimedAveragesGraphPage.propTypes = {
  appUser: PropTypes.object,
};
GlucoseDataTimedAveragesGraphPage.defaultProps = {
  appUser: null,
};

export default GlucoseDataTimedAveragesGraphPage;
