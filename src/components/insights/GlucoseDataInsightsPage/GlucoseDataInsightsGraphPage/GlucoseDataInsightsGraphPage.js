import PropTypes from 'prop-types';
import { Stack } from 'react-bootstrap';

import { InsightsGraphFilterProvider } from 'src/context';
import GlucoseDataTimedAveragesGraphPage from './GlucoseDataTimedAveragesGraphPage';
import GlucoseDataCumulativeAveragesGraphPage from './GlucoseDataCumulativeAveragesGraphPage';
import InsightsGraphFilter from '../../InsightsGraphFilter';
import GlucoseDataInsightsGraphReload from './GlucoseDataInsightsGraphReload';

function GlucoseDataInsightsGraphPage({ appUser }) {
  return (
    <div>
      <div className="page-title">Glucose Data Insights</div>
      <div className="page-content">
        <InsightsGraphFilterProvider>
          <InsightsGraphFilter />
          <div className="d-flex justify-content-end">
            <GlucoseDataInsightsGraphReload useTooltip appUser={appUser} />
          </div>
          <Stack gap={2}>
            <GlucoseDataCumulativeAveragesGraphPage appUser={appUser} />
            <GlucoseDataTimedAveragesGraphPage appUser={appUser} />
          </Stack>
        </InsightsGraphFilterProvider>
      </div>
    </div>
  );
}

GlucoseDataInsightsGraphPage.propTypes = {
  appUser: PropTypes.object,
};
GlucoseDataInsightsGraphPage.defaultProps = {
  appUser: null,
};

export default GlucoseDataInsightsGraphPage;
