import { useCallback, useContext } from 'react';
import PropTypes from 'prop-types';

import { getListOfDates } from 'src/utils';
import { useResetTemperatureDataInsights } from 'src/hooks/api';

import { InsightsGraphFilterContext } from 'src/context';
import { RefreshButton } from 'src/components/lib';

function TemperatureDataInsightsGraphReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const { date, count, interval, order } = useContext(
    InsightsGraphFilterContext
  );
  const listOfDates = getListOfDates(date, count, interval, order);

  const _handleResetTemperatureDataInsights = useResetTemperatureDataInsights({
    userId: account?.customerId,
    period: 'Specific dates',
    createdOn: listOfDates.join(','),
  });

  const handleResetTemperatureDataInsights = useCallback(() => {
    _handleResetTemperatureDataInsights();
  }, [_handleResetTemperatureDataInsights]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetTemperatureDataInsights}
      text="Reload temperature data insights"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

TemperatureDataInsightsGraphReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
TemperatureDataInsightsGraphReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default TemperatureDataInsightsGraphReload;
