import PropTypes from 'prop-types';
import { Stack } from 'react-bootstrap';

import { InsightsGraphFilterProvider } from 'src/context';
import TemperatureDataTimedAveragesGraphPage from './TemperatureDataTimedAveragesGraphPage';
import TemperatureDataCumulativeAveragesGraphPage from './TemperatureDataCumulativeAveragesGraphPage';
import InsightsGraphFilter from '../../InsightsGraphFilter';
import TemperatureDataInsightsGraphReload from './TemperatureDataInsightsGraphReload';

function TemperatureDataInsightsGraphPage({ appUser }) {
  return (
    <div>
      <div className="page-title">Temperature Data Insights</div>
      <div className="page-content">
        <InsightsGraphFilterProvider>
          <InsightsGraphFilter />
          <div className="d-flex justify-content-end">
            <TemperatureDataInsightsGraphReload useTooltip appUser={appUser} />
          </div>
          <Stack gap={2}>
            <TemperatureDataCumulativeAveragesGraphPage appUser={appUser} />
            <TemperatureDataTimedAveragesGraphPage appUser={appUser} />
          </Stack>
        </InsightsGraphFilterProvider>
      </div>
    </div>
  );
}

TemperatureDataInsightsGraphPage.propTypes = {
  appUser: PropTypes.object,
};
TemperatureDataInsightsGraphPage.defaultProps = {
  appUser: null,
};

export default TemperatureDataInsightsGraphPage;
