import PropTypes from 'prop-types';

import TemperatureDataTimedAveragesAllGraphPage from './TemperatureDataTimedAveragesAllGraphPage';

function TemperatureDataTimedAveragesGraphPage({ appUser }) {
  return <TemperatureDataTimedAveragesAllGraphPage appUser={appUser} />;
}

TemperatureDataTimedAveragesGraphPage.propTypes = {
  appUser: PropTypes.object,
};
TemperatureDataTimedAveragesGraphPage.defaultProps = {
  appUser: null,
};

export default TemperatureDataTimedAveragesGraphPage;
