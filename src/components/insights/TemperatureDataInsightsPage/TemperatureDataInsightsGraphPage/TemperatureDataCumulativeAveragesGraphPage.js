import PropTypes from 'prop-types';

import TemperatureDataCumulativeAveragesAllGraphPage from './TemperatureDataCumulativeAveragesAllGraphPage';

function TemperatureDataCumulativeAveragesGraphPage({ appUser }) {
  return <TemperatureDataCumulativeAveragesAllGraphPage appUser={appUser} />;
}

TemperatureDataCumulativeAveragesGraphPage.propTypes = {
  appUser: PropTypes.object,
};
TemperatureDataCumulativeAveragesGraphPage.defaultProps = {
  appUser: null,
};

export default TemperatureDataCumulativeAveragesGraphPage;
