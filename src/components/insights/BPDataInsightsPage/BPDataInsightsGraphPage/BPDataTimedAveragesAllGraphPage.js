import { useContext } from 'react';
import PropTypes from 'prop-types';

import { getListOfDates } from 'src/utils';

import { InsightsGraphFilterContext } from 'src/context';
import { BPDataTimedAveragesChartCardView } from 'src/components/bp-data';

function BPDataTimedAveragesAllGraphPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { date, count, period, interval, order } = useContext(
    InsightsGraphFilterContext
  );
  const listOfDates = getListOfDates(date, count, interval, order);
  const item = {
    label: 'All',
    query: {
      userId: account?.customerId,
      contentCategory: undefined,
      createdOn: listOfDates.join(','),
      period: 'Specific dates',
    },
    appUser,
    handleClick: undefined,
  };

  return (
    <div>
      <div className="page-subtitle">{period} timed average blood pressure</div>
      <BPDataTimedAveragesChartCardView
        label={item.label}
        query={item.query}
        appUser={item.appUser}
        onClick={item.handleClick}
      />
    </div>
  );
}

BPDataTimedAveragesAllGraphPage.propTypes = {
  appUser: PropTypes.object,
  onClick: PropTypes.func,
};
BPDataTimedAveragesAllGraphPage.defaultProps = {
  appUser: null,
  onClick: undefined,
};

export default BPDataTimedAveragesAllGraphPage;
