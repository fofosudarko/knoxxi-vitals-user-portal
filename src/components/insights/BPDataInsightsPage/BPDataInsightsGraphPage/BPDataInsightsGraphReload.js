import { useCallback, useContext } from 'react';
import PropTypes from 'prop-types';

import { getListOfDates } from 'src/utils';
import { useResetBPDataInsights } from 'src/hooks/api';

import { InsightsGraphFilterContext } from 'src/context';
import { RefreshButton } from 'src/components/lib';

function BPDataInsightsGraphReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const { date, count, interval, order } = useContext(
    InsightsGraphFilterContext
  );
  const listOfDates = getListOfDates(date, count, interval, order);

  const _handleResetBPDataInsights = useResetBPDataInsights({
    userId: account?.customerId,
    period: 'Specific dates',
    createdOn: listOfDates.join(','),
  });

  const handleResetBPDataInsights = useCallback(() => {
    _handleResetBPDataInsights();
  }, [_handleResetBPDataInsights]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetBPDataInsights}
      text="Reload BP data insights"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

BPDataInsightsGraphReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
BPDataInsightsGraphReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default BPDataInsightsGraphReload;
