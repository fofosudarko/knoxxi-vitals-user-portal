import PropTypes from 'prop-types';

import BPDataTimedAveragesAllGraphPage from './BPDataTimedAveragesAllGraphPage';

function BPDataTimedAveragesGraphPage({ appUser }) {
  return <BPDataTimedAveragesAllGraphPage appUser={appUser} />;
}

BPDataTimedAveragesGraphPage.propTypes = {
  appUser: PropTypes.object,
};
BPDataTimedAveragesGraphPage.defaultProps = {
  appUser: null,
};

export default BPDataTimedAveragesGraphPage;
