import { useContext } from 'react';
import PropTypes from 'prop-types';

import { getListOfDates } from 'src/utils';

import { InsightsGraphFilterContext } from 'src/context';
import { BPDataCumulativeAveragesNumberCardView } from 'src/components/bp-data';

function BPDataCumulativeAveragesAllGraphPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { date, count, interval, period, order } = useContext(
    InsightsGraphFilterContext
  );
  const listOfDates = getListOfDates(date, count, interval, order);
  const item = {
    label: 'All',
    query: {
      userId: account?.customerId,
      contentCategory: undefined,
      createdOn: listOfDates.join(','),
      period: 'Specific dates',
    },
    appUser,
    handleClick: undefined,
  };

  return (
    <div>
      <div className="page-subtitle">
        {period} cumulative average blood pressure
      </div>
      <BPDataCumulativeAveragesNumberCardView
        label={item.label}
        query={item.query}
        appUser={item.appUser}
        onClick={item.handleClick}
      />
    </div>
  );
}

BPDataCumulativeAveragesAllGraphPage.propTypes = {
  appUser: PropTypes.object,
  onClick: PropTypes.func,
};
BPDataCumulativeAveragesAllGraphPage.defaultProps = {
  appUser: null,
  onClick: undefined,
};

export default BPDataCumulativeAveragesAllGraphPage;
