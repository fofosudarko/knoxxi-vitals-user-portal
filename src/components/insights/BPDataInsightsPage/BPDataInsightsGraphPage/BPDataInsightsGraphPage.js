import PropTypes from 'prop-types';
import { Stack } from 'react-bootstrap';

import { InsightsGraphFilterProvider } from 'src/context';
import BPDataTimedAveragesGraphPage from './BPDataTimedAveragesGraphPage';
import BPDataCumulativeAveragesGraphPage from './BPDataCumulativeAveragesGraphPage';
import InsightsGraphFilter from '../../InsightsGraphFilter';
import BPDataInsightsGraphReload from './BPDataInsightsGraphReload';

function BPDataInsightsGraphPage({ appUser }) {
  return (
    <div>
      <div className="page-title">BP Data Insights</div>
      <div className="page-content">
        <InsightsGraphFilterProvider>
          <InsightsGraphFilter />
          <div className="d-flex justify-content-end">
            <BPDataInsightsGraphReload useTooltip appUser={appUser} />
          </div>
          <Stack gap={2}>
            <BPDataCumulativeAveragesGraphPage appUser={appUser} />
            <BPDataTimedAveragesGraphPage appUser={appUser} />
          </Stack>
        </InsightsGraphFilterProvider>
      </div>
    </div>
  );
}

BPDataInsightsGraphPage.propTypes = {
  appUser: PropTypes.object,
};
BPDataInsightsGraphPage.defaultProps = {
  appUser: null,
};

export default BPDataInsightsGraphPage;
