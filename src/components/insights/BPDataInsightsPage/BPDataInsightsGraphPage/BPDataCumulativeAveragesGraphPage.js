import PropTypes from 'prop-types';

import BPDataCumulativeAveragesAllGraphPage from './BPDataCumulativeAveragesAllGraphPage';

function BPDataCumulativeAveragesGraphPage({ appUser }) {
  return <BPDataCumulativeAveragesAllGraphPage appUser={appUser} />;
}

BPDataCumulativeAveragesGraphPage.propTypes = {
  appUser: PropTypes.object,
};
BPDataCumulativeAveragesGraphPage.defaultProps = {
  appUser: null,
};

export default BPDataCumulativeAveragesGraphPage;
