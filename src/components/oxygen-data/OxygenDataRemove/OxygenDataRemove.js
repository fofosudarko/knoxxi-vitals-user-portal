import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useRemoveOxygenData } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

import { RemoveButton } from 'src/components/lib';

function OxygenDataRemove({ oxygenData, appUser, useTooltip }) {
  const account = appUser?.account ?? null;
  const {
    handleRemoveOxygenData: _handleRemoveOxygenData,
    error,
    setError,
    oxygenDataRemoved,
    setOxygenDataRemoved,
  } = useRemoveOxygenData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (oxygenDataRemoved) {
      handleApiResult();
    }
  }, [oxygenDataRemoved, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification('Oxygen data removed successfully').getAlertNotification()
    );
    setOxygenDataRemoved(false);
  }, [handleNotification, setOxygenDataRemoved]);

  const handleRemoveOxygenData = useCallback(async () => {
    await _handleRemoveOxygenData(oxygenData);
  }, [_handleRemoveOxygenData, oxygenData]);

  return (
    <div>
      <RemoveButton
        onClick={handleRemoveOxygenData}
        variant="white"
        text="Remove oxygen data"
        autoWidth={!useTooltip}
        textNormal
        textColor="danger"
        useTooltip={useTooltip}
      />
    </div>
  );
}

OxygenDataRemove.propTypes = {
  oxygenData: PropTypes.object,
  appUser: PropTypes.object,
  useTooltip: PropTypes.bool,
};
OxygenDataRemove.defaultProps = {
  oxygenData: null,
  appUser: null,
  useTooltip: false,
};

export default OxygenDataRemove;
