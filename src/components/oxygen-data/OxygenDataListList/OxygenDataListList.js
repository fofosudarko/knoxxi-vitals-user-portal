import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListOxygenDataList } from 'src/hooks/api';

import OxygenDataListReload from './OxygenDataListReload';
import OxygenDataNewRoute from '../OxygenDataNew/OxygenDataNewRoute';
import {
  OxygenDataListList,
  OxygenDataListListEmpty,
} from './_OxygenDataListList';

function OxygenDataListListContainer({ appUser, query }) {
  const {
    oxygenDataList,
    error: oxygenDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListOxygenDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (oxygenDataListError) {
      handleNotification(oxygenDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, oxygenDataListError, setError]);

  const handleLoadMoreOxygenDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <OxygenDataListList
      oxygenDataList={oxygenDataList}
      onLoadMore={handleLoadMoreOxygenDataList}
      loadingText="Loading oxygen data list..."
      appUser={appUser}
      OxygenDataListListEmpty={OxygenDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function OxygenDataListListView({ appUser, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end my-1">
        <OxygenDataNewRoute />
      </div>
      <div className="d-flex justify-content-end">
        <OxygenDataListReload useTooltip appUser={appUser} />
      </div>
      <div className="my-2">
        <OxygenDataListListContainer appUser={appUser} query={query} />
      </div>
    </div>
  );
}

OxygenDataListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
OxygenDataListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
OxygenDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  oxygenDataList: PropTypes.array,
  OxygenDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
OxygenDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading oxygen data list...',
  onLoadMore: undefined,
  oxygenDataList: null,
  OxygenDataListListEmpty: null,
  isDisabled: false,
};
OxygenDataListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
OxygenDataListListView.defaultProps = {
  appUser: null,
  query: null,
};

export default OxygenDataListList;
