import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import { OxygenDataTableItem, OxygenDataGridItem } from './OxygenDataItem';

export function OxygenDataListList({
  oxygenDataList,
  loadingText,
  appUser,
  onLoadMore,
  OxygenDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!oxygenDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(OxygenDataListListEmpty);

  return oxygenDataList.length ? (
    <div>
      {isLargeDevice ? (
        <OxygenDataListListTable
          oxygenDataList={oxygenDataList}
          appUser={appUser}
        />
      ) : (
        <OxygenDataListListGrid
          oxygenDataList={oxygenDataList}
          appUser={appUser}
        />
      )}

      {oxygenDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more oxygen data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function OxygenDataListListGrid({ oxygenDataList, appUser }) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {oxygenDataList.map((item) => (
          <Col key={item.id}>
            <OxygenDataGridItem oxygenData={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function OxygenDataListListTable({ oxygenDataList, appUser }) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">
              Blood oxygen(% SpO<sub>2</sub>)
            </th>
            <th className="list-table-head-cell">Severity</th>
            <th className="list-table-head-cell">Interpretation</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {oxygenDataList.map((item, index) => (
            <OxygenDataTableItem
              oxygenData={item}
              appUser={appUser}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function OxygenDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no oxygen data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

OxygenDataListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  oxygenDataList: PropTypes.array,
  OxygenDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
OxygenDataListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading oxygen data list...',
  onLoadMore: undefined,
  oxygenDataList: null,
  OxygenDataListListEmpty: null,
  isDisabled: false,
};
OxygenDataListListGrid.propTypes = {
  appUser: PropTypes.object,
  oxygenDataList: PropTypes.array,
};
OxygenDataListListGrid.defaultProps = {
  appUser: null,
  oxygenDataList: null,
};
OxygenDataListListTable.propTypes = {
  appUser: PropTypes.object,
  oxygenDataList: PropTypes.array,
};
OxygenDataListListTable.defaultProps = {
  appUser: null,
  oxygenDataList: null,
};
OxygenDataListListEmpty.propTypes = {
  appUser: PropTypes.object,
};
OxygenDataListListEmpty.defaultProps = {
  appUser: null,
};
