import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetOxygenDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function OxygenDataListReload({ useTooltip, appUser }) {
  const account = appUser?.account ?? null;
  const _handleResetOxygenDataList = useResetOxygenDataList({
    userDataId: account?.customerId,
  });

  const handleResetOxygenDataList = useCallback(() => {
    _handleResetOxygenDataList();
  }, [_handleResetOxygenDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetOxygenDataList}
      text="Reload oxygen data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

OxygenDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
};
OxygenDataListReload.defaultProps = {
  useTooltip: false,
  appUser: null,
};

export default OxygenDataListReload;
