import { useMemo } from 'react';
import PropTypes from 'prop-types';

import { ChartCard } from 'src/components/lib';

function OxygenDataTimedAveragesListChartCard({
  oxygenDataTimedAveragesList,
  label,
  onClick,
}) {
  const chartProps = useMemo(() => {
    return {
      type: 'bar',
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
      data: {
        labels:
          oxygenDataTimedAveragesList?.map((item) => item.createdOn) ?? [],
        datasets: [
          {
            label,
            data:
              oxygenDataTimedAveragesList?.map((item) => item.bloodOxygen) ??
              [],
            backgroundColor:
              oxygenDataTimedAveragesList?.map(
                (item) => item.oxygenDataSeverity.colorCode
              ) ?? [],
          },
        ],
      },
    };
  }, [oxygenDataTimedAveragesList, label]);
  return oxygenDataTimedAveragesList ? (
    <ChartCard
      label={label}
      valueColor="primary"
      onClick={onClick}
      chartProps={chartProps}
    />
  ) : (
    <div>Loading...</div>
  );
}

OxygenDataTimedAveragesListChartCard.propTypes = {
  oxygenDataTimedAveragesList: PropTypes.array,
  appUser: PropTypes.object,
  label: PropTypes.string,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
};
OxygenDataTimedAveragesListChartCard.defaultProps = {
  oxygenDataTimedAveragesList: null,
  appUser: null,
  label: null,
  isDisabled: false,
  onClick: undefined,
};

export default OxygenDataTimedAveragesListChartCard;
