import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { useNotificationHandler } from 'src/hooks';
import { useListOxygenDataTimedAverages } from 'src/hooks/api';

import OxygenDataTimedAveragesListChartCard from './OxygenDataTimedAveragesListChartCard';

function OxygenDataTimedAveragesContainer({
  appUser,
  query,
  label,
  isCard,
  onClick,
}) {
  const {
    oxygenDataTimedAverages,
    error: oxygenDataTimedAveragesError,
    setError,
  } = useListOxygenDataTimedAverages({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (oxygenDataTimedAveragesError) {
      handleNotification(oxygenDataTimedAveragesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, oxygenDataTimedAveragesError, setError]);

  return (
    <OxygenDataTimedAverages
      oxygenDataTimedAverages={oxygenDataTimedAverages}
      label={label}
      appUser={appUser}
      isDisabled={
        oxygenDataTimedAverages && oxygenDataTimedAverages.length === 0
      }
      query={query}
      isCard={isCard}
      onClick={onClick}
    />
  );
}

function OxygenDataTimedAverages({
  oxygenDataTimedAverages,
  appUser,
  isDisabled,
  isCard,
  onClick,
}) {
  const oxygenDataTimedAveragesList = [];

  oxygenDataTimedAverages?.forEach((oxygenDataTimedAverage) => {
    const {
      createdOn,
      bloodOxygen,
      oxygenDataInterpretation,
      oxygenDataSeverity,
    } = oxygenDataTimedAverage;
    oxygenDataTimedAveragesList.push({
      createdOn,
      bloodOxygen,
      oxygenDataInterpretation,
      oxygenDataSeverity,
    });
  });

  return (
    <div>
      <Row
        xs={{ cols: 1 }}
        //md={{ cols: 2 }}
        //lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        <Col>
          {isCard ? (
            <OxygenDataTimedAveragesListChartCard
              isDisabled={isDisabled}
              oxygenDataTimedAveragesList={oxygenDataTimedAveragesList}
              label="Reaading(%SpO2)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
      </Row>
    </div>
  );
}

export function OxygenDataTimedAveragesChartCardView({
  appUser,
  query,
  label,
  onClick,
}) {
  return (
    <div className="d-block">
      <OxygenDataTimedAveragesContainer
        appUser={appUser}
        query={query}
        label={label}
        isCard
        onClick={onClick}
      />
    </div>
  );
}

OxygenDataTimedAveragesContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
OxygenDataTimedAveragesContainer.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  isCard: false,
  onClick: undefined,
};
OxygenDataTimedAverages.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  query: PropTypes.object,
  oxygenDataTimedAverages: PropTypes.array,
  isDisabled: PropTypes.bool,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
OxygenDataTimedAverages.defaultProps = {
  appUser: null,
  label: undefined,
  query: null,
  oxygenDataTimedAverages: undefined,
  isDisabled: false,
  isCard: false,
  onClick: undefined,
};
OxygenDataTimedAveragesChartCardView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
OxygenDataTimedAveragesChartCardView.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  onClick: undefined,
};

export default OxygenDataTimedAverages;
