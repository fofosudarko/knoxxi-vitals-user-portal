import PropTypes from 'prop-types';

import { OxygenDataListListView } from '../OxygenDataListList/OxygenDataListList';

function OxygenDataListPage({ appUser }) {
  const account = appUser?.account ?? null;
  const query = { userDataId: account?.customerId };
  return (
    <div>
      <div className="page-title">Oxygen data</div>
      <div className="page-content">
        <OxygenDataListListView appUser={appUser} query={query} />
      </div>
    </div>
  );
}

OxygenDataListPage.propTypes = {
  appUser: PropTypes.object,
};
OxygenDataListPage.defaultProps = {
  appUser: null,
};

export default OxygenDataListPage;
