import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useCreateOxygenData } from 'src/hooks/api';
import useRoutes from 'src/hooks/routes';
import { getNotification } from 'src/utils/notification';
import { DEFAULT_PARTNERED_BY_ALIAS, DEFAULT_DATA_CHANNEL } from 'src/config';

import { ContentContainer, GoBack } from 'src/components/lib';
import OxygenDataInput from '../OxygenDataInput/OxygenDataInput';

function OxygenDataNewPage({ appUser }) {
  const account = appUser?.account ?? null;
  const { handleOxygenDataListRoute } = useRoutes().useOxygenDataListRoute();
  const {
    handleCreateOxygenData: _handleCreateOxygenData,
    error,
    setError,
    processing,
    oxygenDataCreated,
    setOxygenDataCreated,
  } = useCreateOxygenData({ appUser, userDataId: account?.customerId });
  const handleNotification = useNotificationHandler();
  let handleApiResult;
  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (oxygenDataCreated) {
      handleApiResult();
    }
  }, [oxygenDataCreated, handleApiResult]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Oxygen data created successfully'
      ).getSuccessNotification(),
      () => {
        setOxygenDataCreated(false);
      }
    );
    handleOxygenDataListRoute();
  }, [handleNotification, handleOxygenDataListRoute, setOxygenDataCreated]);

  const handleCreateOxygenData = useCallback(
    async (oxygenData) => {
      const body = {
        ...oxygenData,
        kycId: account?.customerId,
        mobileNumber: null,
        partneredByAlias: DEFAULT_PARTNERED_BY_ALIAS,
        dataChannel: DEFAULT_DATA_CHANNEL,
      };
      await _handleCreateOxygenData(body);
    },
    [_handleCreateOxygenData, account?.customerId]
  );

  return (
    <div>
      <div className="justify-between">
        <div className="page-title">New oxygen data</div>
        <GoBack />
      </div>
      <ContentContainer widthClass="w-50">
        <OxygenDataInput
          appUser={appUser}
          onSubmit={handleCreateOxygenData}
          processing={processing}
        />
      </ContentContainer>
    </div>
  );
}

OxygenDataNewPage.propTypes = {
  appUser: PropTypes.object,
};
OxygenDataNewPage.defaultProps = {
  appUser: null,
};

export default OxygenDataNewPage;
