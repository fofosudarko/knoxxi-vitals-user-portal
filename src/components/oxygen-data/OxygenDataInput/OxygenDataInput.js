import { useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';

import { YUP_OXYGEN_DATA_BLOOD_OXYGEN_VALIDATOR } from 'src/config/validators';
import { useFormReset, useDeviceDimensions } from 'src/hooks';

import { CancelButton, CreateButton, EditButton } from 'src/components/lib';

function OxygenDataInput({ oxygenData, isEditing, onSubmit, processing }) {
  const oxygenDataInputSchema = Yup({
    bloodOxygen: YUP_OXYGEN_DATA_BLOOD_OXYGEN_VALIDATOR,
  });
  const { isLargeDevice } = useDeviceDimensions();
  const defaultValues = useMemo(
    () => ({
      bloodOxygen: oxygenData?.bloodOxygen ?? '',
    }),
    [oxygenData?.bloodOxygen]
  );

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    reset,
  } = useForm({ defaultValues, resolver: yupResolver(oxygenDataInputSchema) });

  useFormReset({ values: defaultValues, item: oxygenData, reset });

  const handleSubmitOxygenData = useCallback(
    ({ bloodOxygen }) => {
      const newOxygenData = {
        bloodOxygen,
      };

      onSubmit && onSubmit(newOxygenData);
    },
    [onSubmit]
  );

  const handleCancelOxygenData = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitOxygenData)} noValidate>
        <Row xs={{ cols: 1 }}>
          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">
              Blood oxygen(% SpO<sub>2</sub>)
            </Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Blood oxygen"
                  {...field}
                  isInvalid={!!errors.bloodOxygen}
                />
              )}
              name="bloodOxygen"
              control={control}
            />
            {errors.bloodOxygen ? (
              <Form.Control.Feedback type="invalid">
                {errors.bloodOxygen.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>
              Give the blood oxygen reading e.g. 95% SpO<sub>2</sub>
            </Form.Text>
          </Form.Group>
        </Row>

        <div className="d-flex flex-column-reverse flex-md-row justify-content-end my-3">
          <div className="my-1 my-sm-0 mx-1">
            <CancelButton
              onClick={handleCancelOxygenData}
              autoWidth={isLargeDevice}
              text="Cancel"
            />
          </div>
          <div className="my-1 my-sm-0 mx-1">
            {isEditing ? (
              <EditButton
                type="submit"
                clicked={processing}
                text="Edit oxygen data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            ) : (
              <CreateButton
                type="submit"
                clicked={processing}
                text="Add oxygen data"
                autoWidth={isLargeDevice}
                isDisabled={processing || !isValid}
              />
            )}
          </div>
        </div>
      </Form>
    </div>
  );
}

OxygenDataInput.propTypes = {
  appUser: PropTypes.object,
  oxygenData: PropTypes.object,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
};
OxygenDataInput.defaultProps = {
  appUser: null,
  oxygenData: null,
  isEditing: false,
  onSubmit: undefined,
  processing: false,
};

export default OxygenDataInput;
