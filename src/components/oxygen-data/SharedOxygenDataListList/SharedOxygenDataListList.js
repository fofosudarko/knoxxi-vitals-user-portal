import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListSharedOxygenDataList } from 'src/hooks/api';

import SharedOxygenDataListReload from './SharedOxygenDataListReload';
import {
  SharedOxygenDataListList,
  SharedOxygenDataListListEmpty,
} from './_SharedOxygenDataListList';

function SharedOxygenDataListListContainer({ healthDataSharePublic, query }) {
  const {
    sharedOxygenDataList,
    error: sharedOxygenDataListError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListSharedOxygenDataList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (sharedOxygenDataListError) {
      handleNotification(sharedOxygenDataListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, sharedOxygenDataListError, setError]);

  const handleLoadMoreSharedOxygenDataList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <SharedOxygenDataListList
      sharedOxygenDataList={sharedOxygenDataList}
      onLoadMore={handleLoadMoreSharedOxygenDataList}
      loadingText="Loading shared oxygen data list..."
      healthDataSharePublic={healthDataSharePublic}
      SharedOxygenDataListListEmpty={SharedOxygenDataListListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function SharedOxygenDataListListView({ healthDataSharePublic, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <SharedOxygenDataListReload
          useTooltip
          healthDataSharePublic={healthDataSharePublic}
        />
      </div>
      <div className="my-2">
        <SharedOxygenDataListListContainer
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedOxygenDataListListContainer.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedOxygenDataListListContainer.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};
SharedOxygenDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedOxygenDataList: PropTypes.array,
  SharedOxygenDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedOxygenDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared oxygen data list...',
  onLoadMore: undefined,
  sharedOxygenDataList: null,
  SharedOxygenDataListListEmpty: null,
  isDisabled: false,
};
SharedOxygenDataListListView.propTypes = {
  healthDataSharePublic: PropTypes.object,
  query: PropTypes.object,
};
SharedOxygenDataListListView.defaultProps = {
  healthDataSharePublic: null,
  query: null,
};

export default SharedOxygenDataListList;
