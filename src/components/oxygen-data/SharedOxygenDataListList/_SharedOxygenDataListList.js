import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { FcOpenedFolder } from 'react-icons/fc';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import {
  SharedOxygenDataTableItem,
  SharedOxygenDataGridItem,
} from './SharedOxygenDataItem';

export function SharedOxygenDataListList({
  sharedOxygenDataList,
  loadingText,
  healthDataSharePublic,
  onLoadMore,
  SharedOxygenDataListListEmpty,
  isDisabled,
}) {
  const { isLargeDevice } = useDeviceDimensions();

  if (!sharedOxygenDataList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(SharedOxygenDataListListEmpty);

  return sharedOxygenDataList.length ? (
    <div>
      {isLargeDevice ? (
        <SharedOxygenDataListListTable
          sharedOxygenDataList={sharedOxygenDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      ) : (
        <SharedOxygenDataListListGrid
          sharedOxygenDataList={sharedOxygenDataList}
          healthDataSharePublic={healthDataSharePublic}
        />
      )}

      {sharedOxygenDataList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more shared oxygen data list"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty healthDataSharePublic={healthDataSharePublic} />
  );
}

export function SharedOxygenDataListListGrid({
  sharedOxygenDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        xl={{ cols: 4 }}
        className="gx-2"
      >
        {sharedOxygenDataList.map((item) => (
          <Col key={item.id}>
            <SharedOxygenDataGridItem
              oxygenData={item}
              healthDataSharePublic={healthDataSharePublic}
            />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function SharedOxygenDataListListTable({
  sharedOxygenDataList,
  healthDataSharePublic,
}) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head-sticky">
          <tr>
            <th className="list-table-head-cell">Created</th>
            <th className="list-table-head-cell">
              Blood oxygen(% SpO<sub>2</sub>)
            </th>
            <th className="list-table-head-cell">Severity</th>
            <th className="list-table-head-cell">Interpretation</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {sharedOxygenDataList.map((item, index) => (
            <SharedOxygenDataTableItem
              oxygenData={item}
              healthDataSharePublic={healthDataSharePublic}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function SharedOxygenDataListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <FcOpenedFolder size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no shared oxygen data list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

SharedOxygenDataListList.propTypes = {
  healthDataSharePublic: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  sharedOxygenDataList: PropTypes.array,
  SharedOxygenDataListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
SharedOxygenDataListList.defaultProps = {
  healthDataSharePublic: null,
  loadingText: 'Loading shared oxygen data list...',
  onLoadMore: undefined,
  sharedOxygenDataList: null,
  SharedOxygenDataListListEmpty: null,
  isDisabled: false,
};
SharedOxygenDataListListGrid.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedOxygenDataList: PropTypes.array,
};
SharedOxygenDataListListGrid.defaultProps = {
  healthDataSharePublic: null,
  sharedOxygenDataList: null,
};
SharedOxygenDataListListTable.propTypes = {
  healthDataSharePublic: PropTypes.object,
  sharedOxygenDataList: PropTypes.array,
};
SharedOxygenDataListListTable.defaultProps = {
  healthDataSharePublic: null,
  sharedOxygenDataList: null,
};
SharedOxygenDataListListEmpty.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedOxygenDataListListEmpty.defaultProps = {
  healthDataSharePublic: null,
};
