import PropTypes from 'prop-types';
import { Row, Col, Badge } from 'react-bootstrap';
//import { FaEllipsisV } from 'react-icons/fa';

//import { useDeviceDimensions } from 'src/hooks';
import { humanizeDate, getVariantFromHealthDataSeverityName } from 'src/utils';

export function useOxygenDataDetails(oxygenData) {
  const {
    createdOn,
    bloodOxygen,
    oxygenDataSeverity,
    oxygenDataInterpretation = null,
  } = oxygenData ?? {};
  const {
    name: oxygenDataInterpretationName,
    description: oxygenDataInterpretationDescription,
    explanation: oxygenDataInterpretationExplanation,
  } = oxygenDataInterpretation ?? {};

  return {
    createdOn,
    bloodOxygen,
    oxygenDataSeverity,
    oxygenDataInterpretationName,
    oxygenDataInterpretationDescription,
    oxygenDataInterpretationExplanation,
  };
}

export function SharedOxygenDataGridItem({ oxygenData }) {
  const {
    createdOn,
    bloodOxygen,
    oxygenDataSeverity,
    oxygenDataInterpretationName,
    oxygenDataInterpretationDescription,
    oxygenDataInterpretationExplanation,
  } = useOxygenDataDetails(oxygenData);

  return (
    <div className="grid-item-container">
      <div onClick={undefined} style={{ cursor: 'pointer' }}>
        <Row xs={{ cols: 1 }}>
          <Col>
            <div className="item-title">Created</div>
            <div className="item-subtitle">
              {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
            </div>
          </Col>
          <Col>
            <div className="item-title">
              Reading(% SpO<sub>2</sub>)
            </div>
            <div className="item-subtitle fw-bold">
              <span style={{ color: oxygenDataSeverity.colorCode }}>
                {bloodOxygen !== undefined ? bloodOxygen : 'N/A'}
              </span>
            </div>
          </Col>
          <Col>
            <div className="item-title">Severity</div>
            <Badge
              bg={getVariantFromHealthDataSeverityName(oxygenDataSeverity.name)}
            >
              {oxygenDataSeverity.name}
            </Badge>
          </Col>
          <Col>
            <div className="item-title">Interpretation</div>
            <div className="item-subtitle">
              {oxygenDataInterpretationName !== undefined ? (
                <span>
                  <span style={{ color: oxygenDataSeverity.colorCode }}>
                    {oxygenDataInterpretationName}
                  </span>
                  : {oxygenDataInterpretationDescription},{' '}
                  {oxygenDataInterpretationExplanation}
                </span>
              ) : (
                'N/A'
              )}
            </div>
          </Col>
          <Col>
            <div className="w-100 d-flex justify-content-end">
              {/*<OxygenDataItemActions
                oxygenData={oxygenData}
                healthDataSharePublic={healthDataSharePublic}
              />*/}
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export function SharedOxygenDataTableItem({ oxygenData }) {
  const {
    createdOn,
    bloodOxygen,
    oxygenDataSeverity,
    oxygenDataInterpretationName,
    oxygenDataInterpretationDescription,
    oxygenDataInterpretationExplanation,
  } = useOxygenDataDetails(oxygenData);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body fw-bold">
          <span style={{ color: oxygenDataSeverity.colorCode }}>
            {bloodOxygen !== undefined ? bloodOxygen : 'N/A'}
          </span>
        </div>
      </td>
      <td>
        <Badge
          bg={getVariantFromHealthDataSeverityName(oxygenDataSeverity.name)}
        >
          {oxygenDataSeverity.name}
        </Badge>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {oxygenDataInterpretationName !== undefined ? (
            <span>
              <span style={{ color: oxygenDataSeverity.colorCode }}>
                {oxygenDataInterpretationName}
              </span>
              : {oxygenDataInterpretationDescription},{' '}
              {oxygenDataInterpretationExplanation}
            </span>
          ) : (
            'N/A'
          )}
        </div>
      </td>
      <td>
        <div className="w-100 justify-end">
          {/*<OxygenDataItemActions
            oxygenData={oxygenData}
            healthDataSharePublic={healthDataSharePublic}
          />*/}
        </div>
      </td>
    </tr>
  );
}

/*export function OxygenDataItemActions({ oxygenData, healthDataSharePublic }) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item>
            <OxygenDataRemove
              oxygenData={oxygenData}
              healthDataSharePublic={healthDataSharePublic}
            />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}*/

SharedOxygenDataGridItem.propTypes = {
  oxygenData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedOxygenDataGridItem.defaultProps = {
  oxygenData: null,
  healthDataSharePublic: null,
};
SharedOxygenDataTableItem.propTypes = {
  oxygenData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
SharedOxygenDataTableItem.defaultProps = {
  oxygenData: null,
  healthDataSharePublic: null,
};
/*OxygenDataItemActions.propTypes = {
  oxygenData: PropTypes.object,
  healthDataSharePublic: PropTypes.object,
};
OxygenDataItemActions.defaultProps = {
  oxygenData: null,
  healthDataSharePublic: null,
};*/
