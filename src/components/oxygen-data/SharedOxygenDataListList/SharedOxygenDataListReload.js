import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetSharedOxygenDataList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function SharedOxygenDataListReload({ useTooltip }) {
  const _handleResetSharedOxygenDataList = useResetSharedOxygenDataList();

  const handleResetSharedOxygenDataList = useCallback(() => {
    _handleResetSharedOxygenDataList();
  }, [_handleResetSharedOxygenDataList]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetSharedOxygenDataList}
      text="Reload shared oxygen data list"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

SharedOxygenDataListReload.propTypes = {
  useTooltip: PropTypes.bool,
  healthDataSharePublic: PropTypes.object,
};
SharedOxygenDataListReload.defaultProps = {
  useTooltip: false,
  healthDataSharePublic: null,
};

export default SharedOxygenDataListReload;
