import PropTypes from 'prop-types';

import { GoBack } from 'src/components/lib';
import { SharedOxygenDataListListView } from '../SharedOxygenDataListList/SharedOxygenDataListList';

function SharedOxygenDataListPage({ healthDataSharePublic }) {
  const query = { healthDataSharePublic };
  return (
    <div>
      <div className="justify-between">
        <div className="page-title">Shared oxygen data</div>
        <GoBack />
      </div>
      <div className="page-content">
        <SharedOxygenDataListListView
          healthDataSharePublic={healthDataSharePublic}
          query={query}
        />
      </div>
    </div>
  );
}

SharedOxygenDataListPage.propTypes = {
  healthDataSharePublic: PropTypes.object,
};
SharedOxygenDataListPage.defaultProps = {
  healthDataSharePublic: null,
};

export default SharedOxygenDataListPage;
