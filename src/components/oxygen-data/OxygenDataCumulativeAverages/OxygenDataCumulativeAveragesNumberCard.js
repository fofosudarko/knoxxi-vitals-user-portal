import PropTypes from 'prop-types';

import { getVariantFromHealthDataSeverityName } from 'src/utils';

import { NumberCard } from 'src/components/lib';

function OxygenDataCumulativeAveragesNumberCard({
  oxygenDataCumulativeAverages,
  label,
  onClick,
}) {
  const bloodOxygen = oxygenDataCumulativeAverages?.bloodOxygen ?? 'N/A';
  const oxygenDataSeverity =
    oxygenDataCumulativeAverages?.oxygenDataSeverity ?? {};
  return (
    <NumberCard
      value={bloodOxygen}
      label={label}
      valueColor={getVariantFromHealthDataSeverityName(oxygenDataSeverity.name)}
      onClick={onClick}
    />
  );
}

OxygenDataCumulativeAveragesNumberCard.propTypes = {
  oxygenDataCumulativeAverages: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
OxygenDataCumulativeAveragesNumberCard.defaultProps = {
  oxygenDataCumulativeAverages: null,
  label: null,
  onClick: undefined,
};

export default OxygenDataCumulativeAveragesNumberCard;
