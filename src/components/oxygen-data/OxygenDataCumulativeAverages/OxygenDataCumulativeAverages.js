import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { useNotificationHandler } from 'src/hooks';
import { useListOxygenDataCumulativeAverages } from 'src/hooks/api';

import OxygenDataCumulativeAveragesNumberCard from './OxygenDataCumulativeAveragesNumberCard';

function OxygenDataCumulativeAveragesContainer({
  appUser,
  query,
  label,
  isCard,
  onClick,
}) {
  const {
    oxygenDataCumulativeAverages,
    error: oxygenDataCumulativeAveragesError,
    setError,
  } = useListOxygenDataCumulativeAverages({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (oxygenDataCumulativeAveragesError) {
      handleNotification(oxygenDataCumulativeAveragesError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, oxygenDataCumulativeAveragesError, setError]);

  return (
    <OxygenDataCumulativeAverages
      oxygenDataCumulativeAverages={oxygenDataCumulativeAverages}
      label={label}
      appUser={appUser}
      isDisabled={
        oxygenDataCumulativeAverages &&
        oxygenDataCumulativeAverages.length === 0
      }
      query={query}
      isCard={isCard}
      onClick={onClick}
    />
  );
}

function OxygenDataCumulativeAverages({
  oxygenDataCumulativeAverages: _oxygenDataCumulativeAverages,
  appUser,
  isDisabled,
  isCard,
  onClick,
}) {
  let oxygenDataCumulativeAverages = null;

  if (_oxygenDataCumulativeAverages?.length) {
    const { bloodOxygen, oxygenDataInterpretation, oxygenDataSeverity } =
      _oxygenDataCumulativeAverages[0] ?? {};
    oxygenDataCumulativeAverages = {
      bloodOxygen,
      oxygenDataInterpretation,
      oxygenDataSeverity,
    };
  }

  return (
    <div>
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 1 }}
        lg={{ cols: 1 }}
        className="gx-3 gy-3"
      >
        <Col>
          {isCard ? (
            <OxygenDataCumulativeAveragesNumberCard
              isDisabled={isDisabled}
              oxygenDataCumulativeAverages={oxygenDataCumulativeAverages}
              label="Reading(SpO2)"
              appUser={appUser}
              onClick={onClick}
            />
          ) : null}
        </Col>
      </Row>
    </div>
  );
}

export function OxygenDataCumulativeAveragesNumberCardView({
  appUser,
  query,
  label,
  onClick,
}) {
  return (
    <div className="d-block">
      <OxygenDataCumulativeAveragesContainer
        appUser={appUser}
        query={query}
        label={label}
        isCard
        onClick={onClick}
      />
    </div>
  );
}

OxygenDataCumulativeAveragesContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
OxygenDataCumulativeAveragesContainer.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  isCard: false,
  onClick: undefined,
};
OxygenDataCumulativeAverages.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  query: PropTypes.object,
  oxygenDataCumulativeAverages: PropTypes.array,
  isDisabled: PropTypes.bool,
  isCard: PropTypes.bool,
  onClick: PropTypes.func,
};
OxygenDataCumulativeAverages.defaultProps = {
  appUser: null,
  label: undefined,
  query: null,
  oxygenDataCumulativeAverages: undefined,
  isDisabled: false,
  isCard: false,
  onClick: undefined,
};
OxygenDataCumulativeAveragesNumberCardView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
  label: PropTypes.string,
  onClick: PropTypes.func,
};
OxygenDataCumulativeAveragesNumberCardView.defaultProps = {
  appUser: null,
  query: null,
  label: undefined,
  onClick: undefined,
};

export default OxygenDataCumulativeAverages;
