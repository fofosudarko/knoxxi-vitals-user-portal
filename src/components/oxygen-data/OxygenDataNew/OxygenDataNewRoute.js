import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { CreateButton } from 'src/components/lib';

export default function OxygenDataNewRoute({ text }) {
  const { handleOxygenDataNewRoute } = useRoutes().useOxygenDataNewRoute();

  return (
    <CreateButton
      variant="primary"
      textColor="white"
      onClick={handleOxygenDataNewRoute}
      text={text}
    />
  );
}

OxygenDataNewRoute.propTypes = {
  text: PropTypes.string,
};
OxygenDataNewRoute.defaultProps = {
  text: 'New oxygen data',
};
