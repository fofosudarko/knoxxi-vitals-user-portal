import React from 'react';
import PropTypes from 'prop-types';

import { useInsightsGraphFilterStore } from 'src/stores/insights-graph-filter';

const InsightsGraphFilterContext = React.createContext();

function InsightsGraphFilterProvider({ children }) {
  const {
    date,
    count,
    period,
    interval,
    order,
    setDate,
    setCount,
    setPeriod,
    setInterval,
    setOrder,
    clearInsightsGraphFilterState,
  } = useInsightsGraphFilterStore((state) => state);
  const state = { date, count, period, interval, order };

  return (
    <InsightsGraphFilterContext.Provider
      value={{
        ...state,
        setDate,
        setCount,
        setPeriod,
        setInterval,
        setOrder,
        clearInsightsGraphFilterState,
      }}
    >
      {children}
    </InsightsGraphFilterContext.Provider>
  );
}

export { InsightsGraphFilterContext, InsightsGraphFilterProvider };

InsightsGraphFilterProvider.propTypes = {
  children: PropTypes.node,
};
InsightsGraphFilterProvider.defaultProps = {
  children: null,
};
