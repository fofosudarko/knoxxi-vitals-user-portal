import { useCallback, createContext } from 'react';
import PropTypes from 'prop-types';

import { getId } from 'src/utils';
import useItem from 'src/hooks/item';

export const ItemsBuilderContext = createContext(null);

export function ItemsBuilderProvider({ children }) {
  const {
    builtItems,
    addBuiltItem: _addBuiltItem,
    addBuiltItems: _addBuiltItems,
    removeBuiltItem: _removeBuiltItem,
    clearBuiltItems: _clearBuiltItems,
  } = useItem();

  const addBuiltItem = useCallback(
    (item, options = null) => {
      const { propertyFilter = 'id', isUnique = false } = options ?? {};
      _addBuiltItem(
        {
          [propertyFilter]: item[propertyFilter],
          id: getId(),
          data: item,
        },
        { propertyFilter, isUnique }
      );
    },
    [_addBuiltItem]
  );

  const addBuiltItems = useCallback(
    (items, options = null) => {
      const { propertyFilter = 'id', isUnique = false } = options ?? {};
      _addBuiltItems(
        items.map((item) => ({
          [propertyFilter]: item[propertyFilter],
          id: getId(),
          data: item,
        })),
        { propertyFilter, isUnique }
      );
    },
    [_addBuiltItems]
  );

  const removeBuiltItem = useCallback(
    (item) => {
      _removeBuiltItem(item);
    },
    [_removeBuiltItem]
  );

  const clearBuiltItems = useCallback(() => {
    _clearBuiltItems();
  }, [_clearBuiltItems]);

  return (
    <ItemsBuilderContext.Provider
      value={{
        builtItems,
        addBuiltItem,
        addBuiltItems,
        removeBuiltItem,
        clearBuiltItems,
      }}
    >
      {children}
    </ItemsBuilderContext.Provider>
  );
}

ItemsBuilderProvider.propTypes = {
  children: PropTypes.node,
};
ItemsBuilderProvider.defaultProps = {
  children: null,
};
