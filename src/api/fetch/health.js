import { useAuthStore } from 'src/stores/auth';
import {
  KNOXXI_HEALTH_SERVICE_API_URL,
  CORS_VALUE,
  REFERRER_VALUE,
  REFERRER_POLICY_VALUE,
} from 'src/config';
import { cleanupResource } from 'src/utils/index';

export async function fetchHealthResource(resource, options = null) {
  const headers = new Headers();
  const url = KNOXXI_HEALTH_SERVICE_API_URL + '/' + cleanupResource(resource);
  const state = useAuthStore.getState() ?? null;
  if (options?.headers) {
    const optionsHeaders = options?.headers;
    for (const headerKey of Object.keys(optionsHeaders)) {
      headers.set(headerKey, optionsHeaders[headerKey]);
    }
    delete options?.headers;
  }
  if (state?.appUserAccessToken) {
    headers.set(
      'Authorization',
      `Bearer ${state.appUserAccessToken.account.token}`
    );
  }
  return await fetch(url, {
    ...options,
    headers,
    mode: CORS_VALUE,
    referrer: REFERRER_VALUE,
    referrerPolicy: REFERRER_POLICY_VALUE,
  });
}

export async function fetchHealthResourceNoAuth(resource, options = null) {
  const headers = new Headers();
  const useBaseurl =
    options?.useBaseurl === undefined ? true : options?.useBaseurl;
  const url = useBaseurl
    ? KNOXXI_HEALTH_SERVICE_API_URL + '/' + cleanupResource(resource)
    : resource;
  if (options?.headers) {
    const optionsHeaders = options?.headers;
    for (const headerKey of Object.keys(optionsHeaders)) {
      headers.set(headerKey, optionsHeaders[headerKey]);
    }
    delete options?.headers;
  }
  delete options?.useBaseurl;
  return await fetch(url, {
    ...options,
    headers,
    mode: CORS_VALUE,
    referrer: REFERRER_VALUE,
    referrerPolicy: REFERRER_POLICY_VALUE,
  });
}

export function xhrHealthResource(resource, options = null) {
  const xhr = new XMLHttpRequest();
  const url = KNOXXI_HEALTH_SERVICE_API_URL + '/' + cleanupResource(resource);
  xhr.open(options?.method, url, options?.isAsync);
  const state = useAuthStore.getState() ?? null;
  if (options?.headers) {
    const optionsHeaders = options?.headers;
    for (const headerKey of Object.keys(optionsHeaders)) {
      xhr.setRequestHeader(headerKey, optionsHeaders[headerKey]);
    }
    delete options?.headers;
  }
  if (state?.appUserAccessToken) {
    xhr.setRequestHeader(
      'Authorization',
      `Bearer ${state.appUserAccessToken.account.token}`
    );
  }
  xhr.onabort = options?.onAbort;
  xhr.onerror = options?.onError;
  xhr.onload = options?.onLoad;
  xhr.onloadend = options?.onLoadEnd;
  xhr.onloadstart = options?.onLoadStart;
  xhr.onprogress = options?.onProgress;
  xhr.onreadystatechange = options?.onReadyStateChange
    ? options.onReadyStateChange(xhr)
    : undefined;
  xhr.ontimeout = options?.onTimeout;
  if (options?.isFileUpload) {
    xhr.upload.onabort = options?.onFileUploadAbort;
    xhr.upload.onerror = options?.onFileUploadError;
    xhr.upload.onload = options?.onFileUploadLoad;
    xhr.upload.onloadend = options?.onFileUploadLoadEnd;
    xhr.upload.onloadstart = options?.onFileUploadLoadStart;
    xhr.upload.onprogress = options?.onFileUploadProgress;
    xhr.upload.ontimeout = options?.onFileUploadTimeout;
  }
  xhr.send(options?.body ?? undefined);
}
