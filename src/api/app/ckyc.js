import fetchCkycResource from '../fetch/ckyc';
import { generateQuerystring } from 'src/utils/index';

export default class CkycService {
  static async addCustomer(body = null) {
    return await fetchCkycResource('customer/add_customer', {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async updateCustomer(body = null) {
    return await fetchCkycResource(`customer/update_customer`, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async viewCustomer(customer = null) {
    const customerId = customer?.id;
    return await fetchCkycResource(
      `customer/view_customer${generateQuerystring({
        customerId,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async addAccount(body = null) {
    return await fetchCkycResource(`account/add_account`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async updateAccount(body = null) {
    return await fetchCkycResource(`account/update_account`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async viewAccount(body = null) {
    return await fetchCkycResource(`account/view_account`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async changePassword(body = null) {
    return await fetchCkycResource(`customer/changepassword`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async doForgotPassword(body = null) {
    return await fetchCkycResource(`customer/forgotpassword`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async sendInvite(email) {
    return await fetchCkycResource(`customer/invite/${email}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async logOut(body = null) {
    return await fetchCkycResource(`customer/logout`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async resendOTP(body = null) {
    return await fetchCkycResource(`customer/resendotp`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async signIn(body = null) {
    return await fetchCkycResource(`customer/signin`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async verifyOTPDuringRegistration(body = null) {
    return await fetchCkycResource(`customer/verify/otp`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async verifyOTPDuringForgotPassword(body = null) {
    return await fetchCkycResource(`customer/verifyotp`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }
}
