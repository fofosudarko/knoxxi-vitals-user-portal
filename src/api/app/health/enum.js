import { fetchHealthResource } from '../../fetch/health';

class EnumService {
  static async listDataChannel() {
    return await fetchHealthResource(`vitals/enums/data-channel`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    });
  }

  static async listMeal() {
    return await fetchHealthResource(`vitals/enums/meal`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    });
  }

  static async listHealthDataShareRecipientType() {
    return await fetchHealthResource(
      `vitals/enums/health-data-share-recipient-type`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
      }
    );
  }

  static async listHealthDataShareScope() {
    return await fetchHealthResource(`vitals/enums/health-data-share-scope`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    });
  }

  static async listHealthDataShareCategory() {
    return await fetchHealthResource(
      `vitals/enums/health-data-share-category`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
      }
    );
  }

  static async listAdditionalLifeRelationship() {
    return await fetchHealthResource(
      `vitals/enums/additional-life-relationship`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
      }
    );
  }
}

export default EnumService;
