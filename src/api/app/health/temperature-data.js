import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class TemperatureDataService {
  static async listTemperatureDataList({
    userDataId,
    userDataMobileNumber,
    userDataFullname,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchHealthResource(
      `vitals/temperature-data${generateQuerystring({
        userDataId,
        userDataMobileNumber,
        userDataFullname,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listSharedTemperatureDataList({
    healthDataSharePublic = null,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    const healthDataSharePublicSecurityCode =
      healthDataSharePublic?.securityCode;
    return await fetchHealthResource(
      `vitals/temperature-data/shared/${healthDataSharePublicSecurityCode}${generateQuerystring(
        {
          page,
          size,
        }
      )}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getTemperatureData(temperatureData = null) {
    const temperatureDataId = temperatureData?.id;
    return await fetchHealthResource(
      `vitals/temperature-data/${temperatureDataId}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async createTemperatureData(body = null) {
    return await fetchHealthResource(`vitals/temperature-data`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async removeTemperatureData(temperatureData = null) {
    const temperatureDataId = temperatureData?.id;
    return await fetchHealthResource(
      `vitals/temperature-data/${temperatureDataId}`,
      {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }
}
