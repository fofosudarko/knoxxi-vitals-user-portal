import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class WaterDataService {
  static async listWaterDataList({
    userDataId,
    userDataMobileNumber,
    userDataFullname,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchHealthResource(
      `vitals/water-data${generateQuerystring({
        userDataId,
        userDataMobileNumber,
        userDataFullname,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listSharedWaterDataList({
    healthDataSharePublic = null,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    const healthDataSharePublicSecurityCode =
      healthDataSharePublic?.securityCode;
    return await fetchHealthResource(
      `vitals/water-data/shared/${healthDataSharePublicSecurityCode}${generateQuerystring(
        {
          page,
          size,
        }
      )}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getWaterData(waterData = null) {
    const waterDataId = waterData?.id;
    return await fetchHealthResource(`vitals/water-data/${waterDataId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async createWaterData(body = null) {
    return await fetchHealthResource(`vitals/water-data`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async removeWaterData(waterData = null) {
    const waterDataId = waterData?.id;
    return await fetchHealthResource(`vitals/water-data/${waterDataId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }
}
