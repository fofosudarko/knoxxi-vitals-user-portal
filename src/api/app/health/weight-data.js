import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class WeightDataService {
  static async listWeightDataList({
    userDataId,
    userDataMobileNumber,
    userDataFullname,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchHealthResource(
      `vitals/weight-data${generateQuerystring({
        userDataId,
        userDataMobileNumber,
        userDataFullname,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listSharedWeightDataList({
    healthDataSharePublic = null,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    const healthDataSharePublicSecurityCode =
      healthDataSharePublic?.securityCode;
    return await fetchHealthResource(
      `vitals/weight-data/shared/${healthDataSharePublicSecurityCode}${generateQuerystring(
        {
          page,
          size,
        }
      )}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getWeightData(weightData = null) {
    const weightDataId = weightData?.id;
    return await fetchHealthResource(`vitals/weight-data/${weightDataId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async createWeightData(body = null) {
    return await fetchHealthResource(`vitals/weight-data`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async removeWeightData(weightData = null) {
    const weightDataId = weightData?.id;
    return await fetchHealthResource(`vitals/weight-data/${weightDataId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }
}
