import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';

export default class InsightService {
  static async listBPDataTimedAverages({ userId, createdOn, period }) {
    return await fetchHealthResource(
      `vitals/insights/bp-data/timed-averages${generateQuerystring({
        userId,
        createdOn,
        period,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listBPDataCumulativeAverages({ userId, createdOn, period }) {
    return await fetchHealthResource(
      `vitals/insights/bp-data/cumulative-averages${generateQuerystring({
        userId,
        createdOn,
        period,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listGlucoseDataTimedAverages({ userId, createdOn, period }) {
    return await fetchHealthResource(
      `vitals/insights/glucose-data/timed-averages${generateQuerystring({
        userId,
        createdOn,
        period,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listGlucoseDataCumulativeAverages({
    userId,
    createdOn,
    period,
  }) {
    return await fetchHealthResource(
      `vitals/insights/glucose-data/cumulative-averages${generateQuerystring({
        userId,
        createdOn,
        period,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listTemperatureDataTimedAverages({ userId, createdOn, period }) {
    return await fetchHealthResource(
      `vitals/insights/temperature-data/timed-averages${generateQuerystring({
        userId,
        createdOn,
        period,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listTemperatureDataCumulativeAverages({
    userId,
    createdOn,
    period,
  }) {
    return await fetchHealthResource(
      `vitals/insights/temperature-data/cumulative-averages${generateQuerystring(
        {
          userId,
          createdOn,
          period,
        }
      )}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listOxygenDataTimedAverages({ userId, createdOn, period }) {
    return await fetchHealthResource(
      `vitals/insights/oxygen-data/timed-averages${generateQuerystring({
        userId,
        createdOn,
        period,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listOxygenDataCumulativeAverages({ userId, createdOn, period }) {
    return await fetchHealthResource(
      `vitals/insights/oxygen-data/cumulative-averages${generateQuerystring({
        userId,
        createdOn,
        period,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listCholesterolDataTimedAverages({ userId, createdOn, period }) {
    return await fetchHealthResource(
      `vitals/insights/cholesterol-data/timed-averages${generateQuerystring({
        userId,
        createdOn,
        period,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listCholesterolDataCumulativeAverages({
    userId,
    createdOn,
    period,
  }) {
    return await fetchHealthResource(
      `vitals/insights/cholesterol-data/cumulative-averages${generateQuerystring(
        {
          userId,
          createdOn,
          period,
        }
      )}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }
}
