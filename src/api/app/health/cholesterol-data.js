import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class CholesterolDataService {
  static async listCholesterolDataList({
    userDataId,
    userDataMobileNumber,
    userDataFullname,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchHealthResource(
      `vitals/cholesterol-data${generateQuerystring({
        userDataId,
        userDataMobileNumber,
        userDataFullname,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getCholesterolData(cholesterolData = null) {
    const cholesterolDataId = cholesterolData?.id;
    return await fetchHealthResource(
      `vitals/cholesterol-data/${cholesterolDataId}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async createCholesterolData(body = null) {
    return await fetchHealthResource(`vitals/cholesterol-data`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async removeCholesterolData(cholesterolData = null) {
    const cholesterolDataId = cholesterolData?.id;
    return await fetchHealthResource(
      `vitals/cholesterol-data/${cholesterolDataId}`,
      {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }
}
