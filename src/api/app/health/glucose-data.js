import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class GlucoseDataService {
  static async listGlucoseDataList({
    userDataId,
    userDataMobileNumber,
    userDataFullname,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchHealthResource(
      `vitals/glucose-data${generateQuerystring({
        userDataId,
        userDataMobileNumber,
        userDataFullname,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async listSharedGlucoseDataList({
    healthDataSharePublic = null,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    const healthDataSharePublicSecurityCode =
      healthDataSharePublic?.securityCode;
    return await fetchHealthResource(
      `vitals/glucose-data/shared/${healthDataSharePublicSecurityCode}${generateQuerystring(
        {
          page,
          size,
        }
      )}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getGlucoseData(glucoseData = null) {
    const glucoseDataId = glucoseData?.id;
    return await fetchHealthResource(`vitals/glucose-data/${glucoseDataId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async createGlucoseData(body = null) {
    return await fetchHealthResource(`vitals/glucose-data`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async updateGlucoseData(glucoseData = null, body = null) {
    const glucoseDataId = glucoseData?.id;
    return await fetchHealthResource(`vitals/glucose-data/${glucoseDataId}`, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async removeGlucoseData(glucoseData = null) {
    const glucoseDataId = glucoseData?.id;
    return await fetchHealthResource(`vitals/glucose-data/${glucoseDataId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }
}
