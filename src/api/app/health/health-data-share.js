import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class HealthDataShareService {
  static async listHealthDataShares({
    sharedById,
    scope,
    recipientType,
    category,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchHealthResource(
      `vitals/health-data-shares${generateQuerystring({
        sharedById,
        scope,
        recipientType,
        category,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getHealthDataShare(healthDataShare = null) {
    const healthDataShareId = healthDataShare?.id;
    return await fetchHealthResource(
      `vitals/health-data-shares/${healthDataShareId}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getHealthDataSharePublic(healthDataShare = null) {
    const healthDataShareSecurityCode = healthDataShare?.securityCode;
    return await fetchHealthResource(
      `vitals/health-data-shares/public/${healthDataShareSecurityCode}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async cancelHealthDataShare(healthDataShare = null) {
    const healthDataShareId = healthDataShare?.id;
    return await fetchHealthResource(
      `vitals/health-data-shares/${healthDataShareId}/cancel`,
      {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async allowHealthDataShare(healthDataShare = null) {
    const healthDataShareId = healthDataShare?.id;
    return await fetchHealthResource(
      `vitals/health-data-shares/${healthDataShareId}/allow`,
      {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async removeHealthDataShare(healthDataShare = null) {
    const healthDataShareId = healthDataShare?.id;
    return await fetchHealthResource(
      `vitals/health-data-shares/${healthDataShareId}`,
      {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }
}
