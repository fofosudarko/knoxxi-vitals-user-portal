import { fetchHealthResource } from 'src/api/fetch/health';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class AdditionalLifeService {
  static async listAdditionalLives({
    enteredById,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchHealthResource(
      `vitals/additional-lives${generateQuerystring({
        enteredById,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async getAdditionalLife(additionalLife = null) {
    const additionalLifeId = additionalLife?.id;
    return await fetchHealthResource(
      `vitals/additional-lives/${additionalLifeId}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async createAdditionalLife(body = null) {
    return await fetchHealthResource(`vitals/additional-lives`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
  }

  static async updateAdditionalLife(additionalLife = null, body = null) {
    const additionalLifeId = additionalLife?.id;
    return await fetchHealthResource(
      `vitals/additional-lives/${additionalLifeId}`,
      {
        method: 'PUT',
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async removeAdditionalLife(additionalLife = null) {
    const additionalLifeId = additionalLife?.id;
    return await fetchHealthResource(
      `vitals/additional-lives/${additionalLifeId}`,
      {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }
}
